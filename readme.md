## VenVast Web Application

This project uses next technologies:
1. Laravel framework on the backend
2. Composer as a backend dependency management
2. AngularJS on the frontend
3. Gulp is used to build("compile") frontend app
4. Bower is for dependency management


### Check this repos:
1. [Laravel API Documentation Generator](https://github.com/mpociot/laravel-apidoc-generator)
2. [Laravel 5 Inverse Seed Generator](https://github.com/orangehill/iseed)
3. [Laravel Likeable](https://github.com/nanosolutions/laravel-likeable)
4. [Laravel5Calendar](https://github.com/Arkhas/Laravel5Calendar)
5. [Presenter](https://github.com/laracasts/Presenter)
6. [Stapler file upload](https://github.com/CodeSleeve/laravel-stapler)
7. [Translatable models](https://github.com/dimsav/laravel-translatable)


### Frontend Development process
As far as it's very important to keep things clear on the frontend,
I've decided to separate all frontend code to the directories named by features. On the other hand, having
a lot of *.js and *.css files is not a good idea because of the performance(The HTTTP specification says that before
each request it will establish connection with the server which takes the time. So, downloading 7 files by 10Kb will take
much more time then one 70kb file). That'swhy we gonna use Gulp to compile and minify all the *.js and *.css files
into the bigger ones(for example if we want to load scripts only in the `<head>` HTML tag we will compile all the *.js
files into one file called "app.js" for example). Minifying files is additional benefit making our code
more difficult to explore to other users and boosting page loading speed.

So, what do you need to do to prepare your computer to work with Gulp and Bower?
1. Install NodeJS and NPM
2. run `npm install -g gulp` it will install Gulp globally
3. run `npm install -g bower`
4. run `npm install` from the root of the project

If everything is ready you just need to run `gulp` from the root of project and it will compile everything for you.
Gulp is too complicated to explain in readme doc, so I HIGHLY recommend you to read the gulp docs and tutorials.
I also recommend to take a look at `/gulpfile.js` to become familiar with it and try to understand what's going on there


