<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Event;
use App\Models\Venue;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Category::deleting(function ($category) {
            $venues = $category->venues()->get();
            /** @var Venue $venue */
            foreach ($venues as $venue) {
                $venue->categories()->detach($category->id);
                if ($category->parent_id) {
                    $venue->categories()->attach($category->parent_id);
                }
            }
        });

        Event::saving(function ($event) {
            if ($event->start_at instanceof Carbon) {
                if (\Session::has('timezone')) {
                    $event->start_at = new Carbon($event->start_at, \Session::get('timezone', 'UTC'));
                }
            }
            if ($event->end_at instanceof Carbon) {
                if (\Session::has('timezone')) {
                    $event->end_at = new Carbon($event->end_at, \Session::get('timezone', 'UTC'));
                }
            }
        });
    

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $connection = new Connection();
        $config = new Configuration();
        $config->setProxyDir(app_path('DoctrineProxies'));
        $config->setProxyNamespace('DoctrineProxies');
        $config->setHydratorDir(app_path('DoctrineHydrators'));
        $config->setHydratorNamespace('DoctrineHydrators');
        $config->setDefaultDB('test');
        $config->setMetadataDriverImpl(AnnotationDriver::create(app_path('Entities')));
        AnnotationDriver::registerAnnotationClasses();

        $this->app->singleton('dm', function ($app) use ($connection, $config) {
            return DocumentManager::create($connection, $config);
        });

    }
}
