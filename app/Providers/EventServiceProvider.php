<?php

namespace App\Providers;

use App\Events\EventModel\EventWithoutVenueSaved;
use App\Events\EventModel\GotEventFbData;
use App\Events\EventModel\GotVenueFbData;
use App\Events\User\Activated as UserActivated;
use App\Events\User\RestorePassword;
use App\Events\User\UserRegistered;
use App\Events\Venue\GotGoogleVenue;
use App\Listeners\Scraping\Facebook\SaveVenue;
use App\Listeners\Scraping\Facebook\SearchVenueForEvent;
use App\Listeners\Scraping\Facebook\UpdateEventWithFbData;
use App\Listeners\User\SendActivationEmail;
use App\Listeners\User\SendGreetings;
use App\Listeners\User\SendRestorePasswordEmail;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        GotEventFbData::class => [
            UpdateEventWithFbData::class,
        ],
        GotVenueFbData::class => [
            SaveVenue::class,
        ],
        EventWithoutVenueSaved::class => [
            SearchVenueForEvent::class,
        ],
        GotGoogleVenue::class => [
            \App\Listeners\Scraping\Google\SaveVenue::class,
        ],
        UserRegistered::class => [
            SendActivationEmail::class,
        ],
        UserActivated::class => [
            SendGreetings::class,
        ],
        RestorePassword::class => [
            SendRestorePasswordEmail::class
        ],
        
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
    }
}
