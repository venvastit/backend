<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/30/16
 * Time: 9:39 AM.
 */
namespace App\ServiceProviders;

class GoogleTimeZoneServiceProvider extends AbstractServiceProvider
{
    protected $base_url = 'https://maps.googleapis.com/maps/api/timezone/json';

    protected $key;

    public function __construct()
    {
        parent::__construct();
        $this->key = config('services.google.key');
    }

    public function getTimeZoneByLocation($lat, $lng)
    {
        return $this->get($this->buildRequest($lat, $lng));
    }

    public function buildRequest($lat, $lng)
    {
        return '?location='.$lat.','.$lng.'&timestamp='.time().'&key='.$this->key;
    }
}
