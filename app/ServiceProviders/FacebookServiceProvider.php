<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 4/17/16
 * Time: 11:14 PM.
 */
namespace App\ServiceProviders;

use App\Models\Source;
use App\ServiceProviders\Exception\FbAccessTokenException;
use App\User;

class FacebookServiceProvider extends AbstractServiceProvider
{
    private $access_token;
    protected $base_url = 'https://graph.facebook.com/';
    protected $source_id = Source::FACEBOOK;

    public function __construct($access_token)
    {
        parent::__construct();
        $this->setAccessToken($access_token);
    }

    /**
     * @param string $access_token
     */
    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCategory($id)
    {
        $this->get($this->makeCategoryQuery($id));

        return $this->getBody();
    }

    /**
     * @return array
     */
    protected function getCategoryFields()
    {
        return ['id', 'name','parent_ids'];
    }

    /**
     * @param $id
     * @return string
     * @throws FbAccessTokenException
     */
    protected function makeCategoryQuery($id)
    {
        $query = $id.'?fields='.implode(',', $this->getCategoryFields()).'&access_token='.$this->getAccessToken();

        return $query;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getEvent($id)
    {
        $this->get($this->makeEventQuery($id));
        $event = $this->getBody();

        return $event;
    }

    /**
     * @param $id
     * @return string
     * @throws FbAccessTokenException
     */
    protected function makeEventQuery($id)
    {
        $query = 'v2.6/'.$id.'?fields='.implode(',', $this->getEventFields()).'&access_token='.$this->getAccessToken();

        return $query;
    }

    protected function getEventFields()
    {
        return [
            'id',
            'description',
            'cover',
            'start_time',
            'end_time',
            'place',
            'attending_count',
            'name',
            'owner',
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getVenue($id)
    {
        $this->get($this->makeVenueQuery($id));
        $venue = $this->getBody();

        return $venue;
    }

    /**
     * @param $id
     * @return string
     * @throws FbAccessTokenException
     */
    protected function makeVenueQuery($id)
    {
        $query = 'v2.6/'.$id.'?access_token='.$this->getAccessToken().'&fields='.implode(',', $this->getVenueFields());

        return $query;
    }

    /**
     * @return array
     */
    protected function getVenueFields()
    {
        return [
            'about','emails','birthday','category_list',
            'contact_address','description','cover',
            'website', 'phone', 'location','public_transit', 'name',
            'hours','events.limit(100){id,description,cover,start_time,end_time,attending_count,name,place,owner}',
        ];
    }

    /**
     * @return string
     * @throws FbAccessTokenException
     */
    public function getAccessToken()
    {
        if (!$this->access_token) {
            throw new FbAccessTokenException('Access token required');
        }

        return $this->access_token;
    }

    public function getAppAccessToken()
    {
        $secret = config('services.facebook.appSecret');
        $id = config('services.facebook.appId');
        $query = 'oauth/access_token?client_id='.$id.'&client_secret='.$secret;
        $query .= '&grant_type=client_credentials';
        $this->get($query);
        $data = str_replace('access_token=', '', $this->getResponse()->getBody()->getContents());

        return $data;
    }

    /**
     * @return bool
     * @throws FbAccessTokenException
     */
    public function checkAccessToken()
    {
        $query = '/debug_token?input_token='.$this->getAccessToken().'&access_token='.$this->getAppAccessToken();
        $appId = config('services.facebook.appId');
        $this->get($query);
        $data = $this->getBody();
        $data = $data['data'];

        return $data['app_id'] == $appId && $data['is_valid'] === true;
    }

    /**
     * @return User | null
     * @throws FbAccessTokenException
     */
    public function getUserByToken()
    {
        $query = '/me?access_token='.$this->getAccessToken().'&fields='.implode(',', $this->getUserFields());
        $this->get($query);
        $data = $this->getBody();
        if (!array_key_exists('id', $data)) {
            return;
        }
        $user = User::where('fb_id', '=', $data['id'])->get()->first();
        if (!$user && array_key_exists('email', $data)) {
            $user = User::where('email', '=', $data['email'])->get()->first();
        }
        if (!$user && array_key_exists('id', $data)) {
            $userData = [];
            foreach ($data as $key => $value) {
                if ($key === 'id') {
                    $userData['fb_id'] = $value;
                } elseif ($key === 'picture' && array_key_exists('data', $value) && array_key_exists('url', $value['data'])) {
                    $userData['fb_picture'] = $value['data']['url'];
                } else {
                    $userData[$key] = $value;
                }
            }
            $userData['fb_access_token'] = $this->getAccessToken();
            $user = User::create($userData);
        }

        return $user;
    }

    private function getUserFields()
    {
        return [
            'id', 'name', 'first_name', 'last_name',
            'picture{url}', 'age_range', 'devices', 'email',
        ];
    }
}
