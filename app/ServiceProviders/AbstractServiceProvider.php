<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 4/17/16
 * Time: 10:59 PM.
 */
namespace App\ServiceProviders;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractServiceProvider
{
    /**
     * @var string
     */
    protected $base_url;

    /**
     * @var int
     */
    protected $source_id;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var ResponseInterface
     */
    private $response;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param $requestString
     * @return ResponseInterface
     */
    public function get($requestString)
    {
        try {
            $this->setResponse($this->getClient()->request('GET', $this->getBaseUrl().$requestString));

            return $this->getResponse();
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody()->getContents();
            if (is_string($body)) {
                $body = json_decode($body, true);
            }
            if (intval($body['error']['code']) === 100) {
                return;
            }
        }
    }

    /**
     * @return mixed
     */
    protected function getBody()
    {
        $response = json_decode($this->getResponse()->getBody(), true);
        if (!empty($response)) {
            return $response;
        }
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->base_url;
    }

    /**
     * @param string $base_url
     */
    public function setBaseUrl($base_url)
    {
        $this->base_url = $base_url;
    }

    /**
     * @return int
     */
    public function getSourceId()
    {
        return $this->source_id;
    }

    /**
     * @param int $source_id
     * @return self
     */
    public function setSourceId($source_id)
    {
        $this->source_id = $source_id;

        return $this;
    }

    /**
     * @return ClientInterface
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param ClientInterface $client
     * @return self
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param ResponseInterface $response
     * @return self
     */
    public function setResponse(ResponseInterface $response)
    {
        $this->response = $response;

        return $this;
    }
}
