<?php

namespace App\Console;

use App\Console\Commands\GoogleVenuesByLocation;
use App\Console\Commands\MigrateFromRedisToSQL;
use App\Console\Commands\MigrateToMongo;
use App\Console\Commands\MigrateVenuesCommand;
use App\Console\Commands\RebuildCategories;
use App\Console\Commands\UpdateMongoDatesInEvents;
use App\Console\Commands\VenuesEventsByLocation;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        MigrateFromRedisToSQL::class,
        MigrateVenuesCommand::class,
        RebuildCategories::class,
        MigrateToMongo::class,
        UpdateMongoDatesInEvents::class,
        VenuesEventsByLocation::class,
        GoogleVenuesByLocation::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //        $schedule->command('inspire')
//                 ->hourly();
    }
}
