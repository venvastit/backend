<?php

namespace App\Console\Commands;

use App\Models\Event;
use Illuminate\Console\Command;
use Carbon\Carbon;

class UpdateMongoDatesInEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mongo:event_dates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update dates to match necessary format';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $events = Event::all();
        foreach ($events as $event) {
            $event->setStartAtAttribute();
        }
    }
}
