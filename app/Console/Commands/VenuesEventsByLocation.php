<?php

namespace App\Console\Commands;

use App\Jobs\Scraping\Facebook\GetVenuesByBounds;
use App\Jobs\Scraping\Facebook\GetVenuesByLocation;
use App\Models\Category;
use App\Models\Event;
use App\Models\Source;
use App\Models\Venue;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Redis;

class VenuesEventsByLocation extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap:all {--lat=} {--lng=} {--t=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get venues by latitude and longitude';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dispatch(new GetVenuesByBounds($this->option('lat'), $this->option('lng'), $this->option('t')));
    }
}
