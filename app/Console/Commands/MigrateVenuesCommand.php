<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/19/16
 * Time: 2:33 PM.
 */
namespace App\Console\Commands;

use App\Jobs\VenueMigrate;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\MigrateVenues;

class MigrateVenuesCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:venue {--access-token=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate venues from redis-storage to mysql';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dispatch(new VenueMigrate($this->option('access-token')));
    }
}
