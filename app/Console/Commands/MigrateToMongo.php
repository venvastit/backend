<?php

namespace App\Console\Commands;

use App\Jobs\Migrate\All;
use App\Models\Category;
use App\Models\EventCategory;
use App\Models\MongoCategory;
use App\Models\Source;
use App\Models\Venue;
use App\Models\Event;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class MigrateToMongo extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mongo:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate data from MySQL to MongoDB';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dispatch(new All());
    }
}
