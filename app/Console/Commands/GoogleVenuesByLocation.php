<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 7/2/16
 * Time: 10:11 PM.
 */
namespace App\Console\Commands;

use App\Jobs\Scraping\Google\GetVenuesByLocation;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class GoogleVenuesByLocation extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap:google {--lat=} {--lng=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get venues from google by latitude and longitude';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dispatch(new GetVenuesByLocation($this->option('lat'), $this->option('lng')));
    }
}
