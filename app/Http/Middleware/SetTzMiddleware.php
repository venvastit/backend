<?php

namespace App\Http\Middleware;

use Closure;

class SetTzMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('timezone')) {
            date_default_timezone_set($request->get('timezone'));
            \Session::put('timezone', $request->get('timezone'));
        }

        return $next($request);
    }
}
