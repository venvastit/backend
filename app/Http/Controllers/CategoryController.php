<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 4/2/16
 * Time: 9:56 PM.
 */
namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Repositories\Criteria\Common\OrderBy;
use App\Repositories\Criteria\Common\WithType;
use App\Repositories\PostCriteria\Category\WithEventsCountByLocation;
use App\Repositories\PostCriteria\Category\WithUpcomingEventsCountByLocation;
use Illuminate\Http\JsonResponse;
use App\Repositories\Category as CategoryRepository;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $repository;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(CategoryRequest $request)
    {
        $this->repository->pushCriteria(new WithType($request->getType()));
        $this->repository->pushCriteria(new OrderBy('priority', OrderBy::ASC));
        $this->repository->applyCriteria();
//        $upcomingByLocationCriteria = new WithUpcomingEventsCountByLocation($request->getLng(), $request->getLat());
//        $categories = $this->repository->applyPostCriteria($upcomingByLocationCriteria);
        return new JsonResponse($this->repository->all());
    }
}
