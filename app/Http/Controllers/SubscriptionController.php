<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/30/16
 * Time: 7:12 PM.
 */
namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Newsletter;
use Mail;

class SubscriptionController extends Controller
{
    public function subscribe(Request $request)
    {
        if ($request->has('email') && Newsletter::hasMember($request->get('email')) === false) {
            Newsletter::subscribe($request->get('email'));
        }

        return new JsonResponse([
            'subscription' => Newsletter::hasMember($request->get('email')),
        ]);
    }

    public function unsubscribe()
    {
    }

    public function contact(Request $request)
    {
        $data = $request->all();
        if (!empty($data['email']) && !empty($data['subject']) && !empty($data['name']) && !empty($data['message'])) {
            $text = 'User: '.$data['name'].' '.$data['email'].' send message with subject'.
                ' '.$data['subject'].' '.'from contact form. '."\n".'Content of message: '.$data['message'].'</p>';
            Mail::raw($text, function ($message) use ($data) {
                $message->from('info@venvast.com', 'Contact');
                $message->sender($data['email'], $data['name']);
                $message->to('info@venvast.com', 'Info at VenVast');
                $message->replyTo($data['email'], $data['name']);
                $message->subject($data['subject']);
            });
        }
    }
}
