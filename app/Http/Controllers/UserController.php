<?php

namespace App\Http\Controllers;

use App\Events\User\Activated;
use App\Events\User\RestorePassword;
use App\Events\User\UserRegistered;
use App\Models\Location;
use App\Role;
use Event;
use App\ServiceProviders\FacebookServiceProvider;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\User\ResetPassword;
use App\Http\Controllers\Controller;
use Psy\Util\Json;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class UserController extends Controller
{
    protected $fbService;

    public function __construct()
    {
        $this->fbService = new FacebookServiceProvider(null);
    }
    
    
    public function activate(Requests\User\Activate $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if(!$user) {
            abort(404);
        }
        /** @var User $user */
        if($user->activation_code === $request->get('activation_code')) {
            $user->activated = true;
            $user->save();
            Event::fire(new Activated($user));
            echo "<script type='text/javascript'>window.close()</script>";
        } else {
            abort(403);
        }
    }
    

    public function register(Requests\RegisterRequest $request)
    {
        if ($request->hasCredentials()) {
            return $this->registerCredentials($request);
        } elseif ($request->getFbToken()) {
            //There is nothing here
        }
    }
    

    public function registerFacebook(Requests\RegisterRequest $request)
    {
    }

    public function registerGoogle(Requests\RegisterRequest $request)
    {
    }

    public function registerCredentials(Requests\RegisterRequest $request)
    {
        $credentials = array_merge($request->all(), [
            'password' => \Hash::make($request->get('password')),
            'activated' => false,
            'activation_code' => \Hash::make($request->get('email'))
        ]);
        $user = User::where('email', $request->getEmail())->get()->first();
        if ($user) {
            return $this->emailInUse();
        }
        $user = User::create($credentials);
        Event::fire(new UserRegistered($user));
        return $this->registerSuccess();
    }
    
    
    public function resetPassword(ResetPassword $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if(!$user) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'email' => 'There is no user with this email registered'
                ],
            ]);
        }
        $user->activation_code = \Hash::make($user->password);
        Event::fire(new RestorePassword($user));
        $user->save();
        return new JsonResponse([
            'success' => true,
            'errors' => false,
            'message' => 'Instructions were sent to email ' . $request->get('email'),
        ]);
    }


    /**
     * Resend activation email
     *
     * @param Requests\User\ResendActivation $request
     * @return JsonResponse
     */
    public function resendActivation(Requests\User\ResendActivation $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if(!$user) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'email' => 'There is no user with this email registered'
                ],
            ]);
        }

        Event::fire(new UserRegistered($user));
        return new JsonResponse([
            'success' => true,
            'errors' => false,
            'message' => 'Activation instructions sent to email: ' . $user->email,
        ]);
    }

    public function logout()
    {
        \Auth::logout();
        \Session::flush();

        return new JsonResponse([
            'success' => true,
        ]);
    }

    public function home(Request $request)
    {
        return new JsonResponse(\Auth::user());
    }

    public function registerSuccess()
    {
        return new JsonResponse([
            'success' => true,
            'errors' => false
        ]);
    }

    public function wrongCreadentials()
    {
        return new JsonResponse([
            'success' => false,
            'errors' => [
                'credentials' => 'Wrong login credentials',
            ],
        ]);
    }
    
    public function emailInUse() 
    {
        return new JsonResponse([
            'success' => false,
            'errors' => [
                'email' => 'Email is already in use'
            ],
        ]);
    }

    public function find(Request $request)
    {
        if (!\Auth::getUser()) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    ['auth' => 'You must be authenticated to make requests'],
                ],
            ]);
        }
        $params = $request->all();
        $user = null;
        foreach ($params as $field => $value) {
            if ($user === null) {
                $user = User::where($field, $value);
            } else {
                $user->where($field, $value);
            }
        }
        $id = $request->get('id');
        if ($id) {
            $user = User::find($id);

            return new JsonResponse([
                'success' => true,
                'user' => $user,
            ]);
        }
        if ($user === null) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'Your query cannot be satisfied',
                ],
            ]);
        } else {
            return new JsonResponse([
                'success' => true,
                'users' => $user->get()->all(),
            ]);
        }
    }

    public function restorePassword(Request $request)
    {
        return new JsonResponse($request->all());
    }

    private function update(Request $request)
    {
        if (!$request->get('current_password') && $request->get('password')) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'current_password' => 'You should provide your current password to set the new one',
                ],
            ]);
        }
        if ($request->get('password') !== $request->get('password_confirmation')) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'password_confirmation' => 'Password and confirmation doensn\'t match ',
                ],
            ]);
        }
        /** @var User $user */
        $user = \Auth::user();
        if ($request->get('password') && !\Hash::check($user->password, $request->get('current_password'))) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'current_password' => 'Wrong password!',
                ],
            ]);
        }
        $credentials = $request->all();
        if ($request->get('password')) {
            $credentials = array_merge($credentials, ['password' => \Hash::make($request->get('password'))]);
        }
        if ($user->update($credentials)) {
            return new JsonResponse([
                'success' => true,
                'errors' => false,
            ]);
        }
    }
}
