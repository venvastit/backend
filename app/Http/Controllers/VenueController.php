<?php

namespace App\Http\Controllers;

use App\Models\Venue;
use App\Repositories\Criteria\Common\ByCoords;
use App\Repositories\Criteria\Common\FieldEquals;
use App\Repositories\Criteria\Common\FieldNotNull;
use App\Repositories\Criteria\Common\HasCategory;
use App\Repositories\Criteria\Common\InCategory;
use App\Repositories\Criteria\Common\Organizer;
use App\Repositories\Criteria\Common\Paginate;
use App\Repositories\Criteria\Common\TextSearch;
use App\Repositories\Criteria\Venue\WantToHost;
use App\Repositories\Criteria\Venue\WithEvents;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests;
use Cache;
use App\Http\Requests\VenueRequest;
use App\Repositories\Venue as VenueRepository;
use Toin0u\Geotools\Geotools;

class VenueController extends Controller
{
    const PER_PAGE = 60;

    /**
     * @var VenueRepository
     */
    protected $venueRepository;

    public function __construct(VenueRepository $venueRepository)
    {
        $this->setVenueRepository($venueRepository);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(VenueRequest $request)
    {
        if ($request->get('latitude') && $request->get('longitude')) {
            $this->venueRepository->pushCriteria(new ByCoords($request->getLat(), $request->getLng(), $request->getRadius()));
        }
        if (!$request->getOrganizer()) {
            $this->venueRepository->pushCriteria(new FieldNotNull('cover_image'));
            if (env('APP_DEBUG', true) === false) {
                $this->venueRepository->setModel($this->venueRepository->getModel()->where('published', '=', true));
            }
        }

        if ($request->get('hosts')) {
            $this->venueRepository->pushCriteria(new WantToHost());
        }

        if ($request->getText()) {
            $this->venueRepository->pushCriteria(new TextSearch($request->getText(), 'name'));
        }

        if ($request->getOrganizer()) {
            $this->getVenueRepository()->pushCriteria(new Organizer());
        }
        $this->getVenueRepository()->pushCriteria(new HasCategory());

        if ($request->getCategory()) {
            $this->venueRepository->pushCriteria(new InCategory($request->getCategory()));
        }

        $cacheKey = $this->getVenueRepository()->getCacheKey();
            /** @var Collection $venues */
        $venues = $this->venueRepository->applyCriteria()->getModel()->with(['organizersRelation', 'events'])->get();
        Cache::add($cacheKey, json_encode($venues), 30);
        $geoTools = new Geotools();

        $venues = $venues->sortBy(function ($venue, $key) use ($request, $geoTools) {
            if (!is_array($venue)) {
                $venueLatitude = $venue->location['coordinates'][1];
                $venueLongitude = $venue->location['coordinates'][0];
            } else {
                $venueLatitude = $venue['location']['coordinates'][1];
                $venueLongitude = $venue['location']['coordinates'][0];
            }
            $userLatitude = $request->getLat();
            $userLongitude = $request->getLng();

            $venueCoord = $geoTools->coordinate([$venueLatitude, $venueLongitude]);
            $userCoord = $geoTools->coordinate([$userLatitude, $userLongitude]);
            $d = number_format($geoTools->distance()->setFrom($userCoord)->setTo($venueCoord)->in('km')->flat(), 2, '.', '');
            if (!is_array($venue)) {
                $venue->d = $d;
            } else {
                $venue['d'] = $d;
            }
            return $d;
        });
        if ($request->isWebCall() && !$request->isMarkerRequest()) {
            $venues = $venues->forPage($request->getPage(), self::PER_PAGE);
        } else {
            $venues = $venues->forPage(1, 2000);
        }

        if($request->isTypeAhed()) {
            $venues = $venues->toArray();
            /**
             * @var  $key
             * @var Venue $venue
             */
            foreach ($venues as $key => $venue) {
                $venues[$key] = [
                    'name' => $venue['name'],
                    'id' => $venue['_id'],
                    'category' => $venue['categories'][0] ? $venue['categories'][0]->name : 'Unspecified',
                    'image' => array_key_exists('cover_image', $venue) ? Venue::getImagePath($venue['cover_image']) : null,
//                    'venue' => $event['venue_relation']['name'],
                ];
            }
            return array_values($venues);
        }

        if($request->isMarkerRequest()) {
            $venues = $venues->toArray();
            foreach ($venues as $key => $venue) {
                $venues[$key] = [
                    'title' => mb_substr($venue['name'], 0, 14) . '...',
                    'id' => $venue['_id'],
                    'category' => $venue['categories'][0] ? $venue['categories'][0]->name : 'Unspecified',
                    'icon' => $venue['categories'][0] ? 'http://venvast.com/img/cache/original/categories/' . $venue['categories'][0]->image : '/assets/img/question.png',
                    'image' => array_key_exists('cover_image', $venue) ? Venue::getImagePath($venue['cover_image']) : null,
                    'center' => [$venue['location']['coordinates'][1], $venue['location']['coordinates'][0]],
                ];
            }
            return array_values($venues);
        }

        return array_values($venues->toArray());
    }

    public function find(Request $request)
    {
        $params = $request->all();
        if ($request->get('_id')) {
            return new JsonResponse([Venue::find($request->get('_id'))]);
        }
        foreach ($params as $key => $value) {
            if ($key !== 'latitude' && $key !== 'longitude' && $key !== 'name') {
                $this->venueRepository->pushCriteria(new FieldEquals($key, $value));
            }
            if ($key === 'name') {
                $value = str_replace('+', ' ', $value);
                $this->venueRepository->pushCriteria(new TextSearch($value, $key));
            }
        }
        if ($request->get('latitude') && $request->get('longitude')) {
            $this->venueRepository->pushCriteria(new ByCoords($request->get('latitude'), $request->get('longitude'), 0.15));
        }

        return new JsonResponse($this->venueRepository->applyCriteria()->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** @var User $user */
        $user = \Auth::user();

        $requestData = $request->all();
        foreach ($requestData as $key => $value) {
            if ($key === '$$hashKey' || $key === '$hashKey') {
                unset($requestData[$key]);
            }
        }

        if (!$user) {
            return new JsonResponse([
                'error' => true,
                'success' => false,
            ]);
        }
        $venue = [];

        foreach ($requestData as $key => $value) {
            if ($key !== 'cover_image') {
                $this->setIfHasInRequest($request, $key, $venue);
            }
        }
        if (isset($venue['hostEvents'])) {
            $venue['hostEvents'] = (bool) $venue['hostEvents'];
        }
        $location = $request->get('location');
        $venue['location'] = [
            'type' => $location['type'],
            'coordinates' => [
                floatval($location['coordinates'][0]),
                floatval($location['coordinates'][1]),
            ],
        ];
        $venue['organizers'] = [$user->_id];
        unset($venue['_id']);
        if ($request->get('_id') !== null) {
            /** @var Venue $newVenue */
            $newVenue = Venue::find($request->get('_id'));
            if (!$newVenue) {
                return new JsonResponse([
                    'error' => true,
                    'success' => false,
                    'errorBag' => [
                        'Cannot perform update venue action. Venue does not exist',
                    ],
                ]);
            }
            $newVenue->setRawAttributes($venue);
        } else {
            $newVenue = Venue::create($venue);
        }
        $newVenue->setBusinessHours($request->get('business_hours'));
        $newVenue->setCategories($request->get('categories'));
        $newVenue->setEventCategories($request->get('eventCategories'));
        if ($request->file('cover_image')) {
            $newVenue->setImage('cover_image', $request->file('cover_image'));
        }
        $newVenue->save();

        return new JsonResponse([
            'error' => false,
            'success' => true,
            'venue' => $newVenue,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->getVenueRepository()->pushCriteria(new WithEvents());
        $this->getVenueRepository()->applyCriteria();

        return new JsonResponse($this->getVenueRepository()->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** @var Venue $existingVenue */
        $existingVenue = Venue::find($id);
        if (!$existingVenue) {
            return new JsonResponse([
                'error' => true,
                'success' => false,
                'errorBag' => [
                    'Cannot perform update venue action. Venue does not exist',
                ],
            ]);
        }

        $attributes = $request->all();
        unset($attributes['_id']);
        $existingVenue->setRawAttributes($attributes);
        $existingVenue->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venue = Venue::find($id);
        $organizers = User::find($venue->organizers);
        $canDelete = false;
        $user = \Auth::user();
        foreach ($organizers as $organizer) {
            if ($user->_id === $organizer->_id) {
                $canDelete = true;
            }
        }
        if ($canDelete) {
            Venue::destroy($id);

            return new JsonResponse([
                'error' => false,
                'success' => true,
            ]);
        } else {
            return new JsonResponse([
                'error' => true,
                'success' => false,
            ]);
        }
    }

    /**
     * @return VenueRepository
     */
    public function getVenueRepository()
    {
        return $this->venueRepository;
    }

    /**
     * @param VenueRepository $venueRepository
     */
    public function setVenueRepository(VenueRepository $venueRepository)
    {
        $this->venueRepository = $venueRepository;
    }
}
