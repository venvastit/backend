<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 4/2/16
 * Time: 9:57 PM.
 */
namespace App\Http\Controllers;

use App\Models\Location;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function index()
    {
        return new JsonResponse(Location::all());
    }

    public function one(Request $request, $id)
    {
        return new JsonResponse(Location::find($id));
    }

    /**
     * Returns current longitude and latitude of the user.
     * @return JsonResponse
     */
    public function current()
    {
        $location = \GeoIP::getLocation();
        $existingLocation = Location::where('name', $location['city'])->get()->first();
        if ($existingLocation) {
            return new JsonResponse([
                'latitude' => $existingLocation->latitude,
                'longitude' => $existingLocation->longitude,
            ]);
        }

        return new JsonResponse([
            'latitude' => $location['lat'],
            'longitude' => $location['lon'],
        ]);
    }

    /**
     * Returns current location entity based on GeoIp.
     * @return JsonResponse|null
     */
    public function currentEntity()
    {
        //        $location = \GeoIP::getLocation();
//        if($location['lat'] == 41.31) {
            $existingLocation = Location::where('name', 'Bangkok')->get()->first();

        return new JsonResponse([$existingLocation]);
//        }
//        $existingLocation = Location::where('name', $location['city'])->get()->first();
//        if($existingLocation)
//        return null;
    }
}
