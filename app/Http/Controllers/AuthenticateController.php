<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/17/16
 * Time: 8:39 AM.
 */
namespace App\Http\Controllers;

use App\ServiceProviders\FacebookServiceProvider;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class AuthenticateController extends Controller
{
    /**
     * Facebook service provider instance.
     *
     * @var FacebookServiceProvider
     */
    public $facebookServiceProvider;

    public $token;

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Authenticate user
     * Returns JWT.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        if ($request->get('email') !== null && $request->get('password') !== null) {
            $response = $this->authenticateByCredentials($request->all());
            if ($request->has('uuid')) {
                $user = JWTAuth::authenticate($this->token);
                $user->setAttribute('uuid', $request->get('uuid'));
                $user->save();
            }

            return $response;
        } elseif ($request->get('facebook_access_token')) {
            $this->createProvider($request->get('facebook_access_token'));
            $response = $this->authenticateByFbToken($request->get('facebook_access_token'));
            if ($request->has('uuid')) {
                $user = JWTAuth::authenticate($this->token);
                $user->setAttribute('uuid', $request->get('uuid'));
                $user->save();
            }

            return $response;
        }

        return response()->json(['error' => 'invalid_credentials'], 401);
    }

    /**
     * Authenticate user by email and password.
     * @param $credentials
     * @return \Illuminate\Http\JsonResponse
     */
    private function authenticateByCredentials($credentials)
    {
        $email = $credentials['email'];
        $user = User::where('email', $email)->first();
        try {
            $credentials = array_only($credentials, ['email', 'password']);
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // if no errors are encountered we can return a JWT
        $this->token = $token;
        if($user->activated != true) {
            return response()->json([
                'error' => 'User is not activated! Check your email for activation instructions'
            ]);
        }
        return response()->json(compact('token'));
    }

    private function createIfNew($credentials)
    {
        $user = User::where('email', $credentials['email'])->get()->first();
        if (!$user) {
            $credentials['password'] = \Hash::make($credentials['password']);
            $user = User::create($credentials);

            return $this->authenticateByUser($user);
        }

        return;
    }

    /**
     * Authenticate user by given facebook token.
     *
     * @param string $token
     * @return \Illuminate\Http\JsonResponse
     */
    private function authenticateByFbToken(string $token)
    {
        if (!$user = $this->validateFbToken($token)) {
            return response()->json(['error' => 'invalid_token'], 401);
        }

        return $this->authenticateByUser($user);
    }

    private function authenticateByGoogleToken(array $credentials)
    {
        //        if(! $user = $this->getUserByGoogleCredentials($credentials)) {
//            return response()->json(['error' => 'invalid_token'], 401);
//        }
//        return $this->authenticateByUser($user);
    }

    /**
     * @param array $credentials
     * @return null|User
     */
    private function getUserByGoogleCredentials(array $credentials)
    {
        //        if(!array_key_exists('email', $credentials) || !array_key_exists('google_id', $credentials)) {
//            return null;
//        }
//        $user = User::where('email', $credentials['email'])->get()->first();
//        if(!$user) {
//            User::where('google_id', $credentials['google_id'])->get()->first();
//        }
//        if(!$user) {
//            $user = User::create($credentials);
//        }
//        return $user;
    }

    /**
     * Validate facebook token. Returns user instance if token is valid and null otherwise.
     *
     * @param string $token
     * @return User | null
     */
    private function validateFbToken(string $token)
    {
        $this->facebookServiceProvider->setAccessToken($token);
        if ($this->facebookServiceProvider->checkAccessToken() === true) {
            return $this->facebookServiceProvider->getUserByToken();
        }

        return;
    }

    /**
     * Authenticate by User instance.
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    private function authenticateByUser(User $user)
    {
        if (!$user) {
            return response()->json(['error' => 'invalid_credentials'], 401);
        }
        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::fromUser($user)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // if no errors are encountered we can return a JWT
        $this->token = $token;

        return response()->json(compact('token'));
    }

    public function createProvider(string $token)
    {
        $this->facebookServiceProvider = new FacebookServiceProvider($token);

        return $this->facebookServiceProvider;
    }
}
