<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 4/2/16
 * Time: 9:57 PM.
 */
namespace App\Http\Controllers;

use App\Models\Source;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SourceController extends Controller
{
    public function index()
    {
        return new JsonResponse(Source::all());
    }

    public function one(Request $request, $id)
    {
        return new JsonResponse(Source::find($id));
    }
}
