<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function setIfHasInRequest(Request $request, $key, &$venue)
    {
        if ($request->get($key)) {
            $venue[$key] = $request->get($key);
        }
    }

    protected function setFromRequest(Request $request, $key, &$venue)
    {
        if (!$request->get($key)) {
            throw new HttpResponseException(new JsonResponse([
                'success' => false,
            ]));
        } else {
            $venue[$key] = $request->get($key);
        }
    }
}
