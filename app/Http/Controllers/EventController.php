<?php

namespace App\Http\Controllers;

use App\Repositories\Criteria\Common\InCategory;
use App\Repositories\Criteria\Common\ByCoords;
use App\Repositories\Criteria\Common\Organizer;
use App\Repositories\Criteria\Common\Paginate;
use App\Repositories\Criteria\Common\TextSearch;
use App\Repositories\Criteria\Event\ByDate;
use App\Repositories\Criteria\Event\DateRange;
use App\Repositories\Criteria\Event\Upcoming;
use App\Repositories\Event as EventRepository;
use App\Repositories\Criteria\Common\OrderBy;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Criteria\Event\When;
use App\Http\Requests\EventRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Event;
use Carbon\Carbon;
use Cache;
use Auth;
use MongoDB\BSON\ObjectID;

class EventController extends Controller
{
    const O_TYPE_ONGOING = 'ongoing';
    const O_TYPE_PLANNED = 'planned';
    const O_TYPE_PAST = 'past';

    const PER_PAGE = 60;

    /**
     * @var EventRepository
     */
    protected $eventRepository;

    /**
     * EventController constructor.
     * @param EventRepository $eventRepository
     */
    public function __construct(EventRepository $eventRepository)
    {
        $this->setEventRepository($eventRepository);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(EventRequest $request)
    {
        if ($request->get('latitude') && $request->get('longitude')) {
            $this->getEventRepository()->pushCriteria(new ByCoords($request->getLat(), $request->getLng(), $request->getRadius()));
        }
        if ($request->getOrganizer() !== null && $request->getOrganizer() != false) {
            $this->getEventRepository()->pushCriteria(new Organizer());
        } else {
            $this->eventRepository->setModel($this->eventRepository->getModel()->where('published', '=', true));
        }
        if (!$request->getOrganizer() && !$request->getDate()) {
            $when = $request->getWhen();
            $dates = $request->getDateRange();
            if ($when === null && count($dates) === 0) {
//                $this->getEventRepository()->pushCriteria(new Upcoming());
            } else if (count($dates) !== 0) {
                $this->getEventRepository()->pushCriteria(new DateRange($dates));
            } else {
                $this->getEventRepository()->pushCriteria(new When($when));
            }
        }
        if ($request->getDate()) {
            $this->getEventRepository()->pushCriteria(new ByDate($request->getDate()));
        }

        if ($request->getText()) {
            $this->getEventRepository()->pushCriteria(new TextSearch($request->getText()));
        }

        if ($request->getCategory()) {
            $this->getEventRepository()->pushCriteria(new InCategory($request->getCategory()));
        }
        $this->getEventRepository()->pushCriteria(new OrderBy('start_at', 'asc'));

        if ($request->isWebCall() && $request->get('markers') !== 'true') {
            $this->getEventRepository()->pushCriteria(new Paginate($request->getPage() - 1, self::PER_PAGE));
        } elseif ($request->get('markers') === 'true') {
            $this->getEventRepository()->pushCriteria(new Paginate(0, 2000));
        }
        $cacheKey = $this->getEventRepository()->getCacheKey();
//        if(Cache::has($cacheKey)){
//            return new JsonResponse(json_decode(Cache::get($cacheKey), true));
//        } else {
            $this->eventRepository->setModel($this->eventRepository->getModel()->with('venueRelation'));
        $events = $this->eventRepository->applyCriteria();
        if($request->isTypeAhed()) {
            $events = $events->all(['_id', 'title', 'picture', 'categories', 'venue_id']);
            foreach ($events as $key => $event) {
                $event = $event->toArray();
                $events[$key] = [
                    'name' => mb_substr($event['title'], 0, 10) . '...',
                    'id' => $event['_id'],
                    'category' => $event['categories'][0] ? $event['categories'][0]->name : 'Unspecified',
                    'image' => array_key_exists('picture', $event) ? Event::getImagePath($event['picture']) : null,
                    'venue' => $event['venue_relation']['name'],
                ];
            }
        } else if($request->isMarkerRequest()) {
            $events = $events->all(['_id', 'title', 'picture', 'categories', 'venue_id']);
            foreach ($events as $key => $event) {
                $event = $event->toArray();
                if(!array_key_exists('categories', $event) || count($event['categories']) === 0) {
                    unset($event[$key]);
                    continue;
                }
                $events[$key] = [
                    'title' => mb_substr($event['title'], 0, 14) . '...',
                    'id' => $event['_id'],
                    'category' => $event['categories'][0] ? $event['categories'][0]->name : 'Unspecified',
                    'icon' => $event['categories'][0] ? 'http://venvast.com/img/cache/original/categories/' . $event['categories'][0]->image : '/assets/img/question.png',
                    'image' => array_key_exists('picture', $event) ? Event::getImagePath($event['picture']) : null,
                    'center' => [$event['venue_relation']['location']['coordinates'][1], $event['venue_relation']['location']['coordinates'][0]],
                ];
            }
        } else {
            $events = $events->all();
        }
        Cache::add($cacheKey, json_encode($events), 30);
        return new JsonResponse($events);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if (!$user) {
            return new JsonResponse([
                'error' => true,
                'success' => false,
            ]);
        }
        $eventData = [];
        $this->setIfHasInRequest($request, 'details', $eventData);
        $this->setIfHasInRequest($request, 'picture', $eventData);
        $this->setFromRequest($request, 'title', $eventData);
        $this->setFromRequest($request, 'venue_id', $eventData);
        $eventData['start_at'] = Carbon::createFromFormat('Y.d.m H:i', $request->get('start_at'));
        $eventData['end_at'] = Carbon::createFromFormat('Y.d.m H:i', $request->get('end_at'));
        $eventData['duration'] = $eventData['end_at']->diffInMinutes($eventData['start_at']);
        $eventData['organizers'] = [$user->_id];
        $eventData['categories'] = [];
        $categories = $request->get('categories');
        foreach ($categories as $category) {
            if ($category !== null) {
                $eventData['categories'][] = $category;
            }
        }
        $this->setFromRequest($request, 'address', $eventData);
        if ($request->get('_id') !== null) {
            /* @var Event $newVenue */
            $event = Event::find($request->get('_id'));
            if (!$event) {
                return new JsonResponse([
                    'error' => true,
                    'success' => false,
                    'errorBag' => [
                        'Cannot perform update event action. Event does not exist',
                    ],
                ]);
            }
            $event->setRawAttributes($eventData);
        } else {
            $event = Event::create($eventData);
        }
        if ($request->hasFile('picture')) {
            $event->setImage('picture', $request->file('picture'));
        }
        $event->save();

        return new JsonResponse([
            'error' => false,
            'success' => true,
            'event' => $event,
        ]);
    }

//    /**
//     * @return mixed
//     */
//    public function listMy()
//    {
//        return Event::whereHas('organizers', function($query){
//            $query->where('id', \Auth::user()->id);
//        })->get();
//    }

    public function myCount()
    {
        return new JsonResponse([[Event::whereHas('organizers', function ($query) {
            $query->where('id', \Auth::user()->id);
        })->count()]]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        return new JsonResponse(Event::where('_id', $id)->with('venueRelation')->get()->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if (!$user) {
            return new JsonResponse([
                'error' => true,
                'success' => false,
            ]);
        }
        $attributes = $request->all();
        unset($attributes['organizers']);
        unset($attributes['_id']);
        /** @var Event $event */
        $event = Event::find($id);
//        dd($event->organizers());
        foreach ($attributes as $key => $value) {
            if ($event->$key != $value) {
                if ($key === 'location') {
                    $event->setLocation($value);
                } elseif ($key === 'categories') {
                    $event->setCategories($value);
                } else {
                    $event->$key = $value;
                }
            }
        }
        $event->save();

        return new JsonResponse([
            'success' => true,
            'errors' => false,
            'event' => $event,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $organizers = User::find($event->organizers);
        $venue = $event->venue;
        if ($venue) {
            $venueOrganizers = User::find($event->venue->organizers);
            $organizers = array_merge($organizers, $venueOrganizers);
        }
        $canDelete = false;
        $user = \Auth::user();
        foreach ($organizers as $organizer) {
            if ($user->_id === $organizer->_id) {
                $canDelete = true;
            }
        }
        if ($canDelete) {
            Event::destroy($id);

            return new JsonResponse([
                'error' => false,
                'success' => true,
            ]);
        } else {
            return new JsonResponse([
                'error' => true,
                'success' => false,
            ]);
        }
    }

    /**
     * @return EventRepository
     */
    public function getEventRepository()
    {
        return $this->eventRepository;
    }

    /**
     * @param EventRepository $eventRepository
     */
    public function setEventRepository($eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }
}
