<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 4/17/16
 * Time: 10:01 PM.
 */
namespace App\Http\Controllers\Admin;

use App\Jobs\MigrateVenues;
use App\Models\Event;
use App\Models\Venue;
use App\ServiceProviders\FacebookServiceProvider;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use View;
use Illuminate\Contracts\Redis\Database as RedisContract;

class ScrapVenueController extends Controller
{
    use DispatchesJobs;

    protected $fb;
    protected $fbServiceProvider;
    protected $redis;

    public function __construct(LaravelFacebookSdk $fb, RedisContract $redis)
    {
        $this->fb = $fb;
        $this->fbServiceProvider = new FacebookServiceProvider(\Session::get('fb_user_access_token'));
        $this->redis = $redis;
    }

    public function getIndex()
    {
        $login_url = $this->fb->getLoginUrl(['email']);

        return View::make('admin.scrap_venues', ['login_url' => $login_url]);
    }

    public function saveFbVenues(Request $request)
    {
        $notAuthenticated = $this->notAuthenticatedResponse();
        if ($notAuthenticated !== false) {
            return $notAuthenticated;
        }
        $fbIds = $request->get('fb_ids');
        foreach ($fbIds as $fbId) {
            if (!empty($fbId)) {
                $existing = $this->checkExistingVenue($fbId);
                if ($existing !== false) {
                    continue;
                }

                $eventData = $this->getVenueFromFacebook($fbId);
                $this->saveVenue($eventData);
            }
        }

        return back();
    }

    public function migrateToSql()
    {
        \Artisan::call('scrapper:venue', ['--access-token' => $this->fbServiceProvider->getAccessToken()]);

        return back();
    }

    private function checkExistingVenue($eventId)
    {
        $event = Venue::where('id_on_source', $eventId)->get()->first();
        if ($event) {
            return true;
        } else {
            return false;
        }
    }

    private function getVenueFromFacebook($eventId)
    {
        return $this->fbServiceProvider->getVenue($eventId);
    }

    private function saveVenue($fbVenue)
    {
        $this->redis->set('facebook:venues:'.$fbVenue['id'], json_encode($fbVenue));
    }

    public function notAuthenticatedResponse()
    {
        if (!\AdminAuth::user()) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'auth' => 'You are not allowed to perform this action',
                ],
            ]);
        } else {
            return false;
        }
    }
}
