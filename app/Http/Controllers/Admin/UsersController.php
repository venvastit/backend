<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/19/16
 * Time: 4:21 PM.
 */
namespace App\Http\Controllers\Admin;

use App\Models\Event;
use App\Models\Venue;
use App\Repositories\Criteria\Common\Paginate;
use App\Repositories\Event as EventRepo;
use App\Repositories\Venue as VenueRepo;
use App\Repositories\Category as CategoryRepo;
use App\Repositories\User as UserRepo;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MongoDB\BSON\ObjectID;
use Yajra\Datatables\Datatables;

class UsersController extends BaseAdminController
{
    /**
     * @var VenueRepo
     */
    public $venueRepository;

    /**
     * @var CategoryRepo
     */
    public $categoryRepository;

    /**
     * @var UserRepo
     */
    public $userRepository;

    public function __construct(EventRepo $repository, VenueRepo $venue, CategoryRepo $category, UserRepo $user)
    {
        parent::__construct($repository);
        $this->venueRepository = $venue;
        $this->categoryRepository = $category;
        $this->userRepository = $user;
    }

    public function modelClass()
    {
        return User::class;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $length = intval($request->get('length'));
            $skip = intval($request->get('start'));
            $this->userRepository->pushCriteria(new Paginate(null, $length, $skip));
            $modifyModel = $this->userRepository->getModel()->with('eventOrganizer')->with('venueOrganizer');
            $this->userRepository->setModel($modifyModel);

            return Datatables::of($this->userRepository->applyCriteria()->all())->make(true);
        }

        return view('admin.users.all', ['datUrl' => route('admin.users.list'), 'header' => 'All users']);
    }

    public function organizers(Request $request)
    {
        if ($request->ajax()) {
            $length = intval($request->get('length'));
            $skip = intval($request->get('start'));
            $this->userRepository->pushCriteria(new Paginate(null, $length, $skip));
            $modifyModel = $this->userRepository->getModel()->where('type', '=', 1)->with('eventOrganizer')->with('venueOrganizer');
            $this->userRepository->setModel($modifyModel);

            return Datatables::of($this->userRepository->applyCriteria()->all())->make(true);
        }

        return view('admin.users.all', ['datUrl' => route('admin.users.organizers'), 'header' => 'Organizers']);
    }

    public function attendees(Request $request)
    {
        if ($request->ajax()) {
            $length = intval($request->get('length'));
            $skip = intval($request->get('start'));
            $this->userRepository->pushCriteria(new Paginate(null, $length, $skip));
            $modifyModel = $this->userRepository->getModel()->where('type', 0);
            $this->userRepository->setModel($modifyModel);

            return Datatables::of($this->userRepository->applyCriteria()->all())->make(true);
        }

        return view('admin.users.all', ['datUrl' => route('admin.users.attendees'), 'header' => 'Attendees']);
    }

    public function admins(Request $request)
    {
        if ($request->ajax()) {
            $length = intval($request->get('length'));
            $skip = intval($request->get('start'));
            $this->userRepository->pushCriteria(new Paginate(null, $length, $skip));
            $modifyModel = $this->userRepository->getModel()->where('admin', '=', 1)->with('eventOrganizer')->with('venueOrganizer');
            $this->userRepository->setModel($modifyModel);

            return Datatables::of($this->userRepository->applyCriteria()->all())->make(true);
        }

        return view('admin.users.all', ['datUrl' => route('admin.users.admins'), 'header' => 'Admins']);
    }

    public function edit(Request $request, $id)
    {
        /** @var User $user */
        $user = User::find($id);

        return view('admin.users.form', compact('user'));
    }

    public function create(Request $request)
    {
        $user = new User();
        $user->type = 0;
        $user->admin = 0;
        $user->activated = false;

        return view('admin.users.form', compact('user'));
    }

    public function store(Request $request)
    {
        if ($request->has('_id')) {
            $user = User::find($request->get('_id'));
        } else {
            $user = new User();
        }

        $attributes = $request->all();
        $attributes['type'] = intval($attributes['type']);
        $attributes['admin'] = intval($attributes['admin']);
        unset($attributes['_id']);
        $user->setRawAttributes($attributes);
        $user->save();

        return redirect()->route('admin.users.list');
    }

    public function stats(Request $request)
    {
        $totalUser = User::all()->count();
        $attendees = User::where('type', '=', 0)->count();
        $organizers = User::where('type', '=', 1)->count();
        $date = Carbon::now()->setTime(0, 0);
        /** @var Carbon[] $dates */
        $dates = [$date];
        $keys = [$date->format('d.m.Y')];
        $data = [];

        for ($i = 1; $i < 7; $i++) {
            $d = clone($dates[$i - 1]);
            $d->subDay();
            $dates[] = $d;
            $keys[] = $d->format('d.m.Y');
        }

        foreach ($dates as $date) {
            $nextDay = clone($date);
            $items = User::where('created_at', '>=', $date)->where('created_at', '<=', $nextDay->addDay())->count();
            $data[] = $items;
        }
        $data = json_encode($data);
        $keys = json_encode($keys);

        return view('admin.users.stats', compact('totalUser', 'attendees', 'organizers',
            'hasVenues', 'hasEvents', 'keys', 'data'));
    }
}
