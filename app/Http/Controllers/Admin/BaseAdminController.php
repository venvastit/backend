<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/17/16
 * Time: 12:01 PM.
 */
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Event;
use App\Models\Venue;
use App\Repositories\AbstractRepository;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class BaseAdminController extends Controller
{
    /**
     * @var Model
     */
    public $model;

    /**
     * @var AbstractRepository
     */
    public $repository;

    public function __construct(AbstractRepository $repository)
    {
        $this->model = $this->model();
        $this->repository = $repository;
    }

    /**
     * @return string
     */
    abstract public function modelClass();

    public function model()
    {
        if (!$this->model) {
            $class = $this->modelClass();
            $this->model = new $class();
        }

        return $this->model;
    }

    protected function getRepo()
    {
        return $this->repository;
    }

    public function index(Request $request)
    {
        $all = $this->getRepo()->all();

        return $all;
    }

    public function publish(Request $request, $id)
    {
        $model = $this->model()->find($id);
        if ($model) {
            $model->published = true;
        }
        $model->save();

        return redirect()->back();
    }

    public function unpublish(Request $request, $id)
    {
        $model = $this->model()->find($id);
        if ($model) {
            $model->published = false;
        }
        $model->save();
        if (!$request->ajax()) {
            return redirect()->back();
        }
    }

    public function delete(Request $request, $id)
    {
        $model = $this->model()->find($id);
        $model->delete();
        if (!$request->ajax()) {
            return redirect()->back();
        }
    }
}
