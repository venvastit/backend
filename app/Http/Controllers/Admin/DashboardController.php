<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/17/16
 * Time: 10:46 AM.
 */
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard');
    }
}
