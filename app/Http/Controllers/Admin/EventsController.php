<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/17/16
 * Time: 12:15 PM.
 */
namespace App\Http\Controllers\Admin;

use App\Dictionary\CategoryType;
use App\Dictionary\Scraping\Facebook\PageType;
use App\Entities\Common\Location;
use App\Entities\Reference;
use App\Entities\Test;
use App\Jobs\Migrate\Events;
use App\Jobs\Migrate\Sources;
use App\Jobs\Scraping\Facebook\GetPageInfo;
use App\Jobs\Scraping\Facebook\GetVenuesByLocation;
use App\Jobs\Scraping\Google\GetVenuesByLocation as GoogleVenues;
use App\Models\Category;
use App\Models\Event;
use App\Models\Venue;
use App\Repositories\Event as EventRepository;
use App\User;
use Carbon\Carbon;
use Doctrine\ODM\MongoDB\Tests\Functional\Ticket\Car;
use Illuminate\Http\Request;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Yajra\Datatables\Datatables;

class EventsController extends BaseAdminController
{
    protected $fb;

    public function __construct(EventRepository $repository, LaravelFacebookSdk $fb)
    {
        parent::__construct($repository);
        $this->fb = $fb;
    }

    public function modelClass()
    {
        return Event::class;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $all = Event::with('venueRelation')->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.events.list'));

        return view('admin.events.all', ['datUrl' => route('admin.events.list')]);
    }

    public function published(Request $request)
    {
        if ($request->ajax()) {
            $all = Event::where('published', '=', true)->with('venueRelation')->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.events.published'));

        return view('admin.events.all', ['datUrl' => route('admin.events.published')]);
    }

    public function unpublished(Request $request)
    {
        if ($request->ajax()) {
            $all = Event::where('published', '!=', true)->with('venueRelation')->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.events.unpublished'));

        return view('admin.events.all', ['datUrl' => route('admin.events.unpublished')]);
    }

    public function upcoming(Request $request)
    {
        if ($request->ajax()) {
            $all = Event::where('start_at', '>=', Carbon::now())->with('venueRelation')->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.events.upcoming'));

        return view('admin.events.all', ['datUrl' => route('admin.events.upcoming')]);
    }

    public function passed(Request $request)
    {
        if ($request->ajax()) {
            $all = Event::where('start_at', '<', Carbon::now())->with('venueRelation')->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.events.passed'));

        return view('admin.events.all', ['datUrl' => route('admin.events.passed')]);
    }

    public function edit(Request $request, $id)
    {
        $categories = Category::where('type', CategoryType::TYPE_EVENT)->get()->all();
        $venues = Venue::all();
        $event = Event::find($id);
        $users = User::where('type', 1)->get()->all();

        return view('admin.events.form', compact('event', 'venues', 'categories', 'users'));
    }

    public function create(Request $request)
    {
        $categories = Category::where('type', CategoryType::TYPE_EVENT)->get()->all();
        $venues = Venue::all();
        $event = new Event();
        $users = User::where('type', 1)->get()->all();

        return view('admin.events.form', compact('event', 'venues', 'categories', 'users'));
    }

    public function store(Request $request)
    {
        if ($request->get('_id')) {
            $event = Event::find($request->get('_id'));
        } else {
            $event = new Event();
        }
        $attributes = $request->all();
        foreach ($attributes as $key => &$value) {
            if (is_string($value) && strlen($value) === 0) {
                $value = null;
            }
            if ($value === 'on') {
                $value = true;
            }
            if ($value === 'off') {
                $value = false;
            }
        }
        unset($attributes['_id']);
        $event->setRawAttributes($attributes);

        if ($request->hasFile('picture')) {
            $event->setImage('picture', $request->get('picture'));
        }
        $event->setCategories($request->get('categories'));
        $event->setAttribute('organizers', $request->get('organizers'));
        $event->setAttribute('start_at', Carbon::createFromFormat('Y.d.m H:i', $attributes['start_at']));
        $event->setAttribute('end_at', Carbon::createFromFormat('Y.d.m H:i', $attributes['start_at']));
        $event->save();
        if (\Session::has('list_url')) {
            return redirect()->to(\Session::get('list_url'));
        }

        return redirect()->back();
    }

    public function publish(Request $request, $id)
    {
        $event = Event::find($id);
        if ($event) {
            $event->published = true;
        }
        $event->save();

        return redirect()->back();
    }

    public function unpublish(Request $request, $id)
    {
        $event = Event::find($id);
        if ($event) {
            $event->published = false;
        }
        $event->save();
        if (!$request->ajax()) {
            return redirect()->back();
        }
    }

    public function delete(Request $request, $id)
    {
        $event = Event::find($id);
        $event->delete();
        if (!$request->ajax()) {
            return redirect()->back();
        }
    }

    public function stats(Request $request)
    {
        $date = Carbon::now()->setTime(0, 0);
        /** @var Carbon[] $dates */
        $dates = [$date];
        $keys = [$date->format('d.m.Y')];
        $data = [];
        $updates = [];
        for ($i = 1; $i < 7; $i++) {
            $d = clone($dates[$i - 1]);
            $d->subDay();
            $dates[] = $d;
            $keys[] = $d->format('d.m.Y');
        }

        foreach ($dates as $date) {
            $nextDay = clone($date);
            $items = Event::where('created_at', '>=', $date)->where('created_at', '<=', $nextDay->addDay())->count();
            $data[] = $items;
        }
        foreach ($dates as $date) {
            $nextDay = clone($date);
            $items = Event::where('updated_at', '>=', $date)->where('updated_at', '<=', $nextDay->addDay())->count();
            $updates[] = $items;
        }
        $totalEvents = Event::all()->count();
        $unpublished = Event::where('published', '=', false)->count();
        $uncategorized = Event::whereRaw(['categories' => ['$size' => 0]])->count();
        $uncategorizedOfTotal = (int) (($uncategorized / $totalEvents) * 100);
        $unpublishedOfTotal = (int) (($unpublished / $totalEvents) * 100);
        $keys = json_encode($keys);
        $data = json_encode($data);
        $updates = json_encode($updates);

        $params = compact('keys', 'data', 'totalEvents', 'unpublished', 'unpublishedOfTotal', 'uncategorized',
            'uncategorizedOfTotal', 'updates');

        return view('admin.events.stats', $params);
    }

    public function test()
    {
        $dm = \App::make('dm');
        $location  = new Location(12.173, 100.123);
        $reference = $dm->find('App\Entities\Reference', '57972635fa84570624595962');
        $test = $dm->find('App\Entities\Test', '57971ebcfa84570624595961');
        $dm->persist($reference);
        $reference->getReferences()->removeElement($test);
        $dm->flush();
        $references = $reference->getReferences()->toArray();
        
        dd($references);
//        $this->dispatch(new GoogleVenues(13.754474, 100.4685397));
    }

    public function scrapExisting()
    {
        $events = Event::where('start_at', '>=', Carbon::now()->subMonth())->get();
        foreach ($events as $event) {
            if($event->id_on_source) {
                $this->dispatch(new GetPageInfo(PageType::EVENT, $event->id_on_source, $event->_id));
            }
        }

        return redirect()->back();
    }

    public function scrapText(Request $request)
    {
        $items = stripcslashes($request->get('idList'));
        $items = str_replace('\n', '', $items);
        $items = str_replace('\r', '', $items);
        $items = str_replace('\r\n', '', $items);
        $items = explode(',', $items);
        foreach ($items as &$item) {
            $item = trim($item);
            $this->dispatch(new GetPageInfo(PageType::EVENT, $item, null));
        }
        return redirect()->back();
    }

    public function removePassed()
    {
        $events = Event::where('start_at', '<=', Carbon::now()->subMonth(2))->get();
        foreach ($events as $event) {
            $event->delete();
        }

        return redirect()->back();
    }

    public function scraping()
    {
        $login_url = $this->fb->getLoginUrl(['email']);

        return view('admin.events.scraping', ['login_url' => $login_url]);
    }
}
