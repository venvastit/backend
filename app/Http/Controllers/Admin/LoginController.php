<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/17/16
 * Time: 6:34 AM.
 */
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;

class LoginController extends Controller
{
    /**
     * @param Request $request
     */
    public function index(Request $request)
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $user = \Auth::user();
        if ($user && intval($user->admin) === 1) {
            return redirect(route('admin.dashboard'));
        }
        $attempt = \Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')], true);
        $errors = new MessageBag();
        if ($attempt) {
            /** @var User $user */
            $user = \Auth::user();
            if (intval($user->admin) === 1) {
                return redirect(route('admin.dashboard'));
            } else {
                $errors->add('permission', 'Permission denied');
            }
        } else {
            $errors->add('email', 'Wrong email or password!');
        }

        return redirect()->back()->withErrors($errors);
    }
}
