<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/25/16
 * Time: 7:37 PM.
 */
namespace App\Http\Controllers\Admin;

use App\Dictionary\CategorySource;
use App\Dictionary\CategoryType;
use App\Models\Category;
use App\Repositories\Category as CategoryRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class CategoriesController extends BaseAdminController
{
    public function __construct(CategoryRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * @return mixed
     */
    public function modelClass()
    {
        return Category::class;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $all = Category::sourceVenVast()->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.categories.list'));

        return view('admin.categories.all', ['dataUrl' => route('admin.categories.list'), 'header' => 'All VenVast categories']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function event(Request $request)
    {
        if ($request->ajax()) {
            $all = Category::typeEvent()->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.categories.event'));

        return view('admin.categories.all', ['dataUrl' => route('admin.categories.event'), 'header' => 'VenVast event categories']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function venue(Request $request)
    {
        if ($request->ajax()) {
            $all = Category::typeVenue()->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.categories.venue'));

        return view('admin.categories.all', ['dataUrl' => route('admin.categories.venue'), 'header' => 'VenVast venue categories']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function deal(Request $request)
    {
        if ($request->ajax()) {
            $all = Category::typeDeal()->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.categories.deal'));

        return view('admin.categories.all', ['dataUrl' => route('admin.categories.deal'), 'header' => 'VenVast deal categories']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function facebook(Request $request)
    {
        if ($request->ajax()) {
            $all = Category::sourceFacebook()->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.categories.facebook'));

        return view('admin.categories.all', ['dataUrl' => route('admin.categories.facebook'), 'header' => 'VenVast facebook categories']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function google(Request $request)
    {
        if ($request->ajax()) {
            $all = Category::sourceGoogle()->get();

            return Datatables::of($all)->make(true);
        }
        \Session::set('list_url', route('admin.categories.google'));

        return view('admin.categories.all', ['dataUrl' => route('admin.categories.google'), 'header' => 'VenVast google categories']);
    }

    public function create()
    {
        $venvastCategories = Category::sourceVenVast()->get();
        $category = new Category();
        $types = CategoryType::lists();
        $sources = CategorySource::lists();

        return view('admin.categories.form', compact('venvastCategories', 'category', 'types', 'sources'));
    }

    public function edit(Request $request, $id)
    {
        $venvastCategories = Category::sourceVenVast()->get();
        $category = Category::find($id);
        $types = CategoryType::lists();
        $sources = CategorySource::lists();

        return view('admin.categories.form', compact('venvastCategories', 'category', 'types', 'sources'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (empty($data['source'])) {
            unset($data['source']);
        }
        if (empty($data['type'])) {
            unset($data['type']);
        }
        unset($data['_id']);
        $data['priority'] = (int) $data['priority'];
        if ($request->has('_id')) {
            /** @var Category $category */
            $category = Category::find($request->get('_id'));
            if ($category) {
                $category->setRawAttributes($data);
            }
        } else {
            $category = new Category();
            $category->setRawAttributes($data);
        }
        if ($request->hasFile('image')) {
            $category->setImage('image', $request->file('image'));
        }
        $category->save();
        if (\Session::has('list_url')) {
            return redirect()->to(\Session::get('list_url'));
        }

        return redirect()->back();
    }
}
