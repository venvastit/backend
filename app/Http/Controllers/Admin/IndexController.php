<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 4/17/16
 * Time: 10:01 PM.
 */
namespace App\Http\Controllers\Admin;

use App\Jobs\MigrateFromRedisToSQL;
use App\Models\Event;
use App\ServiceProviders\FacebookServiceProvider;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Redis;
use View;

class IndexController extends Controller
{
    use DispatchesJobs;

    protected $fb;
    protected $fbServiceProvider;

    public function __construct(LaravelFacebookSdk $fb)
    {
        $this->fb = $fb;
        $this->fbServiceProvider = new FacebookServiceProvider(\Session::get('fb_user_access_token'));
    }

    public function getIndex()
    {
        $login_url = $this->fb->getLoginUrl(['email']);

        return View::make('admin.index', ['login_url' => $login_url]);
    }

    public function saveFbEvents(Request $request)
    {
        $notAuthenticated = $this->notAuthenticatedResponse();
        if ($notAuthenticated !== false) {
            return $notAuthenticated;
        }
        $fbIds = $request->get('fb_ids');
        foreach ($fbIds as $fbId) {
            if (!empty($fbId)) {
                $existing = $this->checkExistingEvent($fbId);
                if ($existing !== false) {
                    continue;
                }

                $eventData = $this->getEventFromFacebook($fbId);
                $this->saveEvent($eventData);
            }
        }

        return back();
    }

    public function migrateToSql()
    {
        \Artisan::call('scrapper:migrate', ['--access-token' => $this->fbServiceProvider->getAccessToken()]);

        return back();
    }

    private function checkExistingEvent($eventId)
    {
        $event = Event::where('original_id', $eventId)->get()->first();
        if ($event) {
            return true;
        } else {
            return false;
        }
    }

    private function getEventFromFacebook($eventId)
    {
        return $this->fbServiceProvider->getEvent($eventId);
    }

    private function saveEvent($fbEvent)
    {
        Redis::connection()->set('facebook:events:'.$fbEvent['id'], json_encode($fbEvent));
        if (array_key_exists('place', $fbEvent) && array_key_exists('id', $fbEvent['place'])) {
            $fbEvent['venue'] = $this->fbServiceProvider->getVenue($fbEvent['place']['id']);
            Redis::connection()->set('facebook:venues:'.$fbEvent['place']['id'], json_encode($fbEvent['venue']));
        }
    }

    public function notAuthenticatedResponse()
    {
        if (!\AdminAuth::user()) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'auth' => 'You are not allowed to perform this action',
                ],
            ]);
        } else {
            return false;
        }
    }
}
