<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/17/16
 * Time: 10:05 PM.
 */
namespace App\Http\Controllers\Admin;

use App\Dictionary\CategoryType;
use App\Dictionary\Scraping\Facebook\PageType;
use App\Jobs\Scraping\Facebook\GetPageInfo;
use App\Models\Category;
use App\Models\Event;
use App\Models\Venue;
use App\Repositories\Criteria\Common\HasOrganizer;
use App\Repositories\Criteria\Venue\WantToHost;
use App\Repositories\Criteria\Venue\WithEvents;
use App\Repositories\PostCriteria\Venue\WithEvents as WEventsPost;
use App\Repositories\PostCriteria\Venue\WithUpcomingEvents;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MongoDB\BSON\ObjectID;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Yajra\Datatables\Datatables;
use App\Repositories\Venue as VenueRepository;

class VenuesController extends BaseAdminController
{
    /**
     * @var LaravelFacebookSdk
     */
    protected $fb;

    public function __construct(VenueRepository $repository, LaravelFacebookSdk $fb)
    {
        parent::__construct($repository);
        $this->fb = $fb;
    }

    public function modelClass()
    {
        return Venue::class;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $this->getRepo()->pushCriteria(new WithEvents());

            return Datatables::of($this->getRepo()->applyCriteria()->all())->make(true);
        }
        \Session::set('list_url', route('admin.venues.list'));

        return view('admin.venues.all', ['datUrl' => route('admin.venues.list'), 'header' => 'All venues']);
    }

    public function published(Request $request)
    {
        if ($request->ajax()) {
            $this->getRepo()->pushCriteria(new WithEvents());
            $modelCandidate = $this->getRepo()->getModel()->where('published', '=', true);
            $this->getRepo()->setModel($modelCandidate);

            return Datatables::of($this->getRepo()->applyCriteria()->all())->make(true);
        }
        \Session::set('list_url', route('admin.venues.published'));

        return view('admin.venues.all', ['datUrl' => route('admin.venues.published'), 'header' => 'Published venues']);
    }

    public function unpublished(Request $request)
    {
        if ($request->ajax()) {
            $this->getRepo()->pushCriteria(new WithEvents());
            $modelCandidate = $this->getRepo()->getModel()->where('published', '=', false);
            $this->getRepo()->setModel($modelCandidate);

            return Datatables::of($this->getRepo()->applyCriteria()->all())->make(true);
        }
        \Session::set('list_url', route('admin.venues.unpublished'));

        return view('admin.venues.all', ['datUrl' => route('admin.venues.unpublished'), 'header' => 'Unpublished venues']);
    }

    public function withUpcomingEvents(Request $request)
    {
        if ($request->ajax()) {
            $this->getRepo()->pushCriteria(new WithEvents());
            $this->getRepo()->applyCriteria();

            return Datatables::of($this->getRepo()->applyPostCriteria(new WithUpcomingEvents()))->make(true);
        }
        \Session::set('list_url', route('admin.venues.upcoming_events'));

        return view('admin.venues.all', ['datUrl' => route('admin.venues.upcoming_events'), 'header' => 'Venues with upcoming events']);
    }

    public function noUpcomingEvents(Request $request)
    {
        if ($request->ajax()) {
            $this->getRepo()->pushCriteria(new WithEvents());
            $this->getRepo()->applyCriteria();

            return Datatables::of($this->getRepo()->applyPostCriteria(new WithUpcomingEvents(true)))->make(true);
        }
        \Session::set('list_url', route('admin.venues.no_upcoming_events'));

        return view('admin.venues.all', ['datUrl' => route('admin.venues.no_upcoming_events'), 'header' => 'Venues without upcoming events']);
    }

    public function registered(Request $request)
    {
        if ($request->ajax()) {
            $this->getRepo()->pushCriteria(new WithEvents());
            $this->getRepo()->pushCriteria(new HasOrganizer());

            return Datatables::of($this->getRepo()->applyCriteria()->all())->make(true);
        }
        \Session::set('list_url', route('admin.venues.registered'));

        return view('admin.venues.all', ['datUrl' => route('admin.venues.registered'), 'header' => 'Venues claimed by real users']);
    }

    public function hosts(Request $request)
    {
        if ($request->ajax()) {
            $this->getRepo()->pushCriteria(new WantToHost());

            return Datatables::of($this->getRepo()->applyCriteria()->all())->make(true);
        }
        \Session::set('list_url', route('admin.venues.hosts'));

        return view('admin.venues.all', ['datUrl' => route('admin.hosts'), 'header' => 'Hosts']);
    }

    public function edit(Request $request, $id)
    {
        $categories = Category::where('type', CategoryType::TYPE_VENUE)->get()->all();
        $eventCategories = Category::where('type', CategoryType::TYPE_EVENT)->get()->all();
        $venue = Venue::where('_id', '=', $id)->get()->first();
        $businessHours = $this->createBusinessHoursTemplate();
        $dayLabels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        $events = [];
        if (!$venue->address) {
            $venue->address = $venue->name.', '.$venue->street.', '.$venue->zip;
        }
        $position = $venue->location['coordinates'];
        if ($venue->businessHours) {
            $businessHours = $venue->businessHours;
        } else {
            $businessHours = $this->createBusinessHoursTemplate();
        }

        $users = User::where('type', 1)->get()->all();

        return view('admin.venues.form', compact('events', 'venue', 'businessHours',
            'categories', 'users', 'eventCategories', 'dayLabels', 'position'
        ));
    }

    public function create(Request $request)
    {
        $categories = Category::where('type', CategoryType::TYPE_VENUE)->get()->all();
        $eventCategories = Category::where('type', CategoryType::TYPE_EVENT)->get()->all();
        $venue = new Venue();
        $businessHours = $this->createBusinessHoursTemplate();
        $dayLabels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        $events = [];
        $users = User::where('type', 1)->get()->all();

        return view('admin.venues.form', compact('events', 'venue', 'businessHours',
            'categories', 'users', 'eventCategories', 'dayLabels'
        ));
    }

    public function store(Request $request)
    {
        if (!$request->get('_id')) {
            /** @var Venue $venue */
            $venue = new Venue();
        } else {
            /** @var Venue $venue */
            $venue = Venue::find($request->get('_id'));
        }

        $skipKeys = ['organizers', 'businessHoursOpen', 'businessHoursClose', 'category'];
        $attributes = $request->all();
        unset($attributes['_id']);
        foreach ($attributes as $key => &$value) {
            if (is_string($value) && strlen($value) === 0) {
                $value = null;
            }
            if ($value === 'on') {
                $value = true;
            }
            if ($value === 'off') {
                $value = false;
            }

            if ($value !== null && !in_array($key, $skipKeys)) {
                $venue->setAttribute($key, $value);
            }
        }
        $organizer = User::find($attributes['organizers'][0]);
        if ($organizer) {
            if (!is_array($venue->organizers)) {
                $venue->setOrganizers([]);
            }
            if (!in_array($organizer->_id, $venue->organizers)) {
                $venue->setOrganizers(array_merge($venue->organizers, [$organizer->_id]));
            }
        }

//        $venue->categories = [$attributes['category']];
//        $venue->organizers = [$attributes['organizer']];

        $venue->businessHours = $this->businessHoursFromForm($attributes);
        $venue->setLocation($venue->location);
        $venue->save();
        if ($request->hasFile('cover_image')) {
            $venue->setImage('cover_image', $request->get('cover_image'));
        }
        if (\Session::has('list_url')) {
            return redirect()->to(\Session::get('list_url'));
        }

        return redirect()->route('admin.venues.list');
    }

    protected function businessHoursFromForm($attributes)
    {
        $businessHours = $attributes['businessHours'];
        $open = $attributes['businessHoursOpen'];
        $close = $attributes['businessHoursClose'];
        $finalArray = [];
        for ($i = 0; $i < 7; $i++) {
            if (in_array((string) $i, $businessHours)) {
                if (!empty($open[$i])) {
                    $finalArray[$i] = [
                        'day' => $i,
                        'open' => [
                            'day' => $i,
                            'time' => $open[$i],
                        ],
                    ];
                } else {
                    $finalArray[$i] = [
                        'day' => $i,
                        'open' => false,
                    ];
                    continue;
                }
                if (!empty($close[$i])) {
                    $finalArray[$i]['close'] = [
                        'day' => $i,
                        'time' => $open[$i],
                    ];
                }
            } else {
                $finalArray[$i] = [
                    'day' => $i,
                    'open' => false,
                ];
            }
        }

        return $finalArray;
    }

    protected function createBusinessHoursTemplate()
    {
        $businessHours = [];
        for ($i = 0; $i < 7; $i++) {
            if ($i !== 0 && $i !== 6) {
                $businessHours[] = [
                    'day' => $i,
                    'open' => [
                        'time' => '0900',
                        'day' => $i,
                    ],
                    'close' => [
                        'time' => '2000',
                        'day' => $i,
                    ],
                ];
            } else {
                $businessHours[] = [
                    'day' => $i,
                    'open' => false,
                    'close' => false,
                ];
            }
        }

        return $businessHours;
    }

    public function stats(Request $request)
    {
        $date = Carbon::now()->setTime(0, 0);
        /** @var Carbon[] $dates */
        $dates = [$date];
        $keys = [$date->format('d.m.Y')];
        $data = [];
        $updates = [];

        for ($i = 1; $i < 7; $i++) {
            $d = clone($dates[$i - 1]);
            $d->subDay();
            $dates[] = $d;
            $keys[] = $d->format('d.m.Y');
        }

        foreach ($dates as $date) {
            $nextDay = clone($date);
            $items = Venue::where('created_at', '>=', $date)->where('created_at', '<=', $nextDay->addDay())->count();
            $data[] = $items;
        }
        foreach ($dates as $date) {
            $nextDay = clone($date);
            $items = Venue::where('updated_at', '>=', $date)->where('updated_at', '<=', $nextDay->addDay())->count();
            $updates[] = $items;
        }

        $this->getRepo()->clearCriteria();
        $this->getRepo()->pushCriteria(new WithEvents());
        $all = $this->getRepo()->applyCriteria()->all();

        $withUpcomingEventsCriteria = new WithUpcomingEvents();
        $withEventsPostCriteria = new WEventsPost();

        $totalVenues = Venue::all()->count();
        $venuesRegistered = $this->getRepo()->pushCriteria(new HasOrganizer())->applyCriteria()->count();
        $venuesUnregistered = $totalVenues - $venuesRegistered;
        $withUpcomingEvents = $withUpcomingEventsCriteria->apply($all)->count();
        $withoutUpcomingEvents = $totalVenues - $withUpcomingEvents;
        $withEvents = $withEventsPostCriteria->apply($all)->count();
        $withoutEvents = $totalVenues - $withEvents;
        $contacted = Venue::where('contacted', '=', true)->count();
        $published = Venue::where('published', '=', true)->count();
        $unpublished = $totalVenues - $published;

        return view('admin.venues.stats', [
            'total' => $totalVenues,
            'registered' => $venuesRegistered,
            'registeredPercent' => (int) (($venuesRegistered / $totalVenues) * 100),
            'unregistered' => $venuesUnregistered,
            'unregisteredPercent' => (int) (($venuesUnregistered / $totalVenues) * 100),
            'upcoming' => $withUpcomingEvents,
            'upcomingPercent' => (int) (($withUpcomingEvents / $totalVenues) * 100),
            'noUpcoming' => $withoutUpcomingEvents,
            'noUpcomingPercent' => (int) (($withoutUpcomingEvents / $totalVenues) * 100),
            'events' => $withEvents,
            'eventsPercent' => (int) (($withEvents / $totalVenues) * 100),
            'noEvents' => $withoutEvents,
            'noEventsPercent' => (int) (($withoutEvents / $totalVenues) * 100),
            'contacted' => $contacted,
            'contactedPercent' => (int) (($contacted / $totalVenues) * 100),
            'published' => $published,
            'publishedPercent' => (int) (($published / $totalVenues) * 100),
            'unpublished' => $unpublished,
            'unpublishedPercent' => (int) (($unpublished / $totalVenues) * 100),
            'data' => json_encode($data),
            'keys' => json_encode($keys),
            'updates' => json_encode($updates),
        ]);
    }

    public function scraping()
    {
        $login_url = $this->fb->getLoginUrl(['email']);

        return view('admin.venues.scraping', ['login_url' => $login_url]);
    }

    public function scrapExisting()
    {
        $venues = Venue::all();
        foreach ($venues as $venue) {
            if ($venue->id_on_source && $venue->id_on_source !== null) {
                $this->dispatch(new GetPageInfo(PageType::VENUE, $venue->id_on_source, $venue->_id));
            }
        }

        return redirect()->back();
    }

    public function scrapPublished()
    {
        $venues = Venue::where('published', '=', true)->get();
        foreach ($venues as $venue) {
            $this->dispatch(new GetPageInfo(PageType::VENUE, $venue->id_on_source, $venue->_id));
        }

        return redirect()->back();
    }

    public function scrapUnpublished()
    {
        $venues = Venue::where('published', '=', false)->get();
        foreach ($venues as $venue) {
            $this->dispatch(new GetPageInfo(PageType::VENUE, $venue->id_on_source, $venue->_id));
        }

        return redirect()->back();
    }

    public function scrapText(Request $request)
    {
        $items = $request->get('idList');
        $items = explode(',', $items);
        foreach ($items as &$item) {
            $item = trim($item);
            $this->dispatch(new GetPageInfo(PageType::VENUE, $item, null));
        }

        return redirect()->back();
    }

    public function fixEventVenueRelation()
    {
        $all = Event::all();
        $count = 0;
        $errors = 0;
        foreach ($all as $event) {
            if ($event->venue_id instanceof ObjectID) {
                $event->venue_id = $event->venue_id->__toString();
                $event->save();
                $count++;
            } else {
                if (is_numeric($event->venue_id)) {
                    $venue = Venue::where('old_id', $event->venue_id)->first();
                    if (!$venue) {
                        $errors++;
                        $event->delete();
                    } else {
                        $event->venue_id = $venue->_id;
                        $event->save();
                        $count++;
                    }
                }
            }
        }
    }
}
