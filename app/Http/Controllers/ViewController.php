<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ViewController extends Controller
{
    public function testView($viewName)
    {
        return view('dev.'.$viewName, ['viewName' => $viewName]);
    }

    public function test(Request $request)
    {
        $files = \File::files(base_path().'/resources/views/dev');
        foreach ($files as &$file) {
            $file = str_replace(base_path().'/resources/views/dev/', '', $file);
        }

        return new JsonResponse($files);
    }
}
