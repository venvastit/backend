<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/29/16
 * Time: 11:01 AM.
 */
namespace App\Http\Requests;

class EventRequest extends BaseApiRequest
{
    /**
     * Returns requested Category.
     * @return string|null
     */
    public function getCategory()
    {
        return $this->get('category');
    }

    /**
     * Returns requested location.
     *
     * @return string|null
     */
    public function getWhere()
    {
        return $this->get('where');
    }

    public function getDate()
    {
        return $this->get('date');
    }

    /**
     * return date range filters(date_from, date_to)
     *
     * @return array
     */
    public function getDateRange() : array
    {
        $dates = [];
        if($this->get('date_from') !== null) {
            $dates['from'] = $this->get('date_from');
        }
        if($this->get('date_to') !== null) {
            $dates['to'] = $this->get('date_to');
        }
        return $dates;
    }
}
