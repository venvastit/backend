<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/1/16
 * Time: 12:57 PM.
 */
namespace App\Http\Requests;

class VenueRequest extends BaseApiRequest
{
    /**
     * Returns requested Category.
     * @return string|null
     */
    public function getCategory()
    {
        return $this->get('category');
    }
}
