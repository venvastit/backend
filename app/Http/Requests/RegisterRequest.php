<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/17/16
 * Time: 10:11 AM.
 */
namespace App\Http\Requests;

class RegisterRequest extends Request
{
    public function authorize() : bool
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required_with:password|email',
            'password' => 'required_with:email',
//            'fb_token' => 'required_without:email,password,google_token',
//            'google_token' => 'required_without:email,password,fb_token',
        ];
    }

    public function hasCredentials()
    {
        return $this->get('email') && $this->get('password');
    }

    public function getCredentials()
    {
        return ['email' => $this->getEmail(), 'password' => $this->getPassword()];
    }

    public function getEmail()
    {
        return $this->get('email');
    }

    public function getFbToken()
    {
        return $this->get('fb_token');
    }

    public function getGoogleToken()
    {
        return $this->get('fb_token');
    }

    public function getPassword()
    {
        return $this->get('password');
    }
}
