<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/29/16
 * Time: 10:26 AM.
 */
namespace App\Http\Requests;

use App\Models\Location;

abstract class  BaseApiRequest extends Request
{
    public function authorize() : bool
    {
        return true;
    }

    public function getOrder()
    {
        return $this->get('order');
    }

    /**
     * TODO: remove latitude mock
     * TODO: location search with Google autocomplete.
     * @return float|mixed
     */
    public function getLat()
    {
        if ($this->get('latitude')) {
            return (float) number_format($this->get('latitude'), 4, '.', '');
        }

        return;
    }

    /**
     * TODO: remove longitude mock
     * TODO: location search with Google autocomplete.
     * @return float|mixed
     */
    public function getLng()
    {
        if ($this->get('longitude')) {
            return (float) number_format($this->get('longitude'), 4, '.', '');
        }

        return;
    }

    public function getRadius()
    {
        return (float) $this->get('radius', 25);
    }

    public function getPage()
    {
        if ($this->get('page') !== null) {
            return ((int) $this->get('page')) + 1;
        }

        return;
    }

    public function isWebCall() : bool
    {
        if ($this->getPage() !== null || $this->get('web') !== null) {
            return true;
        }

        return false;
    }

  public function isMarkerRequest() : bool
  {
      return $this->get('markers') === 'true';
  }

    public function isMobileCall() : bool
    {
        if ($this->get('mobile') !== null) {
            return true;
        }

        return !$this->isWebCall();
    }

    public function getWhen()
    {
        $when = $this->get('when', null);

        return $when;
    }

    public function rules()
    {
        return [];
    }

    public function getText()
    {
        return $this->get('text');
    }


    public function isTypeAhed()
    {
        return $this->get('type_ahead') === 'true';
    }

    public function getOrganizer()
    {
        $organizer = $this->get('organizer');
        if($organizer !== null) {
            return $organizer === 'true';
        }
        return $organizer;
    }
}
