<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/29/16
 * Time: 11:42 PM
 */

namespace App\Http\Requests\User;


use App\Http\Requests\Request;

class Activate extends Request
{

    public function authorize() : bool
    {
        return true;
    }
    
    
    public function rules()
    {
        return [
            'email' => 'email|required',
            'activation_code' => 'required'
        ];
    }

}