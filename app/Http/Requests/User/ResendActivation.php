<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/30/16
 * Time: 12:22 AM
 */

namespace App\Http\Requests\User;


use App\Http\Requests\Request;

class ResendActivation extends Request
{

    public function authorize() : bool
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }

}