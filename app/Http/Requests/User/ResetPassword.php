<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/30/16
 * Time: 12:07 AM
 */

namespace App\Http\Requests\User;


use App\Http\Requests\Request;

class ResetPassword extends Request
{

    public function authorize() : bool
    {
        return true;
    }


    public function rules()
    {
        return [
            'email' => 'email|required',
        ];
    }

}