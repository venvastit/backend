<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/29/16
 * Time: 10:25 AM.
 */
namespace App\Http\Requests;

use App\Dictionary\CategoryType;
use App\Models\Category;

class CategoryRequest extends BaseApiRequest
{
    /**
     * Return integer representation of category type requested
     * If not specified, returns Event type code.
     * @return int
     */
    public function getType() : int
    {
        return CategoryType::getByAlias($this->get('type', 'event'));
    }
}
