<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/17/16
 * Time: 9:12 AM.
 */
namespace App\Http\Requests;

use App\ServiceProviders\FacebookServiceProvider;
use App\User;
use Illuminate\Http\JsonResponse;

class LoginFacebookRequest extends Request
{
    public function authorize() : bool
    {
        return true;
    }

    public function rules()
    {
        \Validator::extend('fb_token_valid', function ($attribute, $value, $parameters) {
            $fbService = new FacebookServiceProvider($value);

            return $fbService->checkAccessToken();
        });

        return [
            'fb_access_token' => 'required|fb_token_valid',
        ];
    }

    public function hasCredentials()
    {
        return $this->get('email') && $this->get('password');
    }

    public function getCredentials()
    {
        return ['email' => $this->getEmail(), 'password' => $this->getPassword()];
    }

    public function getToken()
    {
        return $this->get('fb_access_token');
    }

    public function getEmail()
    {
        return $this->get('email');
    }

    public function getPassword()
    {
        return $this->get('password');
    }

    /**
     * @return bool
     */
    public function isTokenValid() : bool
    {
        $fbService = new FacebookServiceProvider($this->getToken());

        return $fbService->checkAccessToken();
    }

    public function getFbId()
    {
        return $this->get('fb_id');
    }

    public function userNotExistsResponse()
    {
        return new JsonResponse([
            'error' => [
                'user' => 'User not found',
            ],
            'success' => false,
        ]);
    }

    public function successResponse(User $user)
    {
        return new JsonResponse([
            'success' => true,
            'errors' => false,
            'user' => $user,
        ]);
    }

    public function getType()
    {
        return intval($this->get('type', 0));
    }

    public function getGender()
    {
        return strtolower($this->get('gender')) === 'male' ? 1 : 0;
    }
}
