<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/17/16
 * Time: 6:37 AM.
 */
namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class LoginRequest extends Request
{
    public function authorize() : bool
    {
        return $this->get('email') && $this->get('password');
    }

    public function rules()
    {
        return [

        ];
    }

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->get('email');
    }

    /**
     * @return null|string
     */
    public function getPassword()
    {
        return $this->get('password');
    }
}
