<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['domain' => 'mobile.' . config('app.url')], function () {
    Route::get('/', function (\App\Repositories\Event $repo, \App\Repositories\Venue $venueRepo) {
        $bangkokWeek = $repo->pushCriteria(new \App\Repositories\Criteria\Event\When('week'))
            ->pushCriteria(new \App\Repositories\Criteria\Common\ByCoords(13.71, 100.58, 25.0))->applyCriteria()->count();
        $repo->clearCriteria();
        $repo->makeModel();
        $allCount = $repo->applyCriteria()->all()->count();
        $upcomingApproxCount = number_format($allCount - (($allCount / 100) * 20));
        $bkkVenues = $venueRepo->pushCriteria(new \App\Repositories\Criteria\Common\ByCoords(13.71, 100.58, 25.0))->applyCriteria()->count();

        $repo->clearCriteria();
        $repo->makeModel();
        $nextStarts = $repo->pushCriteria(new \App\Repositories\Criteria\Common\OrderBy('start_at', 'asc'))
            ->pushCriteria(new \App\Repositories\Criteria\Event\Upcoming())->applyCriteria()->all()->first();

        return view('landing.mobile.index')->with(['week' => $bangkokWeek, 'year' => $upcomingApproxCount, 'venues' => $bkkVenues, 'next' => $nextStarts->start_at->diffInMinutes(\Carbon\Carbon::now())]);
    });
});


Route::group(['domain' => 'app.' . config('app.url')], function () {
    
});

Route::any('/view/{viewName}', ['as' => 'view.test', 'uses' => 'ViewController@testView']);
Route::any('/test', ['as' => 'test', 'uses' => 'ViewController@test']);

Route::any('/', 'AngularController@serveApp');

Route::any('/drop_categories-x-163', function () {
    \App\Models\Category::where('source_id', 1)->delete();
});

Route::group(['prefix' => 'api'], function () {
    Route::post('authenticate', 'AuthenticateController@authenticate');
});

Route::group(['prefix' => 'subscribe'], function () {
    Route::post('/', ['as' => 'subscribe', 'uses' => 'SubscriptionController@subscribe']);
    Route::post('/unsubscribe', ['as' => 'unsubscribe', 'uses' => 'SubscriptionController@unsubscribe']);
    Route::post('/contact', ['as' => 'contact', 'uses' => 'SubscriptionController@contact']);
});

Route::any('/!/event-{id}', function ($id) {
    return redirect('/!#event-'.$id);
});

Route::group(['prefix' => 'admin'], function () {

    Route::group(['prefix' => 'events', 'middleware' => 'admin'], function () {
        Route::get('/', ['as' => 'admin.events.list', 'uses' => 'Admin\EventsController@index']);
        Route::get('delete/{id}', ['as' => 'admin.events.delete', 'uses' => 'Admin\EventsController@delete']);
        Route::get('published', ['as' => 'admin.events.published', 'uses' => 'Admin\EventsController@published']);
        Route::get('publish/{id}', ['as' => 'admin.events.publish', 'uses' => 'Admin\EventsController@publish']);
        Route::get('unpublished', ['as' => 'admin.events.unpublished', 'uses' => 'Admin\EventsController@unpublished']);
        Route::get('unpublish/{id}', ['as' => 'admin.events.unpublish', 'uses' => 'Admin\EventsController@unpublish']);
        Route::get('create', ['as' => 'admin.events.create', 'uses' => 'Admin\EventsController@create']);
        Route::post('store', ['as' => 'admin.events.store', 'uses' => 'Admin\EventsController@store']);
        Route::get('stats', ['as' => 'admin.events.stats', 'uses' => 'Admin\EventsController@stats']);
        Route::get('upcoming', ['as' => 'admin.events.upcoming', 'uses' => 'Admin\EventsController@upcoming']);
        Route::get('passed', ['as' => 'admin.events.passed', 'uses' => 'Admin\EventsController@passed']);
        Route::get('passed/remove', ['as' => 'admin.events.passed.remove', 'uses' => 'Admin\EventsController@removePassed']);
        Route::get('test', ['as' => 'admin.events.test', 'uses' => 'Admin\EventsController@test']);
        Route::get('scraping', ['as' => 'admin.events.scraping', 'uses' => 'Admin\EventsController@scraping']);
        Route::post('scraping/text', ['as' => 'admin.events.scraping.text', 'uses' => 'Admin\EventsController@scrapText']);
        Route::any('scraping/existing', ['as' => 'admin.events.scraping.existing', 'uses' => 'Admin\EventsController@scrapExisting']);
        Route::get('/{id}', ['as' => 'admin.events.edit', 'uses' => 'Admin\EventsController@edit']);
    });

    Route::group(['prefix' => 'venues', 'middleware' => 'admin'], function () {
        Route::get('/', ['as' => 'admin.venues.list', 'uses' => 'Admin\VenuesController@index']);
        Route::get('upcoming_events', ['as' => 'admin.venues.upcoming_events', 'uses' => 'Admin\VenuesController@withUpcomingEvents']);
        Route::get('no_upcoming_events', ['as' => 'admin.venues.no_upcoming_events', 'uses' => 'Admin\VenuesController@noUpcomingEvents']);
        Route::get('registered', ['as' => 'admin.venues.registered', 'uses' => 'Admin\VenuesController@registered']);
        Route::get('delete/{id}', ['as' => 'admin.venues.delete', 'uses' => 'Admin\VenuesController@delete']);
        Route::get('published', ['as' => 'admin.venues.published', 'uses' => 'Admin\VenuesController@published']);
        Route::get('publish/{id}', ['as' => 'admin.venues.publish', 'uses' => 'Admin\VenuesController@publish']);
        Route::get('unpublished', ['as' => 'admin.venues.unpublished', 'uses' => 'Admin\VenuesController@unpublished']);
        Route::get('unpublish/{id}', ['as' => 'admin.venues.unpublish', 'uses' => 'Admin\VenuesController@unpublish']);
        Route::get('create', ['as' => 'admin.venues.create', 'uses' => 'Admin\VenuesController@create']);
        Route::post('store', ['as' => 'admin.venues.store', 'uses' => 'Admin\VenuesController@store']);
        Route::get('stats', ['as' => 'admin.venues.stats', 'uses' => 'Admin\VenuesController@stats']);
        Route::get('fix', ['as' => 'admin.venues.fix', 'uses' => 'Admin\VenuesController@fixEventVenueRelation']);
        Route::get('scraping', ['as' => 'admin.venues.scraping', 'uses' => 'Admin\VenuesController@scraping']);
        Route::post('scraping/text', ['as' => 'admin.venues.scraping.text', 'uses' => 'Admin\VenuesController@scrapText']);
        Route::any('scraping/existing', ['as' => 'admin.venues.scraping.existing', 'uses' => 'Admin\VenuesController@scrapExisting']);
        Route::any('scraping/published', ['as' => 'admin.venues.scraping.published', 'uses' => 'Admin\VenuesController@scrapPublished']);
        Route::any('scraping/unpublished', ['as' => 'admin.venues.scraping.unpublished', 'uses' => 'Admin\VenuesController@scrapUnpublished']);
        Route::get('/{id}', ['as' => 'admin.venues.edit', 'uses' => 'Admin\VenuesController@edit']);
    });

    Route::group(['prefix' => 'users', 'middleware' => 'admin'], function () {
        Route::get('/', ['as' => 'admin.users.list', 'uses' => 'Admin\UsersController@index']);
        Route::get('delete/{id}', ['as' => 'admin.users.delete', 'uses' => 'Admin\UsersController@delete']);
        Route::get('create', ['as' => 'admin.users.create', 'uses' => 'Admin\UsersController@create']);
        Route::get('stats', ['as' => 'admin.users.stats', 'uses' => 'Admin\UsersController@stats']);
        Route::get('attendees', ['as' => 'admin.users.attendees', 'uses' => 'Admin\UsersController@attendees']);
        Route::get('org', ['as' => 'admin.users.organizers', 'uses' => 'Admin\UsersController@organizers']);
        Route::post('store', ['as' => 'admin.users.store', 'uses' => 'Admin\UsersController@store']);
        Route::get('admins', ['as' => 'admin.users.admins', 'uses' => 'Admin\UsersController@admins']);
        Route::get('/{id}', ['as' => 'admin.users.edit', 'uses' => 'Admin\UsersController@edit']);
    });

    Route::group(['prefix' => 'categories', 'middleware' => 'admin'], function () {
        Route::get('/', ['as' => 'admin.categories.list', 'uses' => 'Admin\CategoriesController@index']);
        Route::get('event', ['as' => 'admin.categories.event', 'uses' => 'Admin\CategoriesController@event']);
        Route::get('venue', ['as' => 'admin.categories.venue', 'uses' => 'Admin\CategoriesController@venue']);
        Route::get('deal', ['as' => 'admin.categories.deal', 'uses' => 'Admin\CategoriesController@deal']);
        Route::get('facebook', ['as' => 'admin.categories.facebook', 'uses' => 'Admin\CategoriesController@facebook']);
        Route::get('google', ['as' => 'admin.categories.google', 'uses' => 'Admin\CategoriesController@google']);

        Route::get('delete/{id}', ['as' => 'admin.categories.delete', 'uses' => 'Admin\CategoriesController@delete']);
        Route::get('create', ['as' => 'admin.categories.create', 'uses' => 'Admin\CategoriesController@create']);
        Route::post('store', ['as' => 'admin.categories.store', 'uses' => 'Admin\CategoriesController@store']);
        Route::get('/{id}', ['as' => 'admin.categories.edit', 'uses' => 'Admin\CategoriesController@edit']);
    });

    Route::group(['prefix' => 'hosts', 'middleware' => 'admin'], function () {
        Route::get('/', ['as' => 'admin.hosts', 'uses' => 'Admin\VenuesController@hosts']);
    });

    Route::get('login', ['as' => 'admin.login', 'uses' => 'Admin\LoginController@index']);
    Route::post('login', ['as' => 'admin.doLogin', 'uses' => 'Admin\LoginController@login']);
    Route::get('dashboard', ['as' => 'admin.dashboard', 'uses' => 'Admin\DashboardController@index', 'middleware' => 'admin']);

});

Route::any('/!/{path?}', function () {
    return view('index');
})->where('path', '.+');

Route::get('/unsupported-browser', 'AngularController@unsupported');

Route::get('/data', 'DataController@index');

Route::group(['prefix' => 'user'], function () {
    Route::post('facebook', ['as' => 'user.facebook_login', 'uses' => 'AuthenticateController@authenticate']);
    Route::get('logout', ['as' => 'user.logout', 'uses' => 'UserController@logout']);
    Route::post('register', ['as' => 'user.register', 'uses' => 'UserController@register']);
    Route::get('restore_password', ['as' => 'user.restore_password', 'uses' => 'UserController@restorePassword', 'middleware' => ['jwt.auth']]);
    Route::any('home', ['as' => 'user.home', 'uses' => 'UserController@home', 'middleware' => ['jwt.auth']]);
    Route::get('activate', ['as' => 'user.activate', 'uses' => 'UserController@activate']);
});

Route::group(['prefix' => 'event'], function () {
    Route::get('/', ['as' => 'event.list', 'uses' => 'EventController@index']);
    Route::get('/{id}', ['as' => 'event.one', 'uses' => 'EventController@show']);

    Route::post('/create', ['as' => 'event.create', 'uses' => 'EventController@store', 'middleware' => ['jwt.auth']]);
    Route::post('/update/{id}', ['as' => 'event.update', 'uses' => 'EventController@update', 'middleware' => ['jwt.auth']]);
    Route::get('/delete/{id}', ['as' => 'event.one', 'uses' => 'EventController@destroy', 'middleware' => ['jwt.auth']]);
});

Route::group(['prefix' => 'venue'], function () {
    Route::get('/', ['as' => 'venue.list', 'uses' => 'VenueController@index']);
    Route::get('/{id}', ['as' => 'venue.one', 'uses' => 'VenueController@show']);
    Route::get('/find', ['as' => 'venue.find', 'uses' => 'VenueController@find']);

    Route::post('/create', ['as' => 'venue.create', 'uses' => 'VenueController@store', 'middleware' => ['jwt.auth']]);
    Route::post('/update/{id}', ['as' => 'venue.update', 'uses' => 'VenueController@update', 'middleware' => ['jwt.auth']]);
    Route::get('/delete/{id}', ['as' => 'venue.delete', 'uses' => 'VenueController@destroy', 'middleware' => ['jwt.auth']]);

});

Route::group(['prefix' => 'category'], function () {
    Route::get('/', ['as' => 'category.list', 'uses' => 'CategoryController@index']);
});

Route::group(['prefix' => 'source'], function () {
    Route::get('/', ['as' => 'source.list', 'uses' => 'SourceController@index']);
    Route::get('/{id}', ['as' => 'source.one', 'uses' => 'SourceController@one']);
});

Route::group(['prefix' => 'location'], function () {
    Route::get('/', ['as' => 'location.list', 'uses' => 'LocationController@index']);
    Route::get('/current', ['as' => 'location.current', 'uses' => 'LocationController@current']);
    Route::get('/currentEntity', ['as' => 'location.currentEntity', 'uses' => 'LocationController@currentEntity']);
    Route::get('/{id}', ['as' => 'location.one', 'uses' => 'LocationController@one']);
});

Route::get('test', ['uses' => 'Admin\EventsController@test']);

//Route::any('/auth/google', ['middleware' => ['cors'], function(\Illuminate\Http\Request $request) {
//    $google = Socialite::driver('google');
//    $google->setRequest($request);
//    return $google->redirect();
//}]);
//Route::get('/auth/google/callback', ['uses' => 'AuthenticateController@authenticate', 'middleware' => ['cors']]);

// Endpoint that is redirected to after an authentication attempt
Route::get('/facebook/callback', function (SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb) {
    // Obtain an access token.
    try {
        $token = $fb->getAccessTokenFromRedirect();
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        dd($e->getMessage());
    }

    // Access token will be null if the user denied the request
    // or if someone just hit this URL outside of the OAuth flow.
    if (!$token) {
        // Get the redirect helper
        $helper = $fb->getRedirectLoginHelper();

        if (!$helper->getError()) {
            abort(403, 'Unauthorized action.');
        }

        // User denied the request
        dd(
            $helper->getError(),
            $helper->getErrorCode(),
            $helper->getErrorReason(),
            $helper->getErrorDescription()
        );
    }

    if (!$token->isLongLived()) {
        // OAuth 2.0 client handler
        $oauth_client = $fb->getOAuth2Client();

        // Extend the access token.
        try {
            $token = $oauth_client->getLongLivedAccessToken($token);
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }
    }

    $fb->setDefaultAccessToken($token);

    // Save for later
    Session::put('fb_user_access_token', (string) $token);
    $user = \Auth::user();
    if ($user) {
        $user->fb_access_token = (string) $token;
    }

    return redirect()->back();
});
