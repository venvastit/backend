<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/20/16
 * Time: 3:32 AM.
 */
namespace App\Events\EventModel;

use App\Events\Event;
use Carbon\Carbon;

class GotVenueFbData extends Event
{
    public $data;
    public $venueId;
    public $eventId;
    public $rawData;

    public function __construct(array $data, $venueId = null, $eventId = null)
    {
        $this->data = $data;
        $this->venueId = $venueId;
        $this->rawData = $this->getRawVenueData();
        $this->eventId = $eventId;
    }

    public function getRawVenueData()
    {
        if (!array_key_exists('name', $this->data)) {
            return;
        }
        if (!array_key_exists('description', $this->data)) {
            return;
        }
        $rawData = [
            'published' => false,
            'description' => $this->getAttr('description') ?: $this->getAttr('about'),
            'id_on_source' => $this->data['id'],
            'cover_image' => $this->getPicture(),
            'name' => $this->data['name'],
            'email' => $this->getEmail(),
            'website' => $this->getAttr('website'),
            'phone' => $this->getAttr('phone'),
            'public_transit' => $this->getAttr('public_transit'),
            'businessHours' => $this->getBusinessHours(),
            'events' => array_key_exists('events', $this->data) ? $this->data['events'] : null,
        ];
        foreach ($this->getLocation() as $key => $value) {
            $rawData[$key] = $value;
        }

        return $rawData;
    }

    protected function getPicture()
    {
        if (array_key_exists('cover', $this->data) && is_array($this->data['cover'])) {
            if (!empty($this->data['cover']['source'])) {
                return $this->data['cover']['source'];
            }
        }

        return;
    }

    protected function getEmail()
    {
        if (array_key_exists('emails', $this->data) && is_array($this->data['emails']) && count($this->data['emails']) > 0) {
            return $this->data['emails'][0];
        }

        return;
    }

    protected function getAttr($key)
    {
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        return;
    }

    protected function getLocation()
    {
        if (array_key_exists('location', $this->data) && is_array($this->data['location'])) {
            $locationObject = [];
            $location = $this->data['location'];
            if (array_key_exists('latitude', $location) && array_key_exists('longitude', $location)) {
                $locationObject['location'] = [
                            'type' => 'Point',
                            'coordinates' => [
                                floatval($location['longitude']),
                                floatval($location['latitude']),
                            ],
                ];
            }
            if (array_key_exists('street', $location)) {
                $locationObject['street'] = $location['street'];
            }
            if (array_key_exists('city', $location)) {
                $locationObject['city'] = $location['city'];
            }
            if (array_key_exists('country', $location)) {
                $locationObject['country'] = $location['country'];
            }
            if (array_key_exists('zip', $location)) {
                $locationObject['zip'] = $location['zip'];
            }

            return $locationObject;
        }

        return;
    }

    protected function createBusinessHourObj(&$object, $day, $time, $key = 'open')
    {
        $object[$day]['day'] = $day;
        $object[$day][$key]['day'] = $day;
        $object[$day][$key]['time'] = str_replace(':', '', $time);
    }

    public function getBusinessHours()
    {
        $businessHours = [];
        if (array_key_exists('hours', $this->data) && is_array($this->data['hours'])) {
            foreach ($this->data['hours'] as $key => $time) {
                switch ($key) {
                    case 'sun_1_open':
                        $this->createBusinessHourObj($businessHours, 0, $time);
                        break;
                    case 'sun_1_close':
                        $this->createBusinessHourObj($businessHours, 0, $time, 'close');
                        break;
                    case 'mon_1_open':
                        $this->createBusinessHourObj($businessHours, 1, $time);
                        break;
                    case 'mon_1_close':
                        $this->createBusinessHourObj($businessHours, 1, $time, 'close');
                        break;
                    case 'tue_1_open':
                        $this->createBusinessHourObj($businessHours, 2, $time);
                        break;
                    case 'tue_1_close':
                        $this->createBusinessHourObj($businessHours, 2, $time, 'close');
                        break;
                    case 'wed_1_open':
                        $this->createBusinessHourObj($businessHours, 3, $time);
                        break;
                    case 'wed_1_close':
                        $this->createBusinessHourObj($businessHours, 3, $time, 'close');
                        break;
                    case 'thu_1_open':
                        $this->createBusinessHourObj($businessHours, 4, $time);
                        break;
                    case 'thu_1_close':
                        $this->createBusinessHourObj($businessHours, 4, $time, 'close');
                        break;
                    case 'fri_1_open':
                        $this->createBusinessHourObj($businessHours, 5, $time);
                        break;
                    case 'fri_1_close':
                        $this->createBusinessHourObj($businessHours, 5, $time, 'close');
                        break;
                    case 'sat_1_open':
                        $this->createBusinessHourObj($businessHours, 6, $time);
                        break;
                    case 'sat_1_close':
                        $this->createBusinessHourObj($businessHours, 6, $time, 'close');
                        break;
                }
            }

            return $businessHours;
        }

        return;
    }
}
