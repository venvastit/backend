<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/20/16
 * Time: 3:32 AM.
 */
namespace App\Events\EventModel;

use App\Dictionary\Scraping\Facebook\PageType;
use App\Events\Event;
use App\Jobs\Scraping\Facebook\GetPageInfo;
use App\Models\Venue;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;

class GotEventFbData extends Event
{
    
    use DispatchesJobs;
    
    public $data;
    public $eventId;
    public $rawData;

    public function __construct(array $data, $eventId = null)
    {
        $this->data = $data;
        $this->eventId = $eventId;
        $this->rawData = $this->getRawEventData();
    }

    protected function getPicture()
    {
        if (array_key_exists('cover', $this->data) && is_array($this->data['cover'])) {
            if (!empty($this->data['cover']['source'])) {
                return $this->data['cover']['source'];
            }
        }

        return null;
    }

    protected function getStartAt()
    {
        if (array_key_exists('start_time', $this->data) && is_string($this->data['start_time'])) {
            return Carbon::createFromFormat(\DateTime::ISO8601, $this->data['start_time']);
        }

        return null;
    }

    protected function getEndAt()
    {
        if (array_key_exists('end_time', $this->data) && is_string($this->data['end_time'])) {
            return Carbon::createFromFormat(\DateTime::ISO8601, $this->data['end_time']);
        }

        return null;
    }

    public function getRawEventData()
    {
        if (!array_key_exists('name', $this->data)) {
            return null;
        }

        $rawData = [
            'details' => $this->data['description'],
            'attending_count' => (int) $this->data['attending_count'],
            'id_on_source' => $this->data['id'],
            'picture' => $this->getPicture(),
            'start_at' => $this->getStartAt(),
            'end_at' => $this->getEndAt(),
            'title' => $this->data['name'],
        ];
        if ($this->eventId !== null) {
            $eventModel = \App\Models\Event::find($this->eventId);
            if ($eventModel) {
                $rawData['venue_id'] = $eventModel->venue_id;
            }

            return $rawData;
        }
        $rawData['published'] = false;
        if (array_key_exists('place', $this->data)) {
            $venue = $this->data['place'];
            if (
                array_key_exists('location', $venue)
                &&
                array_key_exists('longitude', $venue['location'])
                &&
                array_key_exists('longitude', $venue['location'])
            ) {
                $rawData['location'] = [
                        'type' => 'Point',
                        'coordinates' => [
                            floatval($venue['location']['longitude']),
                            floatval($venue['location']['latitude']),
                        ],
                    ];
            }
            $rawData['address'] = '';
            if (array_key_exists('city', $venue) && array_key_exists('country', $venue) && array_key_exists('street', $venue)) {
                $rawData['address'] .= implode(', ', [$venue['street'], $venue['city'], $venue['country']]);
            }
            if (array_key_exists('id', $venue)) {
                $rawData['venue_source_id'] = $venue['id'];
            }
        }
        if (!(isset($rawData['venue_source_id'])) && array_key_exists('owner', $this->data)) {
            $rawData['venue_source_id'] = $this->data['owner']['id'];
        }
        if (isset($rawData['venue_source_id'])) {
            $venue = Venue::where('id_on_source', '=', $rawData['venue_source_id'])->get()->first();
            if ($venue) {
                $rawData['venue_id'] = $venue->_id;
            } else {
                $this->dispatch(new GetPageInfo(PageType::VENUE, $rawData['venue_source_id']));
            }
        }

        return $rawData;
    }
}
