<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/20/16
 * Time: 5:38 AM.
 */
namespace App\Events\EventModel;

use App\Events\Event;

class EventWithoutVenueSaved extends Event
{
    /**
     * @var string
     */
    public $eventId;

    /**
     * @var string|int
     */
    public $venueSourceId;

    public function __construct($eventId, $venueSourceId)
    {
        $this->eventId = $eventId;
        $this->venueSourceId = $venueSourceId;
    }
}
