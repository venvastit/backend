<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/30/16
 * Time: 12:11 AM
 */

namespace App\Events\User;


use App\Events\Event;
use App\User;

class RestorePassword extends Event
{

    /**
     * @var User
     */
    private $user;

    /**
     * RestorePassword constructor.
     * 
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->setUser($user);    
    }

    /**
     * @return User
     */
    public function getUser() : User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

}