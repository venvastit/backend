<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/23/16
 * Time: 10:54 PM.
 */
namespace App\Events\User;

use App\Events\Event;
use App\User;

class UserRegistered extends Event
{

    /**
     * @var User
     */
    private $user;


    /**
     * UserRegistered event constructor
     * Triggered when user submits the registration form and 
     * user instance in db created successfully
     * .
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->setUser($user);    
    }

    /**
     * @return User
     */
    public function getUser() : User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }
    
    
}
