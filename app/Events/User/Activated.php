<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/23/16
 * Time: 10:54 PM.
 */
namespace App\Events\User;


use App\Events\Event;
use App\User;

class Activated extends Event
{
    /**
     * @var User
     */
    private $user;

    /**
     * Activated event constructor
     * Triggered when user activates account by 
     * following the link sent by email
     * 
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->setUser($user);
    }

    /**
     * @return User
     */
    public function getUser() : User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }



}
