<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 7/1/16
 * Time: 4:21 AM.
 */
namespace App\Events\Venue;

use App\Events\Event;

class GotGoogleVenue extends Event
{
    public $venueData;

    public function __construct($venueData)
    {
        $this->venueData = $venueData;
    }
}
