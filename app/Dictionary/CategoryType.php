<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/1/16
 * Time: 11:54 AM.
 */
namespace App\Dictionary;

/**
 * Class CategoryType.
 */
class CategoryType
{
    const TYPE_VENUE = 1;
    const TYPE_EVENT = 2;
    const TYPE_DEAL = 3;

    /**
     * Returns integer code of Category type.
     * 
     * Return integer code of Category type, will return
     * Event if unspecified.
     * @param string $alias
     * @return int
     */
    public static function getByAlias(string $alias) : int
    {
        switch ($alias) {
            case 'venues':
                return self::TYPE_VENUE;
                break;
            case 'events':
                return self::TYPE_EVENT;
                break;
            case 'deals':
                return self::TYPE_DEAL;
                break;
            default:
                return self::TYPE_EVENT;
                break;
        }
    }

    /**
     * Get type alias by code specified.
     *
     * @param int $code
     * @return string
     */
    public static function getAliasByCode(int $code) : string
    {
        switch ($code) {
            case self::TYPE_EVENT:
                return 'event';
                break;
            case self::TYPE_DEAL:
                return 'deal';
                break;
            case self::TYPE_VENUE:
                return 'venue';
                break;
            default:
                return 'event';
                break;
        }
    }

    public static function lists()
    {
        return [
            self::getAliasByCode(self::TYPE_VENUE) => self::TYPE_VENUE,
            self::getAliasByCode(self::TYPE_EVENT) => self::TYPE_EVENT,
            self::getAliasByCode(self::TYPE_DEAL) => self::TYPE_DEAL,
            'External' => null,
        ];
    }
}
