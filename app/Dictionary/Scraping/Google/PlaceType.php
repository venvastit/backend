<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 7/1/16
 * Time: 3:48 AM.
 */
namespace App\Dictionary\Scraping\Google;

use App\Dictionary\CategorySource;
use App\Models\Category;

class PlaceType
{
    public static function getTypes()
    {
        return [
            'bakery','bar','beauty_salon', 'bicycle_store',
            'book_store','bowling_alley', 'cafe','car_wash',
            'clothing_store','gym','hair_care','library','museum',
            'night_club','pet_store','restaurant', 'shoe_store','shopping_mall','spa','lodging',
        ];
    }

    public static function getCategory($type)
    {
        return Category::where('name', '=', $type)->where('source', '=', CategorySource::GOOGLE)->get()->first();
    }
}
