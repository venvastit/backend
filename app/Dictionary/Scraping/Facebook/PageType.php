<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/20/16
 * Time: 2:28 AM.
 */
namespace App\Dictionary\Scraping\Facebook;

use App\Models\Event;
use App\Models\Venue;

class PageType
{
    const EVENT = 0;
    const VENUE = 1;

    public static function getModel($type)
    {
        switch ($type) {
            case self::EVENT:
                return Event::class;
            case self::VENUE:
                return Venue::class;
        }
    }
}
