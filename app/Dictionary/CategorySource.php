<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/25/16
 * Time: 7:53 PM.
 */
namespace App\Dictionary;

class CategorySource
{
    const FACEBOOK = 1;
    const GOOGLE = 2;

    public static function getByAlias(string $alias)
    {
        $alias = strtolower($alias);
        switch ($alias) {
            case 'facebook':
                return self::FACEBOOK;
            case 'google':
                return self::GOOGLE;
        }
    }

    public static function getAliasByCode(int $code)
    {
        switch ($code) {
            case self::FACEBOOK:
                return 'Facebook';
            case self::GOOGLE:
                return 'Google';
        }
    }

    public static function lists()
    {
        return [
            'VenVast' => null,
            self::getAliasByCode(self::FACEBOOK) => self::FACEBOOK,
            self::getAliasByCode(self::GOOGLE) => self::GOOGLE,
        ];
    }
}
