<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/5/16
 * Time: 7:54 PM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class TextSearch extends Criteria
{
    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $field;

    /**
     * TitleSearch constructor.
     * @param string $text
     * @param string $field
     */
    public function __construct(string $text, string $field = 'title')
    {
        $this->text = $text;
        $this->field = $field;
    }

    public function apply($model, Repository $repository)
    {
        if ($this->text === null) {
            return $model;
        }

        return $model->where($this->field, 'like', '%'.$this->text.'%');
    }

    public function getCacheKey()
    {
        return ':text:'.$this->text;
    }
}
