<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/13/16
 * Time: 11:42 PM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\Exception\InvalidArgumentException;
use App\Repositories\RepositoryInterface as Repository;
use Illuminate\Database\Eloquent\Model;
use MongoDB\BSON\ObjectID;

/**
 * Queries categories related to models
 * Class InCategory.
 */
class InCategory extends Criteria
{
    /**
     * @var array|string|int
     */
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @param Model $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->whereRaw([
            'categories' => [$this->id],
        ]);
    }

    public function getCacheKey()
    {
        $cacheKey = ':in_category:'.$this->id;

        return $cacheKey;
    }
}
