<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/7/16
 * Time: 1:29 AM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class Organizer extends Criteria
{



    public function apply($model, Repository $repository)
    {
        $user = \JWTAuth::toUser(\JWTAuth::getToken());
        return $model->whereRaw([
            'organizers' => ['$in' => [$user->_id]],
        ]);
    }

    public function getCacheKey()
    {
        $user = \Auth::user();

        return ':organizer:'.$user->_id;
    }
}
