<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/6/16
 * Time: 11:02 AM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class FieldNotNull extends Criteria
{
    /**
     * @var string
     */
    protected $field;

    /**
     * FieldNotEquals constructor.
     * @param string $field
     */
    public function __construct($field)
    {
        $this->field = $field;
    }

    public function apply($model, Repository $repository)
    {
        return $model->whereNotNull($this->field);
    }

    public function getCacheKey()
    {
        return ':field_'.$this->field.'NOT-NULL';
    }
}
