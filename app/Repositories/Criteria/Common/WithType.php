<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/29/16
 * Time: 8:36 AM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class WithType extends Criteria
{
    /**
     * @var int
     */
    protected $type;

    public function __construct(int $type)
    {
        $this->type = $type;
    }

    public function apply($model, Repository $repository)
    {
        return $model->where('type', '=', $this->type);
    }

    public function getCacheKey()
    {
        return 'where_type:'.$this->type;
    }
}
