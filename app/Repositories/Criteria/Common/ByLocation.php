<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/14/16
 * Time: 12:14 AM.
 */
namespace App\Repositories\Criteria\Common;

use App\Models\Location;
use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use Illuminate\Database\Eloquent\Model;

class ByLocation extends Criteria
{
    /**
     * @var Location
     */
    protected $location;

    public function __construct($location)
    {
        if ($location instanceof Location) {
            $this->location = $location;
        } elseif (is_string($location) || is_numeric($location)) {
            $this->location = Location::find($location);
        } else {
            throw new \Exception('Location should be string|integer or instance of Models\\Location');
        }
    }

    /**
     * @param Model $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = new ByCoords($model->getTable(), $this->location->latitude, $this->location->longitude);

        return $query->apply($model, $repository);
    }

    public function getCacheKey()
    {
        return ':b_location:'.number_format($this->location->latitude, 2, '.', '').':'.number_format($this->location->longitude, 2, '.', '');
    }
}
