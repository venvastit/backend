<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/7/16
 * Time: 1:29 AM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use App\User;
use Jenssegers\Mongodb\Eloquent\Model;
use MongoDB\BSON\ObjectID;

class HasOrganizer extends Criteria
{
    public function apply($model, Repository $repository)
    {
        return $model->whereRaw([
            'organizers' => [
                '$exists' => true,
                '$not' => [
                    '$size' => 0,
                ],
            ],
        ]);
    }

    public function getCacheKey()
    {
        return ':has_organizers:';
    }
}
