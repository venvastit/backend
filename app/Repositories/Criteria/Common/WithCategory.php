<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/16/16
 * Time: 5:14 PM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class WithCategory extends Criteria
{
    public function apply($model, Repository $repository)
    {
        return $model->with('categories');
    }

    public function getCacheKey()
    {
        return ':w_categories';
    }
}
