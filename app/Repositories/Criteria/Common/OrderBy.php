<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/16/16
 * Time: 4:35 PM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use Illuminate\Database\Eloquent\Model;

class OrderBy extends Criteria
{
    const DESC = 'desc';
    const ASC = 'asc';

    /**
     * @var string
     */
    protected $column;

    /**
     * @var string
     */
    protected $order;

    /**
     * OrderBy constructor.
     * @param string $column
     * @param string $order
     */
    public function __construct($column, $order = self::DESC)
    {
        $this->column = $column;
        $this->order = $order;
    }

    public function apply($model, Repository $repository)
    {
        return $model->orderBy($this->column, $this->order);
    }

    public function getCacheKey()
    {
        return ':order_by:'.$this->column.':'.$this->order;
    }
}
