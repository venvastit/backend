<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/19/16
 * Time: 5:56 PM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use Illuminate\Database\Eloquent\Model;

class Paginate extends Criteria
{
    /**
     * @var int
     */
    protected $page;

    /**
     * @var int
     */
    protected $itemsPerPage;

    /**
     * @var int|null
     */
    protected $skip;

    /**
     * Paginate constructor.
     * @param int $page
     * @param int $itemsPerPage
     * @param int|null $skip
     */
    public function __construct($page = 0, $itemsPerPage = 15, $skip = null)
    {
        $this->page = $page;
        $this->itemsPerPage = $itemsPerPage;
        if ($skip === null) {
            $skip = $this->page * $this->itemsPerPage;
        }
        $this->skip = $skip;
    }

    public function apply($model, Repository $repository)
    {
        return $model->skip($this->skip)->take($this->itemsPerPage);
    }

    public function getCacheKey()
    {
        return ':p_:'.$this->skip.':items_p_p:'.$this->itemsPerPage;
    }
}
