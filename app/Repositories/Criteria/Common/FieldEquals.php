<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/6/16
 * Time: 11:02 AM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class FieldEquals extends Criteria
{
    /**
     * @var string
     */
    protected $field;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * FieldNotEquals constructor.
     * @param string $field
     * @param mixed $value
     */
    public function __construct($field, $value)
    {
        $this->field = $field;
        $this->value = $value;
    }

    public function apply($model, Repository $repository)
    {
        return $model->where($this->field, $this->value);
    }

    public function getCacheKey()
    {
        return ':field_'.$this->field.'_eq_'.$this->value;
    }
}
