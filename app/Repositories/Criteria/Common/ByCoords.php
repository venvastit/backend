<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/14/16
 * Time: 12:04 AM.
 */
namespace App\Repositories\Criteria\Common;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ByCoords extends Criteria
{
    const KM_RADIUS = 6731;

    /**
     * @var string|float
     */
    protected $lat;

    /**
     * @var string|float
     */
    protected $lng;

    /**
     * @var string|float
     */
    protected $distance;

    /**
     * ByCoords constructor.
     * @param float $lat latitude radians
     * @param float $lng longitude radians
     * @param float $distance distance in KM
     */
    public function __construct($lat, $lng, $distance = 25.0)
    {
        $this->lat = floatval($lat);
        $this->lng = floatval($lng);
        $this->distance = floatval($distance);
    }

    /**
     * @param Model $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->whereRaw([
            'location' => [
                '$geoWithin' => [
                    '$centerSphere' => [
                        [$this->lng, $this->lat],
                        $this->distance / self::KM_RADIUS,
                    ],
                ],
            ],
        ]);
    }

    public function getCacheKey()
    {
        return ':b_coords:'.$this->lat.':'.$this->lng.':'.$this->distance;
    }
}
