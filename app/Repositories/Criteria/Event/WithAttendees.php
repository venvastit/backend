<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/16/16
 * Time: 4:09 PM.
 */
namespace App\Repositories\Criteria\Event;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Queries attendees related to models.
 *
 * Class WithAttendees
 */
class WithAttendees extends Criteria
{
    /**
     * @param Model $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->with('attendees');
    }

    public function getCacheKey()
    {
        return ':w_attendees';
    }
}
