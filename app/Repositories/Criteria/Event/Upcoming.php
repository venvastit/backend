<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/19/16
 * Time: 5:32 AM.
 */
namespace App\Repositories\Criteria\Event;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Upcoming extends Criteria
{
    public function apply($model, Repository $repository)
    {
        $now = new Carbon();

        return $model->where('start_at', '>=', $now);
    }

    public function getCacheKey()
    {
        return ':upcoming:';
    }
}
