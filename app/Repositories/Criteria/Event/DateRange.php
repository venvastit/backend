<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/6/16
 * Time: 11:59 PM.
 */
namespace App\Repositories\Criteria\Event;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use Carbon\Carbon;

class DateRange extends Criteria
{
    const CLIENT_DATE_FORMAT = 'd.m.Y';

    /**
     * @var Carbon|null
     */
    protected $from = null;

    /**
     * @var Carbon|null
     */
    protected $to = null;

    /**
     * ByDate constructor.
     * @param array $dates
     */
    public function __construct(array $dates)
    {
        $this->initFrom($dates);
        $this->initTo($dates);
    }
    
    private function initFrom(array $dates) 
    {
        if(array_key_exists('from', $dates)) {
            $this->from = Carbon::createFromFormat(self::CLIENT_DATE_FORMAT, $dates['from']);
        } else {
            $this->from = Carbon::now();
            $this->from->setTime(0, 0, 1);
        }
        $this->from->setTimezone(\Session::get('timezone', 'UTC'));
    }
    
    private function initTo(array $dates)
    {
        if(array_key_exists('to', $dates)) {
            $this->to = Carbon::createFromFormat(self::CLIENT_DATE_FORMAT, $dates['to']);
        } else {
            $this->to = clone($this->from);
            $this->to->addYear();
        }
        $this->to->setTimezone(\Session::get('timezone', 'UTC'));
    }

    public function apply($model, Repository $repository)
    {
        return $model->where('start_at', '>=', $this->from)->where('start_at', '<=', $this->to);
    }

    public function getCacheKey()
    {
        return ':by_date-from:' . $this->from->format('d.m.Y') . '::date-to:' . $this->to->format('d.m.Y');
    }
}
