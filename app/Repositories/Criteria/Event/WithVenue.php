<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/16/16
 * Time: 5:09 PM.
 */
namespace App\Repositories\Criteria\Event;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class WithVenue extends Criteria
{
    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param Repository $repository
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function apply($model, Repository $repository)
    {
        return $model->with('venue');
    }

    public function getCacheKey()
    {
        return ':w_venue';
    }
}
