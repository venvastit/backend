<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/6/16
 * Time: 11:59 PM.
 */
namespace App\Repositories\Criteria\Event;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use Carbon\Carbon;

class ByDate extends Criteria
{
    const CLIENT_DATE_FORMAT = 'Y-m-d';

    /**
     * @var Carbon
     */
    protected $date;

    /**
     * ByDate constructor.
     * @param string $dateString
     */
    public function __construct(string $dateString)
    {
        $this->date = Carbon::createFromFormat(self::CLIENT_DATE_FORMAT, $dateString);
    }

    public function apply($model, Repository $repository)
    {
        $this->date->setTime(0, 0, 1);
        $this->date->setTimezone(\Session::get('timezone', 'UTC'));
        $dateEnd = clone($this->date);
        $dateEnd->addDay();

        return $model->where('start_at', '>', $this->date)->where('start_at', '<', $dateEnd);
    }

    public function getCacheKey()
    {
        return ':by_date-'.$this->date->format('d.m.Y');
    }
}
