<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/16/16
 * Time: 4:52 PM.
 */
namespace App\Repositories\Criteria\Event;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MongoDB\BSON\UTCDatetime;

class When extends Criteria
{
    const TODAY = 'today';
    const TOMORROW = 'tomorrow';
    const WEEK = 'week';
    const WEEKEND = 'weekend';
    const MONTH = 'month';
    /**
     * @var string|null
     */
    protected $when;

    /**
     * When constructor.
     * @param string|null $when
     */
    public function __construct($when)
    {
        $this->when = $when;
    }

    /**
     * @param Model $model
     * @param Repository $repository
     * @return Model
     */
    public function apply($model, Repository $repository)
    {
        $now = Carbon::now();
        $now->setTimezone(\Session::get('timezone', 'UTC'));
        switch ($this->when) {
            case self::TODAY:
                $end = clone($now);
                $end->setTime(23, 59, 59);

                return $model->where('start_at', '>', $now)->where('start_at', '<', $end);
            case self::TOMORROW:
                $start = clone($now);
                $start->addDay();
                $start->setTime(0, 0);
                $end = clone($start);
                $end->setTime(23, 59, 59);

                return $model->where('start_at', '>', $start)->where('start_at', '<=', $end);
            case self::WEEK:
                $start = clone($now);
                $end = clone($start);
                $end->addDays(7);
                $end->setTime(23, 59, 59);

                return $model->where('start_at', '>=', $start)->where('start_at', '<=', $end);
            case self::MONTH:
                $start = clone($now);
                $end = (new Carbon())->endOfMonth();
                $end->setTime(23, 59, 59);

                return $model->where('start_at', '>=', $start)->where('start_at', '<=', $end);
            case null:
                return $model->where('start_at', '>', $now);
        }
    }

    public function getCacheKey()
    {
        return ':when:'.$this->when;
    }
}
