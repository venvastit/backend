<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/13/16
 * Time: 11:35 PM.
 */
namespace App\Repositories\Criteria;

use App\Repositories\RepositoryInterface as Repository;
use Jenssegers\Mongodb\Eloquent\Model as Model;

abstract class Criteria
{
    /**
     * @param Model $model
     * @param Repository $repository
     * @return mixed
     */
    abstract public function apply($model, Repository $repository);

    abstract public function getCacheKey();
}
