<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/14/16
 * Time: 7:50 PM.
 */
namespace App\Repositories\Criteria\Venue;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;
use Illuminate\Database\Eloquent\Model;

class WithEvents extends Criteria
{
    /**
     * @param Model $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->with('events');
    }

    public function getCacheKey()
    {
        return ':w_events';
    }
}
