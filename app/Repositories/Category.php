<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/29/16
 * Time: 7:50 AM.
 */
namespace App\Repositories;

class Category extends AbstractRepository
{
    public function model()
    {
        return \App\Models\Category::class;
    }
}
