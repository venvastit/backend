<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/30/16
 * Time: 6:06 AM.
 */
namespace App\Repositories\Exception;

use Exception;

class InvalidArgumentException extends Exception
{
}
