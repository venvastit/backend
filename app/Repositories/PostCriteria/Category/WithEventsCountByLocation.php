<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/29/16
 * Time: 7:56 AM.
 */
namespace App\Repositories\PostCriteria\Category;

use App\Repositories\PostCriteria\PostCriteria;
use Illuminate\Database\Eloquent\Collection;

class WithEventsCountByLocation extends PostCriteria
{
    /**
     * @var float
     */
    protected $lat;

    /**
     * @var float
     */
    protected $lng;

    /**
     * @var int
     */
    protected $radius;

    const RADIUS_IN_KM = 6371;

    /**
     * WithEventsCountByLocation constructor.
     * @param $lat
     * @param $lng
     * @param int $radius
     */
    public function __construct(float $lat, float $lng, int $radius = 20)
    {
        $this->lat = $lat;
        $this->lng = $lng;
        $this->radius = $radius;
    }

    public function apply(Collection $collection) : Collection
    {
        foreach ($collection as $element) {
            $events = $element->events()->whereRaw([
                'location' => [
                    '$geoWithin' => [
                        '$center' => [
                            [$this->lng, $this->lat],
                            $this->radius / self::RADIUS_IN_KM,
                        ],
                    ],
                ],
            ])->count();
            $element->total_events_count_location = $events;
        }

        return $collection;
    }

    public function getCacheKey() : string
    {
        return 'w_events_count_b_loc:'.$this->lat.':'.$this->lng.':'.$this->radius;
    }
}
