<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/29/16
 * Time: 8:42 AM.
 */
namespace App\Repositories\PostCriteria\Category;

use App\Repositories\PostCriteria\PostCriteria;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class WithUpcomingEventsCountByLocation extends PostCriteria
{
    /**
     * @var float
     */
    protected $lat;

    /**
     * @var float
     */
    protected $lng;

    /**
     * @var int
     */
    protected $radius;

    /**
     * @var Carbon
     */
    protected $time;

    const RADIUS_IN_KM = 6371;

    public function __construct(float $lat, float $lng, $time = null, int $radius = 20)
    {
        $this->lat = $lat;
        $this->lng = $lng;
        $this->radius = $radius;
        if (!$time) {
            $this->time = Carbon::now();
        } else {
            $this->time = $time;
        }
    }

    public function apply(Collection $collection) : Collection
    {
        foreach ($collection as $element) {
            $events = $element->events()->whereRaw([
                'location' => [
                    '$geoWithin' => [
                        '$center' => [
                            [$this->lng, $this->lat],
                            $this->radius / self::RADIUS_IN_KM,
                        ],
                    ],
                ],
                'start_at' => [
                    '$gte' => $this->time,
                ],
            ])->count();
            $element->upcoming_events_count_location = $events;
        }

        return $collection;
    }

    public function getCacheKey() : string
    {
        return strtolower(__CLASS__).':'.$this->time->getTimestamp();
    }
}
