<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/19/16
 * Time: 8:10 AM.
 */
namespace App\Repositories\PostCriteria\Venue;

use App\Models\Event;
use App\Repositories\PostCriteria\PostCriteria;
use App\Models\Venue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class WithUpcomingEvents extends PostCriteria
{
    /**
     * @var Carbon
     */
    protected $time;
    protected $revert;

    public function __construct($revert = false)
    {
        $this->time = Carbon::now();
        $this->revert = $revert;
    }

    public function apply(Collection $collection)
    {
        $collection = $collection->reject(function ($element) {
            $count = count($element->events);
            if ($count > 0) {
                $hasUpcoming = false;
                /** @var Event $event */
                foreach ($element->events as $event) {
                    if (!is_object($event) || !is_object($event->start_at)) {
                        continue;
                    }
                    if ($event->start_at->gte(Carbon::now())) {
                        $hasUpcoming = true;
                    }
                }
                if (!$this->revert) {
                    return !$hasUpcoming;
                }

                return $hasUpcoming;
            }
            if (!$this->revert) {
                return true;
            }

            return false;
        });

        return $collection;
    }

    public function getCacheKey()
    {
        return ':w_no_upcoming_events:'.$this->time->format('d.m.Y');
    }
}
