<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/19/16
 * Time: 8:10 AM.
 */
namespace App\Repositories\PostCriteria\Venue;

use App\Models\Event;
use App\Repositories\PostCriteria\PostCriteria;
use App\Models\Venue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class WithEvents extends PostCriteria
{
    /**
     * @var Carbon
     */
    protected $revert;

    public function __construct($revert = false)
    {
        $this->revert = $revert;
    }

    public function apply(Collection $collection)
    {
        $collection = $collection->reject(function ($element) {
            $count = count($element->events);
            if ($this->revert) {
                return $count > 0;
            }

            return $count === 0;
        });

        return $collection;
    }

    public function getCacheKey()
    {
        return ':w_no_upcoming_events:'.$this->time->format('d.m.Y');
    }
}
