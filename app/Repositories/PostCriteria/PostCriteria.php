<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/29/16
 * Time: 7:57 AM.
 */
namespace App\Repositories\PostCriteria;

use Illuminate\Database\Eloquent\Collection;

abstract class PostCriteria
{
    /**
     * @param Collection $collection
     * @return mixed
     */
    abstract public function apply(Collection $collection);

    abstract public function getCacheKey();
}
