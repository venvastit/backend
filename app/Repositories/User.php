<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/29/16
 * Time: 7:50 AM.
 */
namespace App\Repositories;

use App\User as UserModel;

class User extends AbstractRepository
{
    public function model()
    {
        return UserModel::class;
    }
}
