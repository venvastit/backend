<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/14/16
 * Time: 7:53 PM.
 */
namespace App\Repositories;

use App\Models\Event as EventModel;

class Event extends AbstractRepository
{
    public function model()
    {
        return EventModel::class;
    }
}
