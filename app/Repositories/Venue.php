<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/13/16
 * Time: 11:26 PM.
 */
namespace App\Repositories;

use App\Models\Venue as VenueModel;

class Venue extends AbstractRepository
{
    public function model()
    {
        return VenueModel::class;
    }
}
