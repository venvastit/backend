<?php

namespace App\Models;

use App\Models\Utils\WithLocationTrait;
use App\User;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Models\Utils\WithCategoriesTrait;
use App\Models\Utils\WithEventCategoriesTrait;

/**
 * App\Models\Venue.
 */
class Venue extends Eloquent implements ModelWithImageFieldsInterface
{
    use ModelWithImageOrFileFieldsTrait, WithCategoriesTrait, WithLocationTrait;

    protected $collection = 'venues';

    protected $guarded = [];

    public function getImageFields()
    {
        return [
            'cover_image' => 'venues/',
        ];
    }

    public function setBusinessHours($bHoursData)
    {
        if ($bHoursData === null) {
            return;
        }

        $finalArray = [];

        foreach ($bHoursData as $dayData) {
            if (!is_array($dayData)) {
                continue;
            }

            if (!array_key_exists('open', $dayData) || is_string($dayData['open'])) {
                $finalArray[] = $dayData;
                continue;
            } else {
                $format = 'H:i';
                if (strlen($dayData['open']['time']) === 4) {
                    $format = 'Hi';
                }
                $open = [
                    'day' => intval($dayData['day']),
                    'time' => Carbon::createFromFormat($format, $dayData['open']['time'])->getTimestamp(),
                ];
                $dayData['open'] = $open;
                if (array_key_exists('close', $dayData) && !is_string($dayData['close'])) {
                    if (strlen($dayData['close']['time']) === 4) {
                        $format = 'Hi';
                    } else {
                        $format = 'H:i';
                    }
                    $close = [
                        'day' => intval($dayData['day']),
                        'time' => Carbon::createFromFormat($format, $dayData['close']['time'])->getTimestamp(),
                    ];
                    $dayData['close'] = $close;
                }
                $finalArray[] = $dayData;
            }
        }

        $this->attributes['business_hours'] = $finalArray;
    }

    /**
     * @return \Jenssegers\Mongodb\Relations\EmbedsMany
     */
    public function organizersRelation()
    {
        return $this->embedsMany(User::class, 'organizers');
    }

    public function events()
    {
        return $this->hasMany(Event::class, 'venue_id');
    }

    public function setOrganizers($organizers)
    {
        $this->organizers = $organizers;
    }


    public static function getImagePath($url)
    {
        if(empty($url)) {
            return '//venvast.com/img/question.png';
        }
        if(mb_substr($url, 0, 4 ) !== "http") {
            return 'https://venvast.com/img/cache/original/venues/' . $url;
        }
        return $url;
    }

    public function toClient()
    {
        $this->mapIcon = 'img/question.png';
        if (count($this->categories) > 0) {
            $this->mapIcon = 'https://venvast.com/img/cache/original/categories/'.$this->categories()->first()->image;
        }

        return $this;
    }
}
