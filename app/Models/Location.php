<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;

/**
 * Class Location.
 *
 * @attribute int $id
 * @attribute string $name
 * @attribute string $latitude
 * @attribute string $longitude
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venue[] $venues
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\SleepingOwl\Models\SleepingOwlModel defaultSort()
 * @mixin \Eloquent
 * @property int $id
 * @property string $latitude
 * @property string $longitude
 * @property string $name
 */
class Location extends SleepingOwlModel
{
    protected $table = 'locations';
    protected $guarded = [];
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function venues()
    {
        return $this->belongsToMany(Venue::class, 'venues_locations', 'location_id', 'venue_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany(Event::class, 'events_locations', 'location_id', 'event_id');
    }

    public static function getList()
    {
        $all = self::lists('name', 'id')->all();

        return $all;
    }
}
