<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/16/16
 * Time: 3:22 AM.
 */
namespace App\Models\Utils;

use App\Models\Category;
use MongoDB\BSON\ObjectID;

trait WithEventCategoriesTrait
{
    public function getEventCategoriesAttribute()
    {
        $eventCategories = $this->attributes['eventCategories'];
        foreach ($eventCategories as $key => $value) {
            $eventCategories[$key] = Category::find($value);
        }

        return $eventCategories;
    }

    public function setEventCategories($eventCategories)
    {
        if (is_null($eventCategories)) {
            return;
        }
        $this->attributes['eventCategories'] = [];
        if (is_string($eventCategories)) {
            $this->attributes['eventCategories'] = [new ObjectID($eventCategories)];
        }
        if (is_array($eventCategories)) {
            foreach ($eventCategories as $category) {
                if (is_array($category) && array_key_exists('_id', $category)) {
                    $this->attributes['eventCategories'][] = new ObjectID($category['_id']);
                } elseif (is_string($category)) {
                    $this->attributes['eventCategories'][] = new ObjectID($category);
                }
            }
        }
    }
}
