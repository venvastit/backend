<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/31/16
 * Time: 1:49 PM.
 */
namespace App\Models\Utils;

use App\Models\Category;
use MongoDB\BSON\ObjectID;

trait WithCategoriesTrait
{
    public function getCategoriesAttribute()
    {
        if (!array_key_exists('categories', $this->attributes)) {
            return [];
        }
        $categories = $this->attributes['categories'];
        foreach ($categories as $key => $value) {
            $categories[$key] = Category::find($value);
        }

        return $categories;
    }

    public function getEventCategoriesAttribute()
    {
        if (!array_key_exists('eventCategories', $this->attributes)) {
            $this->attributes['eventCategories'] = [];
        }
        $categories = $this->attributes['eventCategories'];
        foreach ($categories as $key => $value) {
            $categories[$key] = Category::find($value);
        }

        return $categories;
    }

    public function setEventCategories($categories)
    {
        if (is_null($categories)) {
            return;
        }
        $this->attributes['eventCategories'] = [];
        if (is_string($categories)) {
            $this->attributes['eventCategories'] = [$categories];
        }
        if (is_array($categories)) {
            foreach ($categories as $category) {
                if (is_array($category) && array_key_exists('_id', $category)) {
                    $this->attributes['eventCategories'][] = $category['_id'];
                } elseif (is_string($category)) {
                    $this->attributes['eventCategories'][] = $category;
                }
            }
        }
    }

    public function setCategories($categories)
    {
        if (is_null($categories)) {
            return;
        }
        $this->attributes['categories'] = [];
        if (is_string($categories)) {
            $this->attributes['categories'] = [$categories];
        }
        if (is_array($categories)) {
            foreach ($categories as $category) {
                if (is_array($category) && array_key_exists('_id', $category)) {
                    $this->attributes['categories'][] = $category['_id'];
                } elseif (is_string($category)) {
                    $this->attributes['categories'][] = $category;
                }
            }
        }
    }
}
