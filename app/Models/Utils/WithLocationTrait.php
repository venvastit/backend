<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/14/16
 * Time: 5:15 AM.
 */
namespace App\Models\Utils;

trait WithLocationTrait
{
    public function setLocation($locationData)
    {
        $this->attributes['location'] = [
            'type' => $locationData['type'],
            'coordinates' => [
                floatval($locationData['coordinates'][0]),
                floatval($locationData['coordinates'][1]),
            ],
        ];
    }
}
