<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/24/16
 * Time: 7:38 AM.
 */
namespace App\Models\Utils;

use App\Models\Event;

trait EventCountTrait
{
    public function getEventsCountAttribute()
    {
        return $this->events->count();
    }
}
