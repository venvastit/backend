<?php

namespace App\Models;

use App\Dictionary\CategorySource;
use App\Dictionary\CategoryType;
use Jenssegers\Mongodb\Eloquent\Builder;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Event.
 */
class Category extends Eloquent implements ModelWithImageFieldsInterface
{
    use ModelWithImageOrFileFieldsTrait;

    protected $collection = 'categories';
    protected $guarded = [];

    const TYPE_VENUE = 1;
    const TYPE_EVENT = 2;
    const TYPE_DEAL = 3;

    public function getSourceAttribute()
    {
        if (array_key_exists('source', $this->attributes)) {
            return CategorySource::getAliasByCode($this->attributes['source']);
        }

        return 'VenVast';
    }

    public function getTypeAttribute()
    {
        if (array_key_exists('type', $this->attributes)) {
            return CategoryType::getAliasByCode($this->attributes['type']);
        }

        return 'External';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function venues()
    {
        return $this->belongsToMany(Venue::class, 'venues', 'categories');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany(Event::class, 'events', 'categories');
    }

    public function getImageFields()
    {
        return [
            'image' => 'categories/',
        ];
    }

//    public static function getList()
//    {
//        $all = self::lists('name', 'id')->all();
//        return $all;
//    }

    /**
     * Get with type Venue.
     * @param Builder $query
     * @return Builder
     */
    public function scopeTypeVenue($query)
    {
        return $query->where('type', '=', CategoryType::TYPE_VENUE);
    }

    /**
     * Get with type Event.
     * @param Builder $query
     * @return Builder
     */
    public function scopeTypeEvent($query)
    {
        return $query->where('type', '=', CategoryType::TYPE_EVENT);
    }

    /**
     * Get with type Deal.
     * @param Builder $query
     * @return Builder
     */
    public function scopeTypeDeal($query)
    {
        return $query->where('type', '=', CategoryType::TYPE_DEAL);
    }

    /**
     * Get with source Facebook.
     * @param Builder $query
     * @return Builder
     */
    public function scopeSourceFacebook($query)
    {
        return $query->where('source', '=', CategorySource::FACEBOOK);
    }

    /**
     * Get with source Google.
     * @param Builder $query
     * @return Builder
     */
    public function scopeSourceGoogle($query)
    {
        return $query->where('source', '=', (string) CategorySource::GOOGLE);
    }

    /**
     * Get with source VenVast.
     * @param Builder $query
     * @return Builder
     */
    public function scopeSourceVenVast($query)
    {
        return $query->where('source', '!=', CategorySource::GOOGLE)->where('source', '!=', CategorySource::FACEBOOK);
    }
}
