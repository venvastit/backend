<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 5/2/16
 * Time: 7:09 PM.
 */
namespace App\Models;

use App\Models\Utils\EventCountTrait;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;

class EventCategory extends SleepingOwlModel implements ModelWithImageFieldsInterface
{
    use ModelWithImageOrFileFieldsTrait;

    public $timestamps = false;
    protected $table = 'event_categories';
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany(Event::class, 'events_event_categories', 'category_id', 'event_id');
    }

    public function getImageFields()
    {
        return [
            'image' => 'categories/',
        ];
    }

    public static function getList()
    {
        $all = self::lists('name', 'id')->all();

        return $all;
    }
}
