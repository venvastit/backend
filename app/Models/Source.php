<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;

/**
 * Class Source.
 *
 * @attribute int $id
 * @attribute string $name
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venue[] $venues
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property int $id
 * @property string $name
 * @property string $image
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Source whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Source whereName($value)
 * @property string $url
 * @property string $api_url
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Source whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Source whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Source whereApiUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\SleepingOwl\Models\SleepingOwlModel defaultSort()
 */
class Source extends SleepingOwlModel implements ModelWithImageFieldsInterface
{
    use ModelWithImageOrFileFieldsTrait;

    const FACEBOOK = 1;
    const GOOGLE = 2;
    const VENVAST = 3;

    public $timestamps = false;

    protected $table = 'sources';

    protected $guarded = [];

    public function events()
    {
        return $this->hasMany(Event::class, 'source_id');
    }

    public function venues()
    {
        return $this->hasMany(Venue::class, 'source_id');
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'source_id');
    }

    public static function getList()
    {
        return self::lists('name', 'id');
    }

    public function getImageFields()
    {
        return [
            'image' => 'sources/',
        ];
    }
}
