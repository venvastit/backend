<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 5/27/16
 * Time: 3:36 PM.
 */
namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MongoCategory extends Eloquent
{
    protected $collection = 'categories';

    const TYPE_VENUE = 1;
    const TYPE_EVENT = 2;
    const TYPE_DEAL = 3;
}
