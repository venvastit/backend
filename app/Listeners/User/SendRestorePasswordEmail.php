<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/30/16
 * Time: 12:14 AM
 */

namespace App\Listeners\User;


use App\Events\User\RestorePassword;
use Mail;

class SendRestorePasswordEmail
{

    /**
     * @var RestorePassword
     */
    protected $event;
    
    public function handle(RestorePassword $event)
    {
        $this->event = $event;
        $this->sendMail();
    }
    
    
    public function sendMail()
    {
        $user = $this->event->getUser();
        Mail::send('mails.restore_password', ['user' => $user], function($message) use ($user) {
            $message->from('info@venvast.com', 'VenVast team');
            $message->sender('info@venvast.com', 'VenVast team');
            $message->to($user->email);
            $message->subject("Reset your password at VenVast");
        });
    }
    
}