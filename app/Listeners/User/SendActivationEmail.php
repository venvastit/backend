<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/29/16
 * Time: 10:48 PM
 */

namespace App\Listeners\User;

use Mail;
use App\Events\User\UserRegistered;

class SendActivationEmail
{
    /**
     * @var UserRegistered
     */
    protected $event;


    
    public function handle(UserRegistered $userRegistered)
    {
        $this->event = $userRegistered;  
        $this->sendEmail();
    }
    
    public function sendEmail()
    {
        $user = $this->event->getUser();
        Mail::send('mails.activate', ['user' => $user], function($message) use ($user) {
            $message->from('info@venvast.com', 'VenVast team');
            $message->sender('info@venvast.com', 'VenVast team');
            $message->to($user->email);
            $message->subject("Activate your VenVast account!");
        });
    }
    
    

}