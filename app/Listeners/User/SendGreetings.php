<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/30/16
 * Time: 12:01 AM
 */

namespace App\Listeners\User;


use App\Events\User\Activated;
use Mail;

class SendGreetings
{

    /**
     * @var Activated
     */
    protected $event;


    /**
     * @param Activated $event
     */
    public function handle(Activated $event)
    {
        $this->event = $event;
        $this->sendEmail();
    }
    
    public function sendEmail()
    {
        $user = $this->event->getUser();
        Mail::send('mails.greetings', ['user' => $user], function($message) use ($user) {
            $message->from('info@venvast.com', 'VenVast team');
            $message->sender('info@venvast.com', 'VenVast team');
            $message->to($user->email);
            $message->subject("Successful registration at VenVast.com");
        });
    }
    
}