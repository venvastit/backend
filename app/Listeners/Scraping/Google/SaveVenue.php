<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 7/1/16
 * Time: 4:24 AM.
 */
namespace App\Listeners\Scraping\Google;

use App\Dictionary\Scraping\Google\PlaceType;
use App\Events\Venue\GotGoogleVenue;
use App\Models\Venue;
use Illuminate\Queue\Listener;

class SaveVenue
{
    public function handle(GotGoogleVenue $event)
    {
        $existingVenue = Venue::where('google_place_id', '=', $event->venueData['place_id'])->get()->first();
        if ($existingVenue) {
            return true;
        }
        $newVenue = new Venue();
        $newVenue->setAttribute('google_place_id', $event->venueData['place_id']);

        if (array_key_exists('formatted_address', $event->venueData)) {
            $newVenue->setAttribute('street', $event->venueData['formatted_address']);
        }
        $newVenue->setAttribute('name', $event->venueData['name']);

        if (array_key_exists('formatted_phone_number', $event->venueData)) {
            $newVenue->setAttribute('phone', $event->venueData['formatted_phone_number']);
        }
        if (array_key_exists('website', $event->venueData)) {
            $newVenue->setAttribute('website', $event->venueData['website']);
        }
        $this->setImage($newVenue, $event->venueData);
        $this->setLocation($newVenue, $event->venueData);
        $this->setBusinessHours($newVenue, $event->venueData);
        $this->setFromAddressComponents($newVenue, $event->venueData);
        $newVenue->save();
        echo "\nSaved: ".$newVenue->_id;

        return true;
    }

    public function setImage(Venue &$venue, array $venueData)
    {
        if (array_key_exists('photos', $venueData) && count($venueData['photos']) > 0) {
            $photo = $venueData['photos'][0];
            if (array_key_exists('photo_reference', $photo)) {
                $venue->setImage('cover_image', $this->getImageUrlByRef($photo['photo_reference']));
            }
        }
    }

    public function setLocation(Venue &$venue, array $venueData)
    {
        if (array_key_exists('geometry', $venueData) && is_array($venueData['geometry'])) {
            $geometry = $venueData['geometry'];
            if (array_key_exists('location', $geometry) && array_key_exists('lng', $geometry['location'])) {
                $venue->setAttribute('location', [
                    'type' => 'Point',
                    'coordinates' => [
                        (float) $geometry['location']['lng'],
                        (float) $geometry['location']['lat'],
                    ],
                ]);
            }
        }
    }

    public function setBusinessHours(Venue &$venue, array $venueData)
    {
        if (array_key_exists('opening_hours', $venueData) && array_key_exists('periods', $venueData['opening_hours'])) {
            $venue->setAttribute('businessHours', $venueData['opening_hours']['periods']);
        }
    }

    public function setCategory(Venue &$venue, array $venueData)
    {
        if (array_key_exists('types', $venueData) && count('types') > 0) {
            foreach ($venueData['types'] as $type) {
                $category = PlaceType::getCategory($type);
                if ($category) {
                    $venue->setAttribute('categories', [$category->_id]);

                    return;
                }
            }
        }
        $venue->setAttribute('categories', []);
    }

    public function setFromAddressComponents(Venue &$venue, array $venueData)
    {
        foreach ($venueData['address_components'] as $component) {
            foreach ($component['types'] as $typeName) {
                if ($typeName === 'postal_code') {
                    $venue->setAttribute('zip', $component['long_name']);
                } elseif ($typeName === 'country') {
                    $venue->setAttribute($typeName, $component['long_name']);
                } elseif ($typeName === 'locality') {
                    $venue->setAttribute('city', $component['long_name']);
                }
            }
        }
    }

    public function getImageUrlByRef($ref)
    {
        return 'https://maps.googleapis.com/maps/api/place/photo?photoreference='.
        $ref.'&key='.config('services.google.server-key');
    }
}
