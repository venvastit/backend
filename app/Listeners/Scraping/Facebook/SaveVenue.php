<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/20/16
 * Time: 7:25 AM.
 */
namespace App\Listeners\Scraping\Facebook;

use App\Events\EventModel\GotEventFbData;
use App\Events\EventModel\GotVenueFbData;
use App\Models\Event;
use App\Models\Venue;

class SaveVenue
{
    public function handle(GotVenueFbData $event)
    {
        if ($event->rawData === null) {
            return true;
        }
        $venueModel = null;
        if ($event->venueId !== null) {
            $venueModel = Venue::find($event->venueId);
        } else {
            $venueModel = Venue::where('id_on_source', '=', $event->rawData['id_on_source'])->get()->first();
        }
        if (!$venueModel) {
            $venueModel = new Venue();
        }
        foreach ($event->rawData as $key => $value) {
            if (in_array($key, ['categories', 'published', 'events']) === false) {
                $venueModel->setAttribute($key, $value);
            }
        }
        $venueModel->save();
        if (
            array_key_exists('events', $event->rawData)
            &&
            is_array($event->rawData['events'])
            &&
            array_key_exists('data', $event->rawData['events'])
            &&
            is_array($event->rawData['events']['data'])
        ) {
            foreach ($event->rawData['events']['data'] as $key => $eventArray) {
                if (array_key_exists('id', $eventArray)) {
                    $existingEvent = Event::where('id_on_source', '=', $eventArray['id'])->get()->first();
                    if ($existingEvent) {
                        $existingEvent->venue_id = $venueModel->_id;
                        $existingEvent->save();
                    } else {
                        $eventArray['venue_source_id'] = $event->rawData['id_on_source'];
                        event(new GotEventFbData($eventArray));
                    }
                }
            }
        }

        if ($event->eventId !== null) {
            $eventModel = Event::find($event->eventId);
            if ($eventModel) {
                $eventModel->setAttribute('venue_id', $venueModel->_id);
                $eventModel->save();
            }
        }

        return true;
    }
}
