<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/20/16
 * Time: 5:27 AM.
 */
namespace App\Listeners\Scraping\Facebook;

use App\Events\EventModel\EventSaved;
use App\Events\EventModel\EventWithoutVenueSaved;
use App\Events\EventModel\GotEventFbData;
use App\Models\Event;
use App\Models\Venue;
use Carbon\Carbon;

class UpdateEventWithFbData
{
    public function handle(GotEventFbData $event)
    {
        if ($event->rawData === null) {
            return true;
        }

        $eventModel = null;

        if ($event->eventId !== null) {
            $eventModel = Event::find($event->eventId);
        } else {
            $eventModel = Event::where('id_on_source', '=', $event->rawData['id_on_source'])->get()->first();
        }
        if (!$eventModel) {
            if (Carbon::now()->gt($event->rawData['start_at'])) {
                return true;
            }
            $eventModel = new Event();
        }
        foreach ($event->rawData as $key => $value) {
            if (in_array($key, ['categories', 'published']) === false) {
                $eventModel->setAttribute($key, $value);
            }
        }
        if (isset($eventModel->venue_source_id)) {
            $venue = Venue::where('id_on_source', '=', $eventModel->venue_source_id)->get()->first();
            if ($venue) {
                $eventModel->setAttribute('venue_id', $venue->_id);
                $eventModel->save();
            } else {
                $eventModel->save();
                event(new EventWithoutVenueSaved($eventModel->_id, $eventModel->venue_source_id));
            }
        }
        echo "\n Event: ".$eventModel->id_on_source.' '.$eventModel->title." saved! \n";

        return true;
    }
}
