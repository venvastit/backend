<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/20/16
 * Time: 5:27 AM.
 */
namespace App\Listeners\Scraping\Facebook;

use App\Dictionary\Scraping\Facebook\PageType;
use App\Events\EventModel\EventSaved;
use App\Events\EventModel\EventWithoutVenueSaved;
use App\Jobs\Scraping\Facebook\GetPageInfo;
use App\Models\Event;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SearchVenueForEvent
{
    use DispatchesJobs;

    /**
     * @var EventWithoutVenueSaved
     */
    protected $event;

    public function handle(EventWithoutVenueSaved $event)
    {
        $eventModel = Event::find($event->eventId);
        if (!$eventModel) {
            return true;
        }
        $this->event = $event;
        $this->startJob();
    }

    protected function startJob()
    {
        $job = new GetPageInfo(PageType::VENUE, $this->event->venueSourceId, null);
        $job->relatedId = $this->event->eventId;
        $this->dispatch($job);
    }
}
