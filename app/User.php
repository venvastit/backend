<?php

namespace App;

use App\Models\Event;
use App\Models\Venue;
use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use SleepingOwl\Models\SleepingOwlModel;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * App\User.
 */
class User extends Eloquent implements AuthenticatableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, CanResetPassword/*, EntrustUserTrait*/;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function save(array $options = [])
    {
        return parent::save($options);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function eventOrganizer()
    {
        return $this->belongsToMany(Event::class, 'events', 'organizers', 'event_ids');
    }

    public function venueOrganizer()
    {
        return $this->belongsToMany(Venue::class, 'venues', 'organizers');
    }
}
