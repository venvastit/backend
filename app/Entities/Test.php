<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 7/26/16
 * Time: 2:48 PM
 */

namespace App\Entities;

use App\Entities\Common\BaseEntity;
use App\Entities\Common\WithLocation;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Test
 * @package App\Entities
 *
 * @ODM\Document
 */
class Test extends BaseEntity
{
    
    use WithLocation;

    /**
     * @var string
     * 
     * @ODM\Field(type="string")
     */
    protected $name;

}