<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/31/16
 * Time: 10:52 AM
 */

namespace App\Entities\Common\Contact;

use App\Entities\Common\BaseEntity;
use App\Entities\Common\Contracts\AddressAware;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Contact
 * @package App\Entities\Common\Contact
 *
 * @ODM\Document
 */
class Contact extends BaseEntity implements AddressAware
{
    
    use WithAddress;

    /**
     * @ODM\Field(type="string")
     *
     * Email(required). Should be a valid email address.
     *
     * @var string
     */
    protected $email;

    /**
     * @ODM\Field(type="string")
     * 
     * Phone number(required). Should be a valid phone number
     * 
     * @var string
     */
    protected $phone;


    /**
     * @ODM\Field(type="string")
     * 
     * Website(optional). Should be a valid URL
     * 
     * @var string
     */
    protected $website;

    /**
     * @ODM\Field(type="string")
     * 
     * Additional information(optional, from 0 to 400 chars)
     * 
     * @var string
     */
    protected $additionalInfo;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite(string $website)
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getAdditionalInfo()
    {
        return $this->additionalInfo;
    }

    /**
     * @param string $additionalInfo
     */
    public function setAdditionalInfo(string $additionalInfo)
    {
        $this->additionalInfo = $additionalInfo;
    }
}