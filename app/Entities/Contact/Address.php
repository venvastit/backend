<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/31/16
 * Time: 10:45 AM
 */

namespace App\Entities\Common\Contact;
use App\Entities\Common\Contracts\LocationAware;
use App\Entities\Common\WithLocation;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Address
 * @package App\Entities\Common
 *
 * @ODM\EmbeddedDocument
 */
class Address implements LocationAware
{

    use WithLocation;

    /**
     * @ODM\Field(type="string")
     * 
     * House number(required). Max length is supposed to be 10 chars
     * 
     * @var string
     */
    protected $house;

    /**
     * @ODM\Field(type="string")
     *
     * Street address field(required)
     * 
     * @var string
     */
    protected $street;

    /**
     * @ODM\Field(type="string")
     *
     * District field(optional)
     * 
     * @var string
     */
    protected $district;

    /**
     * @ODM\Field(type="string")
     *
     * Sub district field(optional)
     * 
     * @var string
     */
    protected $subDistrict;

    /**
     * Get house number
     * 
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set house number
     * 
     * @param string $house
     */
    public function setHouse(string $house)
    {
        $this->house = $house;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param string $district
     */
    public function setDistrict(string $district)
    {
        $this->district = $district;
    }

    /**
     * @return string
     */
    public function getSubDistrict()
    {
        return $this->subDistrict;
    }

    /**
     * @param string $subDistrict
     */
    public function setSubDistrict(string $subDistrict)
    {
        $this->subDistrict = $subDistrict;
    }
    
}