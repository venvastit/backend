<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/31/16
 * Time: 11:07 AM
 */

namespace App\Entities\Common\Contact;

use App\Entities\Common\Location;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

trait WithAddress
{

    /**
     * @ODM\EmbedOne(targetDocument="App\Entities\Contact\Address")
     * 
     * @var Address
     */
    protected $address;

    /**
     * @return Address|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @param string $house
     */
    public function setHouse(string $house)
    {
        $this->address->setHouse($house);
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->getAddress()->getHouse();
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street)
    {
        $this->address->setStreet($street);
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->getAddress()->getStreet();
    }

    /**
     * @param string $district
     */
    public function setDistrict(string $district)
    {
        $this->address->setDistrict($district);
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->getAddress()->getDistrict();
    }

    /**
     * @param string $district
     */
    public function setSubDistrict(string $district)
    {
        $this->address->setSubDistrict($district);
    }

    /**
     * @return string
     */
    public function getSubDistrict()
    {
        return $this->getAddress()->getSubDistrict();
    }

    /**
     * @param Location $location
     */
    public function setLocation(Location $location)
    {
        $this->address->setLocation($location);
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->getAddress()->getLocation();
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat)
    {
        $this->address->getLocation()->setLat($lat);
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->getAddress()->getLocation()->getLat();
    }

    /**
     * @param float $lng
     */
    public function setLng(float $lng)
    {
        $this->getAddress()->getLocation()->setLng($lng);
    }

    /**
     * @return float
     */
    public function getLng()
    {
        return $this->address->getLocation()->getLng();
    }
    
    
}