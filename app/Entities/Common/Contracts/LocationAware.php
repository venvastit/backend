<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/31/16
 * Time: 10:38 AM
 */

namespace App\Entities\Common\Contracts;


use App\Entities\Common\Location;

interface LocationAware
{

    /**
     * @return Location|null
     * 
     * Get Location embed entity
     */
    public function getLocation();

    /**
     * Set location embed entity
     * 
     * @param Location $location
     * @return mixed
     */
    public function setLocation(Location $location);

    /**
     * Get latitude from location embed document
     * 
     * @return float
     */
    public function getLat();

    /**
     * Get longitude from location embed document
     * 
     * @return float
     */
    public function getLng();

    /**
     * Set latitude for location embed document
     * 
     * @param float $lat
     * @return float|null
     */
    public function setLat(float $lat);

    /**
     * Set longitude of location embed document
     * @param float $lng
     * @return float|null
     */
    public function setLng(float $lng);
    

}