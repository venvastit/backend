<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 8/31/16
 * Time: 11:00 AM
 */

namespace App\Entities\Common\Contracts;


use App\Entities\Common\Contact\Address;

interface AddressAware extends LocationAware
{

    /**
     * @return Address|null
     */
    public function getAddress();

    /**
     * @param Address $address
     * 
     * Set address embed entity
     * 
     * @void
     */
    public function setAddress(Address $address);

    /**
     * @return string|null
     */
    public function getHouse();

    /**
     * @param string $house
     * @void
     */
    public function setHouse(string $house);

    /**
     * @return string|null
     */
    public function getStreet();

    /**
     * @param string $street
     * @void
     */
    public function setStreet(string $street);

    /**
     * @return string|null
     */
    public function getDistrict();

    /**
     * @param string $district
     * @void
     */
    public function setDistrict(string $district);

    /**
     * @return string|null
     */
    public function getSubDistrict();

    /**
     * @param string $subDistrict
     * @void
     */
    public function setSubDistrict(string $subDistrict);
    
}