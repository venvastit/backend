<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 7/26/16
 * Time: 4:56 PM
 */

namespace App\Entities\Common;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Location
 * @package App\Entities\Common
 *
 * @ODM\EmbeddedDocument
 */
class Location
{

    /**
     * @ODM\Field(type="string")
     *
     * @var string
     */
    protected $type;

    /**
     * @ODM\Field(type="collection")
     *
     * @var array
     */
    protected $coordinates;


    public function __construct($lat, $lng)
    {
        $this->coordinates = new ArrayCollection([$lng, $lat]);
        $this->type = 'Point';
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param mixed $coordinates
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat)
    {
        $this->coordinates[1] = $lat;
    }

    /**
     * @param float $lng
     */
    public function setLng(float $lng)
    {
        $this->coordinates[0] = $lng;
    }

    /**
     * @return null|float
     */
    public function getLat()
    {
        if(!array_key_exists(1, $this->coordinates)) {
            return null;
        }
        return $this->coordinates[1];
    }

    /**
     * @return null|float
     */
    public function getLng()
    {
        if(!array_key_exists(0, $this->coordinates)) {
            return null;
        }
        return $this->coordinates[0];
    }


}