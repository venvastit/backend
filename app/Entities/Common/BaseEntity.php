<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 7/26/16
 * Time: 4:36 PM
 */

namespace App\Entities\Common;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class BaseEntity
 * @package App\Entities\Common
 *
 * @ODM\MappedSuperclass
 */
abstract class BaseEntity
{

    /**
     * @ODM\Id
     *
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    
}