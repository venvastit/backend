<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 7/26/16
 * Time: 5:00 PM
 */

namespace App\Entities\Common;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

trait WithLocation
{

    /** 
     * @ODM\EmbedOne(targetDocument="App\Entities\Common\Location") 
     */
    protected $location;

    
    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
    }


    /**
     * @param float $lat
     */
    public function setLat(float $lat)
    {
        $this->getLocation()->setLat($lat);
    }

    /**
     * @param float $lng
     */
    public function setLng(float $lng)
    {
        $this->getLocation()->setLng($lng);
    }

    /**
     * @return null|float
     */
    public function getLat()
    {
        return $this->getLocation()->getLat();
    }

    /**
     * @return null|float
     */
    public function getLng()
    {
        return $this->getLocation()->getLng();
    }
    
    

}