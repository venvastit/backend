<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Category;
use App\Models\Event;
use App\Models\Source;
use App\Models\Venue;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Predis\Connection\ConnectionException;
use Redis;

class MigrateFromRedisToSQL extends Job implements SelfHandling/*, ShouldQueue*/
{
    //    use InteractsWithQueue;

    protected $access_token;
    protected $client;

    /**
     * Create a new job instance.
     *
     * @param $access_token
     */
    public function __construct($access_token)
    {
        $this->client = new Client();
        $this->access_token = $access_token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $keys = Redis::command('keys', ['facebook:events:*']);
        foreach ($keys as $key) {
            try {
                $data = Redis::get($key);
            } catch (ConnectionException $e) {
                \Log::alert($key.' loading failed');
                continue;
            }

            $event = json_decode($data, true);
            if (!array_key_exists('eid', $event)) {
                $event['eid'] = $event['id'];
            }
            if (array_key_exists('place', $event) && !array_key_exists('venue', $event)) {
                $event['venue'] = $event['place'];
                if (array_key_exists('location', $event['place'])) {
                    foreach ($event['place']['location'] as $lKey => $lValue) {
                        $event['venue'][$lKey] = $lValue;
                    }
                }
            }
            if (!array_key_exists('venue', $event)) {
                continue;
            }
            $venue = $event['venue'];
            if (!array_key_exists('id', $venue)) {
                continue;
            }
            try {
                $venueData = Redis::get('facebook:venues:'.$venue['id']);
            } catch (ConnectionException $e) {
                \Log::alert('facebook:venues:'.$venue['id'].' loading failed');
                continue;
            }
            $venueModel = json_decode($venueData, true);
            if (array_key_exists('location', $venueModel)) {
                foreach ($venueModel['location'] as $lKey => $lValue) {
                    $venueModel[$lKey] = $lValue;
                }
            }
            $dates = new Carbon($event['start_time']);
            $endAt = array_key_exists('end_time', $event) ? new Carbon($event['end_time']) : null;

            $categories = null;

            $venueEloquent = null;
            $existingVenue = Venue::where('id_on_source', $venueModel['id'])->get()->first();
            switch (array_key_exists('id', $venueModel)) {
                case true:
                    if (!$existingVenue) {
                        $category_ids = [];
                        if ($categories) {
                            $category_ids = $this->setUpCategories($categories);
                        }

                        if (!array_key_exists('location', $venueModel)) {
                            continue;
                        }

                        if (!array_key_exists('latitude', $venueModel['location']) || !array_key_exists('longitude', $venueModel['location'])) {
                            continue;
                        }

                        $venueEloquent = $this->createVenue($venueModel);

                        foreach ($category_ids as $category_id) {
                            $venueEloquent->categories()->attach($category_id);
                        }
                    } else {
                        $venueEloquent = $existingVenue;
                    }
                    break;
                case false:
                    break;
            }
            $existingEvent = Event::where('original_id', $event['eid'])->get()->first();
            if (!$existingEvent && $venueEloquent !== null) {
                $eventEloquent = $this->createEvent($event, $venue, $venueEloquent, $dates, $endAt);
                $category_ids = $this->setUpCategories($categories);
                foreach ($category_ids as $category_id) {
                    if ($eventEloquent !== null) {
                        $eventEloquent->categories()->attach($category_id);
                    }
                }
            }
        }

        \Log::info('Done');
    }

    private function findParentCategories(array $category, array $all = [])
    {
        $category = $this->requestCategory($category['id']);
        if (!$category) {
            return $all;
        }

        $all[] = $category;
        if (array_key_exists('parent_ids', $category) && !empty($category['parent_ids'])) {
            return $this->findParentCategories(['id' => $category['parent_ids'][0]], $all);
        }

        return $all;
    }

    /**
     * @param string $token access token to access facebook
     * @param string|int $categoryId
     * @return array|null
     */
    private function requestCategory($categoryId)
    {
        $fields = ['id', 'name','parent_ids'];
        try {
            $res = $this->client->request('GET', 'https://graph.facebook.com/'.$categoryId.'?fields='.implode(',', $fields).'&access_token='.$this->access_token);
            $response = json_decode($res->getBody(), true);
            if (!empty($response) && array_key_exists('id', $response)) {
                return $response;
            }
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody()->getContents();
            if (is_string($body)) {
                $body = json_decode($body, true);
            }
            if (intval($body['error']['code']) === 100) {
                return;
            }
        }

        return;
    }

    private function buildCategoryTree(array $categories)
    {
        for ($i = 0; $i < count($categories) - 1; $i++) {
            for ($j = 1; $j < count($categories); $j++) {
                /** @var Category $existing */
                $existing = Category::where('name', $categories[$j]['name'])
                    ->where('source_id', Source::FACEBOOK)
                    ->where('foreign_id', $categories[$j]['id'])
                    ->get()->first();

                if ($existing) {
                    $existingChild = Category::where('name', $categories[$i]['name'])
                        ->where('source_id', Source::FACEBOOK)
                        ->where('foreign_id', $categories[$i]['id'])
                        ->get()->first();
                    if ($existingChild) {
                        return $existing;
                    }
                    $existing->children()->create($this->convertCategory($categories[$i]));
                }
            }
        }
    }

    /**
     * @param array $category
     * @return array
     */
    private function convertCategory(array $category)
    {
        return [
            'foreign_id' => $category['id'],
            'source_id' => Source::FACEBOOK,
            'name' => $category['name'],
        ];
    }

    /**
     * @param array $categoryList
     * @return array
     */
    private function setUpCategories(array $categoryList)
    {
        if ($categoryList === null) {
            return [];
        }
        $category_ids = [];
        foreach ($categoryList as $category) {
            $existing = Category::where('name', $category['name'])->where('source_id', Source::FACEBOOK)->get()->first();
            if (!$existing) {
                $categoryEloquent = Category::create([
                    'foreign_id' => $category['id'],
                    'source_id' => Source::FACEBOOK,
                    'name' => $category['name'],
                ]);
                $category_ids[] = $categoryEloquent->id;
                $all = $this->findParentCategories($category);
                $this->buildCategoryTree($all);
            } else {
                $category_ids[] = $existing->id;
            }
        }

        return $category_ids;
    }

    private function createEvent($event, $venue, $venueEloquent, $dates, $endAt)
    {
        $address = '';

        if (array_key_exists('country', $venue)) {
            $address .= $venue['country'];
        }
        if (array_key_exists('city', $venue)) {
            $address .= ', '.$venue['city'];
        }
        if (array_key_exists('street', $venue)) {
            $address .= ', '.$venue['street'];
        }
        if (array_key_exists('zip', $venue)) {
            $address .= ', '.$venue['zip'];
        }

        if (array_key_exists('description', $event)) {
            if (array_key_exists('pic_cover', $event)) {
                $cover = $event['pic_cover']['source'];
            } elseif (array_key_exists('cover', $event)) {
                $cover = $event['cover']['source'];
            } else {
                return;
            }

            $eventEloquent = Event::create([
                'title' => $event['name'],
                'original_id' => $event['eid'],
                'picture' => $cover,
                'start_date' => $dates->toDateString(),
                'start_time' => $dates->toTimeString(),
                'duration' => $endAt ? $endAt->diffInMinutes($dates) : 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'address' => $address,
                'latitude' => $venue['latitude'],
                'longitude' => $venue['longitude'],
                'source_id' => Source::FACEBOOK,
                'venue_id' => $venueEloquent->id,
                'details' => $event['description'],
            ]);

            return $eventEloquent;
        } else {
            return;
        }
    }

    public function createVenue($venueModel)
    {
        $venueEloquent = Venue::create([
            'name' => $venueModel['name'],
            'id_on_source' => array_key_exists('id', $venueModel) ? $venueModel['id'] : null,
            'birthday' => array_key_exists('birthday', $venueModel) ? new Carbon($venueModel['birthday']) : null,
            'cover_image' => array_key_exists('cover', $venueModel) ? $venueModel['cover']['source'] : null,
            'source_id' => Source::FACEBOOK,
            'description' => array_key_exists('description', $venueModel) ? $venueModel['description'] : null,
            'general_info' => array_key_exists('general_info', $venueModel) ? $venueModel['general_info'] : null,
            'latitude' => array_key_exists('location', $venueModel) ? $venueModel['location']['latitude'] : null,
            'longitude' => array_key_exists('location', $venueModel) ? $venueModel['location']['longitude'] : null,
            'street' => array_key_exists('location', $venueModel) && array_key_exists('street', $venueModel['location']) ? $venueModel['location']['street'] : null,
            'zip' => array_key_exists('location', $venueModel) && array_key_exists('zip', $venueModel['location']) ? $venueModel['location']['zip'] : null,
            'phone' => array_key_exists('phone', $venueModel) ? $venueModel['phone'] : null,
            'website' => array_key_exists('website', $venueModel) ? $venueModel['website'] : null,
            'public_transit' => array_key_exists('public_transit', $venueModel) ? $venueModel['public_transit'] : null,
        ]);

        return $venueEloquent;
    }
}
