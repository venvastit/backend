<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Category;
use App\Models\Source;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Contracts\Bus\SelfHandling;
use LogicException;

class RebuildCategories extends Job implements SelfHandling
{
    /**
     * @var Client
     */
    private $client;

    /**
     * Facebook access toke.
     * @var string
     */
    private $access_token;

    public function __construct($access_token)
    {
        $this->client = new Client();
        $this->access_token = $access_token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $categories = Category::all();
        /** @var Category $category */
        foreach ($categories as $category) {
            if ($this->requestCategory($category['foreign_id']) === null) {
                continue;
            }
            $cat = $this->requestCategory($category['foreign_id']);
            if (array_key_exists('parent_ids', $cat) && is_array($cat['parent_ids']) && !empty($cat['parent_ids'])) {
                /** @var Category $parent */
                $parent = Category::where('foreign_id', $cat['parent_ids'][0])->get()->first();
                if (!$parent) {
                    $parent = Category::create([
                        'foreign_id' => $cat['id'],
                        'name' => $cat['name'],
                        'source_id' => Source::FACEBOOK,
                    ]);
                }
                try {
                    $parent->appendNode($category);
                    $category->appendToNode($parent);
                    echo "\n Node ".$category->id.' appended to '.$parent->id;
                } catch (LogicException $e) {
                    echo "\n Catch exception while appending node ".$category->id.' to '.$parent->id;
                    continue;
                }
            }
        }
    }

    /**
     * @param string $token access token to access facebook
     * @param string|int $categoryId
     * @return array|null
     */
    private function requestCategory($categoryId)
    {
        $fields = ['id', 'name','parent_ids'];
        try {
            $res = $this->client->request('GET', 'https://graph.facebook.com/'.$categoryId.'?fields='.implode(',', $fields).'&access_token='.$this->access_token);
            $response = json_decode($res->getBody(), true);
            if (!empty($response) && array_key_exists('id', $response)) {
                return $response;
            }
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody()->getContents();
            if (is_string($body)) {
                $body = json_decode($body, true);
            }
            if (intval($body['error']['code']) === 100) {
                return;
            }
        }

        return;
    }
}
