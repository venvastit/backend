<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/20/16
 * Time: 12:47 PM.
 */
namespace App\Jobs\Migrate;

use App\Dictionary\CategoryType;
use App\Dictionary\Scraping\Facebook\PageType;
use App\Jobs\Job;
use App\Jobs\Scraping\Facebook\GetPageInfo;
use App\Models\Category;
use App\Models\Venue;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use MongoDB\Driver\Exception\BulkWriteException;

class Venues extends Job implements SelfHandling
{
    use DispatchesJobs;

    protected $venues;

    public function __construct()
    {
        $this->venues = \DB::connection('mysql')->table('venues')->get();
    }

    public function handle()
    {
        foreach ($this->venues as $venue) {
            /** @var  array $venue */
            $venue = (array) $venue;
            $venue['old_id'] = $venue['id'];
            unset($venue['id']);
            unset($venue['birthday']);
            unset($venue['source_id']);
            $venue['location'] = $this->getPosition($venue);
            unset($venue['latitude']);
            unset($venue['longitude']);
            $venue['published'] = false;
            $categoryId = $this->getCategoryId($venue);
            $this->migrateCategory($categoryId, $venue);
            /* @var Venue $venue */
            try {
                $venue = Venue::create($venue);
                if ($venue->id_on_source) {
                    $this->dispatch(new GetPageInfo(PageType::VENUE, $venue->id_on_source, $venue->_id));
                }
            } catch (BulkWriteException $e) {
                continue;
            }
        }

        return true;
    }

    protected function getPosition(array $venue)
    {
        if (array_key_exists('longitude', $venue) && array_key_exists('latitude', $venue)) {
            return [
                'type' => 'Point',
                'coordinates' => [
                    floatval($venue['longitude']),
                    floatval($venue['latitude']),
                ],
            ];
        }
    }

    public function getCategoryId($venue)
    {
        $venueId = $venue['old_id'];
        $relations = \DB::connection('mysql')->table('venues_categories')->where('venue_id', '=', $venueId)->get();
        if (count($relations) > 0) {
            $relations[0] = (array) $relations[0];

            return $relations[0]['category_id'];
        }
    }

    public function migrateCategory($oldId, &$venue)
    {
        $migrated = Category::where('old_id', '=', $oldId)->where('type', '=', CategoryType::TYPE_VENUE)->get()->first();
        if ($migrated) {
            $venue['categories'][] = $migrated->_id;

            return;
        } else {
            $categoryData = \DB::connection('mysql')->table('categories')->where('id', '=', $oldId)->get();
            if (count($categoryData) > 0) {
                $categoryData = (array) $categoryData[0];
                $categoryData['old_id'] = $categoryData['id'];
                unset($categoryData['id']);
                $categoryData['type'] = CategoryType::TYPE_VENUE;
                $category = Category::create($categoryData);
                $venue['categories'][] = $category->_id;

                return;
            }
        }

        return;
    }
}
