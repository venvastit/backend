<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/20/16
 * Time: 11:53 AM.
 */
namespace App\Jobs\Migrate;

use App\Dictionary\CategoryType;
use App\Dictionary\Scraping\Facebook\PageType;
use App\Jobs\Job;
use App\Jobs\Scraping\Facebook\GetPageInfo;
use App\Models\Category;
use App\Models\Event;
use App\Models\Venue;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Events extends Job implements SelfHandling
{
    use DispatchesJobs;

    /**
     * @var array
     */
    protected $events;

    public function __construct()
    {
        $this->events = \DB::connection('mysql')->table('events')->get();
    }

    public function handle()
    {
        /** @var array $event */
        foreach ($this->events as $event) {
            $event = (array) $event;
            $event['location'] = $this->getPosition($event);
            unset($event['latitude']);
            unset($event['longitude']);
            $event['old_id'] = $event['id'];
            unset($event['id']);
            $venueId = $event['venue_id'];
            unset($event['venue_id']);
            $venue = Venue::where('old_id', '=', $venueId)->get()->first();
            if (!empty($venue)) {
                $event['venue_id'] = $venue->_id;
            }
            $event['id_on_source'] = $event['original_id'];
            $event['start_at'] = $this->getStartAt($event);
            $event['end_at'] = $this->getEndAt($event);
            $event['organizers'] = [];
            $event['attendees'] = [];
            unset($event['original_id']);
            unset($event['duration']);
            unset($event['start_date']);
            unset($event['start_time']);
            unset($event['source_id']);

            $categoryId = $this->getCategoryId($event);
            $this->migrateCategory($categoryId, $event);
            $event['published'] = false;
            /** @var Event $event */
            $event = Event::create($event);
            if ($event->id_on_source) {
                $this->dispatch(new GetPageInfo(PageType::EVENT, $event->id_on_source, $event->_id));
            }
        }

        return true;
    }

    /**
     * @param array $event
     * @return array
     */
    public function getPosition(array $event)
    {
        if (array_key_exists('longitude', $event) && array_key_exists('latitude', $event)) {
            return [
                'type' => 'Point',
                'coordinates' => [
                    floatval($event['longitude']),
                    floatval($event['latitude']),
                ],
            ];
        }
    }

    /**
     * @param array $event
     * @return Carbon
     */
    public function getStartAt(array $event)
    {
        $startString = $event['start_date'].' '.$event['start_time'];
        $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $startString);

        return $startDate;
    }

    public function getEndAt(array $event)
    {
        $startDate = $this->getStartAt($event);

        return $startDate->addMinutes(intval($event['duration']));
    }

    public function getCategoryId($event)
    {
        $eventId = $event['old_id'];
        $relations = \DB::connection('mysql')->table('events_event_categories')->where('event_id', '=', $eventId)->get();
        if (count($relations) > 0) {
            $relations[0] = (array) $relations[0];

            return $relations[0]['category_id'];
        }
    }

    public function migrateCategory($oldId, &$event)
    {
        $migrated = Category::where('old_id', '=', $oldId)->where('type', '=', CategoryType::TYPE_EVENT)->get()->first();
        if ($migrated) {
            $event['categories'][] = $migrated->_id;

            return;
        } else {
            $categoryData = \DB::connection('mysql')->table('event_categories')->where('id', '=', $oldId)->get();
            if (count($categoryData) > 0) {
                $categoryData = (array) $categoryData[0];
                $categoryData['old_id'] = $categoryData['id'];
                unset($categoryData['id']);
                $categoryData['type'] = CategoryType::TYPE_EVENT;
                $category = Category::create($categoryData);
                $event['categories'][] = $category->_id;

                return;
            }
        }

        return;
    }
}
