<?php

namespace App\Jobs;

use App\Models\Category;
use App\Jobs\Job;
use App\Models\Event;
use App\Models\Source;
use App\Models\Venue;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Contracts\Bus\SelfHandling;
use Predis\Connection\ConnectionException;

class MigrateVenues extends Job implements SelfHandling/*, ShouldQueue*/
{
    protected $access_token;
    protected $client;

    /**
     * Create a new job instance.
     *
     * @param $access_token
     */
    public function __construct($access_token)
    {
        dd(1);
        $this->client = new Client();
        $this->access_token = $access_token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $keys = Redis::command('keys', ['facebook:venues:*']);
        foreach ($keys as $key) {
            try {
                $data = Redis::get($key);
            } catch (ConnectionException $e) {
                \Log::alert($key.' loading failed');
                continue;
            }

            $venue = json_decode($data, true);
            if (!array_key_exists('id', $venue)) {
                continue;
            }
            if (!array_key_exists('location', $venue)) {
                continue;
            } else {
                foreach ($venue['location'] as $lKey => $lValue) {
                    $venue[$lKey] = $lValue;
                }
            }
            $venueModel = json_decode($venueData, true);
            if (array_key_exists('location', $venueModel)) {
                foreach ($venueModel['location'] as $lKey => $lValue) {
                    $venueModel[$lKey] = $lValue;
                }
            }
            $categories = $venue['category_list'];
            $existingVenue = Venue::where('id_on_source', $venueModel['id'])->get()->first();
            if (!$existingVenue) {
                $category_ids = $this->setUpCategories($categories);
                $venueEloquent = $this->createVenue($venueModel);
                foreach ($category_ids as $category_id) {
                    $venueEloquent->categories()->attach($category_id);
                }
            } else {
                $venueEloquent = $existingVenue;
            }
        }
        \Log::info('Done');
    }

    private function findParentCategories(array $category, array $all = [])
    {
        $category = $this->requestCategory($category['id']);
        if (!$category) {
            return $all;
        }

        $all[] = $category;
        if (array_key_exists('parent_ids', $category) && !empty($category['parent_ids'])) {
            return $this->findParentCategories(['id' => $category['parent_ids'][0]], $all);
        }

        return $all;
    }

    /**
     * @param string $token access token to access facebook
     * @param string|int $categoryId
     * @return array|null
     */
    private function requestCategory($categoryId)
    {
        $fields = ['id', 'name','parent_ids'];
        try {
            $res = $this->client->request('GET', 'https://graph.facebook.com/'.$categoryId.'?fields='.implode(',', $fields).'&access_token='.$this->access_token);
            $response = json_decode($res->getBody(), true);
            if (!empty($response) && array_key_exists('id', $response)) {
                return $response;
            }
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody()->getContents();
            if (is_string($body)) {
                $body = json_decode($body, true);
            }
            if (intval($body['error']['code']) === 100) {
                return;
            }
        }

        return;
    }

    private function buildCategoryTree(array $categories)
    {
        for ($i = 0; $i < count($categories) - 1; $i++) {
            for ($j = 1; $j < count($categories); $j++) {
                /** @var Category $existing */
                $existing = Category::where('name', $categories[$j]['name'])
                    ->where('source_id', Source::FACEBOOK)
                    ->where('foreign_id', $categories[$j]['id'])
                    ->get()->first();

                if ($existing) {
                    $existingChild = Category::where('name', $categories[$i]['name'])
                        ->where('source_id', Source::FACEBOOK)
                        ->where('foreign_id', $categories[$i]['id'])
                        ->get()->first();
                    if ($existingChild) {
                        return $existing;
                    }
                    $existing->children()->create($this->convertCategory($categories[$i]));
                }
            }
        }
    }

    /**
     * @param array $category
     * @return array
     */
    private function convertCategory(array $category)
    {
        return [
            'foreign_id' => $category['id'],
            'source_id' => Source::FACEBOOK,
            'name' => $category['name'],
        ];
    }

    /**
     * @param array $categoryList
     * @return array
     */
    private function setUpCategories(array $categoryList)
    {
        $category_ids = [];
        foreach ($categoryList as $category) {
            $existing = Category::where('name', $category['name'])->where('source_id', Source::FACEBOOK)->get()->first();
            if (!$existing) {
                $categoryEloquent = Category::create([
                    'foreign_id' => $category['id'],
                    'source_id' => Source::FACEBOOK,
                    'name' => $category['name'],
                ]);
                $category_ids[] = $categoryEloquent->id;
                $all = $this->findParentCategories($category);
                $this->buildCategoryTree($all);
            } else {
                $category_ids[] = $existing->id;
            }
        }

        return $category_ids;
    }

    public function createVenue($venueModel)
    {
        $venueEloquent = Venue::create([
            'name' => $venueModel['name'],
            'id_on_source' => array_key_exists('id', $venueModel) ? $venueModel['id'] : null,
            'birthday' => array_key_exists('birthday', $venueModel) ? new Carbon($venueModel['birthday']) : null,
            'cover_image' => array_key_exists('cover', $venueModel) ? $venueModel['cover']['source'] : null,
            'source_id' => Source::FACEBOOK,
            'description' => array_key_exists('description', $venueModel) ? $venueModel['description'] : null,
            'general_info' => array_key_exists('general_info', $venueModel) ? $venueModel['general_info'] : null,
            'latitude' => array_key_exists('location', $venueModel) ? $venueModel['location']['latitude'] : null,
            'longitude' => array_key_exists('location', $venueModel) ? $venueModel['location']['longitude'] : null,
            'street' => array_key_exists('location', $venueModel) && array_key_exists('street', $venueModel['location']) ? $venueModel['location']['street'] : null,
            'zip' => array_key_exists('location', $venueModel) && array_key_exists('zip', $venueModel['location']) ? $venueModel['location']['zip'] : null,
            'phone' => array_key_exists('phone', $venueModel) ? $venueModel['phone'] : null,
            'website' => array_key_exists('website', $venueModel) ? $venueModel['website'] : null,
            'public_transit' => array_key_exists('public_transit', $venueModel) ? $venueModel['public_transit'] : null,
        ]);

        return $venueEloquent;
    }
}
