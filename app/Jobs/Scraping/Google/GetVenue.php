<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 7/1/16
 * Time: 4:13 AM.
 */
namespace App\Jobs\Scraping\Google;

use App\Events\Venue\GotGoogleVenue;
use App\Jobs\Job;
use App\Models\Venue;
use GuzzleHttp\Client;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;

class GetVenue extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, DispatchesJobs;

    protected $id;

    protected $key;

    public function __construct($id)
    {
        $this->id = $id;
        $this->key = config('services.google.server-key');
    }

    public function handle()
    {
        if (Venue::where('google_place_id', '=', $this->id)->get()->first()) {
            $this->delete();

            return true;
        }
        if ($this->attempts() > 3) {
            $this->delete();

            return true;
        }
        $response = \GuzzleHttp\json_decode($this->getClient()->get($this->getQuery())->getBody()->getContents(), true);
        if (array_key_exists('result', $response)) {
            event(new GotGoogleVenue($response['result']));
        } else {
            print_r($response);
        }
        $this->delete();
    }

    public function getQuery()
    {
        return 'https://maps.googleapis.com/maps/api/place/details/json?placeid='.$this->id.'&key='.$this->key;
    }

    public function getClient()
    {
        return new Client();
    }
}
