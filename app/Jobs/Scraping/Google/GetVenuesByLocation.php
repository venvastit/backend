<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/30/16
 * Time: 11:43 PM.
 */
namespace App\Jobs\Scraping\Google;

use App\Dictionary\Scraping\Google\PlaceType;
use App\Jobs\Job;
use GuzzleHttp\Client;
use Illuminate\Contracts\Bus\SelfHandling;
use GooglePlaces;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;

class GetVenuesByLocation extends Job implements SelfHandling, ShouldQueue
{
    use DispatchesJobs, InteractsWithQueue;

    protected $venues;

    protected $location;

    protected $lat;

    protected $lng;

    protected $pageToken;

    protected $radius = 22000;

    public function __construct($lat, $lng, $radius = null, $pageToken = null)
    {
        $this->location = $lat.','.$lng;
        $this->lat = $lat;
        $this->lng = $lng;
        if ($radius) {
            $this->radius = $radius;
        }
        if ($pageToken) {
            $this->pageToken = $pageToken;
        }
    }

    public function handle()
    {
        if ($this->attempts() > 3) {
            $this->delete();
        }
        if ($this->pageToken) {
            $this->getPageByRef($this->pageToken);
        } else {
            $this->getPageByLocation();
        }
    }

    public function getPageByLocation()
    {
        $locations = $this->buildLocationsArray();
        foreach ($locations as $location) {
            $types = PlaceType::getTypes();
            foreach ($types as $type) {
                $place = GooglePlaces::nearbySearch($location, $this->radius, [
                    'types' => $type,
                ]);
                $items = $place->all();
                $results = $items['results']->all();
                foreach ($results as $result) {
                    $this->dispatch((new GetVenue($result['place_id']))->onQueue('scrap_item'));
                }
                if (array_key_exists('next_page_token', $items) && !empty($items['next_page_token']) && $items['next_page_token'] != 0) {
                    $this->dispatch(new self($this->lat, $this->lng, $this->radius, $items['next_page_token']));
                }
            }
        }
        $this->delete();
    }

    public function getPageByRef($reference)
    {
        if ($reference) {
            $place = GooglePlaces::nearbySearch($this->location, $this->radius, [
                'page_token' => $reference,
//                'types' => implode('|', array_rand(shuffle(PlaceType::getTypes()), 4)),
            ]);
            $items = $place->all();
            $collection = $items['results']->all();
            foreach ($collection as $result) {
                $this->dispatch((new GetVenue($result['place_id']))->onQueue('scrap_item'));
            }

            if (array_key_exists('next_page_token', $items) && $items['next_page_token'] && $items['next_page_token'] != 0) {
                $this->dispatch(new self($this->lat, $this->lng, $this->radius, $items['next_page_token']));
            }
            $this->delete();
        }
    }

    public function buildLocationsArray()
    {
        $defaultLat = $this->lat;
        $defaultLng = $this->lng;
        $resultArray = [];
        for ($i = 0; $i < 20; $i++) {
            $resultArray[] = ((float) $defaultLat + 0.01).','.((float) $defaultLng + 0.01);
        }
        $defaultLat = $this->lat;
        $defaultLng = $this->lng;
        for ($i = 0; $i < 20; $i++) {
            $resultArray[] = ((float) $defaultLat + 0.01).','.((float) $defaultLng - 0.01);
        }
        $defaultLat = $this->lat;
        $defaultLng = $this->lng;
        for ($i = 0; $i < 20; $i++) {
            $resultArray[] = ((float) $defaultLat - 0.01).','.((float) $defaultLng + 0.01);
        }
        $defaultLat = $this->lat;
        $defaultLng = $this->lng;
        for ($i = 0; $i < 20; $i++) {
            $resultArray[] = ((float) $defaultLat - 0.01).','.((float) $defaultLng - 0.01);
        }

        return $resultArray;
    }
}
