<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/20/16
 * Time: 2:23 AM.
 */
namespace App\Jobs\Scraping\Facebook;

use App\Dictionary\Scraping\Facebook\PageType;
use App\Events\EventModel\GotEventFbData;
use App\Events\EventModel\GotVenueFbData;
use App\Jobs\Job;
use App\Models\Event;
use App\Models\Venue;
use App\ServiceProviders\FacebookServiceProvider;
use App\User;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class GetPageInfo extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue;

    protected $type;

    protected $pageId;

    protected $objectId;

    /**
     * @var FacebookServiceProvider
     */
    protected $fbServiceProvider;

    protected $access_token;

    public $relatedId;

    public function __construct(int $type, string $pageId, $objectId = null)
    {
        $this->type = $type;
        $this->pageId = $pageId;
        $this->objectId = $objectId;
        $user = \Auth::user();
        if ($user && \Session::has('fb_user_access_token')) {
            $this->access_token = \Session::get('fb_user_access_token');
        }
    }

    public function handle()
    {
        if (!isset($this->access_token)) {
            return true;
        }
        $this->initServiceProvider();
        if (intval($this->type) === PageType::EVENT) {
            if (! $evt = Event::where('id_on_source', '=', $this->pageId)->get()->first()) {
                $data = $this->fbServiceProvider->getEvent($this->pageId);
                event(new GotEventFbData($data, $this->objectId));
            } else {
                if(!$evt->venue_id || empty($evt->venue_id) || !is_string($evt->venue_id)) {
                    if($evt->venue_source_id) {
                        $venue = Venue::where('id_on_source', '=', $evt->venue_source_id)->get()->first();
                        if($venue) {
                            $evt->venue_id = $venue->_id;
                            $evt->save();
                            return true;
                        } else {
                            $data = $this->fbServiceProvider->getVenue($evt->venue_source_id);
                            event(new GotVenueFbData($data, null, $evt->_id));
                            return true;
                        }
                    }
                }
            }
        } elseif (intval($this->type) === PageType::VENUE) {
            if (!Venue::where('id_on_source', '=', $this->pageId)->get()->first()) {
                $data = $this->fbServiceProvider->getVenue($this->pageId);
                event(new GotVenueFbData($data, $this->objectId, $this->relatedId));
            }
        }
        $this->delete();

        return true;
    }

    protected function getServiceProvider($access_token)
    {
        if (!isset($this->fbServiceProvider)) {
            $this->fbServiceProvider = new FacebookServiceProvider($access_token);
        }

        return $this->fbServiceProvider;
    }


    /**
     * Initialize facebook service provider
     */
    protected function initServiceProvider()
    {
        $this->fbServiceProvider = new FacebookServiceProvider($this->access_token);
    }
}
