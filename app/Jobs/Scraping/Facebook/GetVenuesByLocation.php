<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/28/16
 * Time: 2:52 AM.
 */
namespace App\Jobs\Scraping\Facebook;

use App\Dictionary\Scraping\Facebook\PageType;
use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use App\ServiceProviders\FacebookServiceProvider;

class GetVenuesByLocation extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, DispatchesJobs;

    protected $access_token;

    protected $fbServiceProvider;

    protected $lat;

    protected $lng;

    protected $distance = 5000;

    protected $page = 0;

    protected $venuesCount = 0;

    public function __construct($lat, $lng, $accessToken = null)
    {
        $user = \Auth::user();
        if ($user && \Session::has('fb_user_access_token')) {
            $this->access_token = \Session::get('fb_user_access_token');
        } else {
            $this->access_token = $accessToken;
        }
        $this->lat = $lat;
        $this->lng = $lng;
    }

    public function handle()
    {
        $this->makeRequest();
        echo "\n".$this->venuesCount."\n";
        $this->delete();
    }

    protected function getServiceProvider($accessToken)
    {
        if (!isset($this->fbServiceProvider)) {
            $this->fbServiceProvider = new FacebookServiceProvider($accessToken);
        }

        return $this->fbServiceProvider;
    }

    protected function buildQuery()
    {
        return 'v2.6/search?q=&type=place&center='.$this->lat.','
            .$this->lng.'&distance='.$this->distance.'&access_token='.$this->access_token;
    }

    protected function makeRequest($url = null)
    {
        $provider = $this->getServiceProvider($this->access_token);
        if ($url === null) {
            $result = json_decode($provider->get($this->buildQuery())->getBody()->getContents(), true);
        } else {
            $result = json_decode($provider->setResponse($provider->getClient()->request('GET', $url))
                ->getResponse()->getBody()->getContents(), true);
        }
        $this->page++;
        if (array_key_exists('data', $result) && count($result['data']) > 0) {
            $this->venuesCount += count($result['data']);
            foreach ($result['data'] as $key => $venue) {
                if (array_key_exists('id', $venue)) {
                    $this->dispatch(new GetPageInfo(PageType::VENUE, $venue['id']));
                }
            }
            if (array_key_exists('paging', $result) && array_key_exists('next', $result['paging'])) {
                $this->makeRequest($result['paging']['next']);
            }
        }

        return;
    }
}
