<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 6/28/16
 * Time: 6:47 AM.
 */
namespace App\Jobs\Scraping\Facebook;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;

class GetVenuesByBounds extends Job implements SelfHandling
{
    protected $lat;

    protected $lng;

    protected $access_token;

    use DispatchesJobs;

    public function __construct($lat, $lng, $token = null)
    {
        $this->lng = $lng;
        $this->lat = $lat;
        $user = \Auth::user();
        if ($user && \Session::has('fb_user_access_token')) {
            $this->access_token = \Session::get('fb_user_access_token');
        } else {
            $this->access_token = $token;
        }
    }

    public function handle()
    {
        $bounds = $this->makeBoundsArray();
        foreach ($bounds as $latLng) {
            $this->dispatch(new GetVenuesByLocation($latLng['lat'], $latLng['lng'], $this->access_token));
        }
    }

    public function makeBoundsArray()
    {
        $lat = $this->lat;
        $lng = $this->lng;
        $bounds = [];
        for ($i = 0; $i < 10; $i++) {
            $lat += 0.01;
            $lng += 0.01;
            $bounds[] = [
                'lat' => $lat,
                'lng' => $lng,
            ];
        }
        for ($i = 0; $i < 10; $i++) {
            $lat -= 0.01;
            $lng += 0.01;
            $bounds[] = [
                'lat' => $lat,
                'lng' => $lng,
            ];
        }

        for ($i = 0; $i < 10; $i++) {
            $lat += 0.01;
            $lng -= 0.01;
            $bounds[] = [
                'lat' => $lat,
                'lng' => $lng,
            ];
        }

        for ($i = 0; $i < 10; $i++) {
            $lat -= 0.01;
            $lng -= 0.01;
            $bounds[] = [
                'lat' => $lat,
                'lng' => $lng,
            ];
        }

        return $bounds;
    }
}
