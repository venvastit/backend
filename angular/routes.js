(function(){
    "use strict";

    angular.module('app.routes').config( function($stateProvider, $urlRouterProvider, $locationProvider ) {

        var getView = function( viewName ){
            return '/views/app/' + viewName + '/' + viewName + '.html';
        };
        
        var getOrgView = function(viewName) {
            return '/views/app/org/' + viewName + '/' + viewName + '.html';
        };
        $locationProvider.html5Mode(true).hashPrefix('!');
        $urlRouterProvider.otherwise('/!/events');
        $stateProvider
            .state('home', {
                url: '/!',
                views: {
                    main: {
                        templateUrl: getView('home')
                    }
                },
                abstract: true
            })
            .state('google_token', {
                template: '',
                //https://venvast.com/#access_token=ya29.Ci8bAyk4PdagtFGaaOymcxDmCo-u91AvQGf1FjFIPLT34Bpj4QPkAI8un3KFHRenlg&token_type=Bearer&expires_in=3600
                url: '/#access_token=:accessToken',
                controller: function ($location, $rootScope) {
                    var hash = $location.path().substr(1);
                    var splitted = hash.split('&');
                    var params = {};
                    console.log("TOKEEEN!!");
                    // for (var i = 0; i < splitted.length; i++) {
                    //     var param  = splitted[i].split('=');
                    //     var key    = param[0];
                    //     var value  = param[1];
                    //     params[key] = value;
                    //     $rootScope.accesstoken=params;
                    // }
                    // $location.path("/about");
                }
            })
            .state('home.events', {
                url: '/events',
                views: {
                    main: {
                        templateUrl: getView('home')
                    },
                    catalogue: {
                        templateUrl: getView('events')
                    },
                    r_sidebar: {
                        templateUrl: getView('sidebar')
                    },
                    filters: {
                        templateUrl: getView('event_filter')
                    }
                }
            })
            .state('home.venues', {
                url: '/venues',
                views: {
                    main: {
                        templateUrl: getView('home')
                    },
                    catalogue: {
                        templateUrl: getView('venues')
                    },
                    r_sidebar: {
                        templateUrl: getView('sidebar')
                    },
                    filters: {
                        templateUrl: getView('venue_filter')
                    }
                }
            })
            .state('home.deals', {
                url: '/deals',
                views: {
                    main: {
                        templateUrl: getView('home')
                    },
                    catalogue: {
                        templateUrl: getView('catalogue')
                    },
                    r_sidebar: {
                        templateUrl: getView('sidebar')
                    }
                }
            })
            .state('organizer', {
                url: '/!',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    }
                },
                abstract: true
            })
            .state('organizer.events', {
                url: '/organize',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('events')
                    },
                    r_sidebar: {
                        templateUrl: getOrgView('sidebar')
                    },
                    filters: {
                        templateUrl: getView('event_filter')
                    }
                }
            })
            .state('organizer.create_event', {
                url: '/create_event',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('create_event')
                    }
                }
            })
            .state('organizer.venues', {
                url: '/organize_venues',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('venues')
                    },
                    r_sidebar: {
                        templateUrl: getOrgView('sidebar')
                    },
                    filters: {
                        templateUrl: getView('venue_filter')
                    }
                }
            })
            .state('organizer.hosts', {
                url: '/hosts',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('hosts')
                    },
                    r_sidebar: {
                        templateUrl: getOrgView('sidebar')
                    }
                    // filters: {
                    //     templateUrl: getView('venue_filter')
                    // }
                }
            })
            .state('organizer.create_venue', {
                url: '/create_venue',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('create_venue')
                    }
                }
            })
            .state('organizer.deals', {
                url: '/organize_deals',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('deals')
                    },
                    r_sidebar: {
                        templateUrl: getOrgView('sidebar')
                    },
                    filters: {
                        templateUrl: getView('deal_filter')
                    }
                }
            });
    } );
})();