(function(){
    "use strict";
    var app = angular.module('app',
        [
            'app.controllers',
            'app.filters',
            'app.services',
            'app.directives',
            'app.routes',
            'app.config',
            'facebook',
            'geolocation',
            'djds4rce.angular-socialshare',
            'uiGmapgoogle-maps',
            'jkuri.datepicker',
            'ui.date',
            'ui.timepicker',
            'ui.utils.masks',
            'ngStorage',
            'satellizer'
        ], function($interpolateProvider, $locationProvider, FacebookProvider, $httpProvider, $authProvider){
            $interpolateProvider.startSymbol('{%');
            $interpolateProvider.endSymbol('%}');
            FacebookProvider.init($('#fb_app_id').text());//localhost: 1631594013727920 //1621403594746962
            $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
                return {
                    'request': function (config) {
                        config.headers = config.headers || {};
                        config.params = config.params || {};
                        if ($localStorage.token) {
                            // config.headers.Authorization = 'Bearer ' + $localStorage.token;
                            config.params.token = $localStorage.token;
                        }
                        return config;
                    }
                };
            }]);

        });

    angular.module('app.routes', ['ui.router']);
    angular.module('app.controllers', ['ui.router', 'restangular', 'ui.bootstrap', 'facebook', 'uiGmapgoogle-maps', 'geolocation', 'google.places', 'ngFileUpload', 'ngImgCrop']);
    angular.module('app.filters', []);
    angular.module('app.services', ['restangular', 'ui.router', 'facebook', 'geolocation']);
    angular.module('app.directives', []);
    angular.module('app.config', ['ui.bootstrap']);
})();