/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with venues list view
     */
    angular.module('app.controllers').controller('VenueFilterCtrl', function($q, $timeout, $scope, $rootScope, $state, HtmlHelper, ChangesNotifier, VenueStorage){

        var _timeout;
        $scope.textSearchInAction = false;
        $scope.textSearchfirstCall = true;

        $scope.getVenueRequest = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.venues':
                    return $scope.venuesRequest;
                    break;
                case 'organizer.venues':
                    return $scope.orgVenuesRequest;
                    break;
            }
        };

        angular.element(document).ready(function () {
            HtmlHelper.init();

            var category = $scope.getVenueRequest().category;
            var categories = $scope.venueCategories;
            var categoryName = null;
            if(category !== null && typeof categories !== 'undefined') {
                for(var i = 0; i < categories.length; i++) {
                    if(categories[i]._id == category){
                        categoryName = categories[i].name;
                    }
                }
            }
            if(categoryName !== null) {
                $timeout(function() {
                    $('#category_search').children('span').text(categoryName);
                });
            }

            new HtmlHelper.StyledDropDown('#category_search', $scope.venuesRequest.category);
            new HtmlHelper.StyledDropDown('#when_search', $scope.venuesRequest.category);
            var input = $('.location-input').first();
            $scope.autocomplete = new google.maps.places.Autocomplete(input[0], {types: ['(cities)']});
            $scope.autocomplete.addListener('place_changed', function () {
                var place = $scope.autocomplete.getPlace();
                $scope.getVenueRequest().setWhere(place.geometry.location.lat(), place.geometry.location.lng());
                $scope.search();
            });
        });

        $scope.searchByText = function () {
            if(_timeout){ //if there is already a timeout in process cancel it
                $timeout.cancel(_timeout);
            }
            _timeout = $timeout(function(){
                $scope.search();
                _timeout = null;
            }, 500);
        };


        $scope.selectCategory = function(category) {
            if(category === null){
                $scope.getVenueRequest().category = null;
                $('#category_search').children('span').text('-- All --');
            } else {
                $scope.getVenueRequest().category = category._id;
                $('#category_search').children('span').text(category.name);
            }
            $scope.search();
        };

        $scope.organizerSearch = function() {
            HtmlHelper.showLoader();
            $scope.orgVenuesRequest.page = 0;
            $scope.orgVenues = [];
            $scope.orgVenuesRequest.type = 'venues';
            $scope.orgVenuesRequest.hasMorePages = true;
            $scope.orgVenuesRequest.organizer = true;
            var venues = new VenueStorage();
            return $q.when(venues.load($scope.orgVenuesRequest)).then(function(venueStorage) {
                $scope.orgVenues = venueStorage.data;
                ChangesNotifier.org_venues = $scope.orgVenues;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_VENUES_LOADED);
                HtmlHelper.hideLoader();
            });
        };

        $scope.userSearch = function() {
            HtmlHelper.showLoader();
            $scope.venuesRequest.page = 0;
            $scope.venues = [];
            $scope.venuesRequest.type = 'venues';
            $scope.venuesRequest.hasMorePages = true;
            var venues = new VenueStorage();
            return $q.when(venues.load($scope.venuesRequest)).then(function(venueStorage) {
                $scope.venues = venueStorage.data;
                ChangesNotifier.venues_value = $scope.venues;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_VENUES_LOADED);
                HtmlHelper.hideLoader();
            });
        };


        $scope.search = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.venues':
                    $scope.userSearch();
                    break;
                case 'organizer.venues':
                    $scope.organizerSearch();
                    break;
            }
        };
    });
})();