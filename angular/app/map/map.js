/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('MapCtrl', function($q, $scope, $state, UserService, VenVastRequest, MapService, ChangesNotifier, uiGmapGoogleMapApi, HtmlHelper){

        // angular.element(document).ready(function () {
            var category = $('.category');
            var stickySelector = '.sticky';
            var el = $(stickySelector);
            var stickyTop = $(stickySelector).offset().top; // returns number
            var stickyHeight = $(stickySelector).height();

            $(window).scroll(function(){
                if($('footer').offset()) {
                    var limit = $('footer').offset().top - stickyHeight - 20 - 200;
                    var windowTop = $(window).scrollTop(); // returns number
                    if (windowTop > 200 && stickyTop < windowTop){
                        el.css({ position: 'fixed', top: 0, "margin-left": "72%"});
                    }
                    else {
                        el.css({ position: '', top: "", "margin-left": ""});
                    }

                    if (limit < windowTop) {
                        var diff = limit - windowTop;
                        el.css({top: diff});
                    }
                }
            });
        // });

        $scope.initializeMap = function() {
            $q.when(MapService.initialize($scope.getStateRequest())).then(function(mapService) {
                $scope.mapService = mapService;
                uiGmapGoogleMapApi.then(function(maps) {
                    $scope.loadMarkers();
                });
            });
        };

        $scope.loadMarkers = function(objects) {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    $scope.loadEventMarkers(objects);
                    break;
                case 'home.venues':
                    $scope.loadVenueMarkers(objects);
                    break;
            }
       };
        
        $scope.loadEventMarkers = function(objects) {
            if(objects)
                $scope.mapService.setEventMarkers(objects);
            else
                $scope.mapService.setEventMarkers($scope.events);

            $scope.mapService.showUserMarker();
        };

        $scope.loadVenueMarkers = function(objects) {
            if(objects)
                $scope.mapService.setVenueMarkers(objects);
            else
                $scope.mapService.setVenueMarkers($scope.venues);

            $scope.mapService.showUserMarker();
        };
        

        $scope.reloadMarkers = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    $scope.reloadEventMarkers();
                    break;
                case 'home.venues':
                    $scope.reloadVenueMarkers();
                    break;
            }
        };
        
        $scope.reloadEventMarkers = function() {
            $q.when(MapService.initialize($scope.eventRequest)).then(function(mapService) {
                $scope.mapService = mapService;
                $scope.mapService.deleteAllMarkers();
                if(typeof ChangesNotifier.value !== "undefined" && ChangesNotifier.value !== null){
                    $scope.loadMarkers(ChangesNotifier.value);
                } else {
                    $scope.loadMarkers();
                }
            });
        };
        
        $scope.reloadVenueMarkers = function() {
            $q.when(MapService.initialize($scope.venuesRequest)).then(function(mapService) {
                $scope.mapService = mapService;
                $scope.mapService.deleteAllMarkers();
                if(typeof ChangesNotifier.venues_value !== "undefined" && ChangesNotifier.venues_value !== null){
                    $scope.loadMarkers(ChangesNotifier.venues_value);
                } else {
                    $scope.loadMarkers();
                }
            });
        };
        
        $scope.addMarkers = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    $scope.addEventMarkers();
                    break;
                case 'home.venues':
                    $scope.addVenueMarkers();
                    break;
            }
        };

        $scope.getStateRequest = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    return $scope.eventRequest;
                    break;
                case 'home.venues':
                    return $scope.venuesRequest;
                    break;
            }
        };
        
        
        $scope.addVenueMarkers = function() {
            if(ChangesNotifier.venues_value)
                $scope.mapService.addVenueMarkers(ChangesNotifier.venues_value);
        };
        
        $scope.addEventMarkers = function() {
            if(ChangesNotifier.value)
                $scope.mapService.addEventMarkers(ChangesNotifier.value);
        };

        angular.element(document).ready(function () {
            $scope.initializeMap();
        });


        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_EVENTS_LOADED, $scope, $scope.reloadMarkers);
        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_VENUES_LOADED, $scope, $scope.reloadMarkers);

        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_EVENTS_ADDED, $scope, $scope.addMarkers);
        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_VENUES_ADDED, $scope, $scope.addMarkers);
    });
})();