/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";
    angular.module('app.controllers').controller('HeaderCtrl', function($scope, $rootScope, $localStorage, $timeout, $q, $http, $state, $location, $auth, HtmlHelper, UserService, Facebook){

        $scope.render_tab = HtmlHelper.render_tab;
        $scope.closePopups = HtmlHelper.closePopups;
        $scope.showPopUp = HtmlHelper.showPopUp;

        $scope.loginForm = {
            email: "",
            password: ""
        };

        $scope.registerForm = {
            type: 0
        };

        $('body').click(function(event) {
            if(
                !$(event.target).is('a')
                &&
                !$(event.target).is('input')
                &&
                !$(event.target).is('label')
            ){
                $scope.closePopups();
            }
        });


        $rootScope.$on('$stateChangeSuccess', function(){
            var eventLi = $('#menu-events').closest('li');
            var venueLi = $('#menu-venues').closest('li');
            var dealLi = $('#menu-deals').closest('li');
            switch($location.path().substring(3)){
                case 'events':
                    venueLi.removeClass('active');
                    dealLi.removeClass('active');
                    eventLi.addClass('active');
                    break;
                case 'venues':
                    dealLi.removeClass('active');
                    eventLi.removeClass('active');
                    venueLi.addClass('active');
                    break;
                case  'deals':
                    eventLi.removeClass('active');
                    venueLi.removeClass('active');
                    dealLi.addClass('active');
                    break;
            }
        });

        /**
         * Log current user out
         */
        $scope.logout = function() {
            $localStorage.token = null;
            $q.when(UserService.logout()).then(function() {
                $timeout(function() {
                    $scope.user = null;
                });
            });
        };

        /**
         * Log user in
         */
        $scope.submitLogin = function() {
            $scope.togglePopUpLoading();
            UserService.login($scope.loginForm).then(function(response) {
                if(typeof response._id !== "undefined") {
                    $scope.user = response;
                    $scope.togglePopUpLoading();
                    $scope.closePopups();
                } else {
                        $scope.loginError = 'Error occured, please try reload the page and repeat your action';
                        $('#login-error').removeClass('hidden');
                }
            });
        };

        /**
         * Login with facebook
         */
        $scope.loginFacebook = function () {
            $q.when(UserService.loginFacebook(Facebook)).then(function(us) {
                if(us.authResponse && us.authResponse.accessToken) {
                    $scope.loginForm = {facebook_access_token: us.authResponse.accessToken};
                    $scope.submitLogin();
                }  else {
                    $scope.loginError = 'Error occured, please try reload the page and repeat your action';
                    $('#login-error').removeClass('hidden');
                }
            });
        };

        /**
         * Login with facebook
         */
        $scope.loginGoogle = function () {
            $http.post('https://venvast.com/auth/google').success(function (response, status, headers) {
                console.log("Some resp: ", response);
                console.log("Status: ", status);
                console.log("Headers: ", headers);
            });
        };

        $scope.togglePopUpLoading = function() {
            $('#login_form').toggleClass('hidden');
            $('#register_form').toggleClass('hidden');
            $('.loader-wrapper').toggle();
        };

        /**
         * Register new user
         */
        $scope.register = function() {
            $scope.submitLogin();
        };

        /**
         * Connect facebook account on registration
         */
        $scope.registerFacebook = function() {
            $scope.loginFacebook();
        };
        




        switch($location.path().substring(3)){
            case '':
                $('#menu-events').closest('li').addClass('active');
                break;
            case 'events':
                $('#menu-events').closest('li').addClass('active');
                break;
            case 'venues':
                $('#menu-venues').closest('li').addClass('active');
                break;
            case  'deals':
                $('#menu-deals').closest('li').addClass('active');
                break;
        }
    });
})();