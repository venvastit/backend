/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('EventFilterCtrl', function($q, $timeout, $scope, $rootScope, $state, HtmlHelper, ChangesNotifier, EventsStorage){

        var _timeout;


        $scope.getEventRequest = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    return $scope.eventRequest;
                    break;
                case 'organizer.events':
                    return $scope.orgEventRequest;
                    break;
            }
        };


        angular.element(document).ready(function () {
            HtmlHelper.init();
            var category = $scope.getEventRequest().category;
            var categories = $scope.eventCategories;
            var categoryName = null;
            if(category !== null && typeof categories !== 'undefined') {
                for(var i = 0; i < categories.length; i++) {
                    if(categories[i]._id == category){
                        categoryName = categories[i].name;
                    }
                }
            }
            if(categoryName !== null) {
                $timeout(function() {
                    $('#category_search').children('span').text(categoryName);
                });
            }
            new HtmlHelper.StyledDropDown('#category_search', category);
            new HtmlHelper.StyledDropDown('#when_search', $scope.getEventRequest().when);
            var input = $('.location-input').first();
            $scope.autocomplete = new google.maps.places.Autocomplete(input[0], {types: ['(cities)']});
            $scope.autocomplete.addListener('place_changed', function () {
                var place = $scope.autocomplete.getPlace();
                $scope.getEventRequest().setWhere(place.geometry.location.lat(), place.geometry.location.lng(), place.name);
                $scope.search();
            });
        });

        $scope.searchByText = function () {
            if(_timeout){ //if there is already a timeout in process cancel it
                $timeout.cancel(_timeout);
            }
            _timeout = $timeout(function(){
                $scope.search();
                _timeout = null;
            }, 500);
        };


        $scope.selectCategory = function(category) {
            if(category === null){
                $scope.getEventRequest().category = null;
                $('#category_search').children('span').text('-- All --');
            } else {
                $scope.getEventRequest().category = category._id;
                $('#category_search').children('span').text(category.name);
            }
            $scope.search();
        };

        $scope.selectWhen = function(value) {
            $scope.getEventRequest().date = null;
            $scope.getEventRequest().when = value;
            if(value === null) {
                $('#when_search').children('span').text('-- All --');
            }
            else {
                $('#when_search').children('span').text(value);
            }
            $scope.search();
        };

        $scope.organizerSearch = function() {
            HtmlHelper.showLoader();
            $scope.orgEventRequest.page = 0;
            $scope.events = [];
            $scope.orgEventRequest.type = 'events';
            $scope.orgEventRequest.hasMorePages = true;
            var events = new EventsStorage();
            return $q.when(events.load($scope.orgEventRequest)).then(function(eventStorage) {
                $scope.orgEvents = eventStorage.data;
                ChangesNotifier.org_value = $scope.orgEvents;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_EVENTS_LOADED);
                HtmlHelper.hideLoader();
            });
        };

        $scope.userSearch = function() {
            HtmlHelper.showLoader();
            $scope.eventRequest.page = 0;
            $scope.events = [];
            $scope.eventRequest.type = 'events';
            $scope.eventRequest.hasMorePages = true;
            var events = new EventsStorage();
            return $q.when(events.load($scope.eventRequest)).then(function(eventStorage) {
                $scope.events = eventStorage.data;
                ChangesNotifier.value = $scope.events;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_EVENTS_LOADED, $scope.events);
                HtmlHelper.hideLoader();
            });
        };

        $scope.search = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    $scope.userSearch();
                    break;
                case 'organizer.events':
                    $scope.organizerSearch();
                    break;
            }
        };
    });
})();