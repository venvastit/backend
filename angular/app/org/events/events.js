/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('OrgEventsCtrl', function($q, $rootScope, $scope, $state, HtmlHelper, ChangesNotifier){

        // $('.l-content').width('72%');
        
        $scope.editEvent = function ($event, event) {
            $event.stopPropagation();
            $rootScope.newEvent = event;
            $state.go('organizer.create_event', {});
        };

        $scope.deleteEvent = function ($event, event) {
            $event.stopPropagation();
            for(var i = 0; i < $scope.orgEvents.length; i++) {
                if($scope.orgEvents[i]._id === event._id) {
                    $scope.orgEvents.splice(i, 1);
                }
            }
            event.deleteFromBackend();
        };
        
        $scope.openEventCreation = function() {
            $state.go('organizer.create_event');
        };
        

        $scope.$watch('events', function() {
            if($scope.events) {
                for(var i = 0; i < $scope.events.length; i++) {
                    var distance = $scope.events[i].computeDistanceTo($scope.eventRequest.getUserPosition());
                    if(distance > 30) {
                        distance = '> 30';
                    }
                    $scope.events[i].distance = distance ? distance + ' km away' : '';
                }
                HtmlHelper.hideLoader();
            }
        });
    });
})();