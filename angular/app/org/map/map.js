/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('OrgMapCtrl', function($q, $scope, $state, UserService, VenVastRequest, MapService, ChangesNotifier, uiGmapGoogleMapApi){


        var category = $('.category');
        var stickySelector = '.sticky';
        var el = $(stickySelector);
        var stickyTop = $(stickySelector).offset().top; // returns number
        var stickyHeight = $(stickySelector).height();

        $(window).scroll(function(){
            var limit = $('footer').offset().top - stickyHeight - 20 - 200;
            var windowTop = $(window).scrollTop(); // returns number
            if (windowTop > 200 && stickyTop < windowTop){
                el.css({ position: 'fixed', top: 0, "margin-left": "72%"});
            }
            else {
                el.css({ position: '', top: "", "margin-left": ""});
            }

            if (limit < windowTop) {
                var diff = limit - windowTop;
                el.css({top: diff});
            }
        });

        $scope.initializeMap = function() {
            $q.when(MapService.initialize($scope.getStateRequest(), 'org-map-canvas')).then(function(mapService) {
                $scope.orgMapService = mapService;
                uiGmapGoogleMapApi.then(function(maps) {
                    $scope.loadMarkers();
                });
            });
        };

        $scope.loadMarkers = function(objects) {
            var state = $state.current.name;
            switch (state) {
                case 'organizer.events':
                    $scope.loadEventMarkers(objects);
                    break;
                case 'organizer.venues':
                    $scope.loadVenueMarkers(objects);
                    break;
                case 'organizer.hosts':
                    $scope.loadHostsMarkers(objects);
                    break;
            }
       };

        $scope.loadEventMarkers = function(objects) {
            if(objects && typeof objects !== "undefined"){
                $scope.orgMapService.setEventMarkers(objects);
            } else {
                $scope.orgMapService.setEventMarkers($scope.orgEvents);
            }

            $scope.orgMapService.showUserMarker();
        };

        $scope.loadVenueMarkers = function(objects) {
            if(objects)
                $scope.orgMapService.setVenueMarkers(objects);
            else
                $scope.orgMapService.setVenueMarkers($scope.orgVenues);

            $scope.orgMapService.showUserMarker();
        };

        $scope.loadHostsMarkers = function(objects) {
            if(objects)
                $scope.orgMapService.setVenueMarkers(objects);
            else
                $scope.orgMapService.setVenueMarkers($scope.hosts);

            $scope.orgMapService.showUserMarker();
        };
        

        $scope.reloadMarkers = function() {
            var state = $state.current.name;
            switch (state) {
                case 'organizer.events':
                    $scope.reloadEventMarkers();
                    break;
                case 'organizer.venues':
                    $scope.reloadVenueMarkers();
                    break;
            }
        };
        
        $scope.reloadEventMarkers = function() {
             $q.when(MapService.initialize($scope.getStateRequest(), 'org-map-canvas')).then(function(mapService) {
                 $scope.orgMapService = mapService;
                $scope.orgMapService.deleteAllMarkers();
                if(typeof ChangesNotifier.org_value !== "undefined" && ChangesNotifier.org_value !== null){
                    $scope.loadMarkers(ChangesNotifier.org_value);
                } else {
                    $scope.loadMarkers();
                }
             });
        };
        
        $scope.reloadVenueMarkers = function() {
            $q.when(MapService.initialize($scope.getStateRequest(), 'org-map-canvas')).then(function(mapService) {
                $scope.orgMapService = mapService;
                $scope.orgMapService.deleteAllMarkers();
                if(typeof ChangesNotifier.org_venues !== "undefined" && ChangesNotifier.org_venues !== null){
                    $scope.loadMarkers(ChangesNotifier.org_venues);
                } else {
                    $scope.loadMarkers();
                }
            });
        };
        
        $scope.addMarkers = function() {
            var state = $state.current.name;
            switch (state) {
                case 'organizer.events':
                    $scope.addEventMarkers();
                    break;
                case 'organizer.venues':
                    $scope.addVenueMarkers();
                    break;
            }
        };

        $scope.getStateRequest = function() {
            var state = $state.current.name;
            switch (state) {
                case 'organizer.events':
                    return $scope.orgEventRequest;
                    break;
                case 'organizer.venues':
                    return $scope.orgVenuesRequest;
                    break;
                case 'organizer.hosts':
                    return $scope.hostsRequest;
                    break;
            }
        };
        
        
        $scope.addVenueMarkers = function() {
            if(ChangesNotifier.org_venues)
                $scope.orgMapService.addVenueMarkers(ChangesNotifier.org_venues);
        };
        
        $scope.addEventMarkers = function() {
            if(ChangesNotifier.org_value)
                $scope.orgMapService.addEventMarkers(ChangesNotifier.org_venues);
        };

        angular.element(document).ready(function () {
            $scope.initializeMap();
        });


        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_EVENTS_LOADED, $scope, $scope.reloadMarkers);
        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_VENUES_LOADED, $scope, $scope.reloadMarkers);

        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_EVENTS_ADDED, $scope, $scope.addMarkers);
        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_VENUES_ADDED, $scope, $scope.addMarkers);
    });
})();