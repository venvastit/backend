/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('OrgHomeCtrl', function($q, $scope, $timeout, $window, $anchorScroll, $state, AnchorSmoothScroll, HtmlHelper, EventsStorage, VenueStorage, Event, UserService, VenVastRequest, MapService, ChangesNotifier, CategoriesStorage){

        $scope.init = function(){
            console.log("Init called");

            HtmlHelper.showLoader();
            $scope.win_height_padded = $(window).height() * 1.34;
            $window.onscroll = $scope.revealOnScroll;
            $scope.orgEventRequest = new VenVastRequest({when: "week", organizer: true});
            $scope.orgVenuesRequest = new VenVastRequest({organizer: true});
            $scope.hostsRequest = new VenVastRequest({hosts: true});

            $q.when($scope.initUser()).then(function(user) {

                $scope.initEvents();
                $scope.initVenues();
                $scope.initHosts();
            });

            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_VENUES_LOADED, $scope, function() {
                $timeout(function() {
                    if(typeof  ChangesNotifier.org_venues !== "undefined" && ChangesNotifier.org_venues !== null)
                        $scope.orgVenues = ChangesNotifier.org_venues;
                });
            });
            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_EVENTS_LOADED, $scope, function () {
                $timeout(function () {
                    if(typeof  ChangesNotifier.org_value !== "undefined" && ChangesNotifier.org_value !== null)
                        $scope.orgEvents = ChangesNotifier.org_value;
                });
            });
        };

        $scope.initEvents = function() {
            var events = new EventsStorage();
            var categories = new CategoriesStorage();
            $scope.eventCategoriesRequest = new VenVastRequest({type: 'events'});
            return $q.when(events.load($scope.orgEventRequest)).then(function(eventStorage) {
                return $q.when(categories.load($scope.eventCategoriesRequest)).then(function(categoryStorage) {
                    $scope.orgEvents = eventStorage.data;
                    $scope.eventCategories = categoryStorage.data;
                    ChangesNotifier.org_value = $scope.orgEvents;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_EVENTS_LOADED);
                    $scope.addScrollHandler(events, $scope.orgEventRequest, 'orgEvents');
                    return true;
                });
            });
        };

        $scope.initVenues = function() {
            var venues = new VenueStorage();
            var categories = new CategoriesStorage();
            $scope.venueCategoriesRequest = new VenVastRequest({type: 'venues'});
            return $q.when(venues.load($scope.orgVenuesRequest)).then(function(venueStorage) {
                return $q.when(categories.load($scope.venueCategoriesRequest)).then(function(categoryStorage) {
                    $scope.orgVenues = venueStorage.data;
                    $scope.venueCategories = categoryStorage.data;
                    ChangesNotifier.org_venues = $scope.orgVenues;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_VENUES_LOADED);
                    $scope.addScrollHandler(venues, $scope.orgVenuesRequest, 'orgVenues');
                    return true;
                });
            });
        };

        $scope.initHosts = function() {
            var hosts = new VenueStorage();
            var categories = new CategoriesStorage();
            return $q.when(hosts.load($scope.hostsRequest)).then(function(venueStorage) {
                $scope.hosts = venueStorage.data;
                ChangesNotifier.hosts = $scope.hosts;
                // ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_VENUES_LOADED);
                $scope.addScrollHandler(hosts, $scope.hostsRequest, 'hosts');
                return true;
            });
        };

        $scope.initUser = function() {
            $q.when(UserService.getCurrentUser()).then(function(user) {
                if(user === null || typeof user === "undefined")
                    return $state.go('home.events', {}, {reload: true});
                
                $scope.user = user;
            });
        };

        $scope.addScrollHandler = function(storageInstance, requestInstance, scopeStorage) {
            $(window).scroll(function(){
                var lastElement = HtmlHelper.getCoords(document.querySelector('.catalog-wrapper:last-child'));
                if(window.pageYOffset + window.screen.height >= lastElement - 800) {
                    $q.when(storageInstance.load(requestInstance)).then(function(storage) {
                        if(storage && storage.data) {
                            for(var i= 0; i < storage.data.length; i++) {
                                $scope[scopeStorage].push(storage.data[i]);
                            }
                            $scope.notifyMoreItemsLoaded(scopeStorage, storage);
                        }
                    });
                }
            });
        };
        
        $scope.notifyMoreItemsLoaded = function(scopeStorage, storage) {
            switch(scopeStorage) {
                case 'orgEvents':
                    ChangesNotifier.org_value = storage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_EVENTS_ADDED);
                    break;

                case 'orgVenues':
                    ChangesNotifier.org_venues = storage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_VENUES_ADDED);
                    break;
            }
        };

        $scope.revealOnScroll = function () {
            var scrolled = $(window).scrollTop();

           $('.revealOnScroll.animated').each(function(i, element) {
               var __self = $(this);
               var offsetTop = __self.offset().top;
               if (scrolled + $scope.win_height_padded < offsetTop) {
                       __self.removeClass('animated');
                       __self.removeClass(__self.data('animation'));
                   }
            });

            $(".revealOnScroll:not(.animated)").each(function (i, element) {
                var __self = $(this);
                var offsetTop = __self.offset().top;
                if (scrolled + $scope.win_height_padded > offsetTop) {
                    if(__self.data('timeout') && i > 5) {
                        __self.addClass('animated');
                        __self.addClass(__self.data('animation'));
                    }
                }
            });
        };
        
        $scope.toggleInfo = function(object) {
            var element = $('#'+ object.className.toLowerCase() + '-' + object._id);
            if(element.hasClass('catalog-wrapper--full')) {
                $scope.closeInfo();
            } else {
                $scope.showInfo(object);
            }
        };

        $scope.showInfo = function(object) {
            var title = "VenVast " + object.className + " - " + object.title;
            var domIdentfier = object.className.toLowerCase() +'-'+ object._id;
            $('#meta-title').attr('content', title);
            $('title').text(title);
            $('#meta-type').attr('content', "website");
            $('#meta-image').attr('content', object.picture);
            $('#meta-description').attr('content', object.description);
            $('#meta-url').attr('content', 'http://venvast.com/!/'+ domIdentfier);
            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
            MapService.centerOnObject(object);
            AnchorSmoothScroll.scrollTo(domIdentfier);
            $('#' + domIdentfier).addClass('catalog-wrapper--full', {duration: 500});
        };

        $scope.closeInfo = function() {
            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
            $scope.map = MapService.restoreMap();
        };
        
        
        $scope.init();
    });
})();