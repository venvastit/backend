/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('HostsCtrl', function($scope, $state, HtmlHelper){

        $scope.openVenueCreation = function() {
            $state.go('organizer.create_venue');
        };
        
        $scope.$watch('hosts', function() {
            if($scope.hosts) {
                console.log("Hosts: ", $scope.hosts);
                for(var i = 0; i < $scope.hosts.length; i++) {
                    var distance = $scope.hosts[i].computeDistanceTo($scope.hostsRequest.getUserPosition());
                    var d = $scope.hosts[i].computeDistanceTo($scope.hostsRequest.getUserPosition());
                    if(distance > 30) {
                        distance = '> 30';
                    }
                    $scope.hosts[i].distance = distance ? distance + ' km away' : '';
                    $scope.hosts[i].d = d;
                }
                $scope.hosts.sort(function(a,b) {
                    return a.d - b.d;
                });

                HtmlHelper.hideLoader();
            }
        });
    });
})();