/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('OrgVenuesCtrl', function($scope, $rootScope, $state, HtmlHelper){

        // $('.l-content').width('72%');

        $rootScope.newVenue = null;
        
        $scope.openVenueCreation = function() {
            $state.go('organizer.create_venue');
        };

        $scope.editVenue = function(event, venue) {
            event.stopPropagation();
            $rootScope.newVenue = venue;
            $state.go('organizer.create_venue', {});
        };

        $scope.deleteVenue = function(event, venue) {
            event.stopPropagation();
            for(var i = 0; i < $scope.orgVenues.length; i++) {
                if($scope.orgVenues[i]._id === venue._id) {
                    $scope.orgVenues.splice(i, 1);
                }
            }
            venue.deleteFromBackend();
        };
        
        $scope.$watch('orgVenues', function() {
            if($scope.orgVenues) {
                for(var i = 0; i < $scope.orgVenues.length; i++) {
                    var distance = $scope.orgVenues[i].computeDistanceTo($scope.orgVenuesRequest.getUserPosition());
                    var d = $scope.orgVenues[i].computeDistanceTo($scope.orgVenuesRequest.getUserPosition());
                    if(distance > 30) {
                        distance = '> 30';
                    }
                    $scope.orgVenues[i].distance = distance ? distance + ' km away' : '';
                    $scope.orgVenues[i].d = d;
                }
                $scope.orgVenues.sort(function(a,b) {
                    return a.d - b.d;
                });

                HtmlHelper.hideLoader();
            }
        });
    });
})();