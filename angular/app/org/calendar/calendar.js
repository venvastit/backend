/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('OrgCalendarCtrl', function($q, $scope, $timeout, $state, HtmlHelper, EventsStorage, ChangesNotifier){

        var state = $state.current.name;
        switch (state) {
            case 'organizer.events':
                $("#calendar-filter").ionCalendar({
                    lang: "en",
                    sundayFirst: false,
                    years: "2015-2018",
                    format: "YYYY-MM-DD",
                    onClick: function(date){
                        $('#when_search').children('span').text(date);
                        $timeout(function() {
                            $scope.orgEventRequest.when = date;
                        });
                        $scope.orgEventRequest.date = date;
                        $scope.search();
                    }
                });
                break;
            case 'organizer.venues':
                $("#calendar-filter").addClass('hidden');
                break;
        }

        $scope.search = function() {
            HtmlHelper.showLoader();
            $scope.orgEventRequest.page = 0;
            $scope.orgEvents = [];
            $scope.orgEventRequest.type = 'events';
            $scope.orgEventRequest.hasMorePages = true;
            var events = new EventsStorage();
            return $q.when(events.load($scope.orgEventRequest)).then(function(eventStorage) {
                console.log("Storage: ", eventStorage);
                $scope.orgEvents = eventStorage.data;
                ChangesNotifier.org_value = $scope.orgEvents;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_EVENTS_LOADED, $scope.orgEvents);
                HtmlHelper.hideLoader();
            });
            // $scope.orgEventRequest.getRequestObject()
        };
    });
})();