/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";
    angular.module('app.controllers').controller('OrgHeaderCtrl', function($scope, $rootScope, $timeout, $q, $state, $location, HtmlHelper, UserService, Facebook){


        $scope.highlightMenuItem = function () {
            var state = $state.current.name;
            var eventLi = $('#menu-events').closest('li');
            var venueLi = $('#menu-venues').closest('li');
            var dealLi = $('#menu-deals').closest('li');
            var hostsLi = $('#menu-hosts').closest('li');
            switch(state){
                case 'organizer.events':
                    venueLi.removeClass('active');
                    dealLi.removeClass('active');
                    hostsLi.removeClass('active');
                    eventLi.addClass('active');
                    break;
                case 'organizer.venues':
                    dealLi.removeClass('active');
                    eventLi.removeClass('active');
                    hostsLi.removeClass('active');
                    venueLi.addClass('active');
                    break;
                case  'organizer.deals':
                    eventLi.removeClass('active');
                    venueLi.removeClass('active');
                    hostsLi.removeClass('active');
                    dealLi.addClass('active');
                    break;
                case 'organizer.hosts':
                    eventLi.removeClass('active');
                    venueLi.removeClass('active');
                    dealLi.removeClass('active');
                    hostsLi.addClass('active');
                    break;
            }
        };

        /**
         * Log current user out
         */
        $scope.logout = function() {
            $q.when(UserService.logout()).then(function() {
                $timeout(function() {
                    $scope.user = null;
                    $state.go('home.events');
                });
            });
        };

        $(document).ready($scope.highlightMenuItem);
        $rootScope.$on('$stateChangeSuccess', $scope.highlightMenuItem);
    });
})();