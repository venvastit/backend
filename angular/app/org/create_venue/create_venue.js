(function(){
    "use strict";


    angular.module('app.controllers').controller('VenueCreateCtrl', function($scope, $rootScope, $timeout, $q, $state, HtmlHelper, Upload, VenVastRequest, EventsStorage, VenueStorage, Event, Venue, MapService) {

        var _timeout;

        $scope.init = function() {
            if(!$rootScope.newVenue) {
                $scope.newVenue = new Venue({});
                $scope.openingHours = $scope.newVenue.createOpeningHoursTemplate();
            } else {
                $scope.newVenue = $rootScope.newVenue;
                if($scope.newVenue.business_hours){
                    $scope.openingHours = $scope.newVenue.createOpenHours($scope.newVenue.business_hours);
                }
                else{
                    $scope.openingHours = $scope.newVenue.createOpeningHoursTemplate();
                }
            }
            $scope.errors = [];
            $('#category').select2();
            if($scope.newVenue.categories.length > 0){
                $scope.newVenue.category = $scope.newVenue.categories[0];
            }

            $scope.eventMap = MapService.initialize(new VenVastRequest({where: null}), 'venue-map');

            HtmlHelper.hideLoader();
            $('.l-content').width('100%');
            
        };

        $scope.businessHoursChecked = function(dayObject) {
            return typeof dayObject.open !== "undefined";
        };

        $scope.businessHoursToggle = function(event, dayObj) {
            var checked = $(event.target)[0].checked;
            if(checked) {
                dayObj = $scope.newVenue.getOpenHoursDayObject(dayObj.day, '0900', '2000');
                $timeout(function() {
                    $scope.openingHours[dayObj.day] = dayObj;
                });
                console.log(dayObj);
            } else {
                dayObj.open = false;
                dayObj.close = false;
            }
        };


        $scope.removePicture = function() {
            $scope.newVenue.cover_image = null;
        };
        

        $scope.createLocation = function(lat, lng) {
            return {
                type: "Point",
                coordinates: [lng, lat]
            }
        };

        $scope.createVenue = function($event){
            $event.preventDefault();
            $scope.newVenue.categories = [$('#category').val()];
            $scope.newVenue.business_hours = $scope.openingHours;
            $scope.newVenue.address = $scope.newVenue.address.formatted_address;
            $scope.newVenue.street = $scope.newVenue.address;
            $q.when($scope.newVenue.saveOnBackend()).then(function(response) {
                $state.go('organizer.venues', {}, {reload: true});
            });
        };


        $scope.addressSelected = function(){
            if(typeof $scope.newVenue.address === "object") {
                MapService.map.setCenter($scope.newVenue.address.geometry.location);
                MapService.map.setZoom(16);
                var venueStorage = new VenueStorage();
                var addressObj = $scope.newVenue.address;
                var lat = addressObj.geometry.location.lat();
                var lng = addressObj.geometry.location.lng();
                $scope.newVenue.location = $scope.createLocation(lat, lng);
            }
        };

        $scope.toggleSelection = function(event, category) {
            var checked = $(event.target)[0].checked;
            var categories = $scope.newVenue.eventCategories;
            if(checked) {
                if(typeof categories === "undefined")
                    categories = [];
                categories.push(category._id);
            } else {
                if(typeof categories !== "undefined" && categories.length > 0) {
                    var index = categories.indexOf(category._id);
                    categories.splice(index, 1);
                }
            }
            $scope.newVenue.eventCategories = categories;
        };

        $scope.init();
    });
})();
