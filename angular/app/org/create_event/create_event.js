(function(){
    "use strict";


    angular.module('app.controllers').controller('EventCreateCtrl', function($scope, $rootScope, $timeout, $q, $state, HtmlHelper, Upload, VenVastRequest, EventsStorage, VenueStorage, Event, Venue, MapService) {

        var _timeout;

        $scope.init = function() {
            if(!$rootScope.newEvent) {
                $scope.newEvent = {};
            } else {
                $scope.newEvent = $rootScope.newEvent;
            }
            $scope.errors = [];

            $scope.venueCreated = false;

            if($scope.orgVenues && $scope.orgVenues.length > 0) {
                $scope.newEvent.createVenue = false;
            } else {
                $scope.newEvent.createVenue = true;
            }

            $('#category').select2();
            $('#venue').select2();

            $scope.eventMap = MapService.initialize(new VenVastRequest({where: null}), 'event-map');

            $('#startAt').datetimepicker({
                timepicker:true,
                format: "Y.d.m H:i",
                startDate: moment(),
                minDate: moment(),
                onChangeDateTime: function(current, $input) {
                    $scope.newEvent.start_at = $input.val();
                }
            });



            $('#endAt').datetimepicker({
                timepicker:true,
                format: "Y.d.m H:i",
                startDate: moment().add(30, 'm'),
                minDate: moment().add(30, 'm'),
                onChangeDateTime: function(current, $input) {
                    $scope.newEvent.end_at = $input.val();
                }
            });

            HtmlHelper.hideLoader();
            $('.l-content').width('100%');

        };

        $scope.removePicture = function() {
            $scope.newEvent.picture = null;
        };

        $scope.initOrgVenuesSelect = function() {
            if(typeof $scope.newEvent.venue === 'object' && $scope.newEvent.venue !== null) {
                $('#address-search').hide();
                $scope.$watch('orgVenues', function() {
                    $('#venue').val($scope.newEvent.venue._id);
                });
            }
        };


        $scope.toggleAddressSearch = function() {
            if($scope.newEvent.venue === "" || typeof $scope.newEvent.venue === "undefined"){
                $('#address-search').show('slow');
            } else {
                $('#address-search').hide('slow');
            }
        };

        $scope.createLocation = function(lat, lng) {
            return {
                type: "Point",
                coordinates: [lng, lat]
            }
        };

        $scope.createEvent = function($event){
            $event.preventDefault();
            $scope.newEvent.categories = [$('#category').val()];
            //Means that event were picked after address search
            $q.when($scope.setEventAttributes()).then(function() {
                var event = new Event($scope.newEvent);
                $q.when(event.saveOnBackend()).then(function(response) {
                    $state.go('organizer.events', {}, {reload: true});
                });
            });
        };



        $scope.setEventAttributes = function() {
            if(typeof $scope.newEvent.address === "object" && typeof $scope.newEvent.venue === "undefined") {
                var addressObj = $scope.newEvent.address;
                var lat = addressObj.geometry.location.lat();
                var lng = addressObj.geometry.location.lng();
                $scope.newEvent.address = addressObj.formatted_address;
                $scope.newEvent.location = $scope.createLocation(lat, lng);
                if($scope.newEvent.venue_id === null || typeof $scope.newEvent.venue_id === "undefined") {
                    return $q.when($scope.createVenueFromAddress(addressObj)).then(function(venueCreated) {
                        if(venueCreated === false || venueCreated === -1 || typeof venueCreated === "undefined") {
                            var error = "Venue can't be created. Please, try again later or check again the venue data you entered in the form bellow!";
                            $scope.errors.push(error);
                            return false;
                        } else {
                            $scope.newEvent.venue_id = venueCreated._id;
                            return $scope.newEvent;
                        }
                    });

                }
            } else if(typeof $scope.newEvent.venue !== "undefined") { //event owned by user were picked
                $scope.newEvent.venue_id = $scope.newEvent.venue;
                var venueStorage = new VenueStorage();
                return $q.when(venueStorage.find({
                    _id: $scope.newEvent.venue_id
                })).then(function(existingVenues) {
                    console.log("Got existing venues: ", existingVenues);
                    var venue = existingVenues.data[0];
                    $scope.newEvent.address = venue.street;
                    $scope.newEvent.location = venue.location;
                    return $scope.newEvent;
                });
            }
        };

        //
        $scope.createVenueFromAddress = function(addressObj) {
            var venue = new Venue({
                name: addressObj.name ? addressObj.name : null,
                description: "",
                general_info: "",
                phone: addressObj.formatted_phone_number ? addressObj.formatted_phone_number : null,
                website: addressObj.website ? addressObj.website : null,
                street: addressObj.vicinity,
                location: $scope.createLocation(addressObj.geometry.location.lat(), addressObj.geometry.location.lng())
            });
            return $q.when(venue.saveOnBackend()).then(function (response) {
                if(response.status === 200 && response.data.venue !== "undefined") {
                    $scope.venueCreated = response.data.venue;
                } else {
                    $scope.venueCreated = -1;
                }
                return $scope.venueCreated;
            });
        };



        $scope.addressSelected = function(){
            if(typeof $scope.newEvent.address === "object") {
                MapService.map.setCenter($scope.newEvent.address.geometry.location);
                MapService.map.setZoom(16);
                var venueStorage = new VenueStorage();
                var addressObj = $scope.newEvent.address;
                var lat = addressObj.geometry.location.lat();
                var lng = addressObj.geometry.location.lng();
                $q.when(venueStorage.find({
                    latitude: lat,
                    longitude: lng,
                    name: addressObj.name
                })).then(function(existingVenues) {
                    if(existingVenues.data.length > 0) {
                            $scope.existingVenues = existingVenues.data;
                    }
                });

            }
        };

        $scope.nameChanged = function () {
            if(_timeout){ //if there is already a timeout in process cancel it
                $timeout.cancel(_timeout);
            }
            _timeout = $timeout(function(){
                $scope.addressSelected();
                _timeout = null;
            }, 1000);
        };

        $scope.createNewVenueChanged = function() {
            $('.catalog-item').css('border', '0px');
            if($scope.newEvent.createVenue === false) {
                $scope.newEvent.venue_id = null;
            }
        };

        $scope.pickVenue = function(event, venue) {
            $('.catalog-item').css('border', '0px');
            $scope.newEvent.venue_id = venue._id;
            $scope.newEvent.createVenue = false;
            $(event.target).closest('.catalog-item').css('border', '1px solid blue');
        };


        $scope.init();
    });
})();
