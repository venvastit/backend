/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('VenuesCtrl', function($scope, HtmlHelper){
        $scope.$watch('venues', function() {
            if($scope.venues) {
                HtmlHelper.hideLoader();
            }
        });
    });
})();