/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('DealsCtrl', function($scope, $state, UserService, EventService, MapService){
        //$scope.showingDescription = null;
        //$scope.showingEvent = null;
        $scope.init = function(){
            $scope.events = {};
            UserService.getCurrentUser(function(user) {
                $scope.user = user;
            });
            EventService.list(function(events){
                $scope.events.objects = events;
            });
            MapService.requestMap(function(map) {
                $scope.map = map;
            });
        };

        //$scope.showInfo = function(element, event){
        //    if($scope.showingDescription === null && $scope.showingEvent === null){
        //        $('.event-description').removeClass('active');
        //        $('.event').hide();
        //        var eventBlock = $(element.target).closest('.event').eq(0);
        //        eventBlock.show();
        //        $('#venue-'+event.id).addClass('active');
        //        $scope.showingDescription = true;
        //        $scope.showingEvent = true;
        //    } else {
        //        $('.event-description').removeClass('active');
        //        $('.event').show('slow');
        //        $scope.showingDescription = null;
        //        $scope.showingEvent = null;
        //    }
        //
        //};
        $scope.init();
    });
})();