/**
 * Created by oem on 3/19/16.
 */
(function(){
    // "use strict";


    angular.module('app.controllers').controller('HomeCtrl', function($q, $scope, $timeout, $window, $anchorScroll, $state, AnchorSmoothScroll, HtmlHelper, EventsStorage, VenueStorage, Event, UserService, VenVastRequest, MapService, ChangesNotifier, CategoriesStorage){


        $scope.init = function(){
            $scope.win_height_padded = $(window).height() * 1.34;
            $window.onscroll = $scope.revealOnScroll;

            $scope.eventCategoriesRequest = new VenVastRequest({type: 'events'});
            if(typeof $scope.eventRequest === 'undefined' || $scope.eventRequest === null) {
                $scope.eventRequest = new VenVastRequest({when: "week"});
            }
            $scope.venueCategoriesRequest = new VenVastRequest({type: 'venues'});
            if(typeof $scope.venuesRequest === 'undefined' || $scope.venuesRequest === null) {
                $scope.venuesRequest = new VenVastRequest({type: 'venues'});
            }
            HtmlHelper.showLoader();
            $scope.initUser();
            $scope.initEvents();
            $scope.initVenues();
            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_VENUES_LOADED, $scope, function() {
                $timeout(function() {
                    if(typeof  ChangesNotifier.venues_value !== "undefined" && ChangesNotifier.venues_value !== null) {
                        $scope.venues = ChangesNotifier.venues_value;
                    }
                });
            });
            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_EVENTS_LOADED, $scope, function () {
                $timeout(function () {
                    if(typeof  ChangesNotifier.value !== "undefined" && ChangesNotifier.value !== null) {
                        $scope.events = ChangesNotifier.value;
                    }
                });
            });

            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_EVENTS_ADDED, $scope, function() {
                var es = new EventsStorage();
                $scope.addScrollHandler(es, $scope.eventRequest, 'events');
            });
            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_VENUES_ADDED, $scope, function() {
                var vs = new VenueStorage();
                $scope.addScrollHandler(vs, $scope.venuesRequest, 'venues');
            });
        };

        $scope.initEvents = function() {
            var events = new EventsStorage();
            var categories = new CategoriesStorage();
            return $q.when(events.load($scope.eventRequest)).then(function(eventStorage) {
                return $q.when(categories.load($scope.eventCategoriesRequest)).then(function(categoryStorage) {
                    if(typeof categoryStorage === 'undefined' || categoryStorage === null){
                        return false;
                    }
                    $scope.events = eventStorage.data;
                    $scope.eventCategories = categoryStorage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_EVENTS_LOADED);
                    $scope.addScrollHandler(events, $scope.eventRequest, 'events');
                    return true;
                });
            });
        };

        $scope.initVenues = function() {
            var venues = new VenueStorage();
            var categories = new CategoriesStorage();
            return $q.when(venues.load($scope.venuesRequest)).then(function(venueStorage) {
                return $q.when(categories.load($scope.venueCategoriesRequest)).then(function(categoryStorage) {
                    $scope.venues = venueStorage.data;
                    $scope.venueCategories = categoryStorage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_VENUES_LOADED);
                    $scope.addScrollHandler(venues, $scope.venuesRequest, 'venues');
                    return true;
                });
            });
        };

        $scope.initUser = function() {
            $q.when(UserService.getCurrentUser()).then(function(user) {
                $scope.user = user;
            });
        };

        $scope.addScrollHandler = function(storageInstance, requestInstance, scopeStorage) {
            $(window).scroll(function(){
                var state = $state.current.name;
                var lastElement = HtmlHelper.getCoords(document.querySelector('.catalog-wrapper:last-child'));
                var lastElementHeight = $('.catalog-wrapper:last-child').eq(0).outerHeight();
                if(window.pageYOffset + window.screen.height >= lastElement - (lastElementHeight + 50)) {
                    console.log("Reach bottom");
                    if(state === 'home.'+scopeStorage){
                        $q.when(storageInstance.load(requestInstance)).then(function(storage) {
                            if(storage && storage.data) {
                                for(var i= 0; i < storage.data.length; i++) {
                                    $scope[scopeStorage].push(storage.data[i]);
                                }
                                $scope.notifyMoreItemsLoaded(scopeStorage, storage);
                            }
                        });
                    }
                }
            });
        };
        
        $scope.notifyMoreItemsLoaded = function(scopeStorage, storage) {
            switch(scopeStorage) {
                case 'events':
                    ChangesNotifier.value = storage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_EVENTS_ADDED);
                    break;

                case 'venues':
                    ChangesNotifier.venues_value = storage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_VENUES_ADDED);
                    break;
            }
        };

        $scope.revealOnScroll = function () {
            var scrolled = $(window).scrollTop();

           $('.revealOnScroll.animated').each(function(i, element) {
               var __self = $(this);
               var offsetTop = __self.offset().top;
               if (scrolled + $scope.win_height_padded < offsetTop) {
                       __self.removeClass('animated');
                       __self.removeClass(__self.data('animation'));
                   }
            });
            $(".revealOnScroll:not(.animated)").each(function (i, element) {
                var __self = $(this);
                var offsetTop = __self.offset().top;
                if (scrolled + $scope.win_height_padded > offsetTop) {
                    if(__self.data('timeout') && i > 5) {
                        //window.setTimeout(function() {
                            __self.addClass('animated');
                            __self.addClass(__self.data('animation'));
                        //}, 0);
                    }
                }
            });
        };
        
        $scope.toggleInfo = function(object) {
            var element = $('#'+ object.className.toLowerCase() + '-' + object._id);
            if(element.hasClass('catalog-wrapper--full')) {
                $scope.closeInfo();
                // $scope.closeClick(event);
            } else {
                $scope.showInfo(object);
                // $scope.onClick(event);
            }
        };

        $scope.showInfo = function(object) {
            var title = "VenVast " + object.className + " - " + object.title;
            var domIdentfier = object.className.toLowerCase() +'-'+ object._id;
            $('#meta-title').attr('content', title);
            $('title').text(title);
            $('#meta-type').attr('content', "website");
            $('#meta-image').attr('content', object.picture);
            $('#meta-description').attr('content', object.description);
            $('#meta-url').attr('content', 'http://venvast.com/!/'+ domIdentfier);
            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
            MapService.centerOnObject(object);
            AnchorSmoothScroll.scrollTo(domIdentfier);
            $('#' + domIdentfier).addClass('catalog-wrapper--full', {duration: 500});
        };

        $scope.closeInfo = function() {
            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
            $scope.map = MapService.restoreMap();
        };
        
        
        $scope.init();
    });
})();