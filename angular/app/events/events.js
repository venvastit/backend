/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('EventsCtrl', function($q, $scope, $state, HtmlHelper, ChangesNotifier){


        $scope.$watch('events', function() {
            if($scope.events) {
                for(var i = 0; i < $scope.events.length; i++) {
                    var distance = $scope.events[i].computeDistanceTo($scope.eventRequest.getUserPosition());
                    if(distance > 30) {
                        distance = '> 30';
                    }
                    $scope.events[i].distance = distance ? distance + ' km away' : '';
                }
                HtmlHelper.hideLoader();
            }
        });
    });
})();