/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('EventCreateCtrl', function($scope, $state, UserService, EventService, VenueService, MapService, CategoryService) {
        $scope.newEvent = {};
        $scope.datePicker = {
            opened: false
        };

        $scope.init = function(){

            Date.prototype.addHours = function(h) {
                this.setTime(this.getTime() + (h*60*60*1000));
                return this;
            };
            var startDateInit = new Date();
            $scope.newEvent.start_date =
                startDateInit.getFullYear() +"."+
                ("0" + (startDateInit.getMonth()+1)).slice(-2) +"."+
                ("0" + startDateInit.getDate()).slice(-2);/* + " " +*/
                //m.getHours() + ':' + m.getMinutes();
            var endsInInit = new Date().addHours(2);
            $scope.newEvent.ends_in = endsInInit.getFullYear() +"."+
                ("0" + (endsInInit.getMonth()+1)).slice(-2) +"."+
                ("0" + endsInInit.getDate()).slice(-2);
            //$scope.newEvent.duration = 30;
            $scope.newEvent.source_id = 3;
            $scope.newEvent.category = "";

            VenueService.requestListVenues(function(venues) {
                $scope.venues = venues;
            });
            CategoryService.listCategories(function(categories) {
                $scope.categories = categories;
            });

            $('#category').select2();

            $('#eventDate').datetimepicker({
                timepicker:true,
                format: "Y.d.m H:i",
                startDate: new Date(),
                minDate: new Date()
            });

            $('#eventDate').change(function() {
                $scope.newEvent.durationHint = $scope.getEventDurationHint();
                $scope.newEvent.start_date = $('#eventDate').val();
            });

            $('#ends_in').datetimepicker({
                timepicker:true,
                format: "Y.d.m H:i",
                startDate: new Date().addHours(2),
                minDate: new Date().addHours(2)
            });

            $('#ends_in').change(function() {
                $scope.newEvent.durationHint = $scope.getEventDurationHint();
                $scope.newEvent.ends_in = $('#ends_in').val();
            });
            //UserService.getCurrentUser($scope, $state);
        };

        $scope.toggleAddressSearch = function() {
            if($scope.newEvent.venue === "" || typeof $scope.newEvent.venue === "undefined"){
                $('#address-search').show('slow');
            } else {
                $('#address-search').hide('slow');
            }

        };

        $scope.createEvent = function($event){
            $event.preventDefault();
            $scope.newEvent.category = $('#category').val();
            if(typeof $scope.newEvent.address === "object" && typeof $scope.newEvent.venue === "undefined") {
                var addressObj = $scope.newEvent.address;
                var lat = addressObj.geometry.location.lat();
                var lng = addressObj.geometry.location.lng();
                $scope.newEvent.address = addressObj.formatted_address;
                $scope.newEvent.latitude = lat;
                $scope.newEvent.longitude = lng;

                VenueService.find({
                    source: 2,
                    latitude: lat,
                    longitude: lng,
                    name: addressObj.name
                }, function(venue) {
                    if(venue.length === 0){
                        $scope.createVenueFromAddress(addressObj, function(venue) {
                            $scope.newEvent.venue_id = venue.id;
                            $scope.newEvent.latitude = venue.latitude;
                            $scope.newEvent.longitude = venue.longitude;
                            $scope.newEvent.address = venue.street;
                            $scope.newEvent.venue_id = venue.id;
                            EventService.create($scope.newEvent, function() {
                                $state.go('organizer_home');
                            });
                        });
                    } else {
                        venue = venue[0];
                        $scope.newEvent.latitude = venue.latitude;
                        $scope.newEvent.longitude = venue.longitude;
                        $scope.newEvent.address = venue.street;
                        $scope.newEvent.venue_id = venue.id;
                        EventService.create($scope.newEvent, function() {
                            $state.go('organizer_home');
                        });
                    }
                });
            } else {
                VenueService.find({id: $scope.newEvent.venue}, function(venue){
                    if(venue.length === 0)
                        return false;
                    venue = venue[0];
                    $scope.newEvent.latitude = venue.latitude;
                    $scope.newEvent.longitude = venue.longitude;
                    $scope.newEvent.address = venue.street;
                    $scope.newEvent.venue_id = venue.id;
                    EventService.create($scope.newEvent, function() {
                        $state.go('organizer_home');
                    });
                });
            }
        };

        $scope.createVenueFromAddress = function(addressObj, callback) {
            var venue = {};
            venue.name = addressObj.name;
            venue.source_id = 2;
            venue.description = "";
            venue.general_info = "";
            venue.phone = addressObj.formatted_phone_number;
            venue.phone = addressObj.website;
            venue.street = addressObj.vicinity;
            venue.latitude = addressObj.geometry.location.lat();
            venue.longitude = addressObj.geometry.location.lng();
            VenueService.create(venue, callback);
        };

        $scope.addressSelected = function(){
            if(typeof $scope.newEvent.address === "object") {
                $scope.getVenueByLocation($scope.newEvent.address);
                $scope.map = MapService.centerOnEvent({
                    latitude: $scope.newEvent.address.geometry.location.lat(),
                    longitude: $scope.newEvent.address.geometry.location.lng()
                });
                $scope.map.latitude = $scope.newEvent.address.geometry.location.lat();
                $scope.map.longitude = $scope.newEvent.address.geometry.location.lng();
            }
        };

        $scope.getVenueByLocation = function(googleResponse) {
            var location = {
                latitude: googleResponse.geometry.location.lat(),
                longitude: googleResponse.geometry.location.lng()
            };
            VenueService.getByLocation(location, function(venue) {
                if(venue !== null){
                    $scope.newEvent.venue = venue.id;
                }
            });
        };

        $scope.getEventDurationHint = function() {
            var duration = $scope.getEventDuration();
            if(duration.days > 0) {
                return "Duration: " + duration.days + " days";
            }
            return "Duration: " + duration.minutes + " minutes";
        };

        $scope.getEventDuration = function() {
            var startingAt = $scope.parseDate($scope.newEvent.start_date);
            var finishAt = $scope.parseDate($scope.newEvent.ends_in);
            var diffMs = (startingAt - finishAt); // milliseconds between now & Christmas
            var diffDays = Math.round(diffMs / 86400000); // days
            var diffHrs = Math.round((diffMs % 86400000) / 3600000); // hours
            var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes

            return {
                days: diffDays,
                hours: diffHrs,
                minutes: diffMins
            };
        };

        $scope.parseDate = function(dateString) {
            var dateParts = dateString.split(' ');
            var date = dateParts[0].split('.');
            if(typeof dateParts[1] !== "undefined"){
                var time = dateParts[1].split(':');
                return new Date(date[0], date[1], date[2]);
            }
            return new Date(date[0], date[1], date[2]);
        };

        $scope.durationDefined = function() {
            return typeof $scope.newEvent.duration !== "undefined";
        };

        $scope.init();
    });
})();