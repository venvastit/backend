/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('CalendarCtrl', function($q, $scope, $timeout, $state, HtmlHelper, EventsStorage, ChangesNotifier){

        var state = $state.current.name;
        switch (state) {
            case 'home.events':
                $("#calendar-filter").ionCalendar({
                    lang: "en",
                    sundayFirst: false,
                    years: "2015-2018",
                    format: "YYYY-MM-DD",
                    onClick: function(date){
                        $('#when_search').children('span').text(date);
                        $timeout(function() {
                            $scope.eventRequest.when = date;
                        });
                        $scope.eventRequest.date = date;
                        $scope.search();
                    }
                });
                break;
            case 'home.venues':
                $("#calendar-filter").addClass('hidden');
                break;
        }

        $scope.search = function() {
            HtmlHelper.showLoader();
            $scope.eventRequest.page = 0;
            $scope.events = [];
            $scope.eventRequest.type = 'events';
            $scope.eventRequest.hasMorePages = true;
            var events = new EventsStorage();
            return $q.when(events.load($scope.eventRequest)).then(function(eventStorage) {
                $scope.events = eventStorage.data;
                ChangesNotifier.value = $scope.events;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_EVENTS_LOADED, $scope.events);
                HtmlHelper.hideLoader();
            });
            // $scope.eventRequest.getRequestObject()
        };
    });
})();