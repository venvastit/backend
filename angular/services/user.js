(function(){
    "use strict";

    angular.module('app.services').factory('UserModel', function($state, $http, VenVastRequest){

        /**
         *
         * @param userData
         * @constructor
         */
        function User(userData) {
            if(userData) {
                this.loadData(userData);
            }
        }


        /**
         *
         * @type {{
         *
         * GENDER_WOMAN: number,
         * GENDER_MAN: number,
         * TYPE_ATTENDEE: number,
         * TYPE_ORGANIZER: number,
         * login: string|null,
         * email: string|null,
         * first_name: string|null,
         * last_name: string|null,
         * gender: int|null,
         * hide_phone: number,
         * phone: string|null,
         * created_at: string|null,
         * updated_at: string|null,
         * type: number,
         * last_lat: number|null,
         * last_lng: number|null,
         * fb_id: string|null,
         * google_id: string|null,
         * fb_picture: string|null,
         * google_picture: string|null,
         * fb_access_token: string|null,
         * old_id: string|null,
         * _id: string|null,
         * loadData: User.loadData,
         * makeLogin: User.makeLogin,
         * isOrganizer: User.isOrganizer,
         * updateLogin: User.updateLogin,
         * setGenderFromData: User.setGenderFromData,
         * setTypeFromData: User.setTypeFromData,
         * prepareForUpdateRequest: User.prepareForUpdateRequest
         *
         * }}
         */
        User.prototype = {

            GENDER_WOMAN: 0,
            GENDER_MAN: 1,

            TYPE_ATTENDEE: 0,
            TYPE_ORGANIZER: 1,

            login: null,
            email: null,
            first_name: null,
            last_name: null,
            gender: null,
            hide_phone: 0,
            phone: null,
            created_at: null,
            updated_at: null,
            type: this.TYPE_ATTENDEE,
            last_lat: null,
            last_lng: null,
            fb_id: null,
            google_id: null,
            fb_picture: null,
            google_picture: null,
            fb_access_token: null,
            old_id: null,
            _id: null,
            
            initialized: false,


            /**
             * Create object from given data
             * @param userData
             */
            loadData: function (userData) {
                angular.extend(this, userData);
                this.makeLogin(userData);
                this.setGenderFromData(userData);
                this.setTypeFromData(userData);
                this.initialized = true;

            },

            find: function (id) {
                return $http.get('user/' + id).then(function(response) {
                    if(response.success === true && typeof response.user !== "undefined") {
                        return new User(response.user);
                    }
                    return null;
                });
            },

            /**
             * Makes login if it's not set
             *
             * @param userData
             * @returns {*}
             */
            makeLogin: function (userData) {
                    return userData.email;
            },

            /**
             * Can this user create venues, events or deals
             * @returns {boolean}
             */
            isOrganizer: function () {
                return this.type === this.TYPE_ORGANIZER;
            },


            /**
             * Updates login on server
             *
             * @param {string} login
             * @returns {*}
             */
            updateLogin: function(login) {
                var reqObj = this.prepareForUpdateRequest(this);
                reqObj.login = login;
                console.log("before Calling update: ", reqObj);
                return $http.post('user/update', reqObj).then(function(response) {
                    return response.success;
                }, function(error) {
                    console.log("Error occured", error);
                });
            },
            
            updateLatLng: function() {
                var reqObj = this.prepareForUpdateRequest(this);
                return $q.when(VenVastRequest.updateCoordinates()).then(function (position) {
                    reqObj.last_lat = position.latitude;
                    reqObj.last_lng = position.longitude;
                    return $http.post('user/update', {params: reqObj}).then(function(response) {
                        return response.success;
                    }, function(error) {
                        console.log("Error occured", error);
                    });
                });
            },

            /**
             * Updates user on server
             * 
             * @returns {*}
             */
            updateUser: function () {
                var reqObj = this.prepareForUpdateRequest(this);
                return $http.post('user/update', {params: reqObj}).then(function(response) {
                    return response.success;
                }, function(error) {
                    console.log("Error occured", error);
                });
            },


            /**
             * Set gender from data presented
             *
             * @param userData
             * @returns {User}
             */
            setGenderFromData: function (userData) {
                this.gender = parseInt(userData.gender);
                return this;
            },

            /**
             * Set type from data presented
             * @param userData
             * @returns {User}
             */
            setTypeFromData: function (userData) {
                var scope = this;
                if (typeof userData.type === "undefined" || userData.type === "") {
                    this.type = this.TYPE_ATTENDEE;
                } else {
                    userData.type = parseInt(userData.type);
                    switch (userData.type) {
                        case scope.TYPE_ATTENDEE:
                            scope.type = scope.TYPE_ATTENDEE;
                            break;
                        case scope.TYPE_ORGANIZER:
                            scope.type = scope.TYPE_ORGANIZER;
                            break;
                        default:
                            scope.type = scope.TYPE_ATTENDEE;
                            break;
                    }
                }
                return this;
            },
            
            isConnectedWithFacebook: function () {
               return this.fb_id.length > 2; 
            },
            
            isConnectedWithGoogle: function () {
                return this.google_id.length > 2; 
            },

            /**
             * Prepare oject for performing POST update request
             *
             * @param userObject
             * @returns {*}
             */
            prepareForUpdateRequest: function(userObject) {
                var reqObj = {};
                angular.copy(reqObj, userObject);
                delete reqObj.password;
                delete reqObj.created_at;
                delete reqObj._id;
                angular.copy(reqObj.id, userObject._id);
                return reqObj;
            },
        };

        return User;
    });

})();