(function(){
    "use strict";
    angular.module('app.services').factory('HtmlHelper', function($state, $rootScope, Restangular){

        return {

            body: null,
            overlay: null,
            link :null,
            content: null,
            popups:null,
            document:null,


            init: function() {
                $(window).scroll(function(){
                    var scroll = $(window).scrollTop();
                    var category = $('div.category').eq(0);
                    if (scroll > 200) {
                        category.addClass('category--fixed');
                    }
                    else {
                        category.removeClass('category--fixed');
                    }
                });
                this.getLink().on('click', this.render_tab);
                $('.container ul li .active').removeClass('active');
            },

            getCoords: function (elem) {
                if(elem) {
                    var box = elem.getBoundingClientRect();
                    return box.top + pageYOffset;
                }
            },

            closePopups: function() {
                $('html').addClass('scroll');
                $('.popup-overlay').addClass('hide');
                for(var i = 0; i < $('.popup-simple').length; i++){
                    $($('.popup-simple')[i]).addClass('hide');
                }
                $('.category').removeClass('hide');
                $('.loader-wrapper').hide();
            },

            showPopUp: function(popupId) {
                // $('html').removeClass('scroll');
                $('.popup-overlay').removeClass('hide');
                $("#"+popupId).removeClass('hide');
                $('.category').addClass('hide');
                $('.loader-wrapper').hide('hide');
            },

            render_tab: function (event, tabId) {
                $('.tab-link li').removeClass('active');
                $('.tab-content li').removeClass('active');
                $('#'+tabId).addClass('active');
                $(event.target).closest('li').addClass('active');

            },

            getBody: function() {
                if(this.body === null)
                    this.body = $('body');
                return this.body;
            },
            getOverlay: function() {
                if(this.overlay === null)
                    this.overlay = $('.popup-overlay');
                return this.overlay;
            },
            getLink: function() {
                if(this.link === null)
                    this.link = $('.tab-link');
                return this.link;
            },
            getContent: function() {
                if(this.content === null)
                    this.content = $('.tab-content li');
                return this.content
            },
            getPopups: function() {
                if(this.popups === null)
                    this.popups = $('.popup-simple');
                return this.popups
            },

            getDocument: function() {
                return document;
            },

            StyledDropDown: function(selector, model) {
                this.dd = $(selector);
                this.placeholder = this.dd.children('span');
                this.opts = this.dd.find('ul.dropdown li');
                this.val = '';
                this.index = -1;

                this.initEvents = function() {
                    var obj = this;
                    obj.dd.on('click', function(event){
                        $(this).toggleClass('active');
                        return false;
                    });
                    obj.opts.on('click',function(){
                        var opt = $(this);
                        var cat_id = opt.find('a').data('value');
                        obj.val = opt.text();
                        obj.index = opt.index();
                        obj.placeholder.text(obj.val);
                    });

                    $('body').on('click', function () {
                        obj.dd.removeClass('active');
                    });
                };
                
                this.getValue = function() {
                    return this.val;
                };
                this.getIndex= function() {
                    return this.index;
                };

                this.initEvents();
            },

            /**
             * Show loader animation
             */
            showLoader: function () {
                $('html').removeClass('scroll');
                $('#loader-wrapper').show();
                $('.catalog-wrapper').addClass('hide');
            },

            /**
             * Hide loader animation
             */
            hideLoader: function () {
                $('html').addClass('scroll');
                $('#loader-wrapper').hide();
                $('.catalog-wrapper').removeClass('hide');
            }
        };

    });

})();