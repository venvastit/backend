(function(){
    "use strict";


    angular.module('app.services').factory('VenueStorage', function($q, $http, VenVastRequest, Event, EventsStorage, Venue){

        /**
         * 
         * @param {Array} venuesArray
         * @constructor
         */
        function VenueStorage(venuesArray) {
            if(venuesArray) {
                for(var i = 0; i < venuesArray.length; i++) {
                    if(!(venuesArray[i] instanceof Venue)) {
                        venuesArray[i] = new Venue(venuesArray[i]);
                    }
                }
                this.loadArray(venuesArray);
            }
        }

        /**
         * 
         * @type {{
         * 
         * data: Array, 
         * loading: boolean, 
         * loadArray: VenueStorage.loadArray, 
         * list: VenueStorage.list, 
         * create: VenueStorage.create, 
         * getData: VenueStorage.getData
         * 
         * }}
         */
        VenueStorage.prototype = {
            data: [],
            loading: false,

            /**
             * Load array data to storage
             * 
             * @param venuesArray
             * @returns {VenueStorage}
             */
            loadArray: function (venuesArray) {
                this.data = venuesArray;
         
                return this;
            },

            /**
             *
             * @param {VenVastRequest} request
             * @returns {*}
             */
            load: function (request) {
                var scope = this;
                if(this.loading === false) {
                    scope.loading = true;
                    if(request.hasMorePages === false) {
                        return [];
                    }
                    return $http.get('venue', {params: request.getRequestObject()}).success(function(venueData) {
                        for(var i = 0; i < venueData.length; i++) {
                            venueData[i] = new Venue(venueData[i]);
                            if(!venueData[i].d || typeof venueData[i].d === 'undefined') {
                                venueData[i].d = venueData[i].computeDistanceTo(request.getUserPosition());
                            }
                            venueData[i].d = parseFloat(venueData[i].d);
                            venueData[i].distance = venueData[i].d ? venueData[i].d + ' km away' : '';
                        }
                        request.requestPerformed(venueData);
                        scope.loadArray(venueData);
                        scope.loading = false;
                        return scope;
                    });
                }
            },

            find: function(params) {
                var scope = this;
                if(this.loading === false) {
                    scope.loading = true;
                    return $http.get('venue/find', {params: params}).success(function(venueData) {
                        scope.loading = false;
                        return venueData.data;
                    });
                }
            },

            /**
             * Create new venue
             * @param venue
             * @returns {*}
             */
            create: function(venue) {
                return $http.post('venue/create', {params: venue}).success(function(response){
                    return response;
                });
            },

            /**
             * Returns all objects in storage
             * 
             * @returns {Array}
             */
            getData: function () {
                return this.data;
            }
        };

        return VenueStorage;
    });

})();