(function(){
    "use strict";


angular.module('app.services').factory('Category', function($state, $http){

    function Category(categoryData) {
        if (categoryData) {
            this.setData(categoryData);
        }
        //что-то, что еще нужно для инициализации книги
    };

    Category.prototype = {
        setData: function(categoryData) {
            angular.extend(this, categoryData);
        },

        /**
         * 
         * @param categoryId
         * @returns {Promise}
         */
        load: function(categoryId) {
            var scope = this;
            return $http.get('category/' + categoryId).success(function(categoryData) {
                scope.setData(categoryData);
                return scope;
            });
        },
        
        getImageUrl: function() {
            return 'https://venvast.com/img/cache/original/categories/' + this.image
        }
    };
    return Category;

})})();