(function(){
    "use strict";


    angular.module('app.services').factory('VenVastRequest', function($q){

        /**
         * @param requestData
         * @constructor
         */
        function VenVastRequest(requestData) {
            if(requestData) {
                this.loadData(requestData);
                if(!this.where) {
                    this.updateCoordinates();
                }
            }
        }

        function updateCoordinates() {
            var __self = this;
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    return __self.setPosition(position).getPosition();
                }, function(error) {
                    if(error) {
                        return __self.mockPosition().getPosition();
                    }
                });
            }
        }

        /**
         *
         * @type {{
         * when: string|null,
         * where: string|null,
         * category: string|null,
         * latitude: float|null,
         * longitude: float|null,
         * page: number,
         * web: string,
         * radius: number,
         * hasMorePages: boolean,
         * loadData: VenVastRequest.loadData,
         * updateCoordinates: VenVastRequest.updateCoordinates,
         * requestPerformed: VenVastRequest.requestPerformed,
         * mockPosition: VenVastRequest.mockPosition,
         * setPosition: VenVastRequest.setPosition,
         * getPosition: VenVastRequest.getPosition
         * }}
         */
        VenVastRequest.prototype = {

            WHEN_TODAY: 'today',
            WHEN_TOMORROW: 'tomorrow',
            WHEN_WEEK: 'week',
            WHEN_MONTH: 'month',

            when: null,
            where: null,
            category: null,
            latitude: null,
            longitude: null,
            hosts: false,
            page: 0,
            web: 'true',
            radius: 25,
            perPage: 60,
            text: null,
            date: null,

            hasMorePages: true,

            /**
             * Load data to this object
             * @param data
             */
            loadData: function(data) {
                angular.extend(this, data);
            },
            
            
            setWhere: function(latitude, longitude, name) {
                this.where = {
                    latitude: latitude,
                    longitude: longitude,
                    name: name
                };
                return this;
            },



            /**
             * Updates coordinates and returns updated position object
             *
             * @return Promise
             */
            updateCoordinates: function () {
                var __self = this;
                if(navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        return __self.setPosition(position).getPosition();
                    }, function(error) {
                        if(error) {
                            return __self.mockPosition().getPosition();
                        }
                    });
                }
            },

            requestPerformed: function(response) {
                if(response.length === 0 || typeof response === "undefined" || response.length < this.perPage) {
                    this.hasMorePages = false;
                    return;
                }
                this.page++;
            },

            /**
             *
             * @returns {VenVastRequest}
             */
            mockPosition: function() {
                this.latitude = 13.7252764;
                this.longitude = 100.587646;
                return this;
            },

            /**
             * @param position
             * @returns {VenVastRequest}
             */
            setPosition: function(position) {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                return this;
            },

            /**
             * Get position of request
             * @returns {{latitude: float, longitude: float}}
             */
            getPosition: function() {
                var scope = this;
                if(scope.where !== null) {
                    return {
                        latitude: scope.where.latitude,
                        longitude: scope.where.longitude,
                        zoom: 10
                    };
                } else {
                    return {
                        latitude: scope.latitude,
                        longitude: scope.longitude
                    };
                }
            },

            /**
             * Get position of user
             *
             * @returns {{latitude: *, longitude: *}}
             */
            getUserPosition: function() {
                var scope = this;
                return {
                    latitude: scope.latitude,
                    longitude: scope.longitude
                };  
            },

            isValidWhenValue: function(value) {
                switch(value) {
                    case this.WHEN_WEEK:
                        return true;
                    case this.WHEN_TODAY:
                        return true;
                    case this.WHEN_TOMORROW:
                        return true;
                    case this.WHEN_MONTH:
                        return true;
                }
                return false;
            },

            getRequestObject: function () {
                var obj = {};
                var scope = this;
                if(this.when !== null && this.isValidWhenValue(this.when)) {
                    obj.when = this.when;
                }
                if(this.organizer === null || typeof this.organizer === 'undefined') {
                    var pos = this.getPosition();
                    if(pos.latitude === null && pos.longitude === null) {
                        scope.mockPosition();
                        obj.latitude = this.latitude;
                        obj.longitude = this.longitude;
                    } else {
                        obj.latitude = pos.latitude;
                        obj.longitude = pos.longitude;
                    }

                } else {
                    $q.when(this.updateCoordinates()).then(function(coordinates) {
                        scope.mockPosition();
                    });
                }
                if(this.hosts === true) {
                    obj.hosts = 'true';
                }
                if(this.category !== null) {
                    obj.category = this.category;
                }
                if(this.where && typeof this.where.latitude !== 'undefined') {
                    obj.latitude = this.where.latitude;
                    obj.longitude = this.where.longitude;
                }
                if(this.where === null && this.latitude !== null && this.longitude !== null) {
                    obj.latitude = this.latitude;
                    obj.longitude = this.longitude;
                }
                if(this.type !== null) {
                    obj.type = this.type;
                }
                if(this.text !== null) {
                    obj.text = this.text;
                }
                if(this.date !== null) {
                    obj.date = this.date;
                }
                if(this.organizer !== null) {
                    obj.organizer = this.organizer;
                }
                obj.web = this.web;
                obj.radius = this.radius;
                obj.page = this.page;
                obj.timezone = jstz.determine().name();
                return obj;
            }

        };

        return VenVastRequest;
    });

})();