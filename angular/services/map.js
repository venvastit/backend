(function(){
    "use strict";


    angular.module('app.services').factory('MapService', function($q, UserService, VenueStorage, EventsStorage, VenVastRequest, AnchorSmoothScroll){

        return {

            KEY_EVENTS: 'events',
            KEY_DEALS: 'deals',
            KEY_VENUES: 'venues',

            ZOOM_INIT: 14,
            ZOOM_DETAIL: 17,

            request: null,
            markers: {
                events: [],
                venues: [],
                deals: []
            },
            currentMarkersKey: 'events',
            map: null,
            mapOptions: null,
            userMarker: null,

            prevState: null,
            prevRequest: null,

            /**
             * Extends an instance with an object provided
             * @param options
             */
            setOptions: function (options) {
                angular.extend(this, options);
            },

            /**
             * Set markers from object given
             * @param markers
             * @returns {MapService}
             */
            setMarkers: function(markers) {
                this.markers = markers;
                return this;
            },

            /**
             * Set event markers from given array of Events
             *
             * @param {Event[]} events
             */
            setEventMarkers: function(events) {
                var markers = [];
                var scope = this;
                if(events) {
                    for (var i = 0; i < events.length; i++) {
                        var marker = events[i].getMarker();
                        marker.addListener('click', function() {
                            var ma = this;
                            if(scope.mapOptions.zoom !== scope.ZOOM_DETAIL){
                                scope.prevState = scope.mapOptions;
                            }
                            scope.mapOptions = {
                                center: ma.getPosition(),
                                zoom: scope.ZOOM_DETAIL
                            };
                            scope.map.setCenter(scope.mapOptions.center);
                            scope.map.setZoom(scope.mapOptions.zoom);
                            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
                            AnchorSmoothScroll.scrollTo(ma.id);
                            $('#' + ma.id).addClass('catalog-wrapper--full', {duration: 500});
                        });
                        marker.setMap(scope.map);
                        markers.push(marker);
                    }
                }
                this.markers.events = markers;
            },

            addEventMarkers: function(events) {
                var markers = [];
                var scope = this;
                if(events) {
                    for (var i = 0; i < events.length; i++) {
                        var marker = events[i].getMarker();
                        marker.addListener('click', function() {
                            var ma = this;
                            if(scope.mapOptions.zoom !== scope.ZOOM_DETAIL){
                                scope.prevState = scope.mapOptions;
                            }
                            scope.mapOptions = {
                                center: ma.getPosition(),
                                zoom: scope.ZOOM_DETAIL
                            };
                            scope.map.setCenter(scope.mapOptions.center);
                            scope.map.setZoom(scope.mapOptions.zoom);
                            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
                            AnchorSmoothScroll.scrollTo(ma.id);
                            $('#' + ma.id).addClass('catalog-wrapper--full', {duration: 500});
                        });
                        marker.setMap(scope.map);
                        markers.push(marker);
                        this.markers.events.push(marker);
                    }
                }
            },


            /**
             * Set venue markers from given array of Venues
             * @param {Venue[]} venues
             */
            setVenueMarkers: function(venues) {
                var markers = [];
                var scope = this;
                if(venues) {
                    console.log(venues);
                    for (var i = 0; i < venues.length; i++) {
                        var marker = venues[i].getMarker();
                        marker.addListener('click', function() {
                            var ma = this;
                            if(scope.mapOptions.zoom !== scope.ZOOM_DETAIL){
                                scope.prevState = scope.mapOptions;
                            }
                            scope.mapOptions = {
                                center: ma.getPosition(),
                                zoom: scope.ZOOM_DETAIL
                            };
                            scope.map.setCenter(scope.mapOptions.center);
                            scope.map.setZoom(scope.mapOptions.zoom);
                            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
                            AnchorSmoothScroll.scrollTo(ma.id);
                            $('#' + ma.id).addClass('catalog-wrapper--full', {duration: 500});
                        });
                        marker.setMap(scope.map);
                        markers.push(marker);
                    }
                }
                this.markers.venues = markers;
            },

            addVenueMarkers: function(venues) {
                var markers = [];
                var scope = this;
                if(venues) {
                    for (var i = 0; i < venues.length; i++) {
                        var marker = venues[i].getMarker();
                        marker.addListener('click', function() {
                            var ma = this;
                            if(scope.mapOptions.zoom !== scope.ZOOM_DETAIL){
                                scope.prevState = scope.mapOptions;
                            }
                            scope.mapOptions = {
                                center: ma.getPosition(),
                                zoom: scope.ZOOM_DETAIL
                            };
                            scope.map.setCenter(scope.mapOptions.center);
                            scope.map.setZoom(scope.mapOptions.zoom);
                            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
                            AnchorSmoothScroll.scrollTo(ma.id);
                            $('#' + ma.id).addClass('catalog-wrapper--full', {duration: 500});
                        });
                        marker.setMap(scope.map);
                        markers.push(marker);
                        this.markers.venues.push(marker);
                    }
                }
            },

            /**
             * Set deals markers from given array of deals
             *
             * TODO: implement after implementing deals
             * @param markers
             */
            setDealsMarkers: function(markers) {
                this.markers.deals = markers;
            },

            /**
             * Hide all markers from the map and delete them from this request
             */
            deleteAllMarkers: function() {
                var scope = this;
                var events = this.markers.events;
                var venues = this.markers.venues;
                var deals = this.markers.deals;
                this.deleteMarkers(events);
                this.deleteMarkers(venues);
                this.deleteMarkers(deals);
                this.setAllMarkersEmpty();
            },

            /**
             * Set all markers in this object as an empty array
             * It clearing all info about markers in this object
             * but not actually removes them from the map
             */
            setAllMarkersEmpty: function() {
              this.markers = {
                  events: [],
                  venues: [],
                  deals: []
              };
            },

            /**
             * Delete all markers from the map by setting
             * map reference in Marker objects t0 null
             * @param markers
             */
            deleteMarkers: function(markers) {
                if(markers && markers.length > 0) {
                    for(var i = 0; i < markers.length; i++) {
                        var marker = markers[i];
                        marker.setMap(null);
                        delete markers[i];
                    }
                }
            },

            /**
             * Create user marker, attach it to map
             * and save in current MapService instance
             */
            showUserMarker: function() {
                var position = this.request.getUserPosition();
                if(typeof position.latitude !== "udefined" && position.latitude) {
                    var userMarker = new google.maps.Marker({
                        icon: new google.maps.MarkerImage('/img/gpsloc.png', null, null, null, new google.maps.Size(16, 16)),
                        position: {lat: position.latitude, lng: position.longitude},
                        id: "userPos"
                    });
                    userMarker.setMap(this.map);
                    this.userMarker  = userMarker;
                }
            },

            /**
             * Hides user marker on the map and remove it from MapService
             * instance
             */
            hideUserMarker: function() {
                this.userMarker.setMap(null);
                this.userMarker = null;
            },

            /**
             *
             * Initialize map. Sets center as a current user position, we got from request
             *
             * @param {VenVastRequest} request
             * @param {string} elementId
             */
            initialize: function(request, elementId) {

                var scope = this;
                return $q.when(request.updateCoordinates()).then(function (coordinates) {
                    position = request.getPosition();
                    if(typeof position === "undefined") {
                        var position = request.mockPosition();
                    }
                    if(typeof coordinates !== "undefined") {
                        position = coordinates;
                    }
                    if(request.where !== null && typeof request.where.latitude !== 'undefined') {
                        console.log("Reuest where is not null");
                        position = request.where;
                    }
                    var element;
                    if(elementId) {
                        element = document.getElementById(elementId);
                    } else {
                        element = document.getElementById('map-canvas');
                    }
                    var mapOptions = {
                        center: new google.maps.LatLng(position.latitude, position.longitude),
                        zoom: position.zoom ? position.zoom : scope.ZOOM_INIT,
                        zoomControl: true,
                        scaleControl: true,
                        disableDefaultUI: true,
                        styles: [
                            {
                                featureType: "poi",
                                stylers: [
                                    { visibility: "off" }
                                ]
                            }
                        ]
                    };
                    scope.map = new google.maps.Map(element, mapOptions);
                    scope.mapOptions = mapOptions;
                    scope.request = request;
                    return scope;
                });
            },

            /**
             * Check if current request is not equels previous one
             * So we can update markers
             * @returns {boolean}
             */
            requestUpdated: function() {
                return !angular.equals(this.request, this.prevRequest);
            },

            /**
             * Get markers that should be displayed according to current state
             * @returns {*}
             */
            getCurrentStateMarkers: function() {
                return this.markers[this.currentMarkersKey];
            },

            /**
             * Center map on object given
             *
             * TODO: extend with deals
             *
             * @param {Event|Venue} obj
             * @returns {null}
             */
            centerOnObject: function(obj) {
                var scope = this;
                var coordinates = obj.getCoordinates();
                this.prevState = this.mapOptions;
                this.mapOptions = {
                    center: {lat: coordinates.latitude, lng: coordinates.longitude},
                    zoom: scope.ZOOM_DETAIL,
                };
                this.map.setCenter(this.mapOptions.center);
                this.map.setZoom(this.mapOptions.zoom);
                return this.map;
            },

            /**
             * Restore map to previous saved state
             *
             * @returns {null}
             */
            restoreMap: function() {
                var tmp;
                angular.copy(this.mapOptions, tmp);
                this.mapOptions = this.prevState;
                this.prevState = tmp;
                this.map.setCenter(this.mapOptions.center);
                this.map.setZoom(this.mapOptions.zoom);
                return this.map;
            }

        };

    });

})();