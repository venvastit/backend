(function(){
    "use strict";

    
    angular.module('app.services').factory('UserService', function($http, UserModel, $localStorage, $q, $timeout){
        return {
            user: null,
            formFields: {},
            loginError: null,
            homeTimeout: null,

            /**
             * get current user(no matters how)
             * @returns {*}
             */
            getCurrentUser: function() {
                if(this.user !== null && this.user.__id){
                    return this.user;
                }else{
                    return this.requestCurrentUser();
                }
            },

            /**
             * Request current user from server
             *
             * @returns {*}
             */
            requestCurrentUser: function() {
                var scope = this;

                if(this.homeTimeout){ //if there is already a timeout in process cancel it
                    $timeout.cancel(scope.homeTimeout);
                }

                return this.homeTimeout = $timeout(function () {
                    if(!$localStorage.token)
                        return null;
                    return $http.get('user/home').then(function(response){
                        if(typeof response === "undefined" || typeof response.data === 'undefined') {
                            scope.user = null;
                            return scope.user;
                        }
                        response = response.data;
                        if(response == null || response.length === 0 || angular.equals({}, response)){
                            scope.user = null;
                        } else {
                            scope.user = new UserModel(response);
                        }
                        scope.homeTimeout = null;
                        return scope.user;
                    });
                }, 5000);
            },

            /**
             * Do login by user credentials
             * @param {Object} formData
             */
            login: function(formData) {
                var scope = this;
                return $http.post('api/authenticate', formData).then(function(response){
                    response = response.data;
                    if(response.token !== null && typeof response.token !== 'undefined') {
                        $localStorage.token = response.token;
                        if(response.token) {
                            return $http.get('user/home').then(function(userResponse) {
                                userResponse = userResponse.data;
                                if(userResponse !== null && typeof userResponse !== "undefined") {
                                    scope.user = new UserModel(userResponse);
                                }
                                return scope.user;
                            });
                        } else {
                            scope.user = null;
                            return scope.user;
                        }
                    }
                }, function(error) {
                    scope.loginError = error;
                });
            },

            /**
             * Set email and password typed in login form
             *
             * @param email
             * @param password
             */
            setLoginFormFields: function(email, password) {
                this.formFields.email = email;
                this.formFields.password = password;
            },

            /**
             * Login to facebook
             *
             * @param Facebook
             * @returns {User}
             */
            loginFacebook: function(Facebook) {
                var scope = this;
                scope.user = {};
                return Facebook.getLoginStatus(function(fbStatusResponse) {
                    if(fbStatusResponse.status === 'connected') {
                        return fbStatusResponse.authResponse.accessToken;
                        
                    } else {
                        return Facebook.login(function(loginResponse) {
                            return loginResponse.authResponse.accessToken;
                        });
                    }
                });
            },

            /**
             * Updates user model with facebook credentials
             *
             * @param user
             * @returns {*}
             */
            backendFacebookLogin: function(user) {
                var scope = this;
                return $http.post('user/facebook', user).then(function(response){
                    if(response.success === true){
                        scope.user = response.user;
                        scope.user.initialized = true;
                        return scope.user;
                    }
                });
            },

            /**
             * Set data we got from facebook
             * to current user
             *
             * @param fbUserDetails
             * @returns {updateWithFbData}
             */
            updateWithFbData: function(fbUserDetails) {
                var formEmail = typeof this.formFields.email;
                if(typeof formEmail !== "undefined" && formEmail.length > 6) {
                    this.user.email = this.formFields.email;
                } else {
                    this.user.email = fbUserDetails.email;
                }
                this.user.fb_id = fbUserDetails.id;
                this.user.gender = fbUserDetails.gender.toLowerCase() === "male" ? 1 : 0;
                this.user.first_name = fbUserDetails.first_name;
                this.user.last_name = fbUserDetails.last_name;
                this.user.fb_picture = fbUserDetails.picture.data.url;
                return this;
            },

            /**
             * Perform logging out
             * @returns {*}
             */
            logout: function() {
                var scope = this;
                return $http.get('user/logout').then(function(response){
                    response = response.data;
                    if(response.success === true){
                        scope.user = null;
                        $localStorage.token = null;
                        return scope.user;
                    }
                });
            },

            /**
             * Perform register action
             * @param userData
             * @returns {*}
             */
            register: function(userData){
                var scope = this;
                return $http.post('user/register', userData).then(function(response){
                    response = response.data;
                    console.log("Got response: ", response);
                    if(response.success === true){
                        scope.user = new UserModel(response.user);
                        return scope.user;
                    }
                });
            }
        };
    });

})();