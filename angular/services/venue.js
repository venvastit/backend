(function(){
    "use strict";


    angular.module('app.services').factory('Venue', function($http, VenVastRequest, Event, Upload){

        function Venue(venueData) {
            if(venueData) {
                this.setData(venueData);
            }
        }

        Venue.prototype = {

            name: null,
            birthday: null,
            cover_image: null,
            description: null,
            general_info: null,
            street: null,
            zip: null,
            phone: null,
            website : null,
            public_transit : null,
            published : false,
            location : null,
            categories : [],
            upcomingEvents: [],
            currentDeals: [],
            upcomingDeals: [],
            _id : null,
            className: 'Venue',


            setData: function (venueData) {
                angular.extend(this, venueData);
                var scope = this;
                if(typeof scope.events !== 'undefined' && scope.events.length > 0) {
                    for(var i = 0; i < scope.events.length; i++) {
                        scope.events[i] = new Event(scope.events[i]);
                    }
                }
                this.icon = this.getIcon();
                return this;
            },

            find: function(id) {
                return $http.get('venue/' + id).then(function (response) {
                    if(response !== null)
                        return new Venue(response);
                });
            },

            getIcon: function () {
                if(this.categories.length === 0 || !this.categories || typeof this.categories === 'undefined') {
                    return '/img/question.png';
                } else {
                    if(typeof this.categories[0] !== 'undefined' &&  this.categories[0] !== null && this.categories[0].image) {
                        return "/img/cache/original/categories/" + this.categories[0].image;
                    } else {
                        return '/img/question.png';
                    }
                }
            },
            
            getPicture: function() {
                if(this.cover_image === null || typeof this.cover_image === 'undefined' || this.cover_image === ''){
                    return 'https://placeholdit.imgix.net/~text?txtsize=33&txt=' + this.name + '&w=350&h=150';
                }

                if(typeof this.cover_image !== 'string'){
                    return this.cover_image
                }
                if(!this.cover_image.startsWith('http')) {
                    return '/img/cache/original/venues/' + this.cover_image;
                } else {
                        return this.cover_image;
                }
            },


            saveOnBackend: function () {
                if(this.cover_image !== null) {
                    return Upload.upload({url: 'venue/create', data: this})
                        .then(function (resp) {
                            return resp;
                        });
                }
                return $http.post('venue/create', this).then(function(response) {
                    return response;
                });
            },
            
            deleteFromBackend: function () {
                return $http.get('venue/delete/'+this._id).then(function(response) {
                    return response;
                });
            },

            getCoordinates: function () {
                return {
                    longitude: this.location.coordinates[0],
                    latitude: this.location.coordinates[1]
                }
            },

            isPublished: function() {
                return this.published;
            },

            createOpenHours: function(openHours) {
                for(var i = 0; i < openHours.length; i++) {
                    var dayObject = openHours[i];
                    dayObject.day = parseInt(dayObject.day);
                    if(typeof dayObject.open === 'object') {
                        dayObject.open = {
                            day: dayObject.day,
                            time: moment(dayObject.open.time).format('HHmm')
                        }
                    } else {
                        dayObject.open = false;
                    }
                    if(typeof dayObject.close === 'object') {
                        dayObject.close = {
                            day: dayObject.day,
                            time: moment(dayObject.close.time).format('HHmm')
                        }
                    } else {
                        dayObject.open = false;
                    }
                    openHours[i] = dayObject;
                }
                return openHours;
            },

            createOpeningHoursTemplate: function() {
                var hours = [];
                var scope = this;
                for(var i = 0; i < 7; i++) {
                    if(i < 1 || i > 5) {
                        var closed = scope.getClosedDayObject(i);
                        hours.push(closed);
                    } else {
                        var day = scope.getOpenHoursDayObject(i, "0900", "2000");
                        hours.push(day);
                    }
                }
                return hours;
            },

            getOpenHoursDayObject: function(day, open, close) {
                return {
                    day: day,
                    open: {
                        day: day,
                        time: open
                    },
                    close: {
                        day: day,
                        time: close
                    }
                };
            },

            getClosedDayObject: function(day) {
                return {
                    day: day
                }
            },

            getMarker: function() {
                var scope = this;
                var coordinates = scope.getCoordinates();
                return new google.maps.Marker({
                    icon: new google.maps.MarkerImage(scope.getIcon(), null, null, null, new google.maps.Size(32, 32)),
                    position: {lat: coordinates.latitude, lng: coordinates.longitude},
                    id: 'venue-' + scope._id
                });
            },

            /**
             * Returns distance in meter between point specified and
             * this Venue
             *
             * @param {{latitude: float, longitude: float}} point
             * @returns {number}
             */
            computeDistanceTo: function (point) {
                var venueCoordinates = this.getCoordinates();
                var pointLatLng = new google.maps.LatLng({lat: point.latitude, lng: point.longitude});
                var VenueLatLng = new google.maps.LatLng({lat: venueCoordinates.latitude, lng: venueCoordinates.longitude});

                return parseFloat((google.maps.geometry.spherical.computeDistanceBetween(pointLatLng, VenueLatLng) / 1000).toFixed(2));
            },

            /**
             * Return string name of day of week by its int code
             * @param code
             * @returns {*}
             */
            getDayOfWeekByCode: function (code) {
                switch (code) {
                    case 0:
                        return 'sunday';
                    case 1:
                        return 'monday';
                    case 2:
                        return 'tuesday';
                    case 3:
                        return 'wednesday';
                    case 4:
                        return 'thursday';
                    case 5:
                        return 'friday';
                    case 6:
                        return 'saturday';
                }
            }
            
        };

        return Venue;

    });

})();