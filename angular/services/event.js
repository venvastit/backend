(function(){
    "use strict";


    angular.module('app.services').factory('Event', function(Upload, $http) {


        function Event(eventData) {
            if(eventData) {
                this.loadData(eventData);
            }
        };

        Event.prototype = {

            _id: null,
            title: null,
            details: null,
            address: null,
            start_at: null,
            end_at: null,
            location: null,
            categories: [],
            venue: null,
            picture: null,
            className: 'Event',

            attendees: [],
            organizers: [],
            likes: 0,

            loadData: function (data) {
                angular.extend(this, data);
                if(typeof this.venue_relation !== 'undefined' && this.veue_relation !== null) {
                    this.venue = this.venue_relation;
                }
                this.icon = this.getIcon();
                return this;
            },
            getCoordinates: function () {
                if(typeof this.location === "undefined" || this.location === null) {
                    this.location = this.venue.location;
                    this.updateOnBackend();
                }
                return {
                    longitude: this.location.coordinates[0],
                    latitude: this.location.coordinates[1]
                }
            },

            getIcon: function () {
                if(this.categories.length === 0 || !this.categories || typeof this.categories === 'undefined') {
                    return '/img/question.png';
                } else {
                    if(typeof this.categories[0] !== 'undefined' &&  this.categories[0] !== null && this.categories[0].image) {
                        return "/img/cache/original/categories/" + this.categories[0].image;
                    } else {
                        return '/img/question.png';
                    }
                }
            },

            getPicture: function() {
                if(this.picture === null || typeof this.picture === 'undefined' || this.picture === ''){
                    return 'https://placeholdit.imgix.net/~text?txtsize=33&txt=' + this.title + '&w=350&h=150';
                }
                if(typeof this.picture !== 'string'){
                    return this.picture
                }
                if(!this.picture.startsWith('http')) {
                    return '/img/cache/original/events/' + this.picture;
                } else {
                    return this.picture;
                }
            },

            getMarker: function() {
                var scope = this;
                var coordinates = scope.getCoordinates();
                return new google.maps.Marker({
                    icon: new google.maps.MarkerImage(scope.getIcon(), null, null, null, new google.maps.Size(32, 32)),
                    position: {lat: coordinates.latitude, lng: coordinates.longitude},
                    id: 'event-' + scope._id
                });
            },

            saveOnBackend: function () {
                if(this.picture !== null) {
                    return Upload.upload({url: 'event/create', data: this})
                        .then(function (resp) {
                            return resp;
                        });
                }
                return $http.post('event/create', this).then(function(response) {
                    return response;
                });
            },

            updateOnBackend: function() {
                if(typeof this.picture === 'object') {
                    return Upload.upload({url: 'event/update/'+this._id, data: this})
                        .then(function (resp) {
                            return resp;
                        });
                } else {
                    return $http.post('event/update/'+this._id, this).then(function(response) {
                        return response;
                    });
                }
            },

            /**
             *
             * @returns {moment.Moment|*}
             */
            getStartAt: function() {
                return moment(this.start_at);
            },

            /**
             *
             * @returns {moment.Moment|*}
             */
            getEndAt: function() {
                if(this.end_at === null)
                    return null;
                return moment(this.end_at);
            },

            /**
             *
             * @returns {string}
             */
            getStartHours: function () {
                return this.getStartAt().format('h:mm a');
            },

            getEndHours: function() {
                if(this.getEndAt() === null)
                    return "unknown";
                return this.getEndAt().format('h:mm a');
            },

            /**
             *
             * @returns {string}
             */
            getStartDate: function () {
                return this.getStartAt().format('YYYY-MM-DD');
            },

            getEndDate: function() {
                if(this.getEndAt() === null)
                    return "unknown";
                return this.getEndAt().format('YYYY-MM-DD');
            },

            /**
             *
             * @returns {string}
             */
            getStartsIn: function() {
                return moment().to(this.getStartAt());
            },

            isAfter: function() {
                return (this.getStartAt() && this.getStartAt().isAfter(moment())) || (this.getEndAt() && this.getEndAt().isAfter(moment()))
            },

            /**
             * Returns distance in km between point specified and
             * this Event
             *
             * @param {{latitude: float, longitude: float}} point
             * @returns {number}
             */
            computeDistanceTo: function (point) {
                var eventCoordinates = this.getCoordinates();
                var pointLatLng = new google.maps.LatLng({lat: point.latitude, lng: point.longitude});
                var EventLatLng = new google.maps.LatLng({lat: eventCoordinates.latitude, lng: eventCoordinates.longitude});
                return parseFloat((google.maps.geometry.spherical.computeDistanceBetween(pointLatLng, EventLatLng) / 1000).toFixed(2));
            },

            deleteFromBackend: function () {
                return $http.get('event/delete/'+this._id).then(function(response) {
                    return response;
                });
            }

        };
        
        return Event;
    });

})();