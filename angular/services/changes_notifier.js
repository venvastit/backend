(function() {
    "use strict";

    angular.module('app.services').factory('ChangesNotifier', function($rootScope) {
        return {
            
            SUBJ_EVENTS_LOADED: 'EventsLoaded',
            SUBJ_VENUES_LOADED: 'VenuesLoaded',
            SUBJ_DEALS_LOADED: 'DealsLoaded',
            
            SUBJ_EVENTS_RELOADED: 'EventsReloaded',
            SUBJ_VENUES_RELOADED: 'VenuesReloaded',
            SUBJ_DEALS_RELOADED: 'DealsReloaded',
            
            SUBJ_EVENTS_ADDED: 'EventsAdded',
            SUBJ_VENUES_ADDED: 'VenuesAdded',
            SUBJ_DEALS_ADDED: 'DealsAdded',


            SUBJ_ORG_EVENTS_LOADED: 'EventsLoaded',
            SUBJ_ORG_VENUES_LOADED: 'VenuesLoaded',
            SUBJ_ORG_DEALS_LOADED: 'DealsLoaded',

            SUBJ_ORG_EVENTS_RELOADED: 'EventsReloaded',
            SUBJ_ORG_VENUES_RELOADED: 'VenuesReloaded',
            SUBJ_ORG_DEALS_RELOADED: 'DealsReloaded',

            SUBJ_ORG_EVENTS_ADDED: 'EventsAdded',
            SUBJ_ORG_VENUES_ADDED: 'VenuesAdded',
            SUBJ_ORG_DEALS_ADDED: 'DealsAdded',
            
            value: null,
            org_value: null,
            org_venues: null,
            venues_value: null,
            
            subscribe: function(subject, scope, callback) {
                var handler = $rootScope.$on(subject, callback);
                scope.$on('$destroy', handler);
            },

            notify: function(subject, value) {
                $rootScope.$emit(subject);
                if(value)
                    this.value = value;
            }
        };
    });
    
})();