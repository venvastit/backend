(function(){
    "use strict";
    angular.module('app.services').factory('DateService', function(){
        return {
            diff: {
                inHours: function (d1, d2) {
                    var t2 = d2.getTime();
                    var t1 = d1.getTime();
                    return parseInt((t2-t1)/(3600*1000));
                },
                inDays: function(d1, d2) {
                    var t2 = d2.getTime();
                    var t1 = d1.getTime();

                    return parseInt((t2-t1)/(24*3600*1000));
                },
                inWeeks: function(d1, d2) {
                    var t2 = d2.getTime();
                    var t1 = d1.getTime();

                    return parseInt((t2-t1)/(24*3600*1000*7));
                },
                inMonths: function(d1, d2) {
                    var d1Y = d1.getFullYear();
                    var d2Y = d2.getFullYear();
                    var d1M = d1.getMonth();
                    var d2M = d2.getMonth();
                    return (d2M+12*d2Y)-(d1M+12*d1Y);
                },
                inYears: function(d1, d2) {
                    return d2.getFullYear()-d1.getFullYear();
                },
                getDiff: function(d1, d2) {
                    if(this.inHours(d1, d2) > 48){
                        if(this.inDays > 14){
                            return "Starts in " + this.inWeeks(d1, d2) + " weeks";
                        } else {
                            return "Starts in " + this.inDays(d1, d2) + " days";
                        }
                    } else {
                        return "Starts in " + this.inHours(d1, d2) + " hours";
                    }
                }
            }
        };
    });

})();