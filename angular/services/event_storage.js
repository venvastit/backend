(function(){
    
    // "use strict";
    
    angular.module('app.services').factory('EventsStorage', function($http, $timeout, Event, ChangesNotifier) {

        /**
         *
         * @param {Array} eventsArray
         * @constructor
         */
        function EventsStorage(eventsArray) {
            if(eventsArray){
                for(var i = 0; i < eventsArray.length; i++) {
                    if(!(eventsArray[i] instanceof Event)) {
                        eventsArray[i] = new Event(eventsArray[i]);
                    }
                }
                this.loadArray(eventsArray);
            }
        }

        EventsStorage.prototype = {
            data: [],
            loading: false,
            hasNextPage: false,
            _timeout: null,

            loadArray: function (eventsArray) {
                this.data = eventsArray;
            },

            /**
             *
             * @param {VenVastRequest} request
             * @returns {*}
             */
            load: function (request) {
                var scope = this;

                if(this._timeout){ //if there is already a timeout in process cancel it
                    $timeout.cancel(scope._timeout);
                }
                return this._timeout = $timeout(function(){
                    if(scope.loading === false) {
                        scope.loading = true;
                        if(request.hasMorePages === false) {
                            return [];
                        }
                        return $http.get('event', {params: request.getRequestObject()}).success(function(eventData) {
                            for(var i = 0; i < eventData.length; i++) {
                                eventData[i] = new Event(eventData[i]);
                            }
                            request.requestPerformed(eventData);
                            scope.loadArray(eventData);
                            scope.loading = false;
                            return scope;
                        });
                    }
                    scope._timeout = null;
                }, 500);
            }



        };
        
        return EventsStorage;
    });
    
})();