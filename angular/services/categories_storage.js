(function(){
    "use strict";


    angular.module('app.services').factory('CategoriesStorage', function($http, Category){

        /**
         *
         * @param {Array} categoriesArray
         * @constructor
         */
        function CategoriesStorage(categoriesArray) {
            if(categoriesArray && categoriesArray.length > 0){
                for (var i = 0; i < categoriesArray.length; i++) {
                    if ( !(categoriesArray[i] instanceof Category) ) {
                        categoriesArray[i] = new Category(categoriesArray[i]);
                    }
                }
                this.loadArray(categoriesArray);
            }
        };

        CategoriesStorage.prototype = {
            TYPE_EVENT: "event",
            TYPE_VENUE: "venue",
            TYPE_DEAL: "deal",
            data: [],
            loading: false,
            
            loadArray: function(categoriesArray) {
                this.data = categoriesArray;
            },
            /**
             *
             * @param {object} requestObject
             */
            load: function(requestObject) {
                var scope = this;
                if(this.loading === false) {
                    this.loading = true;
                    return $http.get('category', {params: requestObject}).success(function(categoryData) {
                        for(var i = 0; i < categoryData.length; i++) {
                            categoryData[i] = new Category(categoryData[i]);
                        }
                        scope.loadArray(categoryData);
                        scope.loading = false;
                        return scope;
                    });
                }
            },
            getImageUrl: function(width, height) {
                return 'our/image/service/' + this.category.id + '/width/height';
            }
        };

        return CategoriesStorage;

    })})();