@extends('dev.layouts.main')

@section('content')

  <div class="main-container">
    <div class="container">
      <div class="row">
            <div class="col-sm-3 page-sidebar">
          <aside>
            <div class="inner-box">
              <div class="user-panel-sidebar">
                <div class="collapse-box">
                  <h5 class="collapse-title no-border"> My Profile <a href="#MyProfile" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyProfile">
                    <ul class="acc-list">
                      <li><a href="user_home.html"><i class="icon-home"></i> My Home </a></li>
                      <li><a class="active" href="user_edit.html"><i class="icon-user"></i> Profile Settings </a></li>
                      <li><a href="user_mailbox.html"><i class="icon-chat"></i> Mailbox </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Events <a href="#MyEvents" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyEvents">
                    <ul class="acc-list">
                      <li><a href="user_myevents.html"><i class="icon-th-list"></i> My Events <span class="badge">0</span> </a></li>
                      <li><a href="user_mybookings.html"><i class="icon-heart"></i> My Bookings <span class="badge">0</span> </a></li>
                      <li><a href="event_list.html"><i class="icon-search"></i> Find an Event </a></li>
                      <li><a href="user_pastevents.html"><i class="icon-reply"></i> Past Events </a></li>
                    </ul>
                  </div>
                </div>
                
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Calendar <a href="#MyCalendar" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="MyCalendar">
                    <ul class="acc-list">
                      <li><a href="user_mycalendar"><i class="icon-calendar"></i> My Calendar </a></li>
                      <li><a href="calendar_public.html"><i class="icon-calendar-1"></i> Public Calendar</a></li>
                    </ul>
                  </div>
                </div>
                
                <!--/.collapse-box -->
                
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Media <a href="#MyMedia" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="MyMedia">
                    <ul class="acc-list">
                      <li><a href="user_mymemory.html"><i class="icon-briefcase"></i> My Memory Locker </a></li>
                      <li><a href="user_mymemory_upload.html"><i class="icon-calendar-1"></i> Upload New Media </a></li>
                    </ul>
                  </div>
                </div>
                
                <!--/.collapse-box -->
                <!-- appears if the account has the 'organizer' premission set -->
                
                <div class="collapse-box">
                  <h5 class="collapse-title"> Organizers <a href="#Organizers" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="Organizers">
                    <ul class="acc-list">
                      <li><a href="organizers.venvast.com"><i class="icon-vcard"></i> Organizers Dashboard </a></li>
                      <li><a href="user_request_organizers.html"><i class="icon-doc-text"></i> Request Access </a></li>
                    </ul>
                  </div>
                </div>
                
                <!-- /.collapse-box  --> 
              </div>
            </div>
            <!-- /.inner-box  --> 
            
          </aside>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
          <div class="row">
            <div class="col-md-5 col-xs-4 col-xxs-12">
              <h3 class="no-padding text-center-480 useradmin"><a href=""><img class="userImg" src="images/user.jpg" alt="user"> Touy Smith </a> </h3>
            </div>
            <div class="col-md-7 col-xs-8 col-xxs-12">
              <div class="header-data text-center-xs"> 
                
                <!-- Traffic data -->
                <div class="hdata">
                  <div class="mcol-left"> 
                    <!-- Icon with red background --> 
                    <i class="fa fa-eye ln-shadow"></i> </div>
                  <div class="mcol-right"> 
                    <!-- Number of visitors -->
                    <p><a href="#">7000</a> <em>Visits</em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>
                
                <!-- revenue data -->
                <div class="hdata">
                  <div class="mcol-left"> 
                    <!-- Icon with green background --> 
                    <i class="icon-th-thumb ln-shadow"></i> </div>
                  <div class="mcol-right"> 
                    <!-- Number of visitors -->
                    <p><a href="#">12</a><em>Events</em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>
                
                <!-- revenue data -->
                <div class="hdata">
                  <div class="mcol-left"> 
                    <!-- Icon with blue background --> 
                    <i class="fa fa-user ln-shadow"></i> </div>
                  <div class="mcol-right"> 
                    <!-- Number of visitors -->
                    <p><a href="#">18</a> <em>Followers</em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
          </div>
          
          <div class="inner-box">
            <form class="edit-profile"> 
                <h5 class="list-title"><strong>Profile Information</strong></h5>
              
                    <label for="name">Full Name:</label>
                      <input type="text" id="name" class="form-control" placeholder="Touy Smith" />
                    <label for="email">Email:</label>
                      <input type="email" id="email" class="form-control" placeholder="johndoe@example.com" />
                    <label for="profile_pic">Profile Picture:</label>
                      <input id="profile_pic" type="file">
                    <label for="profile_pic">Banner Picture:</label>
                      <input id="banner_pic" type="file">

                <h5 class="list-title"><strong>Change Interests</strong></h5>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Automotive" name="Automotive" value="Automotive">
                    <label for="Automotive">Automotive</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Business" name="Business" value="Business">
                    <label for="Business">Business</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Health" name="Health" value="Health">
                    <label for="Health">Health</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Technology" name="Technology" value="Technology">
                    <label for="Technology">Technology</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Science" name="Science" value="Science">
                    <label for="Science">Science</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Food and Drink" name="Food and Drink" value="Food and Drink">
                    <label for="Food and Drink">Food and Drink</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Family" name="Family" value="Family">
                    <label for="Family">Family</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Education" name="Education" value="Education">
                    <label for="Education">Education</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Fashion" name="Fashion" value="Fashion">
                    <label for="Fashion">Fashion</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Community" name="Community" value="Community">
                    <label for="Community">Community</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Sports" name="Sports" value="Sports">
                    <label for="Sports">Sports</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Travel" name="Travel" value="Travel">
                    <label for="Travel">Travel</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Outdoor" name="Outdoor" value="Outdoor">
                    <label for="Outdoor">Outdoor</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Hobbies" name="Hobbies" value="Hobbies">
                    <label for="Hobbies">Hobbies</label>
                  </div>

                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                    <input type="checkbox" id="Meeting" name="Meeting" value="Meeting">
                    <label for="Meeting">Meeting</label>
                  </div>


                   <h5 class="list-title"><strong>Change Password</strong></h5>

                    <label for="pass">Current Password:</label>
                      <input type="password" id="pass" class="form-control" placeholder="" />

                   <label for="new_pass">New Password:</label>
                      <input type="password" id="new_pass" class="form-control" placeholder="" />

                   <label for="confirm_pass">Confirm New Password:</label>
                      <input type="password" id="confirm_pass" class="form-control" placeholder="" />


                  <input type="submit" class="btn btn-lg btn-block btn-success" value="Save Changes"/>
            </form>
          </div>
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  
  @endsection 