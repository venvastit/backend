@extends('dev.layouts.main')
  
  @section('content')
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-9 page-content col-thin-right">
          <div class="inner-box category-content">
                  <ul class="nav nav-tabs">
          <li class="active"><a href="#popular" data-toggle="tab">Popular<i class="icon-star"></i></a></li>
          <li><a href="#trending" data-toggle="tab">Trending<i class="icon-thumbs-up"></i></a></li>
          <li><a href="#live" data-toggle="tab">Live<i class="icon-globe"></i></a></li>
        </ul>
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="popular">

          @for($i = 0; $i < 5; $i++)          
            <div class="search-item" style="background-image:url('images/index/1-min.jpg');">
                  <a href="#" data-toggle="modal" data-target="#myModal">More Details</a>
                    <div class="item-details">
                      <h2 class="item-title">Sample Event</h2>
                      <h2 class="item-date">03 MAR 2016</h2>
                      <h2 class="item-moreinfo">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ex enim, tincidunt vel porta et, efficitur eu justo. Sed non eros non arcu commodo congue sit amet non lectus.
                      </h2>
                    </div>
                </div>
          @endfor
<!-- 
              <div class="search-item" style="background:url('http://www.panida.org/wp-content/uploads/2014/07/Panida_from-balcony-AllerGale-1920x1080px-v3.jpg');">
                <a href="#">More Details</a>
                  <div class="item-details">
                    <h2 class="item-title">Sample Event</h2>
                    <h2 class="item-date">03 MAR 2016</h2>
                    <h2 class="item-moreinfo">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ex enim, tincidunt vel porta et, efficitur eu justo. Sed non eros non arcu commodo congue sit amet non lectus.
                    </h2>
                  </div>
              </div>


              <div class="search-item" style="background:url('http://atlconventioncenter.com/img/default/002.jpg');">
                <a href="#">More Details</a>
                  <div class="item-details">
                    <h2 class="item-title">Sample Event</h2>
                    <h2 class="item-date">03 MAR 2016</h2>
                    <h2 class="item-moreinfo">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ex enim, tincidunt vel porta et, efficitur eu justo. Sed non eros non arcu commodo congue sit amet non lectus.
                    </h2>
                  </div>
              </div>

              <div class="search-item" style="background:url('http://www.latourelle.com/images/display_photos/weddings1.jpg');">
                <a href="#">More Details</a>
                  <div class="item-details">
                    <h2 class="item-title">Sample Event</h2>
                    <h2 class="item-date">03 MAR 2016</h2>
                    <h2 class="item-moreinfo">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ex enim, tincidunt vel porta et, efficitur eu justo. Sed non eros non arcu commodo congue sit amet non lectus.
                    </h2>
                  </div>
              </div> -->
          </div>
          
          <div class="tab-pane" id="trending">
           <div class="search-item" style="background:url('images/index/5-min.jpg');">
                <a href="#">More Details</a>
                  <div class="item-details">
                    <h2 class="item-title">Sample Event</h2>
                    <h2 class="item-date">03 MAR 2016</h2>
                    <h2 class="item-moreinfo">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ex enim, tincidunt vel porta et, efficitur eu justo. Sed non eros non arcu commodo congue sit amet non lectus.
                    </h2>
                  </div>
              </div>
          </div>
          
           <div class="tab-pane" id="live">
            <p> <strong>..content 3</strong> </p>
          </div>
        </div> <!-- /.tab content -->
            

            
           
            
          </div>
          <div class="inner-box relative">
            <div class="row">
              <div class="col-md-12 col-lg-12">
                <div>
                  <h3 class="title-2"> <i class="icon-location-2"></i> Popular locations  </h3>
                      <div class="row">   
                        <ul class="cat-list col-xs-3">
                            <li> <a href="category.html">Bangkok</a></li>
                            <li> <a href="category.html">Chiang Mai </a></li>
                            <li> <a href="category.html"> Pattaya </a></li>
                            <li> <a href="category.html"> Phuket </a></li>
                        </ul>
                        <ul class="cat-list col-xs-3">
                            <li> <a href="category.html"> Hua Hin  </a></li>
                            <li> <a href="category.html">Ko Samui </a></li>
                            <li> <a href="category.html"> Ko Samet  </a></li>
                            <li> <a href="category.html"> Krabi</a></li>
                        </ul>
                          
                                                 
                      </div>
                </div>
              </div>
            
            
            </div>
          </div>
        </div>
        <div class="col-sm-3 page-sidebar col-thin-left">
          <aside>
            <div class="inner-box no-padding">
              <div class="inner-box-content"> <a href="#"><img class="img-responsive" src="http://placehold.it/400x400" alt="tv"></a> </div>
            </div>
            <div class="inner-box">
              <h2 class="title-2">Popular Interests </h2>
              <div class="inner-box-content">
                <ul class="cat-list arrow">
                  <li> <a href="sub-category-sub-location.html"> Networking </a></li>
                <li> <a href="sub-category-sub-location.html">  Business </a></li>
                <li> <a href="sub-category-sub-location.html"> Technology </a></li>
                <li> <a href="sub-category-sub-location.html"> Party </a></li>
                <li> <a href="sub-category-sub-location.html"> Educational </a></li>
                <li> <a href="sub-category-sub-location.html"> Food and Drink </a></li>
                </ul>
              </div>
            </div>

            <div class="inner-box">
                <h2 class="title-2">Sponsored</h2>
                <img src="http://placehold.it/150x300" width="100%" />
            </div>
          </aside>
        </div>
      </div>
    </div>
  </div>
  @endsection

