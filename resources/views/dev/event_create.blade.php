@extends('dev.layouts.main')

@section('content')
  
  <div class="main-container">
    <div class="container">
    <div class="inner-box">
      <h5 class="list-title"><strong>Create Event</strong></h5>
        <a class="btn btn btn-social btn-facebook">
    <span class="fa fa-facebook"></span> Import from Facebook
  </a>
   <a class="btn btn btn-social btn-google">
    <span class="fa fa-google"></span> Import from Google
  </a>
   <a class="btn btn btn-social btn-adn">
    <span class="fa fa-adn"></span> Import from Other
  </a>
    </div>
      <div class="inner-box">
                                     <ul class="nav nav-tabs">
                <li class="active"><a href="#MainInfo" data-toggle="tab">General Information</a></li>
                <li><a href="#Location" data-toggle="tab">Location</a></li>
                <li><a href="#Tickets" data-toggle="tab">Tickets / Booking</a></li>
                <li><a href="#Interests" data-toggle="tab">Interest / Category</a></li>
              </ul>

              <div class="tab-content">
                <br />
                <div class="tab-pane active" id="MainInfo">
                      <h5>Event Title: </h5>
                      <input type="text" class="form-control" name="eventTitle" placeholder="Event Title"/><br />
                      <div class="row">
                          <div class="col-md-6 col-lg-6 col-sm-6">
                            <h5>Event Date: </h5>
                            <input type="date" class="form-control" name="eventData" />
                          </div>
                          <div class="col-md-6 col-lg-6 col-sm-6">
                            <h5>Event Banner: </h5>
                            <input type="file" name="eventPhoto"/>
                          </div>
                      </div><br />
                      
                      <h5>Event Details: </h5>
                      <textarea rows="10" class="form-control" placeholder="Give us more info on your event" name="eventDescription"></textarea>
                </div>
                <div class="tab-pane" id="Location">
                       <div id="map"></div><br />
                       <textarea class="form-control" placeholder="Full Address"></textarea>
                </div>
                <div class="tab-pane" id="Tickets">

                      <input type="checkbox" name="eventFree" value="1"/> Attendance is Free
                      <hr />
                      <table class="table table-striped">
                          <tr>
                              <th>Ticket Name</th>
                              <th>Ticket Cost</th>
                              <th>Ticket Inventory</th>
                          </tr>

                          <tr>
                              <td><input type="text" class="form-control" name="ticketName1" /></td>
                              <td><input type="text" class="form-control" name="ticketCost1" /></td>
                              <td><input type="text" class="form-control" name="ticketInventory1" /></td>
                          </tr>

                          <tr>
                              <td><input type="text" class="form-control" name="ticketName2" /></td>
                              <td><input type="text" class="form-control" name="ticketCost2" /></td>
                              <td><input type="text" class="form-control" name="ticketInventory2" /></td>
                          </tr>

                          <tr>
                              <td><input type="text" class="form-control" name="ticketName2" /></td>
                              <td><input type="text" class="form-control" name="ticketCost2" /></td>
                              <td><input type="text" class="form-control" name="ticketInventory2" /></td>
                          </tr>
                      </table>
                </div>

                <div class="tab-pane" id="Interests">
                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Automotive" name="Automotive" value="Automotive">
                          <label for="Automotive">Automotive</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Business" name="Business" value="Business">
                          <label for="Business">Business</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Health" name="Health" value="Health">
                          <label for="Health">Health</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Technology" name="Technology" value="Technology">
                          <label for="Technology">Technology</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Science" name="Science" value="Science">
                          <label for="Science">Science</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Food and Drink" name="Food and Drink" value="Food and Drink">
                          <label for="Food and Drink">Food and Drink</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Family" name="Family" value="Family">
                          <label for="Family">Family</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Education" name="Education" value="Education">
                          <label for="Education">Education</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Fashion" name="Fashion" value="Fashion">
                          <label for="Fashion">Fashion</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Community" name="Community" value="Community">
                          <label for="Community">Community</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Sports" name="Sports" value="Sports">
                          <label for="Sports">Sports</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Travel" name="Travel" value="Travel">
                          <label for="Travel">Travel</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Outdoor" name="Outdoor" value="Outdoor">
                          <label for="Outdoor">Outdoor</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Hobbies" name="Hobbies" value="Hobbies">
                          <label for="Hobbies">Hobbies</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Meeting" name="Meeting" value="Meeting">
                          <label for="Meeting">Meeting</label>
                        </div>
                </div>
               
              </div> <!-- /.tab content -->
              <br />
              <input type="submit" class="btn btn-primary btn-lg btn-block" value="Create Event" />
          </div>
    </div>
  </div>
  <!-- /.main-container -->
  
@endsection 