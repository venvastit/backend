@extends('dev.layouts.main')

@section('content')

  <div class="main-container">
    <div class="container">
      <div class="row">
       <div class="col-sm-3 page-sidebar">
          <aside>
            <div class="inner-box">
              <div class="user-panel-sidebar">
                <div class="collapse-box">
                  <h5 class="collapse-title no-border"> My Profile <a href="#MyProfile" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyProfile">
                    <ul class="acc-list">
                      <li><a href="user_home.html"><i class="icon-home"></i> My Home </a></li>
                      <li><a href="user_edit.html"><i class="icon-user"></i> Profile Settings </a></li>
                      <li><a href="user_mailbox.html"><i class="icon-chat"></i> Mailbox </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Events <a href="#MyEvents" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="MyEvents">
                    <ul class="acc-list">
                      <li><a href="user_myevents.html"><i class="icon-th-list"></i> My Events <span class="badge">0</span> </a></li>
                      <li><a href="user_mybookings.html"><i class="icon-heart"></i> My Bookings <span class="badge">0</span> </a></li>
                      <li><a href="event_list.html"><i class="icon-search"></i> Find an Event </a></li>
                      <li><a href="user_pastevents.html"><i class="icon-reply"></i> Past Events </a></li>
                    </ul>
                  </div>
                </div>
                
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Calendar <a href="#MyCalendar" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyCalendar">
                    <ul class="acc-list">
                      <li><a class="active" href="user_mycalendar"><i class="icon-calendar"></i> My Calendar </a></li>
                      <li><a href="calendar_public.html"><i class="icon-calendar-1"></i> Public Calendar</a></li>
                    </ul>
                  </div>
                </div>
                
                <!--/.collapse-box -->
                
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Media <a href="#MyMedia" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="MyMedia">
                    <ul class="acc-list">
                      <li><a href="user_mymemory.html"><i class="icon-briefcase"></i> My Memory Locker </a></li>
                      <li><a href="user_mymemory_upload.html"><i class="icon-calendar-1"></i> Upload New Media </a></li>
                    </ul>
                  </div>
                </div>
                
                <!--/.collapse-box -->
                <!-- appears if the account has the 'organizer' premission set -->
                
                <div class="collapse-box">
                  <h5 class="collapse-title"> Organizers <a href="#Organizers" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="Organizers">
                    <ul class="acc-list">
                      <li><a href="organizers.venvast.com"><i class="icon-vcard"></i> Organizers Dashboard </a></li>
                      <li><a href="user_request_organizers.html"><i class="icon-doc-text"></i> Request Access </a></li>
                    </ul>
                  </div>
                </div>
                
                <!-- /.collapse-box  --> 
              </div>
            </div>
            <!-- /.inner-box  --> 
            
          </aside>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
        <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">My Calendar</h3>
  </div>
  <div class="panel-body">
    <div id="my-calendar"></div>
  </div>
</div>
<div class="panel panel-default">
<a class="btn btn-sm btn-social btn-facebook">
    <span class="fa fa-facebook"></span> Import from Facebook
  </a>
   <a class="btn btn-sm btn-social btn-google">
    <span class="fa fa-google"></span> Import from Google
  </a>
   <a class="btn btn-sm btn-social btn-adn">
    <span class="fa fa-adn"></span> Import from Other
  </a>
</div>
<div class="inner-box">
              <h5 class="list-title"><strong>My Calendar Events</strong></h5>
              <table class="table">
                    <tr>
                        <th>Status</th>
                        <th>Event Name</th>
                        <th>Date</th>
                        <th>Receipt</th>
                        <th>Options</th>
                    </tr>
                    <tr>
                        <td><span class="label label-warning">Pending</span></td>
                        <td><a href="#">Stephanie's wedding</a></td>
                        <td>23 / MAR / 2016</td>
                        <td><a href="#">USD $0.00</a></td>
                        <td><a class="btn btn-xs btn-warning" href="#">Cancel Reservation</a></td>
                    </tr>
                     <tr>
                        <td><span class="label label-success">Confirmed</span></td>
                        <td><a href="#">La Viva Concert</a></td>
                        <td>5 / MAR / 2016</td>
                        <td><a href="#">USD $140.55</a></td>
                        <td><a class="btn btn-xs btn-warning" href="#">Cancel Reservation</a></td>
                    </tr>
                    <tr>
                        <td><span class="label label-success">Confirmed</span></td>
                        <td><a href="#">Skydiving over the bay</a></td>
                        <td>25 / FEB / 2016</td>
                        <td><a href="#">USD $110.00</a></td>
                        <td><a class="btn btn-xs btn-warning" href="#">Cancel Reservation</a></td>
                    </tr>
              </table>
          </div>
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  
@endsection 