@extends('dev.layouts.main')
  
  @section('content')
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-md-12 page-content">
          <div class="inner-box category-content">
            <h2 class="title-2"> <i class="icon-user-add"></i> Create your account, Its free </h2>
            <h3 style="text-align:center;">General Information</h3>
            <div class="row">
              <div class="col-sm-12">
                <form class="form-horizontal">
                  <fieldset>
                    <div class="form-group required">
                      <label class="col-md-4 control-label" >You are a <sup>*</sup></label>
                      <div class="col-md-6">
                        <div class="radio">
                          <label>
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                            Attendee </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                            Organizer </label>
                        </div>
                      </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group required">
                      <label class="col-md-4 control-label" >First Name <sup>*</sup></label>
                      <div class="col-md-6">
                        <input  name="" placeholder="First Name" class="form-control input-md" required="" type="text">
                      </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group required">
                      <label class="col-md-4 control-label" >Last Name <sup>*</sup></label>
                      <div class="col-md-6">
                        <input  name="textinput" placeholder="Last Name" class="form-control input-md" type="text">
                      </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group required">
                      <label class="col-md-4 control-label" >Phone Number <sup>*</sup></label>
                      <div class="col-md-6">
                        <input  name="textinput" placeholder="Phone Number" class="form-control input-md" type="text">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            <small> Hide the phone number on the published ads.</small> </label>
                        </div>
                      </div>
                    </div>
                    
                    <!-- Multiple Radios -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" >Gender</label>
                      <div class="col-md-6">
                        <div class="radio">
                          <label for="Gender-0">
                            <input name="Gender" id="Gender-0" value="1" checked="checked" type="radio">
                            Male </label>
                        </div>
                        <div class="radio">
                          <label for="Gender-1">
                            <input name="Gender" id="Gender-1" value="2" type="radio">
                            Female </label>
                        </div>
                      </div>
                    </div>
                    
                    <!-- Textarea -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textarea">About Yourself</label>
                      <div class="col-md-6">
                        <textarea class="form-control" id="textarea" name="textarea">About Yourself</textarea>
                      </div>
                    </div>
                    <div class="form-group required">
                      <label for="inputEmail3" class="col-md-4 control-label">Email <sup>*</sup></label>
                      <div class="col-md-6">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                      </div>
                    </div>
                    <div class="form-group required">
                      <label for="inputPassword3" class="col-md-4 control-label">Password </label>
                      <div class="col-md-6">
                        <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                        <p class="help-block">At least 5 characters <!--Example block-level help text here.--></p>
                      </div>
                    </div>
                    
                    <div class="form-group required">
                    	<h3 style="text-align:center;">Select 5 or more interests</h3>
                    	<center>
                    	<div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Automotive" name="Automotive" value="Automotive">
	                    <label for="Automotive">Automotive</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Business" name="Business" value="Business">
	                    <label for="Business">Business</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Health" name="Health" value="Health">
	                    <label for="Health">Health</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Technology" name="Technology" value="Technology">
	                    <label for="Technology">Technology</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Science" name="Science" value="Science">
	                    <label for="Science">Science</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Food and Drink" name="Food and Drink" value="Food and Drink">
	                    <label for="Food and Drink">Food and Drink</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Family" name="Family" value="Family">
	                    <label for="Family">Family</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Education" name="Education" value="Education">
	                    <label for="Education">Education</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Fashion" name="Fashion" value="Fashion">
	                    <label for="Fashion">Fashion</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Community" name="Community" value="Community">
	                    <label for="Community">Community</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Sports" name="Sports" value="Sports">
	                    <label for="Sports">Sports</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Travel" name="Travel" value="Travel">
	                    <label for="Travel">Travel</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Outdoor" name="Outdoor" value="Outdoor">
	                    <label for="Outdoor">Outdoor</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Hobbies" name="Hobbies" value="Hobbies">
	                    <label for="Hobbies">Hobbies</label>
	                  </div>
	
	                  <div class="interest-check" style="background:url('http://placehold.it/200x100');">
	                    <input type="checkbox" id="Meeting" name="Meeting" value="Meeting">
	                    <label for="Meeting">Meeting</label>
	                  </div>
	                  </center>
                    </div>
                    <center>
                    <div class="form-group">
                      <label  class="col-md-12 control-label"></label>
                      <div class="col-md-12">
                        <div class="termbox mb10">
                          <label class="checkbox-inline" for="checkboxes-1">
                            <input name="checkboxes" id="checkboxes-1" value="1" type="checkbox">
                            I have read and agree to the <a href="terms-conditions.html">Terms & Conditions</a> </label>
                        </div>
                        <div style="clear:both"></div>
                        <a class="btn btn-primary" href="account-home.html">Register</a> </div>
                    </div>
                    </center>
                    
                   
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.page-content -->
       
      </div>
      <!-- /.row --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.main-container -->
  @endsection
