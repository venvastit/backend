@extends('dev.layouts.main')

@section('content')
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
          <aside>
           
            <div class="inner-box">
              <div class="user-panel-sidebar">
                <div class="collapse-box">
                  <h5 class="collapse-title no-border"> My Profile <a href="#MyClassified" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyClassified">
                    <ul class="acc-list">
                      <li><a class="active" href="account-home.html"><i class="icon-home"></i> Personal Home </a></li>
                      <li><a href="account-edit.html"><i class="icon-pencil"></i> Edit Profile </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Events <a href="#MyEvents" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyEvents">
                    <ul class="acc-list">
                      <li><a href="event-search.html"><i class="icon-search"></i> Search Event </a></li>
                      <li><a href="event-bookings.html"><i class="icon-th-thumb"></i> My Bookings </a></li>
                      <li><a href="event-create.html"><i class="icon-plus"></i> Create an Event </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                
                
              </div>
            </div>
            <!-- /.inner-box  --> 
            
            <div class="inner-box">
                <h2 class="title-2 uppercase"><strong>Sponsored</strong></h2>
                <img src="http://placehold.it/200x600&text=AD" />
            </div>

          </aside>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
            <div class="row">
              <div class="col-md-5 col-xs-4 col-xxs-12">
                <h3 class="no-padding text-center-480 useradmin"><a href=""><img class="userImg" src="images/user.jpg" alt="user"> John Doe </a> </h3>
              </div>
              <div class="col-md-7 col-xs-8 col-xxs-12">
                <div class="header-data text-center-xs"> 
                  
                  <!-- Traffic data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with red background --> 
                      <i class="fa fa-eye ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">7000</a> <em>Visits</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <!-- revenue data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with green background --> 
                      <i class="icon-th-thumb ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">12</a><em>Events</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <!-- revenue data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with blue background --> 
                      <i class="fa fa-user ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">18</a> <em>Followers</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="inner-box">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <div class="profile-banner">
                    <img width="100%" src="http://placehold.it/800x300&text=Profile Banner" alt="Profile Banner" />
                  </div>
                </div>
            </div>
          </div>

          
          <div class="inner-box">
              <div class="social-wall">
                    <h5 class="list-title"><strong>Social Wall</strong></h5>
                    <!-- A sample post -->
                    <div class="social-element">
                          <div class="posted-by">
                              <img src="http://placehold.it/100x100" />
                              <h5><a href="#">Jeff Doe</a></h5>
                          </div>
                          <div class="posted-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at odio nibh. Suspendisse sit amet rhoncus felis. Aenean facilisis odio ac tincidunt facilisis. Vestibulum est nibh, scelerisque non lorem sed</p>
                          </div>
                          <div class="posted-date">
                            <p>Posted <strong>3 Hours Ago</strong></p>
                          </div>
                          <div class="posted-likes">
                              <span class="like-btn"><i class="icon-thumbs-up"></i> 14</span>
                              <span class="dislike-btn"><i class="icon-thumbs-down"></i>3</span>
                          </div>
                    </div>

                    <!-- A sample post -->
                    <div class="social-element">
                          <div class="posted-by">
                              <img src="http://placehold.it/100x100" />
                              <h5><a href="#">Jeff Doe</a></h5>
                          </div>
                          <div class="posted-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at odio nibh. Suspendisse sit amet rhoncus felis. Aenean facilisis odio ac tincidunt facilisis. Vestibulum est nibh, scelerisque non lorem sed</p>
                          </div>
                          <div class="posted-date">
                            <p>Posted <strong>3 Hours Ago</strong></p>
                          </div>
                          <div class="posted-likes">
                              <span class="like-btn"><i class="icon-thumbs-up"></i> 14</span>
                              <span class="dislike-btn"><i class="icon-thumbs-down"></i>3</span>
                          </div>
                    </div>

                    <!-- A sample post -->
                    <div class="social-element">
                          <div class="posted-by">
                              <img src="http://placehold.it/100x100" />
                              <h5><a href="#">Jeff Doe</a></h5>
                          </div>
                          <div class="posted-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at odio nibh. Suspendisse sit amet rhoncus felis. Aenean facilisis odio ac tincidunt facilisis. Vestibulum est nibh, scelerisque non lorem sed</p>
                          </div>
                          <div class="posted-date">
                            <p>Posted <strong>3 Hours Ago</strong></p>
                          </div>
                          <div class="posted-likes">
                              <span class="like-btn"><i class="icon-thumbs-up"></i> 14</span>
                              <span class="dislike-btn"><i class="icon-thumbs-down"></i>3</span>
                          </div>
                    </div>
              </div>
          </div>
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  
  @endsection 