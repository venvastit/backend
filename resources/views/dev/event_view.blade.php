@extends('dev.layouts.main')

@section('content')

  
  <div class="main-container">
    <div class="container">
      <ol class="breadcrumb pull-left">
        <li><a href="#"><i class="icon-home fa"></i></a></li>
        <li><a href="event_list.html">All Events</a></li>
        <li><a href="">Interest</a></li>
        <li class="active">Event Title</li>
      </ol>
      <div class="pull-right backtolist"><a href="event_list.html"> <i class="fa fa-angle-double-left"></i> Back to Results</a></div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-7 page-content col-thin-right">
          <div class="inner inner-box ads-details-wrapper">
            <h2> Event Title <small class="label label-default adlistingtype">by Host</small> </h2>
            <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> Today 1:21 pm </span> - <span class="category">Interest </span>- <span class="item-location"><i class="fa fa-map-marker"></i> Bangkok </span> </span>
                <ul class="nav nav-tabs">
          <li class="active"><a href="#Tab1" data-toggle="tab">Info</a></li>
          <li><a href="#Tab2" data-toggle="tab">Location</a></li>
          <li><a href="#Tab3" data-toggle="tab">Chat</a></li>
        </ul>
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="Tab1">
                 <div class="ads-image">
                <ul class="bxslider">
                <li><img src="images/item/tp-big/Image00014.jpg" alt="img" /></li>
                <li><img src="images/item/tp-big/Image00015.jpg" alt="img" /></li>
                <li><img src="images/item/tp-big/Image00013.jpg" alt="img" /></li>
                <li><img src="images/item/tp-big/Image00024.jpg" alt="img" /></li>
                <li><img src="images/item/tp-big/Image00025.jpg" alt="img" /></li>
              </ul>
              <div id="bx-pager">
                <a class="thumb-item-link" data-slide-index="0" href=""><img src="images/item/tp/Image00014.jpg" alt="img" /></a>
                <a class="thumb-item-link"  data-slide-index="1" href=""><img src="images/item/tp/Image00015.jpg" alt="img" /></a>
                <a class="thumb-item-link"  data-slide-index="2" href=""><img src="images/item/tp/Image00013.jpg" alt="img" /></a>
                <a class="thumb-item-link"  data-slide-index="3" href=""><img src="images/item/tp/Image00024.jpg" alt="img" /></a>
                <a class="thumb-item-link"  data-slide-index="4" href=""><img src="images/item/tp/Image00025.jpg" alt="img" /></a>
              </div>
            </div>
            <!--ads-image-->
            
              </div>
          
          <div class="tab-pane" id="Tab2">
           <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15504.203207574597!2d100.5802505!3d13.715373!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sth!4v1455636822589" width="500" height="350" frameborder="0" style="border:0" allowfullscreen></iframe> 
           </div>
          
           <div class="tab-pane" id="Tab3">
            <div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-heading" id="accordion">
                    <span class="glyphicon glyphicon-comment"></span> Chat
                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-default btn-xs" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <span class="glyphicon glyphicon-chevron-down"></span>
                        </a>
                    </div>
                </div>
            <div class="panel-collapse collapse in" id="collapseOne">
                <div class="panel-body">
                    <ul class="chat">
                        <li class="left clearfix"><span class="chat-img pull-left">
                            <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span>12 mins ago</small>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                        <li class="right clearfix"><span class="chat-img pull-right">
                            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                                    <strong class="pull-right primary-font">Bhaumik Patel</strong>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                        <li class="left clearfix"><span class="chat-img pull-left">
                            <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span>14 mins ago</small>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                        <li class="right clearfix"><span class="chat-img pull-right">
                            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>15 mins ago</small>
                                    <strong class="pull-right primary-font">Bhaumik Patel</strong>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." />
                        <span class="input-group-btn">
                            <button class="btn btn-warning btn-sm" id="btn-chat">
                                Send</button>
                        </span>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
          </div>
        </div> <!-- /.tab content -->
        
        <!-- static information panel --> 
        <div class="row">
          <div class="col-lg-6">
           <h5 class="list-title"><strong>Event Information</strong></h5>
           <h2>Location</h2>
                                                <address>
                                                  <strong>Twitter, Inc.</strong><br>
                                                  1355 Market Street, Suite 900<br>
                                                  San Francisco, CA 94103<br>
                                                  <abbr title="Phone">P:</abbr> (123) 456-7890
                                                  </address>
                                                <address>
                                                  <strong>Full Name</strong><br>
                                                  <a href="mailto:#">first.last@example.com</a>
                                                  </address>
                                                  <h2>Description</h2>
                                                  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo.</p>
          </div>
          <div class="col-lg-6"
          >
           <h5 class="list-title"><strong>Live Information</strong></h5>
           <p>Live information to be displayed here</p>
                         <a class="twitter-timeline"  href="https://twitter.com/VenVast" data-widget-id="699641319870697473">Tweets by @VenVast</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          
          
          </div>
        </div>
          </div>
          <!--/.ads-details-wrapper--> 
          
        </div>
        
                <!--/.page-content-->
        
        <div class="col-sm-5  page-sidebar-right">
          <aside>
            <div class="panel sidebar-panel">
              <div class="panel-heading">Attending?</div>
              <div class="panel-content">
                <div class="row">
                  <div class="col-lg-4"><center><button type="button" class="btn btn-primary btn-sm"><i class="icon-ok-circled"></i>Going</button></center> </div>
                  <div class="col-lg-4"><center><button type="button" class="btn btn-info btn-sm"><i class="icon-plus"></i>Maybe</button></center></div>
                  <div class="col-lg-4"><center><button type="button" class="btn btn-danger btn-sm"><i class="icon-heart"></i>Interested</button></center></div>
                </div>
                <center><ul class="nav nav-tabs">
          <li class="active"><a href="#Tab4" data-toggle="tab">.</a></li>
          <li><a href="#Tab5" data-toggle="tab">..</a></li>
            <li><a href="#Tab6" data-toggle="tab">...</a></li>
        </ul></center>
        
        <!-- Tab panes -->
        <center><div class="tab-content">
          <div class="tab-pane active" id="Tab4">
          <ul class="list list-border">
                      <li>Attendee 1</li>
                      <li>Attendee 2</li>
                      <li>Attendee 3</li>
                      <li>Attendee 4</li>
                      <li>Attendee 5</li>
                      </ul>
          </div>
          
          <div class="tab-pane" id="Tab5">
             <ul class="list list-border">
                      <li>Attendee 1</li>
                      <li>Attendee 2</li>
                      <li>Attendee 3</li>
                      <li>Attendee 4</li>
                      <li>Attendee 5</li>
                      </ul>
          </div>
          
           <div class="tab-pane" id="Tab6">
            <ul class="list list-border">
                      <li>Attendee 1</li>
                      <li>Attendee 2</li>
                      <li>Attendee 3</li>
                      <li>Attendee 4</li>
                      <li>Attendee 5</li>
                      </ul>
          </div>
        </div> </center><!-- /.tab content -->
              </div>
            </div>
            <div class="panel sidebar-panel">
              <div class="panel-heading">Share Event</div>
              <div class="panel-content">
                <div class="row">
                  <div class="col-lg-4"><center><button type="button" class="btn btn-fb btn-sm"><i class="icon-facebook"></i>Facebook</button></center> </div>
                  <div class="col-lg-4"><center><button type="button" class="btn btn-tw btn-sm"><i class="icon-twitter"></i>Twitter</button></center></div>
                  <div class="col-lg-4"><center><button type="button" class="btn btn-danger btn-sm"><i class="icon-googleplus-rect"></i>Google</button></center></div>
                </div>
               </div>
            </div>
            <div class="panel sidebar-panel panel-contact-seller">
              <div class="panel-heading">Contact Host/Organizer</div>
              <div class="panel-content user-info">
                <div class="panel-body text-center">
                  <div class="seller-info">
                    <h3 class="no-margin">John Doe</h3>
                    <p>Location: <strong>Bangkok</strong></p>
                    <p> Joined: <strong>12 Mar 2016</strong></p>
                  </div>
                  <div class="user-ads-action"> <a href="#contactAdvertiser" data-toggle="modal" class="btn   btn-default btn-block"><i class=" icon-mail-2"></i> Send a message </a> <a class="btn  btn-primary btn-block"><i class=" icon-phone-user"></i> Profile </a> </div>
                </div>
              </div>
            </div>
            
            <!--/.categories-list--> 
          </aside>
        </div>
        <!--/.page-side-bar--> 
      </div>
    </div>
  </div>
  <!-- /.main-container -->
  
  <div class="footer" id="footer">
    <div class="container">
      <ul class=" pull-left navbar-link footer-nav">
        <li><a href="index.html"> Home </a> <a href="aboutus.html"> About Us </a> <a href="services.html"> Services </a> <a href="contact.html"> Contact us </a> <a href="faq.html"> FAQ </a>
      </ul>
    
      <ul class=" pull-right navbar-link footer-nav">
        <li> &copy; VenVast </li>
      </ul>
    </div>
    
  </div>
  <!-- /.footer -->
</div>
<!-- /.wrapper --> 


<!-- Modal contactAdvertiser -->

<div class="modal fade" id="contactAdvertiser" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title"><i class=" icon-mail-2"></i> Contact advertiser </h4>
      </div>
      <div class="modal-body">
        <form role="form">
          <div class="form-group">
            <label for="recipient-name" class="control-label">Name:</label>
            <input class="form-control required" id="recipient-name" placeholder="Your name" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text">
          </div>
          <div class="form-group">
            <label for="sender-email" class="control-label">E-mail:</label>
            <input id="sender-email" type="text" data-content="Must be a valid e-mail address (user@gmail.com)" data-trigger="manual" data-placement="top" placeholder="email@you.com" class="form-control email">
          </div>
          <div class="form-group">
            <label for="recipient-Phone-Number"  class="control-label">Phone Number:</label>
            <input type="text"  maxlength="60" class="form-control" id="recipient-Phone-Number">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Message <span class="text-count">(300) </span>:</label>
            <textarea class="form-control" id="message-text"  placeholder="Your message here.." data-placement="top" data-trigger="manual"></textarea>
          </div>
          <div class="form-group">
            <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success pull-right">Send message!</button>
      </div>
    </div>
  </div>
</div>

<!-- /.modal --> 

@endsection 
