@extends('dev.layouts.main')

@section('content')
  
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
          <aside>
            <div class="inner-box">
              <div class="user-panel-sidebar">
                <div class="collapse-box">
                  <h5 class="collapse-title no-border"> My Profile <a href="#MyProfile" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyProfile">
                    <ul class="acc-list">
                      <li><a href="user_home.html"><i class="icon-home"></i> My Home </a></li>
                      <li><a href="user_edit.html"><i class="icon-user"></i> Profile Settings </a></li>
                      <li><a href="user_mailbox.html"><i class="icon-chat"></i> Mailbox </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Events <a href="#MyEvents" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyEvents">
                    <ul class="acc-list">
                      <li><a href="user_myevents.html"><i class="icon-th-list"></i> My Events <span class="badge">0</span> </a></li>
                      <li><a class="active" href="user_mybookings.html"><i class="icon-heart"></i> My Bookings <span class="badge">0</span> </a></li>
                      <li><a href="event_list.html"><i class="icon-search"></i> Find an Event </a></li>
                      <li><a href="user_pastevents.html"><i class="icon-reply"></i> Past Events </a></li>
                    </ul>
                  </div>
                </div>
                
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Calendar <a href="#MyCalendar" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="MyCalendar">
                    <ul class="acc-list">
                      <li><a href="user_mycalendar"><i class="icon-calendar"></i> My Calendar </a></li>
                      <li><a href="calendar_public.html"><i class="icon-calendar-1"></i> Public Calendar</a></li>
                    </ul>
                  </div>
                </div>
                
                <!--/.collapse-box -->
                
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Media <a href="#MyMedia" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="MyMedia">
                    <ul class="acc-list">
                      <li><a href="user_mymemory.html"><i class="icon-briefcase"></i> My Memory Locker </a></li>
                      <li><a href="user_mymemory_upload.html"><i class="icon-calendar-1"></i> Upload New Media </a></li>
                    </ul>
                  </div>
                </div>
                
                <!--/.collapse-box -->
                <!-- appears if the account has the 'organizer' premission set -->
                
                <div class="collapse-box">
                  <h5 class="collapse-title"> Organizers <a href="#Organizers" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="Organizers">
                    <ul class="acc-list">
                      <li><a href="organizers.venvast.com"><i class="icon-vcard"></i> Organizers Dashboard </a></li>
                      <li><a href="user_request_organizers.html"><i class="icon-doc-text"></i> Request Access </a></li>
                    </ul>
                  </div>
                </div>
                
                <!-- /.collapse-box  --> 
              </div>
            </div>
            <!-- /.inner-box  --> 
            
          </aside>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
            <div class="row">
              <div class="col-md-5 col-xs-4 col-xxs-12">
                <h3 class="no-padding text-center-480 useradmin"><a href=""><img class="userImg" src="images/user.jpg" alt="user"> Touy Smith </a> </h3>
              </div>
              <div class="col-md-7 col-xs-8 col-xxs-12">
                <div class="header-data text-center-xs"> 
                  
                  <!-- Traffic data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with red background --> 
                      <i class="fa fa-eye ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">7000</a> <em>Visits</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <!-- revenue data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with green background --> 
                      <i class="icon-th-thumb ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">12</a><em>Events</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <!-- revenue data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with blue background --> 
                      <i class="fa fa-user ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">18</a> <em>Followers</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- End Inner Box -->

          <div class="inner-box">
              <h5 class="list-title"><strong>Your Bookings</strong></h5>
              <table class="table">
                    <tr>
                        <th>Status</th>
                        <th>Event Name</th>
                        <th>Date</th>
                        <th>Receipt</th>
                        <th>Options</th>
                    </tr>
                    <tr>
                        <td><span class="label label-warning">Pending</span></td>
                        <td><a href="#">Stephanie's wedding</a></td>
                        <td>23 / MAR / 2016</td>
                        <td><a href="#">USD $0.00</a></td>
                        <td><a class="btn btn-xs btn-warning" href="#">Cancel Reservation</a></td>
                    </tr>
                     <tr>
                        <td><span class="label label-success">Confirmed</span></td>
                        <td><a href="#">La Viva Concert</a></td>
                        <td>5 / MAR / 2016</td>
                        <td><a href="#">USD $140.55</a></td>
                        <td><a class="btn btn-xs btn-warning" href="#">Cancel Reservation</a></td>
                    </tr>
                    <tr>
                        <td><span class="label label-success">Confirmed</span></td>
                        <td><a href="#">Skydiving over the bay</a></td>
                        <td>25 / FEB / 2016</td>
                        <td><a href="#">USD $110.00</a></td>
                        <td><a class="btn btn-xs btn-warning" href="#">Cancel Reservation</a></td>
                    </tr>
              </table>
          </div>
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
 @endsection 