@extends('dev.layouts.main')

@section('content')
  
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
          <aside>
           
            <div class="inner-box">
              <div class="user-panel-sidebar">
                <div class="collapse-box">
                  <h5 class="collapse-title no-border"> My Profile <a href="#MyClassified" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyClassified">
                    <ul class="acc-list">
                      <li><a href="account-home.html"><i class="icon-home"></i> Personal Home </a></li>
                      <li><a href="account-edit.html"><i class="icon-pencil"></i> Edit Profile </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Events <a href="#MyEvents" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyEvents">
                    <ul class="acc-list">
                      <li><a class="active" href="event-search.html"><i class="icon-search"></i> Search Event </a></li>
                      <li><a href="event-bookings.html"><i class="icon-th-thumb"></i> My Bookings </a></li>
                      <li><a href="event-create.html"><i class="icon-plus"></i> Create an Event </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                
                
              </div>
            </div>
            <!-- /.inner-box  --> 
            
            <div class="inner-box">
                <h2 class="title-2 uppercase"><strong>Sponsored</strong></h2>
                <img src="http://placehold.it/200x600&text=AD" />
            </div>

          </aside>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
            <div class="row">
              <div class="col-md-5 col-xs-4 col-xxs-12">
                <h3 class="no-padding text-center-480 useradmin"><a href=""><img class="userImg" src="images/user.jpg" alt="user"> John Doe </a> </h3>
              </div>
              <div class="col-md-7 col-xs-8 col-xxs-12">
                <div class="header-data text-center-xs"> 
                  
                  <!-- Traffic data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with red background --> 
                      <i class="fa fa-eye ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">7000</a> <em>Visits</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <!-- revenue data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with green background --> 
                      <i class="icon-th-thumb ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">12</a><em>Events</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <!-- revenue data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with blue background --> 
                      <i class="fa fa-user ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">18</a> <em>Followers</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- End Inner Box -->

          <div class="inner-box">
              <h5 class="list-title"><strong>Event Search</strong></h5>
              <form>
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-12">
                        <input type="text" class="form-control" placeholder="Find an event..." />
                        <a href="#collapseSingleOne" data-toggle="collapse" class="collapsed">Advanced Search</a>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12">
                        <input type="submit" class="btn btn-block btn-primary" value="Search"/>
                    </div>
                </div>

                <div class="row collapse" id="collapseSingleOne" style="height: 0px;">

                  <div class="col-md-12 col-lg-12">
                     <hr />
                     <h4>By Type</h4>
                     <div class="row">
                        <div class="col-md-2 col-lg-2 col-sm-6">
                           <input type="radio" name="event_types" value="all"> All
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="class"> Class
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="seminar"> Seminar
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="Conference"> Conference
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="Other"> Other
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="Expo"> Expo
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="Networking"> Networking
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="Festival"> Festival
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="Perfomance"> Perfomance
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="Party"> Party
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="Tour"> Tour
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="Gala"> Gala
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                        <input type="radio" name="event_types" value="Attraction"> Attraction
                        </div>
                      </div>
                      <br />
                      <h4>By Date</h4>
                      <input id="date" class="form-control" name="date" type="date" />
                  </div>
                </div>
              </form>
          </div><!-- End inner-box-->

          <!-- Results Pane -->
          <div class="inner-box">
              <h5 class="list-title"><strong>Search Results</strong> <small class="pull-right">17 events match your criteria</small></h5>

              <div class="search-item" style="background:url('http://djpicshd.com/wp-content/uploads/2014/03/Kazantip-2014-Ukraine-Promo-Gallery-2013-event-HD-Wallpaper-Photo-Party-Pics-most-amazing-Laser-Show-beautiful-dancers.jpg');">
                <a href="#">More Details / Book Spot</a>
                  <div class="item-details">
                    <h2 class="item-title">Sample Event</h2>
                    <h2 class="item-date">03 MAR 2016</h2>
                    <h2 class="item-moreinfo">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ex enim, tincidunt vel porta et, efficitur eu justo. Sed non eros non arcu commodo congue sit amet non lectus.
                    </h2>
                  </div>
              </div>


              <div class="search-item" style="background:url('http://www.panida.org/wp-content/uploads/2014/07/Panida_from-balcony-AllerGale-1920x1080px-v3.jpg');">
                <a href="#">More Details / Book Spot</a>
                  <div class="item-details">
                    <h2 class="item-title">Sample Event</h2>
                    <h2 class="item-date">03 MAR 2016</h2>
                    <h2 class="item-moreinfo">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ex enim, tincidunt vel porta et, efficitur eu justo. Sed non eros non arcu commodo congue sit amet non lectus.
                    </h2>
                  </div>
              </div>


              <div class="search-item" style="background:url('http://atlconventioncenter.com/img/default/002.jpg');">
                <a href="#">More Details / Book Spot</a>
                  <div class="item-details">
                    <h2 class="item-title">Sample Event</h2>
                    <h2 class="item-date">03 MAR 2016</h2>
                    <h2 class="item-moreinfo">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ex enim, tincidunt vel porta et, efficitur eu justo. Sed non eros non arcu commodo congue sit amet non lectus.
                    </h2>
                  </div>
              </div>

              <div class="search-item" style="background:url('http://www.latourelle.com/images/display_photos/weddings1.jpg');">
                <a href="#">More Details / Book Spot</a>
                  <div class="item-details">
                    <h2 class="item-title">Sample Event</h2>
                    <h2 class="item-date">03 MAR 2016</h2>
                    <h2 class="item-moreinfo">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ex enim, tincidunt vel porta et, efficitur eu justo. Sed non eros non arcu commodo congue sit amet non lectus.
                    </h2>
                  </div>
              </div>



          </div>


      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  
@endsection 