@extends('dev.layouts.main')

@section('content')
  
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
          <aside>
           
            <div class="inner-box">
              <div class="user-panel-sidebar">
                <div class="collapse-box">
                  <h5 class="collapse-title no-border"> My Profile <a href="#MyClassified" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyClassified">
                    <ul class="acc-list">
                      <li><a href="account-home.html"><i class="icon-home"></i> Personal Home </a></li>
                      <li><a href="account-edit.html"><i class="icon-pencil"></i> Edit Profile </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Events <a href="#MyEvents" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyEvents">
                    <ul class="acc-list">
                      <li><a href="event-search.html"><i class="icon-search"></i> Search Event </a></li>
                      <li><a href="event-bookings.html"><i class="icon-th-thumb"></i> My Bookings </a></li>
                      <li><a class="active" href="event-create.html"><i class="icon-plus"></i> Find a Event </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                
                
              </div>
            </div>
            <!-- /.inner-box  --> 
            
            <div class="inner-box">
                <h2 class="title-2 uppercase"><strong>Sponsored</strong></h2>
                <img src="http://placehold.it/200x600&text=AD" />
            </div>

          </aside>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
            <div class="row">
              <div class="col-md-5 col-xs-4 col-xxs-12">
                <h3 class="no-padding text-center-480 useradmin"><a href=""><img class="userImg" src="images/user.jpg" alt="user"> John Doe </a> </h3>
              </div>
              <div class="col-md-7 col-xs-8 col-xxs-12">
                <div class="header-data text-center-xs"> 
                  
                  <!-- Traffic data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with red background --> 
                      <i class="fa fa-eye ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">7000</a> <em>Visits</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <!-- revenue data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with green background --> 
                      <i class="icon-th-thumb ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">12</a><em>Events</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <!-- revenue data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with blue background --> 
                      <i class="fa fa-user ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">18</a> <em>Followers</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- End Inner Box -->

          <div class="inner-box">
            <h5 class="list-title"><strong>Create Event</strong></h5>
              <ul class="nav nav-tabs">
                <li class="active"><a href="#MainInfo" data-toggle="tab">General Information</a></li>
                <li><a href="#Location" data-toggle="tab">Location</a></li>
                <li><a href="#Tickets" data-toggle="tab">Tickets / Booking</a></li>
                <li><a href="#Interests" data-toggle="tab">Interest / Category</a></li>
              </ul>

              <div class="tab-content">
                <br />
                <div class="tab-pane active" id="MainInfo">
                      <h5>Event Title: </h5>
                      <input type="text" class="form-control" name="eventTitle" placeholder="Event Title"/><br />
                      <div class="row">
                          <div class="col-md-6 col-lg-6 col-sm-6">
                            <h5>Event Date: </h5>
                            <input type="date" class="form-control" name="eventData" />
                          </div>
                          <div class="col-md-6 col-lg-6 col-sm-6">
                            <h5>Event Banner: </h5>
                            <input type="file" name="eventPhoto"/>
                          </div>
                      </div><br />
                      
                      <h5>Event Details: </h5>
                      <textarea rows="10" class="form-control" placeholder="Give us more info on your event" name="eventDescription"></textarea>
                </div>
                <div class="tab-pane" id="Location">
                       <div id="map"></div><br />
                       <textarea class="form-control" placeholder="Full Address"></textarea>
                </div>
                <div class="tab-pane" id="Tickets">

                      <input type="checkbox" name="eventFree" value="1"/> Attendance is Free
                      <hr />
                      <table class="table table-striped">
                          <tr>
                              <th>Ticket Name</th>
                              <th>Ticket Cost</th>
                              <th>Ticket Inventory</th>
                          </tr>

                          <tr>
                              <td><input type="text" class="form-control" name="ticketName1" /></td>
                              <td><input type="text" class="form-control" name="ticketCost1" /></td>
                              <td><input type="text" class="form-control" name="ticketInventory1" /></td>
                          </tr>

                          <tr>
                              <td><input type="text" class="form-control" name="ticketName2" /></td>
                              <td><input type="text" class="form-control" name="ticketCost2" /></td>
                              <td><input type="text" class="form-control" name="ticketInventory2" /></td>
                          </tr>

                          <tr>
                              <td><input type="text" class="form-control" name="ticketName2" /></td>
                              <td><input type="text" class="form-control" name="ticketCost2" /></td>
                              <td><input type="text" class="form-control" name="ticketInventory2" /></td>
                          </tr>
                      </table>
                </div>

                <div class="tab-pane" id="Interests">
                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Automotive" name="Automotive" value="Automotive">
                          <label for="Automotive">Automotive</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Business" name="Business" value="Business">
                          <label for="Business">Business</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Health" name="Health" value="Health">
                          <label for="Health">Health</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Technology" name="Technology" value="Technology">
                          <label for="Technology">Technology</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Science" name="Science" value="Science">
                          <label for="Science">Science</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Food and Drink" name="Food and Drink" value="Food and Drink">
                          <label for="Food and Drink">Food and Drink</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Family" name="Family" value="Family">
                          <label for="Family">Family</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Education" name="Education" value="Education">
                          <label for="Education">Education</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Fashion" name="Fashion" value="Fashion">
                          <label for="Fashion">Fashion</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Community" name="Community" value="Community">
                          <label for="Community">Community</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Sports" name="Sports" value="Sports">
                          <label for="Sports">Sports</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Travel" name="Travel" value="Travel">
                          <label for="Travel">Travel</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Outdoor" name="Outdoor" value="Outdoor">
                          <label for="Outdoor">Outdoor</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Hobbies" name="Hobbies" value="Hobbies">
                          <label for="Hobbies">Hobbies</label>
                        </div>

                        <div class="interest-check" style="background:url('http://placehold.it/200x100');">
                          <input type="checkbox" id="Meeting" name="Meeting" value="Meeting">
                          <label for="Meeting">Meeting</label>
                        </div>
                </div>
               
              </div> <!-- /.tab content -->
              <br />
              <input type="submit" class="btn btn-primary btn-lg btn-block" value="Create Event" />
          </div>
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  
 @endsection 