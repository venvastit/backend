@extends('dev.layouts.main')

@section('content')
  
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
          <aside>
            <div class="inner-box">
              <div class="user-panel-sidebar">
                <div class="collapse-box">
                  <h5 class="collapse-title no-border"> My Profile <a href="#MyProfile" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyProfile">
                    <ul class="acc-list">
                      <li><a class="active" href="user_home.html"><i class="icon-home"></i> My Home </a></li>
                      <li><a href="user_edit.html"><i class="icon-user"></i> Profile Settings </a></li>
                      <li><a href="user_mailbox.html"><i class="icon-chat"></i> Mailbox </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Events <a href="#MyEvents" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyEvents">
                    <ul class="acc-list">
                      <li><a href="user_myevents.html"><i class="icon-th-list"></i> My Events <span class="badge">0</span> </a></li>
                      <li><a href="user_mybookings.html"><i class="icon-heart"></i> My Bookings <span class="badge">0</span> </a></li>
                      <li><a href="event_list.html"><i class="icon-search"></i> Find an Event </a></li>
                      <li><a href="user_pastevents.html"><i class="icon-reply"></i> Past Events </a></li>
                    </ul>
                  </div>
                </div>
                
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Calendar <a href="#MyCalendar" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="MyCalendar">
                    <ul class="acc-list">
                      <li><a href="user_mycalendar"><i class="icon-calendar"></i> My Calendar </a></li>
                      <li><a href="calendar_public.html"><i class="icon-calendar-1"></i> Public Calendar</a></li>
                    </ul>
                  </div>
                </div>
                
                <!--/.collapse-box -->
                
                <div class="collapse-box">
                  <h5 class="collapse-title"> My Media <a href="#MyMedia" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="MyMedia">
                    <ul class="acc-list">
                      <li><a href="user_mymemory.html"><i class="icon-briefcase"></i> My Memory Locker </a></li>
                      <li><a href="user_mymemory_upload.html"><i class="icon-calendar-1"></i> Upload New Media </a></li>
                    </ul>
                  </div>
                </div>
                
                <!--/.collapse-box -->
                <!-- appears if the account has the 'organizer' premission set -->
                
                <div class="collapse-box">
                  <h5 class="collapse-title"> Organizers <a href="#Organizers" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse" id="Organizers">
                    <ul class="acc-list">
                      <li><a href="organizers.venvast.com"><i class="icon-vcard"></i> Organizers Dashboard </a></li>
                      <li><a href="user_request_organizers.html"><i class="icon-doc-text"></i> Request Access </a></li>
                    </ul>
                  </div>
                </div>
                
                <!-- /.collapse-box  --> 
              </div>
            </div>
            <!-- /.inner-box  --> 
            
          </aside>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
            <div class="row">
              <div class="col-md-5 col-xs-4 col-xxs-12">
                <h3 class="no-padding text-center-480 useradmin"><a href=""><img class="userImg" src="images/user.jpg" alt="user"> Touy Smith </a> </h3>
              </div>
              <div class="col-md-7 col-xs-8 col-xxs-12">
                <div class="header-data text-center-xs"> 
                  
                  <!-- Traffic data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with red background --> 
                      <i class="fa fa-eye ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">7000</a> <em>Visits</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <!-- revenue data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with green background --> 
                      <i class="icon-th-thumb ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">12</a><em>Events</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <!-- revenue data -->
                  <div class="hdata">
                    <div class="mcol-left"> 
                      <!-- Icon with blue background --> 
                      <i class="fa fa-user ln-shadow"></i> </div>
                    <div class="mcol-right"> 
                      <!-- Number of visitors -->
                      <p><a href="#">18</a> <em>Followers</em></p>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="inner-box">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <div class="profile-banner">
                    <img width="100%" src="http://placehold.it/800x300&text=Profile Banner" alt="Profile Banner" />
                  </div>
                </div>
            </div>
          </div>

          
          <div class="inner-box">
              <div class="social-wall">
                    <h5 class="list-title"><strong>Social Wall</strong></h5>
                    <!-- A sample post -->
                    <div class="social-element">
                          <div class="posted-by">
                              <img src="http://placehold.it/100x100" />
                              <h5><a href="#">Jeff Doe</a></h5>
                          </div>
                          <div class="posted-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at odio nibh. Suspendisse sit amet rhoncus felis. Aenean facilisis odio ac tincidunt facilisis. Vestibulum est nibh, scelerisque non lorem sed</p>
                          </div>
                          <div class="posted-date">
                            <p>Posted <strong>3 Hours Ago</strong></p>
                          </div>
                          <div class="posted-likes">
                              <span class="like-btn"><i class="icon-thumbs-up"></i> 14</span>
                              <span class="dislike-btn"><i class="icon-thumbs-down"></i>3</span>
                          </div>
                    </div>

                    <!-- A sample post -->
                    <div class="social-element">
                          <div class="posted-by">
                              <img src="http://placehold.it/100x100" />
                              <h5><a href="#">Jeff Doe</a></h5>
                          </div>
                          <div class="posted-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at odio nibh. Suspendisse sit amet rhoncus felis. Aenean facilisis odio ac tincidunt facilisis. Vestibulum est nibh, scelerisque non lorem sed</p>
                          </div>
                          <div class="posted-date">
                            <p>Posted <strong>3 Hours Ago</strong></p>
                          </div>
                          <div class="posted-likes">
                              <span class="like-btn"><i class="icon-thumbs-up"></i> 14</span>
                              <span class="dislike-btn"><i class="icon-thumbs-down"></i>3</span>
                          </div>
                    </div>

                    <!-- A sample post -->
                    <div class="social-element">
                          <div class="posted-by">
                              <img src="http://placehold.it/100x100" />
                              <h5><a href="#">Jeff Doe</a></h5>
                          </div>
                          <div class="posted-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at odio nibh. Suspendisse sit amet rhoncus felis. Aenean facilisis odio ac tincidunt facilisis. Vestibulum est nibh, scelerisque non lorem sed</p>
                          </div>
                          <div class="posted-date">
                            <p>Posted <strong>3 Hours Ago</strong></p>
                          </div>
                          <div class="posted-likes">
                              <span class="like-btn"><i class="icon-thumbs-up"></i> 14</span>
                              <span class="dislike-btn"><i class="icon-thumbs-down"></i>3</span>
                          </div>
                    </div>
              </div>
          </div>
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  
  @endsection 