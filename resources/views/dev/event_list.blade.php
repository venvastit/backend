@extends('dev.layouts.main')

@section('content')
  <div class="search-row-wrapper">
    <div class="container ">
      <form action="#" method="GET">
        <div class="col-sm-3">
          <input class="form-control keyword" type="text" placeholder="e.g. Business Events">
        </div>
        <div class="col-sm-3">
          <select class="form-control selecter" name="category" id="search-category" >
            <option selected="selected" value="">All Interests</option>
            <option value="AU"> Automotive </option>
<option value="BU"> Business </option>
<option value="HE"> Health </option>
<option value="TE"> Technology </option>
<option value="SC"> Science </option>
<option value="FO"> Food and Drink </option>
<option value="FA"> Fashion </option>
<option value="CO"> Community </option>
<option value="MU"> Music </option>
<option value="SP"> Sports </option>
<option value="TR"> Travel </option>
<option value="FA"> Family and Education </option>
<option value="OU"> Outdoor </option>
<option value="HO"> Hobbies </option>
<option value="ME"> Meeting </option>
<option value="PA"> Party </option>
<option value="AT"> Attraction </option>
<option value="CO"> Confrence </option>
<option value="CL"> Class </option>
            <option value="dropoff" style="background-color:#E9E9E9;font-weight:bold;" disabled="disabled"> - Extra - </option>
            <option value="Other"> Other </option>
          </select>
        </div>
        <div class="col-sm-3">
          <select class="form-control selecter" name="location" id="id-location">
            <option selected="selected" value="">All Locations</option>
            <option value="BK">Bangkok</option>
            <option value="Other-Locations">Other Locations</option>
          </select>
        </div>
        <div class="col-sm-3">
          <button class="btn btn-block btn-primary  "> <i class="fa fa-search"></i> </button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.search-row -->
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
          <aside>
            <div class="inner-box">
              <div class="categories-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Interests</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                  <li><a href=""><span class="title">Automotive</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Business</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Health</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Technology</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Science</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Food and Drink</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Fashion</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Community</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Music</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Sports</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Travel</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Family and Education</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Outdoor</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Hobbies</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Meeting</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Party</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Atttaction</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Confrence</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Class</span><span class="count">&nbsp;.</span></a> </li>
<li><a href=""><span class="title">Other</span><span class="count">&nbsp;.</span></a> </li>

                </ul>
              </div>
              <!--/.interests-list-->
              
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Locations</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                  <li> <a href=""> Bangkok </a></li>
                  <li> <a href="sub-category-sub-location.html"> Other Locations </a></li>
                </ul>
              </div>
              <!--/.locations-list-->
              
              
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Dates</a></strong></h5>
                <form role="form" class="form-inline">
                   <div class="form-group">
            <div class="input-group date" id="datetimepicker1">
                <input type="text" class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
                  <div class="form-group col-sm-1 no-padding text-center"> - </div>
                 <div class="form-group">
            <div class="input-group date" id="datetimepicker2">
                <input type="text" class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
                  </form>
                <div style="clear:both"></div>
              </div>
              </div>
              <!--/.date-filter. date picker not working? https://eonasdan.github.io/bootstrap-datetimepicker/-->
         
              <div style="clear:both"></div>
            </div>
            
            <!--/.categories-list--> 
          </aside>
        </div>
        <!--/.page-side-bar-->
        <div class="col-sm-9 page-content col-thin-left">
          <div class="category-list">
            <div class="tab-box "> 
              
              <!-- Nav tabs -->
              <ul class="nav nav-tabs add-tabs" id="ajaxTabs" role="tablist">
                <li class="active"><a href="#allAds" data-url="ajax/1.html" role="tab" data-toggle="tab">All Events <span class="badge">5</span></a></li>
                <li><a href="#businessAds" data-url="ajax/2.html" role="tab" data-toggle="tab">My Bookings <span class="badge">0</span></a></li>
                <li><a href="#personalAds" data-url="ajax/3.html" role="tab" data-toggle="tab">My Events <span class="badge">0</span></a></li>
              </ul>
              <div class="tab-filter">
                <select class="selectpicker" data-style="btn-select" data-width="auto">
                  <option>Sort by</option>
                  <option>Events Today</option>
                  <option>Events Tomorrow</option>
                  <option>Events Next Week</option>
                  <option>Events Next Month</option>
                </select>
              </div>
            </div>
            <!--/.tab-box-->
            
            <div class="listing-filter">
              <div class="pull-left col-xs-6">
                <div class="breadcrumb-list"> <a href="#" class="current"> <span>All events</span></a> in Bangkok <a href="#selectRegion" id="dropdownMenu1"  data-toggle="modal"> <span class="caret"></span> </a> </div>
              </div>
              <div class="pull-right col-xs-6 text-right listing-view-action"> <span class="list-view active"><i class="  icon-th"></i></span> <span class="compact-view"><i class=" icon-th-list  "></i></span> <span class="grid-view "><i class=" icon-th-large "></i></span> </div>
              <div style="clear:both"></div>
            </div>
            <!--/.listing-filter-->
            
            <div class="adds-wrapper">
              <div class="tab-content">
                <div class="tab-pane active" id="allAds">Loading...</div>
                <div class="tab-pane" id="businessAds"></div>
                <div class="tab-pane" id="personalAds"></div>
              </div>
            </div>
            <!--/.adds-wrapper-->
            
                    </div>
          <div class="pagination-bar text-center">
            <ul class="pagination">
              <li  class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li><a href="#"> ...</a></li>
              <li><a class="pagination-btn" href="#">Next &raquo;</a></li>
            </ul>
          </div>
          <!--/.pagination-bar -->
          
                
        </div>
        <!--/.page-content--> 
        
      </div>
    </div>
  </div>
  <!-- /.main-container -->
  
  <div class="footer" id="footer">
    <div class="container">
      <ul class=" pull-left navbar-link footer-nav">
          <li><a href="index.html"> Home </a> <a href="aboutus.html"> About Us </a> <a href="services.html"> Services </a> <a href="contact.html"> Contact us </a> <a href="faq.html"> FAQ </a>
      <ul class=" pull-right navbar-link footer-nav">
        <li> &copy; VenVast </li>
      </ul>
    </div>
    
  </div>
  <!-- /.footer --> 
</div>
<!-- /.wrapper --> 

<!-- Modal Change City -->

<div class="modal fade" id="selectRegion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><i class=" icon-map"></i> Select your province </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
        
         

<div style="clear:both"></div>            
            <div class="col-sm-6 no-padding">
            <select  class="form-control selecter  " id="region-state" name="region-state">
		<option value="">All Provinces</option>
		<option value="Alabama">Bangkok</option>
			</select>
            </div>
           <div style="clear:both"></div>            

            <hr class="hr-thin">
          </div>
          <div class="col-md-4">
            <ul  class="list-link list-unstyled">
              <li> <a  href="#" title="">All Districts</a> </li>
			 <li> <a  href="#" title="Bang Bon">Bang Bon</a> </li>
		 <li> <a  href="#" title="Bang Kapi">Bang Kapi</a> </li>
		 <li> <a  href="#" title="Bang Khae">Bang Khae</a> </li>
		 <li> <a  href="#" title="Bang Kho Laem">Bang Kho Laem</a> </li>
		 <li> <a  href="#" title="Bang Khun Thian">Bang Khun Thian</a> </li>
		 <li> <a  href="#" title="Bang Na">Bang Na</a> </li>
         <li> <a  href="#" title="Bang Phiat">Bang Phiat</a> </li>
		 <li> <a  href="#" title="Bang Rak">Bang Rak</a> </li>
		 <li> <a  href="#" title="Bang Sue">Bang Sue</a> </li>
         <li> <a  href="#" title="Bangkok Noi">Bangkok Noi</a> </li>
         <li> <a  href="#" title="Bangkok Yai">Bangkok Yai</a> </li>
		 <li> <a  href="#" title="Bueng Kum">Bueng Kum</a> </li>
		 <li> <a  href="#" title="Chatuchak">Chatuchak</a> </li>
		 <li> <a  href="#" title="Chom Thong">Chom Thong</a> </li>
		 <li> <a  href="#" title="Din Daeng">Din Daeng</a> </li>
		 <li> <a  href="#" title="Don Mueang">Don Mueang</a> </li>
         <li> <a  href="#" title="Dusit">Dusit</a> </li>
		 <li> <a  href="#" title="Huai Khwang">Huai Khwang</a> </li>
		 <li> <a  href="#" title="Khan Na Yao">Khan Na Yao</a> </li>
         <li> <a  href="#" title="Klong Sam Wa">Klong Sam Wa</a> </li>
	
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-link list-unstyled">
             <li> <a  href="#" title="Khlong San">Khlong San</a> </li>
    <li> <a  href="#" title="Khlong Toei">Khlong Toei</a> </li>
    <li> <a  href="#" title="Lak Si">Lak Si</a> </li>
    <li> <a  href="#" title="Lat Krabang">Lat Krabang</a> </li>
    <li> <a  href="#" title="Lat Phrao">Lat Phrao</a> </li>
    <li> <a  href="#" title="Min Buri">Min Buri</a> </li>
    <li> <a  href="#" title="Nong Chok">Nong Chok</a> </li>
    <li> <a  href="#" title="Nong Khaem">Nong Khaem</a> </li>
    <li> <a  href="#" title="Pathum Wan">Pathum Wan</a> </li>
    <li> <a  href="#" title="Phasi Charoen">Phasi Charoen</a> </li>
    <li> <a  href="#" title="Phaya Thai">Phaya Thai</a> </li>
    <li> <a  href="#" title="Phra Khanong">Phra Khanong</a> </li>
    <li> <a  href="#" title="Pom Prap Sattru Phai">Pom Prap Sattru Phai</a> </li>
    <li> <a  href="#" title="Prawet">Prawet</a> </li>
    <li> <a  href="#" title="Rat Burana">Rat Burana</a> </li>
    <li> <a  href="#" title="Ratchathewi">Ratchathewi</a> </li>
    <li> <a  href="#" title="Sai Mai">Sai Mai</a> </li>
    <li> <a  href="#" title="Samphanthawong">Samphanthawing</a> </li>
    <li> <a  href="#" title="Saphan Sung">Saphun Sung</a> </li>
    <li> <a  href="#" title="Sathon">Sathon</a> </li>
    

            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-link list-unstyled">
          <li> <a  href="#" title="Suan Luang">Suan Luang</a> </li>
<li> <a  href="#" title="Taling Chan">Taling Chan</a> </li>
<li> <a  href="#" title="Thawi Watthana">Thawi Watthana</a> </li>
<li> <a  href="#" title="Thon Buri">Thon Buri</a> </li>
<li> <a  href="#" title="Thung Khru">Thung Khru</a> </li>
<li> <a  href="#" title="Wang Thonglang">Wang Thonglang</a> </li>
<li> <a  href="#" title="Watthana">Watthana</a> </li>
<li> <a  href="#" title="Yan Nawa">Yan Nawa</a> </li>

	
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- /.modal --> 

@endsection 
