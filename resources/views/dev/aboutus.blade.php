@extends('dev.layouts.main')

@section('content')
 <div class="main-container inner-page">
    <div class="container">
      <div class="row clearfix">
        <h1 class="text-center title-1"> About VenVast </h1>
        <hr class="center-block small text-hr">
        <div class="col-lg-12 text-center">
                 </div>
        <div style="clear:both">
          <hr>
        </div>
        <div class="col-lg-6">
          <div class="panel panel-default">
  <div class="panel-body">
     <img src="images/about/brief-1.png" class="img-responsive" alt="Responsive image">
     <br>
     <img src="images/about/brief-2.png" class="img-responsive" alt="Responsive image">
  </div>
</div>
        </div>
        <div class="col-lg-6"> 
        <div class="panel panel-default">
  <div class="panel-body">
       <ul class="nav nav-tabs">
          <li class="active"><a href="#Tab1" data-toggle="tab">About Us</a></li>
          <li><a href="#Tab2" data-toggle="tab">Features</a></li>
          </ul>
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="Tab1">
          <p><b>VenVast</b> -   is a Social Event Management Platform that aspires to solve day to day problems that people face when trying to find events that will interest them. By providing many different functionalities, VenVast paves a better path between organizers and users, allowing users to not only attend already organized events, but to also craft their days by organizing their own events. With VenVast, every day is event day.</p>
    <p><b>Mission</b> -  is to revolutionizing the event management platforms by providing the one stop solution at the click of a button, VenVast sees that we enrich ourselves with events every day scheduled or not, this is where organic events formalize and help users craft their day. Leading to Social Event Management Platform where all can stay connected with likeminded and fellow interested users. With VenVast pre, during, and post chats allows users to engage and gain in depth event knowledge. Users selecting interests offers the opportunity to personalize their experiences.</p>
    <p><b>Vision</b> -  to be first ever worldwide fully executed social event management platform, which will follow organizers and attendees interest, because with VenVast every day is event day.</p>
          </div>
          
          <div class="tab-pane" id="Tab2">
          <p><b><i class="icon-calendar"></i>Advanced Calendar</b> - Craft you day, Create your day, Consume your day, with our advanced calendar. Love the Calendar again. Connect our Calendar with your favorite existing Calendar.</p>
         <p><b><i class="icon-tag"></i>Your Interests</b> - Your Interests are at the core of what VenVast is. When you select your Interests, our system tailors to your wants and needs. We do the hard work for you, so you dont have too.</p>
         <p><b><i class="icon-chat"></i>I2I Chat</b> - I2I Chat is a advanced chat system, that is purpose built for what we do at VenVast. Pre, During and Post Chat. To get more interactive with the social community that is VenVast.</p>
         <p><b><i class="icon-user"></i>For the User and Organizer</b>- 

We want to be the most simple, easy and fast platform on the web. For the User the interests will automatically intertwine with the system to provide with the best experience. For the organizer we provide many premium tools to help them achieve their hopes and goals.
</p>          
          </div>
          
                  </div> <!-- /.tab content -->
  </div>
</div>
        </div>
                    
          </div>
    </div>
  </div>
  <!-- /.main-container -->
  @endsection 
  