@extends('mails._base')

@section('title')
    VenVast Account Activation
@endsection
@section('content_title')
    Activate your VenVast Account
@endsection
@section('content')
    <p class="text">Hello, {{ $user->email }}</p>
    <p class="text">Thanks for joining VenVast Beta! Updates and new features are released weekly!
    In order to finish registration process follow the link:
        <a href="{{route('user.activate') . '?activation_code=' . $user->activation_code . '&email=' . $user->email}}">Activate account</a>
    </p>
    <p class="text"><small>If you didn't register at VenVast just ignore this message</small></p>
@endsection