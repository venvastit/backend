<html ng-app="app" xmlns="http://www.w3.org/1999/html">

<head>
    @if(env('APP_DEBUG') === false)
        <script type="text/javascript">
//            console = {
//                log: function() {
//
//                }
//            };

        </script>
    @endif
    <base href="/">
    </base>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-80555245-1', 'auto');
            ga('send', 'pageview');

        </script>
    <link rel="stylesheet" href="//venvast.scdn1.secure.raxcdn.com/css/vendor.css">
    <!--[if lte IE 10]>
    <script type="text/javascript">document.location.href ='/unsupported-browser'</script>
    <![endif]-->

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="fragment" content="!">
    <!-- Fav and touch icons -->
    @if(env('APP_DEBUG') === true)
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{public_path('assets/ico/apple-touch-icon-144-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{public_path('assets/ico/apple-touch-icon-114-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{public_path("assets/ico/apple-touch-icon-72-precomposed.png")}}">
        <link rel="apple-touch-icon-precomposed" href="{{public_path("assets/ico/apple-touch-icon-57-precomposed.png")}}">
        <link rel="apple-touch-icon-precomposed" href="{{public_path("assets/ico/apple-touch-icon-57-precomposed.png")}}">
    @else
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="//venvast2.scdn5.secure.raxcdn.com/assets/ico/apple-touch-icon-72-precomposed.png">
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="//venvast3.scdn4.secure.raxcdn.com/assets/ico/apple-touch-icon-144-precomposed.png">
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="//venvast4.scdn1.secure.raxcdn.com/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="//venvast.com/assets/ico/apple-touch-icon-57-precomposed.png">
    @endif
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png" />
    <title>Venvast - events</title>
    <meta property="og:type" id="meta-type"/>
    <meta property="og:title" id="meta-title"/>
    <meta property="og:description" id="meta-description"/>
    <meta property="og:image" id="meta-image"/>
    <!-- Bootstrap core CSS -->
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="1; URL=ie/ie.html">
    <![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"> </script>
    @if(env('APP_DEBUG') === true)
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/autocomplete.css">
        <link rel="stylesheet" href="/css/jquery.datetimepicker.css">
        <link rel="stylesheet" href="/css/ion.calendar.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    @else
        <link rel="stylesheet" href="//venvast.scdn1.secure.raxcdn.com/css/autocomplete.css">
        <link rel="stylesheet" href="//venvast2.scdn5.secure.raxcdn.com/css/app.css">
        <link rel="stylesheet" href="//venvast3.scdn4.secure.raxcdn.com/css/jquery.datetimepicker.css">
        <link rel="stylesheet" href="//venvast.com/css/ion.calendar.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    @endif
        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />

        <script src="//maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyBfnCaACfxQD5YOCC7w1O2ItMIonYxLkXE"></script>
        @if(env('APP_DEBUG') === true)
            <script src="/js/vendor.js"></script>
            <script src="/js/app.js"></script>
            <script src="/js/picker_autocomplete.js"></script>
            <script src="/js/ion.calendar.js"></script>
            <script src="/js/jstz.min.js"></script>
        @else
            <script src="//venvast4.scdn1.secure.raxcdn.com/js/vendor.js"></script>
            <script src="//venvast.com/js/app.js"></script>
            <script src="//venvast.com/js/jstz.min.js"></script>
            <script src="//venvast.scdn1.secure.raxcdn.com/js/picker_autocomplete.js"></script>
            <script src="//venvast4.scdn1.secure.raxcdn.com/js/ion.calendar.js"></script>
        @endif
</head>

    <body>
        <div id="fb_app_id" style="display: none">{{ env('FACEBOOK_APP_ID') ? env('FACEBOOK_APP_ID') : '1631594013727920' }}</div>

        <div ui-view="main" ng-controller="HomeCtrl"></div>
    </body>

    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
</html>