
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Venvast app is the perfect companion to discover events, venues and deals in Bangkok.
                            Get instant access to what is around you based on your location and interests!">
    <meta name="keywords" content="events venues deals restaurants bars clubs party attend place">
    <title>Venvast | Discover Bangkok</title>
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Owl Carousel Assets -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">


    <!-- Pixeden Icon Font -->
    <link rel="stylesheet" href="css/Pe-icon-7-stroke.css">

    <!-- Font Awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">


    <!-- PrettyPhoto -->
    <link href="css/prettyPhoto.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png" />

    <!-- Style -->
    <link href="css/style.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- =========================
    START ABOUT US SECTION
============================== -->
<section class="header parallax home-parallax page" id="HOME">
    <h2></h2>
    <div class="section_overlay">
        <nav class="navbar navbar-default navbsar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img class="visible-xs" src="images/navlogo.svg" alt="Logo">
                        <img class="hidden-xs" src="images/navlogoweb.svg">

                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- NAV -->
                        <li><a href="#HOME">HOME</a> </li>
                        <li><a href="#FEATURES">FEATURES</a></li>
                        <li><a href="#SCREENS">SCREENS</a> </li>
                        <li><a href="#DOWNLOAD">DOWNLOAD </a> </li>
                        <li><a href="#CONTACT">CONTACT </a> </li>
                        <li><a href="//venvast.com" target="_blank">WEB APP</a> </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container- -->
        </nav>

        <div class="container home-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo text-center">
                        <!-- LOGO -->
                        <img src="images/logo.svg" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <div class="home_text">
                        <!-- TITLE AND DESC -->
                        <h1>Explore Bangkok <br> With venvast mobile app</h1>
                        <p>Venvast app is the perfect companion to discover events, venues and deals in Bangkok.
                            Get instant access to what is around you based on your location and interests!</p>

                        <div class="download-btn">
                            <!-- BUTTON -->
                            <a class="btn home-btn wow fadeInLeft" href="#DOWNLOAD">Download</a>
                            <a class="tuor btn wow fadeInRight" href="#FEATURES">FEATURES <i class="fa fa-angle-down"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-1 col-sm-4">
                    <div class="home-iphone">
                        <img src="images/iPhone_Home.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- END HEADER SECTION -->




<!-- =========================
    START ABOUT US SECTION
============================== -->




<!--  <div class="video_area">
      <div class="container">
          <div class="row">
              <div class="col-md-6 wow fadeInLeftBig">
              <!-- VIDEO LEFT TITLE -->
<!--        <div class="video_title">
            <h2>Best App <br>in the market</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
        </div>
        <div class="video-button">
            <!-- BUTTON -->
<!--       <a class="btn btn-primary btn-video" href="#FEATURES" role="button">Features</a>
   </div>
</div>
<div class="col-md-6 wow fadeInRightBig">
    <!-- VIDEO -->
<!--      <div class="video">
          <iframe src="https://player.vimeo.com/video/84787958?title=0&amp;byline=0&amp;portrait=0&amp;color=cc0000" width="560" height="315"></iframe>
      </div>
  </div>
</div>
</div>
</div>
</section>
<!-- End About Us -->









<!-- =========================
     START FEATURES
============================== -->
<section id="FEATURES" class="features page">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- FEATURES SECTION TITLE -->
                <div class="section_title wow fadeIn" data-wow-duration="1s">
                    <h2>Features</h2>
                    <p>The Venvast app is the perfect companion, guiding you through daily events, deals and venues in Bangkok, whether you are new in town or just looking for exciting places and activities.  </p>
                </div>
                <!-- END FEATURES SECTION TITLE -->
            </div>
        </div>
    </div>

    <div class="feature_inner">
        <div class="container">
            <div class="row">
                <div class="col-md-4 right_no_padding wow fadeInLeft" data-wow-duration="1s">
                    <!-- FEATURE -->

                    <div class="left_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-ticket"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>Events</h3>
                        <p>Discover the hottest upcoming events based on your interests and location</p>
                    </div>

                    <!-- END SINGLE FEATURE -->


                    <!-- FEATURE -->
                    <div class="left_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-shopbag"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>Venues</h3>
                        <p>Hungry ? Thirsty ? Locate venues when looking for that special place.</p>
                    </div>
                    <!-- END SINGLE FEATURE -->


                    <!-- FEATURE -->
                    <div class="left_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-gift"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>Deals</h3>
                        <p>Find promotions, discounts and offers from latest fashion trends to after work drinks and tasty cuisines</p>
                    </div>
                    <!-- END SINGLE FEATURE -->

                </div>
                <div class="col-md-4">
                    <div class="feature_iphone">
                        <!-- FEATURE PHONE IMAGE -->
                        <img class="wow bounceIn" data-wow-duration="1s" src="images/iPhone04.png" alt="Venvast App">
                    </div>
                </div>
                <div class="col-md-4 left_no_padding wow fadeInRight" data-wow-duration="1s">

                    <!-- FEATURE -->
                    <div class="right_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-way"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>Discover</h3>
                        <p>Search based on your interests what venues, events or deals are available around you</p>
                    </div>
                    <!-- END SINGLE FEATURE -->


                    <!-- FEATURE -->
                    <div class="right_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-like"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>Interests</h3>
                        <p>Different categories help you quickly find what you are looking for</p>
                    </div>
                    <!-- END SINGLE FEATURE -->


                    <!-- FEATURE -->
                    <div class="right_single_feature">
                        <!-- ICON -->
                        <div><span class="pe-7s-map-2"></span></div>

                        <!-- FEATURE HEADING AND DESCRIPTION -->
                        <h3>Get Directions</h3>
                        <p>Always on hand, guiding and helping you to explore Bangkok</p>
                    </div>
                    <!-- END SINGLE FEATURE -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END FEATURES SECTION -->



<!-- =========================
     START CALL TO ACTION
============================== -->
<!--  <div class="call_to_action">
      <div class="container">
          <div class="row wow fadeInLeftBig" data-wow-duration="1s">
              <div class="col-md-9">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et olore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
              </div>
              <div class="col-md-3">
                  <a class="btn btn-primary btn-action" href="#" role="button">Download Now</a>
              </div>
          </div>
      </div>
  </div>

  <!-- END CALL TO ACTION -->


<!-- =========================
     Start FUN FACTS
============================== -->


<section class="fun_facts parallax">
    <div class="section_overlay">
        <div class="container wow bounceInLeft" data-wow-duration="1s">
            <div class="row text-center">
                <div class="col-md-3">
                    <div class="single_fun_facts">
                        <i class="pe-7s-ticket"></i>
                        <h2><span  class="counter_num">{{$week}}</span> <span>+</span></h2>
                        <p>Events this week</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_fun_facts">
                        <i class="pe-7s-look"></i>
                        <h2><span  class="counter_num">{{$year}}</span> <span>+</span></h2>
                        <p>Upcoming events this year</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_fun_facts">
                        <i class="pe-7s-shopbag"></i>
                        <h2><span  class="counter_num">{{ $venues }}</span> <span>+</span></h2>
                        <p>Venues in Bangkok</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_fun_facts">
                        <i class="pe-7s-timer"></i>
                        <h2><span  class="counter_num">{{$next}}</span> <span>min</span></h2>
                        <p>until next event starts</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- END FUN FACTS -->


<!-- =========================
     START CALL TO ACTION
============================== -->
<div class="call_to_action">
      <div class="container">
          <div class="row wow fadeInLeftBig" data-wow-duration="1s">
              <div class="col-md-9">
                  <p>With Venvast it’s never been easier to explore Bangkok. In upcoming months you can discover daily offers, promotions and discounts and always know where the best deals in town are from shopping to dining</p>
              </div>
              <div class="col-md-3">
                  <a class="btn btn-primary btn-action" href="#DOWNLOAD" role="button">Download Now</a>
              </div>
          </div>
      </div>
</div>

  <!-- END CALL TO ACTION -->


<!-- =========================
     Start APPS SCREEN SECTION
============================== -->
<section class="apps_screen page" id="SCREENS">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 wow fadeInBig" data-wow-duration="1s">
                <!-- APPS SCREEN TITLE -->
                <!--    <div class="section_title">
                       <h2>Screens</h2>
                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                   </div>
                   <!-- END APPS SCREEN TITLE -->
            </div>
        </div>
    </div>

    <div class="screen_slider">
        <div id="demo" class="wow bounceInRight" data-wow-duration="1s">
            <div id="owl-demo" class="owl-carousel">

                <!-- APPS SCREEN IMAGES -->
                <div class="item">
                    <a href="images/screens/iPhone01.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone01.png" width="60" height="60" alt="APPS SCREEN" /></a>
                </div>
                <div class="item">
                    <a href="images/screens/iPhone02.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone02.png" width="60" height="60" alt="APPS SCREEN" /></a>
                </div>
                <div class="item">
                    <a href="images/screens/iPhone03.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone03.png" width="60" height="60" alt="APPS SCREEN" /></a>
                </div>
                <div class="item">
                    <a href="images/screens/iPhone04.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone04.png" width="60" height="60" alt="APPS SCREEN" /></a>
                </div>
                <div class="item">
                    <a href="images/screens/iPhone05.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone05.png" width="60" height="60" alt="APPS SCREEN" /></a>
                </div>
                <div class="item">
                    <a href="images/screens/iPhone06.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone06.png" width="60" height="60" alt="APPS SCREEN" /></a>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- ENS APPS SCREEN -->



<!-- =========================
     START DOWNLOAD NOW
============================== -->
<section class="download page">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- DOWNLOAD NOW SECTION TITLE -->
                <!--   <div class="section_title">
                       <h2>download now</h2>
                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                   </div>
                   <!--END DOWNLOAD NOW SECTION TITLE -->
            </div>
        </div>
    </div>

    <!-- <div class="container">
              <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                      <div class="download_screen text-center wow fadeInUp" data-wow-duration="1s">
                          <img src="images/download_screen.png" alt="">
                      </div>
                  </div>
              </div>
          </div>

        -->  <div class="available_store" id="DOWNLOAD">
        <div class="container  wow bounceInRight" data-wow-duration="1s">
            <div class="col-md-6">
                <div class="available_title">
                    <h2>DOWNLOAD NOW</h2>
                    <p>Available on Android and iOS for free! If you like it, support us by recommending app to your friends!  </p>
                </div>
            </div>

            <!-- DOWNLOADABLE STORE -->
            <div class="col-md-6">
                <div class="row">

                    <div class="col-xs-6 .col-md-4 no_padding">
                        <a href="https://play.google.com/store/apps/details?id=com.venvast.mvp3" target="_blank">
                            <div class="single_store">
                                <i class="fa fa-android"></i>
                                <p>Download from Google Play </p>
                                <div class="store_inner">
                                    <h2>ANDROID</h2>

                                </div>
                            </div>
                        </a>
                    </div>

                    {{--<a href="https:/appstore.com">--}}
                        <div class="col-xs-6 .col-md-4 no_padding">
                            <div class="single_store">
                                <i class="fa fa-apple"></i>
                                <p>Coming soon on iOS </p>
                                <div class="store_inner">
                                    <h2>iOS</h2>
                                </div>
                            </div>
                        </div>
                    {{--</a>--}}
                    <!--         <div class="col-md-4 no_padding">
                                     <a href="">
                                         <div class="single_store last">
                                             <i class="fa fa-windows"></i>
                                             <div class="store_inner">
                                                 <h2>WINDOWS</h2>
                                             </div>
                                         </div>
                                     </a>
                                 </div>
                             </div>
                         </div>
                         <!-- END DOWNLOADABLE STORE -->
                </div>
            </div>
</section>
<!-- END DOWNLOAD -->

<!-- =========================
     START CONTCT FORM AREA
============================== -->
<section class="contact page" id="CONTACT">
    <div class="section_overlay">
        <div class="container">
            <div class="col-md-10 col-md-offset-1 wow bounceIn">
                <!-- Start Contact Section Title-->
                <div class="section_title">
                    <h2>Get in touch</h2>
                    <p>It's always nice to hear from our customers and partners! We will respond within 24 hours.</p>
                </div>
            </div>
        </div>

        <div class="contact_form wow bounceIn">
            <div class="container">

                <!-- START ERROR AND SUCCESS MESSAGE -->
                <div class="form_error text-center">
                    <div class="name_error hide error">Please Enter your name</div>
                    <div class="email_error hide error">Please Enter your Email</div>
                    <div class="email_val_error hide error">Please Enter a Valid Email Address</div>
                    <div class="message_error hide error">Please Enter Your Message</div>
                </div>
                <div class="Sucess"></div>
                <!-- END ERROR AND SUCCESS MESSAGE -->

                <!-- FORM -->
                <form role="form">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="name" placeholder="Name">
                            <input type="email" class="form-control" id="email" placeholder="Email">
                            <input type="text" class="form-control" id="subject" placeholder="Subject">
                        </div>


                        <div class="col-md-8">
                            <textarea class="form-control" id="message" rows="25" cols="10" placeholder="  Message Texts..."></textarea>
                            <button type="button" class="btn btn-default submit-btn form_submit">SEND MESSAGE</button>
                        </div>
                    </div>
                </form>
                <!-- END FORM -->
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 wow bounceInLeft">
                    <div class="social_icons">
                        <ul>
                            <li><a href="https://www.facebook.com/venvast" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="https://twitter.com/VenVast" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="https://www.instagram.com/venvast" target="_blank"><i class="fa fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END CONTACT -->

<!-- =========================
     Start Subscription Form
============================== -->



<section class="subscribe parallax subscribe-parallax" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
    <div class="section_overlay wow BounceIn">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <!-- Start Subscribe Section Title -->
                    <div class="section_title">
                        <h2>Subscribe to our newsletter</h2>
                        <p> Stay updated with our weekly newsletter about trending events and interesting new places. You'll be among the first to know..</p>
                    </div>
                    <!-- End Subscribe Section Title -->
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row  wow bounceInRight">
                <div class="col-md-6 col-md-offset-3">
                    <!-- SUBSCRIPTION SUCCESSFUL OR ERROR MESSAGES -->
                    <div class="subscription-success"></div>
                    <div class="subscription-error"></div>


                    <form id="mc-form" class="subscribe_form">
                        <div class="form-group">
                            <!-- EMAIL INPUT BOX -->
                            <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" placeholder="Enter Email Address">
                        </div>

                        <!-- SUBSCRIBE BUTTON -->
                        <button type="submit" id="subscribe-button" class="btn btn-default subs-btn">Submit</button>
                    </form>


                </div>
            </div>
        </div>
    </div>
</section>

<!-- END SUBSCRIPBE FORM -->


<!-- =========================
     FOOTER
============================== -->

<section class="copyright">
    <h2></h2>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="copy_right_text">
                    <!-- COPYRIGHT TEXT -->
                    <p>Copyright &copy; 2016 <a href="">Venvast limited</a> <span>By </span><a href="http://venvast.com/">venvast</a></p>
                </div>
            </div>

            <div class="col-md-6">
                <div class="scroll_top">
                    <a href="#HOME"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END FOOTER -->


<!-- =========================
     SCRIPTS
============================== -->


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.ajaxchimp.langs.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/script.js"></script>



</body>

</html>