<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Fast tools and info</h1>
        <div class="">
            <a href="{{ $login_url }}">Get fb access_token</a>
            <p>Access token: {{Session::get('fb_user_access_token')}}</p>
        </div>
        <div class="">
            <a href="{{route('admin.event.migrate')}}" class="btn btn-default btn-large">Migrate to database</a>
        </div>
        <form class="form-horizontal" action="{{ route('admin.event.create') }}" method="post">
            {{ csrf_field() }}
            <fieldset>
                <legend>Save events</legend>
                @for($i = 0; $i < 10; $i++)
                <div class="form-group">
                    <label class="col-md-4 control-label" for="fb_id">Add facebook event</label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon">https://facebook.com/events/</span>
                            <input id="fb_id" name="fb_ids[]" class="form-control" placeholder="0000000000000000" type="text">
                        </div>
                        <p class="help-block">facebook_id</p>
                    </div>
                </div>
                @endfor
                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Save!</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>