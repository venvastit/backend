<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Scrap facebook venues</h1>
        <div class>
            <a href="{{ $login_url }}">Get fb access_token</a>
            <p>Access token: {{Session::get('fb_user_access_token')}}</p>
        </div>
        <div>
            <a href="{{route('admin.venue.migrate')}}" class="btn btn-default btn-large">Migrate to database</a>
        </div>
        <form class="form-horizontal" action="{{ route('admin.venue.create') }}" method="post">
            {{ csrf_field() }}
            <fieldset>
                <legend>Save events</legend>
                @for($i = 0; $i < 10; $i++)
                <div class="form-group">
                    <label class="col-md-4 control-label" for="fb_id">Add facebook event</label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon">https://facebook.com/</span>
                            <input id="fb_id" name="fb_ids[]" class="form-control" placeholder="TGTHIncubator" type="text">
                        </div>
                        <p class="help-block">facebook url without params(ex. your current location is 'https://www.facebook.com/TGTHIncubator/timeline' so you should write only 'TGTHIncubator' in this input</p>
                    </div>
                </div>
                @endfor
                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Save!</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>