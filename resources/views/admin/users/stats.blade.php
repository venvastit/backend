@extends('adminlte::page')

@section('title', 'Venue stats')

@section('content_header')
    <h1>Venue stats</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class='box'>
                <div class="box-header with-border">
                    <h3 class="box-title">Users registered in system in last 7 days</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-building"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Users Total</span>
                    <span class="info-box-number">{{ $totalUser }}</span>
                </div>
            </div>
        </div>
        {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box bg-red">--}}
                {{--<span class="info-box-icon"><i class="fa fa-circle-thin"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Has venues</span>--}}
                    {{--<span class="info-box-number">{{ $hasVenues }}</span>--}}

                    {{--<div class="progress">--}}
                        {{--<div class="progress-bar" style="width: {{ (int) (($hasVenues/$totalUser)*100) }}%"></div>--}}
                    {{--</div>--}}
                      {{--<span class="progress-description">--}}
                        {{--{{ (int) (($hasVenues/$totalUser)*100) }}% of created venues--}}
                      {{--</span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box bg-red">--}}
                {{--<span class="info-box-icon"><i class="fa fa-circle-thin"></i></span>--}}
                {{--<div class="info-box-content">--}}
                    {{--<span class="info-box-text">Has events</span>--}}
                    {{--<span class="info-box-number">{{ $hasEvents }}</span>--}}

                    {{--<div class="progress">--}}
                        {{--<div class="progress-bar" style="width: {{ (int) (($hasEvents/$totalUser)*100) }}%"></div>--}}
                    {{--</div>--}}
                      {{--<span class="progress-description">--}}
                        {{--{{ (int) (($hasEvents/$totalUser)*100) }}% of users created events--}}
                      {{--</span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Attendees</span>
                    <span class="info-box-number">{{ $attendees }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ (int) (($attendees / $totalUser) * 100) }}%"></div>
                    </div>
                      <span class="progress-description">
                        {{ (int) (($attendees / $totalUser) * 100) }}% of users are attendees
                      </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Organizers</span>
                    <span class="info-box-number">{{ $organizers }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ (int) (($organizers / $totalUser) * 100) }}%"></div>
                    </div>
                      <span class="progress-description">
                        {{ (int) (($organizers / $totalUser) * 100) }}% of users are organizers
                      </span>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
   <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js" type="text/javascript"></script>
   <script>
       var ctx = document.getElementById("myChart");
       var myChart = new Chart(ctx, {
           type: 'bar',
           data: {
               labels: JSON.parse('{!! $keys !!}'),
               datasets: [{
                   label: '# of Users added',
                   data: JSON.parse('{!! $data !!}'),
                   backgroundColor: [
                       'rgba(255, 99, 132, 0.8)',
                       'rgba(54, 162, 235, 0.7)',
                       'rgba(255, 206, 86, 0.6)',
                       'rgba(153, 102, 255, 0.5)',
                       'rgba(53, 102, 52, 0.4)',
                       'rgba(54, 153, 86, 0.3)',
                       'rgba(99, 206, 153, 0.2)'
                   ],
                   borderWidth: 1
               }]
           }
       });
   </script>
@stop