@extends('adminlte::page')

@section('title', $header)

@section('content_header')
    <h1>{{ $header }}</h1>
@stop

@section('content')
    <div class='box'>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <table id="items" class='table table-bordered table-striped dataTable compact'>
                        <thead>
                        <tr role="row">
                            <th style="width: 20%;">Name</th>
                            <th style="width: 10%;">Surname</th>
                            <th style="width: 5%;">Email</th>
                            <th style="width: 5%;">Type</th>
                            <th style="width: 5%;">Admin</th>
                            <th style="width: 10%;">Registered</th>
                            <th style="width: 15%;">Verified?</th>
                            <th style="width: 20%;">FB connected?</th>
                            <th style="width: 20%;">Events</th>
                            <th style="width: 15%;">Venues</th>
                            <th style="width: 5%;">Image</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                        <tr role="row">
                            <th style="width: 20%;">Name</th>
                            <th style="width: 10%;">Surname</th>
                            <th style="width: 5%;">Email</th>
                            <th style="width: 5%;">Type</th>
                            <th style="width: 5%;">Admin</th>
                            <th style="width: 10%;">Registered</th>
                            <th style="width: 15%;">Verified?</th>
                            <th style="width: 20%;">FB connected?</th>
                            <th style="width: 20%;">Events</th>
                            <th style="width: 15%;">Venues</th>
                            <th style="width: 5%;">Image</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/u/dt/dt-1.10.12/datatables.min.css"">
@stop

@section('js')
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.12/pagination/input.js"></script>
    <script>
        $(document).ready(function () {
            var table = $('#items').DataTable({
                ajax: '{!! $datUrl !!}',
                processing: true,
                serverSide: true,
                stateSave: true,
                pagingType: "input",
                dom: 'fprtip',
                columns: [
                    {
                        data: 'first_name'
                    },
                    {
                        data: 'last_name'
                    },
                    {
                        data: 'email'
                    },
                    {
                        data: 'type',
                        render: function(data, type, row) {
                            if(parseInt(data) === 1){
                                return 'Organizer';
                            }
                            return "Attendee";
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, row) {
                            return typeof row.admin !== 'undefined' && row.admin === 1;
                        }
                    },
                    {
                        data: 'created_at'
                    },
                    {
                        data: null,//'email_verified'
                        render: function(data, type, row) {
                            if(typeof row.email_verified !== 'undefined'){
                                return row.email_verified;
                            }
                            return false;
                        }
                    },
                    {
                        data: null, //is facebook connected?
                        render: function(data, type, row) {
                            if(typeof row.fb_id !== 'undefined' && row.fb_id !== null && row.fb_id.length > 0){
                                return "ID: " + row.fb_id;
                            }
                            return false;
                        }
                    },
                    {
                        data: 'event_organizer',
                        searchable: false,
                        sortable: true,
                        render: function(data, type, row) {
                            if(typeof data !== "undefined" && data.length > 0) {
                                var value = '';
                                for(var i = 0; i < data.length; i++) {
                                    var event = data[i];
                                    if(event._id && event.title){
                                        var date = new Date(event.start_at);
                                        if(date.valueOf() >= Date.now().valueOf()){
                                            value += createEventButton('/admin/events/', data[i]) + '<br/>';
                                        }
                                    }
                                }
                                return value;
                            }
                            return "N/A";
                        }
                    },
                    {
                        data: 'venue_organizer',
                        searchable: false,
                        sortable: true,
                        render: function(data, type, row) {
                            if(typeof data !== "undefined" && data.length > 0) {
                                var value = '';
                                for(var i = 0; i < data.length; i++) {
                                    var venue = data[i];
                                    if(venue._id && venue.name){
                                        value += createVenueButton('/admin/venues/', data[i]) + '<br/>';
                                    }
                                }
                                return value;
                            }
                            return "N/A";
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, row) {
                            if(typeof row.fb_picture !== "undefined" && row.fb_picture.length > 0){
                                return '<img src="' + row.fb_picture + '" style="max-height: 100px">';
                            }
                            return false;
                        }
                    },
                    {
                        data: '_id',
                        render: function(data, type, row) {
                            var value = '';
                            var iconPrefix = 'fa fa-fw fa-';
                            value += createActionButton('pencil', '/admin/users/', data);
                            value += createActionButton('trash', '/admin/users/delete/', data);
//                            value += createActionButton('circle', '/admin/users/publish/', data);
//                            value += createActionButton('circle-thin', '/admin/users/unpublish/', data);
                            return value;
                        }
                    }
                ]
            });
        });
    function createActionButton(iconClass, url, data) {
        var iconPrefix = 'fa fa-fw fa-';
        iconClass = iconPrefix + iconClass;
        var icon = '<i class="' + iconClass + '"></i>';
        url = url + data;
        return '<a href="'+ url +'">' + icon + '</a>';
    }

    function createEventButton(url, data) {
        return createObjectButton(url, data, data.title);
    }

    function createVenueButton(url, data) {
       return createObjectButton(url, data, data.name);
    }

    function createObjectButton(url, data, title) {
        var iconPrefix = 'fa fa-fw fa-';
        var iconClass = iconPrefix + 'pencil';
        var icon = '<i class="' + iconClass + '"></i>';
        url = url + data._id;
        return '<a href="'+ url +'">' + icon + title + '</a>';
    }

    </script>
@stop