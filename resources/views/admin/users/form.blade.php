@extends('adminlte::page')

@section('title', 'Create venue')

@section('content_header')
    @if($user->exists)
        <h1>Edit {{ $user->email }}</h1>
    @else
        <h1>Create User</h1>
    @endif
@stop

@section('content')
    <div class='box'>
        <div class="box-header"></div>
        <div class="box-body">
            <form role="form" method="post" enctype="multipart/form-data" action="{{route('admin.users.store')}}">
                @if($user->exists)
                    <input type="hidden" name="_id" value="{{$user->_id}}">
                @endif
                <div class="row">
                    <div class="col-md-2 form-group">
                        @if($user->fb_picture)
                            <img src="{{$user->fb_picture}}">
                        @endif
                    </div>
                    <div class="col-md-3 form-group">
                        <label>
                            Type
                        <select name="type" class="form-control">
                            @if(intval($user->type) === 1)
                                <option value="0">Attendee</option>
                                <option value="1" selected="selected">Organizer</option>
                            @else
                                <option value="0" selected="selected">Attendee</option>
                                <option value="1">Organizer</option>
                            @endif
                        </select>
                        </label>
                    </div>
                    <div class="col-md-4 form-group">
                        <label>
                            Facebook ID
                            <input class="form-control" type="text" name="fb_id" value="{{$user->fb_id}}">
                        </label>
                    </div>

                    <div class="col-md-3 form-group">
                        <label>
                            Admin
                            @if(intval($user->admin) === 1)
                                <input type="checkbox" checked="checked" name="admin" value="1">
                            @else
                                <input class="checkbox" type="checkbox" name="admin" value="1">
                            @endif
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 form-group">
                        <label> First Name
                            <input class="form-control" type="text" name="first_name" value="{{$user->first_name}}" required>
                        </label>
                    </div>
                    <div class="col-md-3 form-group">
                        <label> Last Name
                            <input class="form-control" type="text" name="last_name" value="{{$user->last_name}}" required>
                        </label>
                    </div>
                    <div class="col-md-3 form-group">
                        <label> Organization
                            <input class="form-control" type="text" name="organization" value="{{$user->organization}}" required>
                        </label>
                    </div>
                    <div class="col-md-3 form-group">
                        <label> Email
                            <input class="form-control" type="email" name="email" value="{{$user->email}}" required>
                        </label>
                    </div>

                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-danger">Cancel</button>
                </div>
            </form>
        </div>
    </div>
@stop

@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="//venvast3.scdn4.secure.raxcdn.com/css/jquery.datetimepicker.css">
    <link rel="stylesheet" href="https://almsaeedstudio.com/themes/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css">
@stop

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script src="//maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyBfnCaACfxQD5YOCC7w1O2ItMIonYxLkXE"></script>
    <script src="/js/bootstrap-timepicker.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#category').select2();
            $('#organizer').select2();
            initMap();



        });

        function initMap() {
            var mapElement = (document.getElementById('map'));/*new google.maps.Map($('#map'), {
//                center: {lat: -33.8688, lng: 151.2195},
//                zoom: 13
//            });*/


            var input = (document.getElementById('address'));

            var autocomplete = new google.maps.places.Autocomplete(input);
            var map = new google.maps.Map(mapElement, {
                center: new google.maps.LatLng(51.508742,-0.120850),
                zoom:5,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });

            var lat = $('#lat');
            var lng = $('#lng');

            lng.on('change', function() {
                if(lng.val() && lat.val()) {
                    var center =  new google.maps.LatLng(lat.val(), lng.val());
                    map.setCenter(center);
                }
            });
            lat.on('change', function () {
                if(lng.val() && lat.val()) {
                    var center =  new google.maps.LatLng(lat.val(), lng.val());
                    map.setCenter(center);
                }
            });

            autocomplete.addListener('place_changed', function() {
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                lat.val(place.geometry.location.lat());
                lng.val(place.geometry.location.lng());
                marker.setIcon(/** @type {google.maps.Icon} */({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                input.val(address);

            });
        }
    </script>
@stop
