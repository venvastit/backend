@extends('adminlte::page')

@section('title', 'Events')

@section('content_header')
    @if($event->exists)
        <h1>Edit {{ $event->title }}</h1>
    @else
        <h1>Create event</h1>
    @endif
@stop

@section('content')
    <div class='box'>
        <div class="box-header"></div>
        <div class="box-body">
            <form role="form" method="post" enctype="multipart/form-data" action="{{route('admin.events.store')}}">
                <!-- text input -->
                @if($event->exists)
                    <input type="hidden" name="_id" value="{{ $event->_id }}">
                @endif
                <div class="form-group col-lg-6">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Event title" name="title" value="{{$event->title}}">
                </div>

                <div class="form-group col-lg-6">
                    <label>Category</label>
                    <select class="form-control" name="categories[]" id="category">
                        <option></option>
                        @foreach($categories as $category)
                            @if(count($event->categories) > 0 && $category->_id == $event->categories[0]->_id)
                                <option selected="selected" value="{{$category->_id}}">{{$category->name}}</option>
                            @else
                                <option value="{{$category->_id}}">{{$category->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div style="clear: both"></div>
                <div class="form-group col-lg-12">
                    <label>Description</label>
                    <textarea class="form-control" name="details" rows="12" placeholder="Description">{{ $event->details }}</textarea>
                </div>

                <div class="form-group col-lg-4">
                    <label>Venue</label>
                    <select class="form-control" id="venue">
                        <option></option>
                        @foreach($venues as $venue)
                            @if($venue->_id == $event->venue_id)
                                <option selected="selected" value="{{$venue->_id}}">{{$venue->name}}, {{$venue->zip}}</option>
                            @else
                                <option value="{{$venue->_id}}">{{$venue->name}}, {{$venue->zip}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-4">
                    <label>Start At</label>
                    <input type="text" id="startAt" value="{{$event->start_at ? $event->start_at->format('Y.d.m H:i') : null}}" name="start_at" class="form-control" placeholder="2016.22.10 18:00">
                </div>
                <div class="form-group col-lg-4">
                    <label>End At</label>
                    <input type="text" id="endAt" value="{{$event->end_at ? $event->end_at->format('Y.d.m H:i') : null}}" name="end_at" class="form-control" placeholder="2016.22.10 19:30">
                </div>
                <div style="clear: both"></div>
                <div class="form-group col-lg-4">
                    <label>Organizer</label>
                    <select class="form-control" name="organizers[]" id="organizer">
                        <option></option>
                        @foreach($users as $user)
                            <option {{ $user->_id === $venue->organizers[0] ? 'selected="selected"' : '' }}value="{{$user->_id}}">{{$user->email}}, {{$user->organization}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-4" style="margin-top: 2%">
                    <div class="checkbox">
                        <label>
                            @if(booleanValue($event->published))
                                <input type="checkbox" name="published" checked>
                            @else
                                <input type="checkbox">
                            @endif
                                Published
                        </label>
                    </div>
                </div>
                <div style="clear: both"></div>
                @if($event->picture && $event->picture->__toString())
                    <div class="col-lg-6">
                        <img src="{!! $event->picture->thumbnail('original') ?: $event->picture !!}" style="max-height: 300px;">
                    </div>
                    <div style="clear: both"></div>
                @endif
                <div class="form-group col-lg-6">
                    <input type="file" class="file" name="picture" accept="image/*" />
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-danger">Cancel</button>
                </div>
            </form>
        </div>
    </div>
@stop

@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="//venvast3.scdn4.secure.raxcdn.com/css/jquery.datetimepicker.css">
@stop

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script src="//venvast3.scdn4.secure.raxcdn.com/js/jquery.datetimepicker.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#startAt').datetimepicker({
                format: "Y.d.m H:i",
                startDate: Date.now(),
                minDate: Date.now()
            });
            $('#endAt').datetimepicker({
                format: "Y.d.m H:i",
                startDate: Date.now(),
                minDate: Date.now()
            });
            $('#category').select2();
            $('#venue').select2();
            $('#organizer').select2();
        });
    </script>
@stop
