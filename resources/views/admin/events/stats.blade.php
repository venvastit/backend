@extends('adminlte::page')

@section('title', 'Events')

@section('content_header')
    <h1>Event stats</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class='box'>
                <div class="box-header with-border">
                    <h3 class="box-title">Events Created in system by date</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-circle-thin"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Unpublished events</span>
                    <span class="info-box-number">{{ $unpublished }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $unpublishedOfTotal }}%"></div>
                    </div>
                      <span class="progress-description">
                        {{$unpublishedOfTotal}}% of events unpublished
                      </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-circle-thin"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Uncategorized events</span>
                    <span class="info-box-number">{{ $uncategorized }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $uncategorizedOfTotal }}%"></div>
                    </div>
                      <span class="progress-description">
                        {{$uncategorizedOfTotal}}% of events are uncategorized
                      </span>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
   <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js" type="text/javascript"></script>
   <script>
       var ctx = document.getElementById("myChart");
       var myChart = new Chart(ctx, {
           type: 'bar',
           data: {
               labels: JSON.parse('{!! $keys !!}'),
               datasets: [{
                   label: '# of Events added',
                   data: JSON.parse('{!! $data !!}'),
                   backgroundColor: [
                       'rgba(255, 99, 132, 0.8)',
                       'rgba(54, 162, 235, 0.7)',
                       'rgba(255, 206, 86, 0.6)',
                       'rgba(153, 102, 255, 0.5)',
                       'rgba(53, 102, 52, 0.4)',
                       'rgba(54, 153, 86, 0.3)',
                       'rgba(99, 206, 153, 0.2)'
                   ],
                   borderWidth: 1
               },
                   {
                       label: '# of Events updated',
                       data: JSON.parse('{!! $updates !!}'),
                       backgroundColor: [
                           'rgba(99, 206, 153, 0.8)',
                           'rgba(255, 206, 86, 0.7)',
                           'rgba(255, 99, 132, 0.6)',
                           'rgba(53, 102, 52, 0.5)',
                           'rgba(54, 162, 235, 0.4)',
                           'rgba(153, 102, 255, 0.3)',
                           'rgba(54, 153, 86, 0.2)'
                       ],
                       borderWidth: 1
                   }
               ]
           }
       });
   </script>
@stop