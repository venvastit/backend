@extends('adminlte::page')

@section('title', 'Events')

@section('content_header')
    <h1>List of all events</h1>
@stop

@section('content')
    <div class='box'>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <table id="items" class='table table-bordered table-striped dataTable'>
                        <thead>
                        <tr role="row">
                            <th style="width: 20%;">Title</th>
                            <th style="width: 5%;">Published</th>
                            <th style="width: 15%;">Start</th>
                            <th style="width: 15%;">End</th>
                            <th style="width: 25%;">Venue</th>
                            <th style="width: 25%;">Address</th>
                            <th style="width: 20%;">Categories</th>
                            <th style="width: 10%;">Image</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                        <tr>
                            <th>Title</th>
                            <th>Published</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Venue</th>
                            <th>Address</th>
                            <th>Categories</th>
                            <th>Image</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/u/dt/dt-1.10.12/datatables.min.css"">
@stop

@section('js')
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.12/pagination/input.js"></script>
    <script>
        $(document).ready(function () {
            var table = $('#items').DataTable({
                ajax: '{!! $datUrl !!}',
                processing: true,
                stateSave: true,
                serverSide: true,
                pagingType: "input",
                dom: 'fprtip',
                columns: [
                    {
                        data: 'title',
                        sortable: true
                    },
                    {
                        data: 'published',
                        searchable: false,
                        sortable: true,
                        render: function(data, type, row) {
                            if(typeof data === 'undefined' || data === null)
                                return 'No';
                            return data;
                        }
                    },
                    {
                        data: 'start_at',
                        sortable: true
                    },
                    {
                        data: 'end_at',
                        sortable: true
                    },
                    {
                        data: '_id', //venue_relation
                        render: function(data, type, row) {
                            data = row.venue_relation;
                            if(typeof data !== "undefined" && data !== null && data.name.length > 0) {
                                var value = createVenueButton('/admin/venues/', data) + '<br/>';
                                return value;
                            }
                            return "N/A";
                        }
                    },
                    {
                        data: '_id', //address
                        sortable: true,
                        render: function (data, type, row) {
                            return row.address;
                        }
                    },
                    {
                        data: 'categories',
                        searchable: false,
                        sortable: true,
                        render: function(data, type, row) {
                            var value = '';
                            if(typeof data === 'undefined')
                                    return value;
                            for(var i=0; i< data.length; i++) {
                                if(data[i] === null || typeof data[i] === "undefined")
                                        continue;
                                var imgPath = 'https://venvast.com/img/cache/original/categories/' + data[i].image;
                                value += '<img style="width: 32px; height: 32px" src="' + imgPath + '">' + data[i].name;
                                if(i !== data.length -1) {
                                    value += ', ';
                                }
                            }
                            return value;
                        }
                    },
                    {
                        data: 'picture',
                        searchable: false,
                        sortable: false,
                        render: function(data, type, row) {

                            var url;
                            if(data === null || typeof data === 'undefined' || data === ''){
                                url = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=' + this.title + '&w=350&h=150';
                            } else {
                                if(typeof data.startsWith !== 'function' || !data.startsWith('http')) {
                                    url = '/img/cache/original/events/' + data;
                                } else {
                                    url = data;
                                }
                            }
                            return '<img style="width: 100px;" src="'+ url +'">'
                        }
                    },
                    {
                        data: '_id',
                        render: function(data, type, row) {
                            var value = '';
                            var iconPrefix = 'fa fa-fw fa-';
                            value += createActionButton('pencil', '/admin/events/', data);
                            value += createActionButton('trash', '/admin/events/delete/', data);
                            value += createActionButton('circle', '/admin/events/publish/', data);
                            value += createActionButton('circle-thin', '/admin/events/unpublish/', data);
                            return value;
                        }
                    }
                ]
            });
        });
        function createActionButton(iconClass, url, data) {
            var iconPrefix = 'fa fa-fw fa-';
            iconClass = iconPrefix + iconClass;
            var icon = '<i class="' + iconClass + '"></i>';
            url = url + data;
            return '<a href="'+ url +'">' + icon + '</a>';
        }

        function createEventButton(url, data) {
            return createObjectButton(url, data, data.title);
        }

        function createVenueButton(url, data) {
            return createObjectButton(url, data, data.name);
        }

        function createObjectButton(url, data, title) {
            var iconPrefix = 'fa fa-fw fa-';
            var iconClass = iconPrefix + 'pencil';
            var icon = '<i class="' + iconClass + '"></i>';
            url = url + data._id;
            return '<a href="'+ url +'">' + icon + title + '</a>';
        }

    </script>
@stop