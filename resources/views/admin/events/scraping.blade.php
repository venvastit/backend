@extends('adminlte::page')

@section('title', 'Scrap Events')

@section('content_header')
        <h1>Scrap events</h1>
@stop

@section('content')
    <div class="row">
        <div class='box'>
            <div class="box-header"></div>
            <div class="box-body">
                <div class="col-md-12">
                    <a href="{{ $login_url }}">Get fb access_token</a>
                    <p>Access token: {{Session::get('fb_user_access_token')}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class='box'>
                <div class="box-header">Enter facebook IDs separated by commas: ex(1234475757, 2737475894, 28283849)</div>
                <div class="box-body">
                    <div class="col-md-12">
                        <form role="form" method="post" action="{{route('admin.events.scraping.text')}}">
                            <textarea class="form-control" name="idList"></textarea>
                            <button type="submit" class="btn btn-success" {{ !Session::get('fb_user_access_token') ? 'disabled="disabled"' : ''  }}><i class="fa fa-download"></i>Scrap!</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class='box'>
                <div class="box-header">Additional</div>
                <div class="box-body">
                    <div class="col-md-12">
                        <a class="btn btn-large btn-danger" {{ !Session::get('fb_user_access_token') ? 'disabled="disabled"' : ''  }} href="{{route('admin.events.passed.remove')}}"><i class="fa fa-trash"></i>Remove passed</a>
                        <a class="btn btn-large btn-warning"  {{ !Session::get('fb_user_access_token') ? 'disabled="disabled"' : ''  }} href="{{route('admin.events.scraping.existing')}}"><i class="fa fa-wrench"></i>Scrap existing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
