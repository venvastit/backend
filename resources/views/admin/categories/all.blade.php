@extends('adminlte::page')

@section('title', 'Categories')

@section('content_header')
    <h1>{{ $header }}</h1>
@stop

@section('content')
    <div class='box'>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <table id="items" class='table table-bordered table-striped dataTable'>
                        <thead>
                        <tr role="row">
                            <th style="width: 25%;">Name</th>
                            <th style="width: 15%;">Source</th>
                            <th style="width: 15%;">Type</th>
                            <th style="width: 15%;">Priority</th>
                            <th style="width: 20%;">Image</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                        <tr role="row">
                            <th style="width: 25%;">Name</th>
                            <th style="width: 15%;">Source</th>
                            <th style="width: 15%;">Type</th>
                            <th style="width: 15%;">Priority</th>
                            <th style="width: 20%;">Image</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/u/dt/dt-1.10.12/datatables.min.css"">
@stop

@section('js')
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.12/pagination/input.js"></script>
    <script>
        $(document).ready(function () {
            var table = $('#items').DataTable({
                ajax: '{!! $dataUrl !!}',
                processing: true,
                stateSave: true,
                serverSide: true,
                pagingType: "input",
                dom: 'fprtip',
                columns: [
                    {
                        data: 'name',
                        sortable: true
                    },
                    {
                        data: 'source',
                        render: function(data, type, row) {
                            if(data !== null && typeof data !== 'undefined') {
                                return data;
                            } else {
                                return "VenVast";
                            }
                        }
                    },
                    {
                        data: 'type',
                        render: function(data, type, row) {
                            if(data !== null && typeof data !== 'undefined') {
                                return data;
                            } else {
                                return "External";
                            }
                        }
                    },
                    {
                       data: 'priority',
                        render: function(data, type, row) {
                            if(typeof data !== "undefined" && data !== null) {
                                return data;
                            }
                            return "undefined";
                        }
                    },
                    {
                        data: 'image',
                        searchable: false,
                        sortable: false,
                        render: function(data, type, row) {

                            var url;
                            if(data === null || typeof data === 'undefined' || data === ''){
                                url = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=' + this.title + '&w=350&h=150';
                            } else {
                                if(!data.startsWith('http')) {
                                    url = '/img/cache/original/categories/' + data;
                                } else {
                                    url = data;
                                }
                            }
                            return '<img style="width: 100px;" src="'+ url +'">'
                        }
                    },
                    {
                        data: '_id',
                        render: function(data, type, row) {
                            var value = '';
                            var iconPrefix = 'fa fa-fw fa-';
                            value += createActionButton('pencil', '/admin/categories/', data);
                            value += createActionButton('trash', '/admin/categories/delete/', data);
                            return value;
                        }
                    }
                ]
            });
        });
        function createActionButton(iconClass, url, data) {
            var iconPrefix = 'fa fa-fw fa-';
            iconClass = iconPrefix + iconClass;
            var icon = '<i class="' + iconClass + '"></i>';
            url = url + data;
            return '<a href="'+ url +'">' + icon + '</a>';
        }
    </script>
@stop