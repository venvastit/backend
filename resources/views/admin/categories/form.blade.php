@extends('adminlte::page')

@section('title', 'Categories')

@section('content_header')
    @if($category->exists)
        <h1>Edit {{ $category->name }}</h1>
    @else
        <h1>Create category</h1>
    @endif
@stop

@section('content')
    <div class='box'>
        <div class="box-header"></div>
        <div class="box-body">
            <form role="form" method="post" enctype="multipart/form-data" action="{{route('admin.categories.store')}}">
                <!-- text input -->
                @if($category->exists)
                    <input type="hidden" name="_id" value="{{ $category->_id }}">
                @endif
                <div class="form-group col-lg-4">
                    <label>Name</label>
                    <input type="text" class="form-control" placeholder="Category name" name="name" value="{{$category->name}}" required>
                </div>

                <div class="form-group col-lg-4">
                    <label>Type</label>
                    <select class="form-control" name="type" id="type" required>
                        <option></option>
                        @foreach($types as $key => $value)
                            @if($category->type == $key)
                                <option selected="selected" value="{{$value}}">{{$key}}</option>
                            @else
                                <option value="{{$value}}">{{$key}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-4">
                    <label>Source</label>
                    <select class="form-control" name="source" id="source" required>
                        <option></option>
                        @foreach($sources as $key => $value)
                            @if($category->source == $key)
                                <option selected="selected" value="{{$value}}">{{$key}}</option>
                            @else
                                <option value="{{$value}}">{{$key}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-6">
                    <label>Related VenVast Category</label>
                    <select class="form-control" name="related_id" id="related_id">
                        <option></option>
                        @foreach($venvastCategories as $venvastCategory)
                            @if($category->related_id == $venvastCategory->_id)
                                <option selected="selected" value="{{$venvastCategory->_id}}">{{$venvastCategory->name}}</option>
                            @else
                                <option value="{{$venvastCategory->_id}}">{{$venvastCategory->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div style="clear: both"></div>

                <div class="form-group col-lg-6">
                    <label>Priority</label>
                    <input type="number" class="form-control" placeholder="0" name="priority" value="{{$category->priority}}" required>
                </div>
            @if($category->image && $category->image->__toString())
                    <div class="col-lg-6">
                        <img src="{!! $category->image->thumbnail('original') ?: $category->image !!}" style="max-height: 300px;">
                    </div>
                    <div style="clear: both"></div>
                @endif
                <div class="form-group col-lg-6">
                    <input type="file" class="file" name="image" accept="image/*" />
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-danger">Cancel</button>
                </div>
            </form>
        </div>
    </div>
@stop

@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
@stop

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script src="//venvast3.scdn4.secure.raxcdn.com/js/jquery.datetimepicker.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#source').select2();
            $('#type').select2();
            $('#related_id').select2();
        });
    </script>
@stop
