@extends('adminlte::page')

@section('title', $header)

@section('content_header')
    <h1>{{ $header }}</h1>
@stop

@section('content')
    <div class='box'>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <table id="items" class='table table-bordered table-striped dataTable compact'>
                        <thead>
                        <tr role="row">
                            <th style="width: 20%;">Name</th>
                            <th style="width: 5%;">Google</th>
                            <th style="width: 10%;">Email</th>
                            <th style="width: 5%;">Contacted?</th>
                            <th style="width: 5%;">Published</th>
                            <th style="width: 10%;">Created</th>
                            <th style="width: 15%;">Organizers</th>
                            {{--<th style="width: 5%;">Upcoming</th>--}}
                            <th style="width: 20%;">Upcoming events</th>
                            <th style="width: 20%;">Address</th>
                            <th style="width: 15%;">Categories</th>
                            <th style="width: 5%;">Image</th>

                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                        <tr role="row">
                            <th style="width: 20%;">Name</th>
                            <th style="width: 5%;">Google</th>
                            <th style="width: 10%;">Email</th>
                            <th style="width: 5%;">Contacted?</th>
                            <th style="width: 5%;">Published</th>
                            <th style="width: 10%;">Created</th>
                            <th style="width: 15%;">Organizers</th>
                            {{--<th style="width: 5%;">Upcoming</th>--}}
                            <th style="width: 20%;">Upcoming events</th>
                            <th style="width: 20%;">Address</th>
                            <th style="width: 15%;">Categories</th>
                            <th style="width: 5%;">Image</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/u/dt/dt-1.10.12/datatables.min.css"">
@stop

@section('js')
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.12/pagination/input.js"></script>
    <script>
        $(document).ready(function () {
            var table = $('#items').DataTable({
                ajax: '{!! $datUrl !!}',
                processing: true,
                serverSide: true,
                stateSave: true,
                pagingType: "input",
                dom: 'fprtip',
                columns: [
                    {
                        data: 'name',
                        sortable: true
                    },
                    {
                        data: 'google_place_id',
                        sortable: true,
                        render: function(data, type, row) {
                            return !(typeof data === "undefined" || data === null);
                        }
                    },
                    {
                        data: null, //email
                        searchable: false,
                        sortable: false,
                        render: function(data, type, row) {
                            if(typeof row.email === 'undefined') {
                                return false;
                            }
                            else {
                                console.log("Email: ", row.email);
                                return row.email;
                            }
                        }
                    },
                    {
                        data: null, //contacted
                        searchable: false,
                        sortable: false,
                        render: function(data, type, row) {
                            if(typeof row.contacted === 'undefined'){
                                return false;
                            }
                            else {
                                return row.contacted;
                            }
                        }
                    },
                    {
                        data: 'published',
                        searchable: false,
                        sortable: true,
                        render: function(data, type, row) {
                            if(typeof data === 'undefined' || data === null)
                                return 'No';
                            return data;
                        }
                    },
                    {
                        data: 'created_at',
                        sortable: true,
                        render: function(data, type, row) {
                            if(typeof data !== "undefined") {
                                return data;
                            }
                            return "N/A";
                        }
                    },
                    {
                        data: 'organizers',
                        searchable: false,
                        sortable: true,
                        render: function(data, type, row) {
                            if(typeof data !== "undefined") {
                                return data.length;
                            }
                            return "N/A";
                        }
                    },
                    {
                        data: 'events',
                        searchable: false,
                        sortable: true,
                        render: function(data, type, row) {
                            if(typeof data !== "undefined" && data.length > 0) {
                                var value = '';
                                for(var i = 0; i < data.length; i++) {
                                    var event = data[i];
                                    if(event._id && event.title){
                                        var date = new Date(event.start_at);
                                        if(date.valueOf() >= Date.now().valueOf()){
                                            value += createEventButton('/admin/events/', data[i]) + '<br/>';
                                        }
                                    }
                                }
                                return value;
                            }
                            return "N/A";
                        }
                    },
                    {
                        data: 'street',
                        sortable: true
                    },
                    {
                        data: 'categories',
                        searchable: false,
                        sortable: true,
                        render: function(data, type, row) {

                            var value = '';
                            if(typeof data === 'undefined' || typeof data.length === 'undefined' || data.length === 0 || data[0] === null) {
                                return 'undefined';
                            }
                                for(var i=0; i< data.length; i++) {
                                    if(typeof data[i] !== "undefined" && data[i] !== null){
                                        var imgPath = 'https://venvast.com/img/cache/original/categories/' + data[i].image;
                                        value += '<img style="width: 32px; height: 32px" src="' + imgPath + '">' + data[i].name;
                                        if(i !== data.length -1) {
                                            value += ', ';
                                        }
                                    }
                                }
                            return value;
                        }
                    },
                    {
                        data: 'cover_image',
                        searchable: false,
                        sortable: false,
                        render: function(data, type, row) {
                            var url;
                            if(data === null || typeof data === 'undefined' || data === ''){
                                url = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=' + row.name + '&w=350&h=150';
                            } else {
                                if(!data.startsWith('http')) {
                                    url = '/img/cache/original/venues/' + data;
                                } else {
                                    url = data;
                                }
                            }
                            return '<img style="width: 100px;" src="'+ url +'">'
                        }
                    },
                    {
                        data: '_id',
                        render: function(data, type, row) {
                            var value = '';
                            var iconPrefix = 'fa fa-fw fa-';
                            value += createActionButton('pencil', '/admin/venues/', data);
                            value += createActionButton('trash', '/admin/venues/delete/', data);
                            value += createActionButton('circle', '/admin/venues/publish/', data);
                            value += createActionButton('circle-thin', '/admin/venues/unpublish/', data);
                            return value;
                        }
                    }
                ]
            });
        });
    function createActionButton(iconClass, url, data) {
        var iconPrefix = 'fa fa-fw fa-';
        iconClass = iconPrefix + iconClass;
        var icon = '<i class="' + iconClass + '"></i>';
        url = url + data;
        return '<a href="'+ url +'">' + icon + '</a>';
    }

    function createEventButton(url, data) {
        var iconPrefix = 'fa fa-fw fa-';
        var iconClass = iconPrefix + 'pencil';
        var icon = '<i class="' + iconClass + '"></i>';
        url = url + data._id;
        return '<a href="'+ url +'">' + icon + data.title + '</a>';
    }

    </script>
@stop