@extends('adminlte::page')

@section('title', 'Create venue')

@section('content_header')
    @if($venue->exists)
        <h1>Edit {{ $venue->name }}</h1>
    @else
        <h1>Create venue</h1>
    @endif
@stop

@section('content')
    <div class='box'>
        <div class="box-header"></div>
        <div class="box-body">
            <form role="form" method="post" action="{{route('admin.venues.store')}}" enctype="multipart/form-data">
            @if($venue->exists)
                <input type="hidden" name="_id" value="{{ $venue->_id }}">
            @endif
                <!-- text input -->
                <div class="form-group col-lg-6">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Venue title" name="name" value="{{$venue->name}}">
                </div>

                <div class="form-group col-lg-6">
                    <label>Category</label>
                    <select name="categories[]" class="form-control" id="category">
                        <option></option>
                        @foreach($categories as $category)
                            @if(count($venue->categories) > 0 && $category->_id == $venue->categories[0]->_id)
                                <option selected="selected" value="{{$category->_id}}">{{$category->name}}</option>
                            @else
                                <option value="{{$category->_id}}">{{$category->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div style="clear: both;"></div>
                <div class="form-group col-lg-3">
                    <label for="google_place_id">Google id</label>
                    <input class="form-control" name="google_place_id" type="text" id="google_place_id" value="{{ $venue->google_place_id }}">
                </div>
                <div class="form-group col-lg-3">
                    <label for="email">Email</label>
                    <input class="form-control" type="email" name="email" id="email" value="{{ $venue->email }}">
                </div>
                <div class="form-group col-lg-3">
                    <label for="website">Website</label>
                    <input class="form-control" name="website" type="url" id="website" value="{{ $venue->website }}">
                </div>
                <div class="form-group col-lg-3">
                    <label for="phone">Phone</label>
                    <input class="form-control" name="phone" type="tel" id="phone" value="{{ $venue->phone }}">
                </div>
                <div class="form-group col-md-12 col-sm-6" style="min-height: 300px">
                    <div class="col-md-4">
                        <label for="address">Address</label>
                        <input class="form-control" name="address" id="address" value="{{ $venue->address }}" placeholder="Search">
                    </div>
                    <div class="col-md-8">
                        <label>Map</label>
                        <div class="field-map" id="map" style="height:380px;"></div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <input type="hidden" name="location[type]" value="Point">
                    <div class="col-md-6">
                        <label>Latitude</label>
                        <input class="form-control" value="{{ isset($position) ? $position[1] : null }}" name="location[coordinates][1]" id="lat">
                    </div>
                    <div class="col-md-6">
                        <label>Longitude</label>
                        <input class="form-control" value="{{ isset($position) ? $position[0] : null }}" name="location[coordinates][0]" id="lng">
                    </div>
                </div>
                @foreach($businessHours as $hour)
                    <div class="form-group col-md-6">
                        <div class="col-md-2">
                                <label>
                                    @if(is_array($hour['open']))

                                        <?php if (!array_key_exists('day', $hour) && array_key_exists('day', $hour['open'])) {
    $hour['day'] = $hour['open']['day'];
} ?>
                                        <input type="checkbox"
                                               name="businessHours[]"
                                               checked
                                               value="{{  $hour['day'] }}"
                                        > {{  $dayLabels[$hour['day']] }}
                                    @else
                                        <input type="checkbox"
                                               name="businessHours[]"
                                               value="{{  $hour['day'] }}"
                                        > {{  $dayLabels[$hour['day']] }}
                                    @endif
                                </label>
                        </div>
                        <div class="col-md-5">
                            @if(array_key_exists('open',$hour) && is_array($hour['open']))
                                <label>Open</label>
                                <input type="text" maxlength="4" size="4" class="form-control timepicker" value="{{$hour['open']['time']}}" name="businessHoursOpen[]">
                            @else
                                <label>Open</label>
                                <input type="text" maxlength="4" size="4" class="form-control timepicker" name="businessHoursOpen[]">
                            @endif
                        </div>
                        <div class="col-md-5">
                            @if(array_key_exists('close',$hour) && is_array($hour['close']))
                                <label>Close</label>
                                <input type="text" maxlength="4" size="4" class="form-control" value="{{$hour['close']['time']}}" name="businessHoursClose[]">
                            @else
                                <label>Close</label>
                                <input type="text" maxlength="4" size="4" class="form-control" name="businessHoursClose[]">
                            @endif
                        </div>
                    </div>
                @endforeach
                <div class="form-group col-md-12">
                    <label>Description</label>
                    <textarea name="description" class="form-control" rows="12" placeholder="Description">{{ $venue->description }}</textarea>
                </div>
                <div class="form-group col-md-12">
                    <label>Public transit</label>
                    <textarea class="form-control" rows="3" name="public_transit">{{$venue->public_transit}}</textarea>
                </div>
                    <div class="form-gorup col-md-4">
                        <label for="hostEvents">Host?</label>
                        @if($venue->hostEvents)
                            <input type="checkbox" checked="checked" class="form-checkbox" name="hostEvents" id="hostEvents">
                        @else
                            <input type="checkbox" class="form-checkbox" name="hostEvents" id="hostEvents">
                        @endif
                    </div>
                    <div class="form-group col-md-12">
                        @foreach($eventCategories as $category)
                            <div class="col-md-3">
                                <label>
                                    <input
                                            <?php
                                                foreach ($venue->eventCategories as $eventCategory) {
                                                    if ($eventCategory->_id === $category->_id) {
                                                        echo 'checked="checked"';
                                                    }
                                                }
                                            ?>
                                           type="checkbox"
                                           name="eventCategories[]"
                                           value="{{ $category->_id }}"
                                    >{{$category->name}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group col-md-12">
                        <label>Host announcement</label>
                        <br/>
                        <textarea name="hostEventsInfo" class="form-control" rows="6">{{$venue->hostEventsInfo}}</textarea>
                    </div>
                <div style="clear: both"></div>
                <div class="form-group col-lg-4">
                    <label>Organizer</label>
                    <select class="form-control" id="organizer" name="organizers[]">
                        <option></option>
                        @foreach($users as $user)
                            @if(count($venue->organizers) > 0 && $user->_id === $venue->organizers[0])
                                <option selected="selected" value="{{$user->_id}}">{{$user->email}}, {{$user->organization}}</option>
                            @else
                                <option value="{{$user->_id}}">{{$user->email}}, {{$user->organization}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-4" style="margin-top: 2%">
                    <div class="checkbox">
                        <label>
                            @if($venue->published)
                                <input name="published" type="checkbox" checked>
                            @else
                                <input name="published" type="checkbox">
                            @endif
                                Published
                        </label>
                    </div>
                </div>
                <div class="form-group col-lg-4" style="margin-top: 2%">
                    <div class="checkbox">
                        <label>
                            @if($venue->contacted)
                                <input name="contacted" type="checkbox" checked>
                            @else
                                <input name="contacted" type="checkbox">
                            @endif
                            Contacted
                        </label>
                    </div>
                </div>
                <div style="clear: both"></div>
                @if($venue->cover_image && $venue->cover_image->__toString())
                    <div class="col-lg-6">
                        <img src="{!! $venue->cover_image->thumbnail('original') ?: $venue->cover_image !!}" style="max-height: 300px;">
                    </div>
                    <div style="clear: both"></div>
                @endif
                <div class="form-group col-lg-6">
                    <input type="file" class="file" name="cover_image" accept="image/*" />
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-danger">Cancel</button>
                </div>
            </form>
        </div>
    </div>
@stop

@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="//venvast3.scdn4.secure.raxcdn.com/css/jquery.datetimepicker.css">
    <link rel="stylesheet" href="https://almsaeedstudio.com/themes/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css">
@stop

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script src="//maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyBfnCaACfxQD5YOCC7w1O2ItMIonYxLkXE"></script>
    <script src="/js/bootstrap-timepicker.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#category').select2();
            $('#organizer').select2();
            initMap();



        });

        function initMap() {
            var mapElement = (document.getElementById('map'));/*new google.maps.Map($('#map'), {
//                center: {lat: -33.8688, lng: 151.2195},
//                zoom: 13
//            });*/


            var input = (document.getElementById('address'));

            var autocomplete = new google.maps.places.Autocomplete(input);
            var position = new google.maps.LatLng(parseFloat('{!! isset($position) ? $position[1] : 51.508742 !!}}'), parseFloat('{!! isset($position) ? $position[0] : -0.120850 !!}}'));
            var map = new google.maps.Map(mapElement, {
                center: position,
                zoom:17,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });

            marker.setIcon(/** @type {google.maps.Icon} */({
//                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));
            marker.setPosition(position);
            marker.setVisible(true);

            var lat = $('#lat');
            var lng = $('#lng');

            lng.on('change', function() {
                if(lng.val() && lat.val()) {
                    var center =  new google.maps.LatLng(lat.val(), lng.val());
                    map.setCenter(center);
                }
            });
            lat.on('change', function () {
                if(lng.val() && lat.val()) {
                    var center =  new google.maps.LatLng(lat.val(), lng.val());
                    map.setCenter(center);
                }
            });

            autocomplete.addListener('place_changed', function() {
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                lat.val(place.geometry.location.lat());
                lng.val(place.geometry.location.lng());
                marker.setIcon(/** @type {google.maps.Icon} */({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                input.val(address);

            });
        }
    </script>
@stop
