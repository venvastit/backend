@extends('adminlte::page')

@section('title', 'Venue stats')

@section('content_header')
    <h1>Venue stats</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class='box'>
                <div class="box-header with-border">
                    <h3 class="box-title">Venues created&updated in system in last 7 days</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-building"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Venues Total</span>
                    <span class="info-box-number">{{ $total }}</span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-circle-thin"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Unpublished venues</span>
                    <span class="info-box-number">{{ $unpublished }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $unpublishedPercent }}%"></div>
                    </div>
                      <span class="progress-description">
                        {{$unpublishedPercent}}% of venues unpublished
                      </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-circle"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Published venues</span>
                    <span class="info-box-number">{{ $published }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $publishedPercent }}%"></div>
                    </div>
                      <span class="progress-description">
                        {{$publishedPercent}}% of venues published
                      </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">With upcoming events</span>
                    <span class="info-box-number">{{ $upcoming }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $upcomingPercent }}%"></div>
                    </div>
                      <span class="progress-description">
                        {{$upcomingPercent}}% of venues has upcoming events
                      </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-calendar-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">No upcoming events</span>
                    <span class="info-box-number">{{ $noUpcoming }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $noUpcomingPercent }}%"></div>
                    </div>
                      <span class="progress-description">
                        {{$noUpcomingPercent}}% of venues have no upcoming events
                      </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-calendar-check-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Venues with events</span>
                    <span class="info-box-number">{{ $events }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $eventsPercent }}%"></div>
                    </div>
                      <span class="progress-description">
                        {{$eventsPercent}}% of venues has at least 1 event
                      </span>
                </div>
            </div>
        </div>
        </div>
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-calendar-times-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Venues without events</span>
                    <span class="info-box-number">{{ $noEvents }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $noEventsPercent }}%"></div>
                    </div>
                      <span class="progress-description">
                        {{$noEventsPercent}}% of venues has no even 1 event
                      </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-registered"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Registered</span>
                    <span class="info-box-number">{{ $registered }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $registeredPercent }}%"></div>
                    </div>
                  <span class="progress-description">
                    {{$registeredPercent}}% of venues registered
                  </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-circle-thin"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Unregistered Venues</span>
                    <span class="info-box-number">{{ $unregistered }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $unregisteredPercent }}%"></div>
                    </div>
                  <span class="progress-description">
                    {{$unregisteredPercent}}% of venues unregistered
                  </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-circle"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Contacted venues</span>
                    <span class="info-box-number">{{ $contacted }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {{ $contactedPercent }}%"></div>
                    </div>
                  <span class="progress-description">
                    {{$contactedPercent}}% were contacted by our team
                  </span>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
   <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js" type="text/javascript"></script>
   <script>
       var ctx = document.getElementById("myChart");
       var myChart = new Chart(ctx, {
           type: 'bar',
           data: {
               labels: JSON.parse('{!! $keys !!}'),
               datasets: [{
                   label: '# of Venues added',
                   data: JSON.parse('{!! $data !!}'),
                   backgroundColor: [
                       'rgba(255, 99, 132, 0.8)',
                       'rgba(54, 162, 235, 0.7)',
                       'rgba(255, 206, 86, 0.6)',
                       'rgba(153, 102, 255, 0.5)',
                       'rgba(53, 102, 52, 0.4)',
                       'rgba(54, 153, 86, 0.3)',
                       'rgba(99, 206, 153, 0.2)'
                   ],
                   borderWidth: 1
               },
                   {
                       label: '# of Venues updated',
                       data: JSON.parse('{!! $updates !!}'),
                       backgroundColor: [
                           'rgba(99, 206, 153, 0.8)',
                           'rgba(255, 206, 86, 0.7)',
                           'rgba(255, 99, 132, 0.6)',
                           'rgba(53, 102, 52, 0.5)',
                           'rgba(54, 162, 235, 0.4)',
                           'rgba(153, 102, 255, 0.3)',
                           'rgba(54, 153, 86, 0.2)'
                       ],
                       borderWidth: 1
                   }
               ]
           }
       });
   </script>
@stop