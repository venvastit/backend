var elixir = require('laravel-elixir');
require('./tasks/angular.task.js');
require('./tasks/bower.task.js');
require('laravel-elixir-livereload');
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat');

var order = require("gulp-order");

gulp.task('sass', function () {
    gulp.src('./scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./css'));
});

elixir(function(mix){
    mix
        .bower(false, 'public/js', 'vendor.css', 'public/css')
        .angular('./angular/', 'public/js')
        //.less('./angular/**/*.less', 'public/css')
        .copy('./angular/app/**/*.html', 'public/views/app/')
        .copy('./angular/directives/**/*.html', 'public/views/directives/')
        .copy('./angular/dialogs/**/*.html', 'public/views/dialogs/')
        .livereload([
            'public/js/vendor.js',
            'public/js/app.js',
            'public/css/vendor.css',
            //'public/css/app.css',
            'public/views/**/*.html'
        ], {liveCSS: true});
    gulp.src('./resources/assets/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(concat('app.css'))
        .pipe(gulp.dest('public/css'));
});