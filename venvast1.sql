-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: venvast
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrators`
--

DROP TABLE IF EXISTS `administrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `administrators_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrators`
--

LOCK TABLES `administrators` WRITE;
/*!40000 ALTER TABLE `administrators` DISABLE KEYS */;
INSERT INTO `administrators` VALUES (1,'admin','$2y$10$pL2hcX3pm.ma0qvOf8FNx.s2OyAX5Q31Ox6f.OlJwA6590PpfSh4y','SleepingOwl Administrator',NULL,'2016-04-02 11:09:45','2016-04-02 11:09:45');
/*!40000 ALTER TABLE `administrators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `foreign_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_source_id_foreign` (`source_id`),
  CONSTRAINT `categories_source_id_foreign` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (55,'164243073639257',1,'Hotel','FqecCjIGsj.png'),(56,'176139629103647',1,'Dance Club',''),(57,'191478144212980',1,'Night Club',''),(58,'136412456432179',1,'Shopping District',''),(59,'109527622457518',1,'Shopping Mall',''),(60,'176831012360626',1,'Professional Services',''),(61,'135930436481618',1,'Lounge',''),(62,'110290705711626',1,'Bar',''),(63,'169925186386431',1,'Entertainment Service',''),(64,'187623434605561',1,'Adult Entertainment','');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_ascii` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `population` int(10) unsigned DEFAULT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_country` (`country`),
  KEY `idx_region` (`region`),
  KEY `idx_lat_long` (`latitude`,`longitude`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `commentable_id` int(10) unsigned NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_commentable_id_commentable_type_index` (`commentable_id`,`commentable_type`),
  KEY `comments_user_id_index` (`user_id`),
  KEY `comments_commentable_id_index` (`commentable_id`),
  KEY `comments_commentable_type_index` (`commentable_type`),
  CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_user_attendee`
--

DROP TABLE IF EXISTS `event_user_attendee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_user_attendee` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_user_attendee`
--

LOCK TABLES `event_user_attendee` WRITE;
/*!40000 ALTER TABLE `event_user_attendee` DISABLE KEYS */;
INSERT INTO `event_user_attendee` VALUES (69,8);
/*!40000 ALTER TABLE `event_user_attendee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_user_organizer`
--

DROP TABLE IF EXISTS `event_user_organizer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_user_organizer` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_user_organizer`
--

LOCK TABLES `event_user_organizer` WRITE;
/*!40000 ALTER TABLE `event_user_organizer` DISABLE KEYS */;
INSERT INTO `event_user_organizer` VALUES (8,4),(8,5),(6,10),(7,8);
/*!40000 ALTER TABLE `event_user_organizer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `duration` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `venue_id` int(10) unsigned NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source_id` int(10) unsigned NOT NULL,
  `original_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (67,'เรียนภาษาทำงานได้ เริ่มต้นราคาหลักหมื่น','สัมมนาฟรี เรียนต่อต่างประเทศหัวข้อ \"เรียนภาษาทำงานได้ เริ่มต้นราคาหลักหมื่น\"\n\nน้องๆรู้หรือไม่ ภาษาอังกฤษเป็นภาษาสากลที่คนทั่วโลกใช้ติดต่อสื่อสารกัน ถ้าเราสื่อสารภาษาอังกฤษได้ เข้าใจภาษาเเละสื่อสารกับผู้คนทั่วโลกได้กว่า พันล้านคนเลยทีเดียว เพราะอย่างนั้นแล้ว ถ้าไม่อยากปิดตัวเองอยู่ในโลกแคบ เรามาร่วมฟังสัมมนา เตรียมพร้อมการไปเรียนเมืองนอกกันดีกว่าคะ\n\nสัมมนาฟรี ไม่มีค่าใช้จ่าย แนะแนวทางให้น้องๆ เพิ่มโอกาสในการเปิดโลกกว้าง\n\nในงานกิจกรรมพบกับเจ้าหน้าที่โรงเรียน พร้อมให้คำตอบอย่างตรงไปตรงมา\n\nกิจกรรม ร่วมสนุกรับของรางวัลภายในงาน\n\nฟรี!!!!ค่าสมัครเรียน สำหรับน้องที่สมัครกับโรงเรียนที่เข้าร่วมกิจกรรม\n\nงานจัดที่ ศูนย์แนวแนะศึกษาต่อต่างประเทศ Vakom Overseas Education สุขุมวิท 71 ใกล้ BTS พระโขนง\n\nวันที่ 9 เมษายน 2559 เวลา 13:00-16:00\n\nสำรองที่นั่งก่อนมีสิทธิ์ก่อนโทร 02-391-1200 / 081-5500-111','13.7155199','100.5948105','2016-04-09','13:00:00',180,'2016-04-02 14:13:44','2016-04-02 14:13:44','Thailand, Bangkok, 257/7 , Sukhumvit 71 Road. ,Khongtan,Wattana ,Bangkok, 10110',60,'https://scontent.xx.fbcdn.net/hphotos-xpl1/v/t1.0-9/s720x720/12524386_10153985995277114_3439568801429298978_n.jpg?oh=04158f5820384edc527aeea73c54e14f&oe=577963E2',1,'1575876706060631'),(68,'Sonkran Session with DJ Charly T','Sonkran Session with DJ Charly T  \n\nSonkran Session with Charly T \"Worldwide Party Rocker\" He is back in Bangkok for one night only. Plan for a night of celebration with great vibes amazing music at the best dry late night party this  Sonkran season.\n\nFrench Cameroonian born, Australian Based. Charly Templar is a party rocker Pro DJ & Youth Mentor who uses Music as a tool to assist the under-privileged. Musical passion and the years of international club experience, combined with the craft and showmanship guarantee highly unique and eclectic sets.\n\nDoors Open: 10pm - Late\nEntrance: 300THB (includes 1 drink)\nMembers: Ladies Card 100THB (includes 1 free drink) \neXpat Card 300THB (includes 1 free drink)\nInfo & VIP table reservations 02 653 2923','13.737935849645','100.55839690887','2016-04-15','22:00:00',18586,'2016-04-02 14:13:45','2016-04-02 14:13:45','Thailand, Bangkok, Sukhumvit Soi 12, 10110',61,'https://scontent.xx.fbcdn.net/hphotos-xal1/t31.0-8/s720x720/1534973_1052541728122957_8169146398160248128_o.jpg',1,'211636979194003'),(69,'Westin Family Pool BBQ','Treat the family to a BBQ-style Saturday lunch and afternoon at the pool this cool season.\n\nCatch some winter rays – or stay in the shade – and unwind as the BBQ station sizzles, cool drinks flow freely, the kids are kept entertained and refreshing dips in the pool beckon.\n\nThe once-a-month only special family occasion leads off on Saturday, April 2nd, 2016. The fun runs from 12:00 till 16:00 hrs. \n\nEnjoy all-you-can eat BBQ specialties along with soft drinks, smoothies, selected alcoholic drink and mocktails.\n\nMeanwhile, the young ones play safely in the supervised Kids Corner while the live DJ lays down the beats and the MC announces the lucky draw prizes that are all part of the package.\n\nOnly THB 890 net per person including drinks. \nKids: 0-3 years eat free\n4-12 years only THB 390 net\n\nTo make a reservation, please call (66)(2) 207 8000 or email fb.bangkok@westin.com','13.7379257','100.5594245','2016-04-02','12:00:00',240,'2016-04-02 14:13:45','2016-04-02 14:13:45','Thailand, Bangkok, 259 Sukhumvit 19, Sukhumvit Road, Klongtoey-Nua, Wattana, 10110',62,'https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-xpa1/t31.0-8/s720x720/12095039_10153292722271266_5378762714988472282_o.jpg',1,'986757304745506'),(70,'ฺBangkok Brawl','6 Top Fights at Muay Thai Boxing Ring, Ambassador Hotel Bangkok. Advance Ticket sales @ THB 600. At the door @ THB 800.','13.742172587061','100.55700317296','2016-04-09','19:00:00',120,'2016-04-02 14:13:45','2016-04-02 14:13:45','Thailand, Bangkok, Ambassador Hotel Bangkok 171 Sukhumvit 11 (Chaiyot), Khlong Toei Nuea, Vadhana,, 10110',63,'https://scontent.xx.fbcdn.net/hphotos-xtp1/v/t1.0-9/10273612_10153393144907478_7613850302518776979_n.jpg?oh=91d86ec5382ddb5c32e1928dbd7a1301&oe=57839C72',1,'235206240166913'),(71,'EXPOSED with DAN & FAMEWAY every Tuesday','EXPOSED TUESDAY\'S \"Lets Get Kinky\" with DAN & FAMEWAY. The BAD ASS Duo coming from USA and Belgium. \n\nTheir \"NO LIMIT\" attitude has brought them international notoriety along with having residencies at BKK\'S best club\'s. What you can expect is a powerful blend of Big Room and Future House mixed with Trap and Twerk. To get that booty shaking!\n\nEvery Tuesday at BKK\'S favorite BIG ROOM, late night party spot with the city\'s Kinky people until late. \"Go Hard or Go Home.\"\n\nLady\'s get free drinks and have a chance to go home with some extra cash in their purse!Sunday ~ Thursday & every group of 4 ladies receives a FREE BOTTLE of (sparkling-) wine. \n\nDoors open : 10pm till VERY LATE\nEntrance Fee : ฿300 incl. 2 drinks for ladies - 1 drink men.\nLadies Members: ฿100 incl 2 drinks \n\nFree Ladies Members & eXpat Card sign ups all month long.\nLine ID: InsanityNightclub - Info & VIP table reservations 02 653 2923\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\nพบกับสัมผัสใหม่ของคืนวังคาร กับดูโอ้รูปหล่อ DJ DAN & DJ FAMEWAY จากอเมริกา และ เบลเยี่ยม \n\nมาร่วมปาร์ตี้ที่ไร้ขีดจำกัด กับร็อคทุกสายพันธ์ ในทุกๆวันอังคารจนคุณอยากกลับบ้าน กับคลับขยาดใหญ่ และความสนุกที่คุณไม่คาดคิด ผสมผสานกับการแดนส์แบบลืมโลก\n\nสำหรับสมาชิกสาว ๆ ของ insanity nightclub ยังรับสิทธิลุ้นเงินรางวัลกลับบ้าน 2 รางวัล. พร้อมกับดื่มฟรี 2 ดื่ม ก่อนเวลาตี 2 แล้วอย่าลืมมาพบกันที่นี่ที่เดียวนะค่ะ','13.737935849645','100.55839690887','2016-03-29','22:00:00',18900,'2016-04-02 14:13:45','2016-04-02 14:13:45','Thailand, Bangkok, Sukhumvit Soi 12, 10110',61,'https://scontent.xx.fbcdn.net/hphotos-xlf1/t31.0-8/q81/s720x720/12711211_1029313033779160_1094895431951476333_o.jpg',1,'866440933463774'),(72,'Onyx & Retox Sessions Present BINGO PLAYERS','Onyx & Retox Sessions\nProudly Present \nBINGO PLAYERS\nFriday April 1st\nAt ONYX\nTicket Info Coming SOON! \n\nABOUT BINGO PLAYERS\nMaarten Hoogstraten wasn’t like other kids his age. When he was 10, his friends were buying toys to play with, but Maarten was already spending his cash on vinyl and CDs.\n“You know how it starts,” he said with a chuckle. “I was the one who bought records when I was young, I bought lots of vinyl, seven-inches, twelve inches, and when CDs came about I bought a lot of CDs, growing up, I spent all my money on music.”\nThe producer behind the hit EDM act, Bingo Players, got hooked on electronic music when he was a kid, listening to The Prodigy’s “Out of Space,” and wishing he could make the kind of tunes that were being played on Dutch radio. But, at the time, to make dance music, you needed physical equipment—synthesizers and drum machines—far out of his reach at his age.\nWhen he was a teenager, two events would change his life. An early music software program called ProTracker was released, and he would meet Paul Baümer, who would become his musical partner for the better part of the next 15 years until he suddenly passed away from cancer in 2013.\nMaarten met Paul through his younger brother in Enschede, Holland, a two-hour drive from Amsterdam. One day, he went over to hang out with Paul’s younger brother, and found that Paul was also making music using ProTracker software. “It was only four channels and was really basic, but it was a start and really fun to do.”\n“When we discovered we were both making similar sounding music with the same program we started spending all our time together and became friends right away’” said Maarten.\nOver the next decade they began recording; and in 2006 they began recording under the moniker, Bingo Players. Their tracks started to resonate with the EDM crowds in America and abroad. Their songs— featuring buzzing, bouncing synthesizers, crunchy, guitar-like riffs reminiscent of the Chemical Brothers, and powerful, driving beats— are shaped by a memorable lyrical hook or a melody. 2009’s “Devotion” utilizes the catchy vocal hook, and sets it against a funky infectious saxophone riff and syncopated backbeat.\n“Most of the time we’d just start with a melody. Then a song would just evolve,” said Maarten of their songwriting process. “We can connect with a song if the melody is strong even though it can still be bare bones. We need to sing along or whistle along or to have an earworm, you know what I mean? Something that sticks in your head.”\nTheir breakout hit “Cry (Just a Little),” featured a re-recording of the hook from a 1988 Brenda Russell song, “Piano in the Dark.” With the memorable lyric looping around an ebullient melody, it became an instant crossover anthem.\n“I was watching VH1 and I saw this ‘Back to the 80s’ program where they played this Brenda Russell song and I thought, ‘Hey, this particular part of the song sounds really cool, maybe we could do something with it in our own style?’”\nThough they didn’t start out as DJs, once the demand grew for them to play out live, they learned how to mix on CDJs. Soon, they became regulars on the EDM circuit, playing massive festivals, like Electric Daily Carnival, Tomorrowland, and Coachella.\n“I think we started to become better producers once we started to DJ,” he said, noting that they would bring new tunes on the road and play for a crowd to gauge their reaction.\nIn between playing festivals and clubs around the world, the duo found the time to start their own label, Hysteria (a hat tip to Def Leppard, one of their favorite bands), releasing singles from Carl Tricks (“Mad Dash”), MAKJ (“Springen”), and Henry Fong (“Jump”) as well as Bingo Players tunes.\nIn 2013, they scored one of their biggest coups. An updated version of their 2011 track, “Rattle,” titled, “Get Up” (Rattle),” with LA-based act, Far East Movement, rapping over a bouncy, catchy beat, became a instant hit. When they first played the tune back in 2011, Maarten and Paul didn’t know if audiences liked it, but then, the single started to pick up steam around the world and slowly continued to grow.\nIt eventually charted in the Top Ten around the world including; Germany, Austria, France, and Australia (where it ended up going double Platinum), Gold in Canada, reaching the coveted #1 spot in the UK charts, and selling over 200,000 records in the U.S., reaching #5 on the dance charts. The success of the single, and the quirky, humorous video featuring ducks squaring off against hoodlums (which now has over 40 million views) helped propel Bingo Players high into the DJ Mag Top 100 list last year.\nBut in June 2013, at the height of their success, Paul suddenly fell ill. He was diagnosed with cancer. Everyone was shocked. Despite the prognosis, Paul remained optimistic.\nIn October, Paul took a turn for the worse and he passed away in December 2013. There was an outpouring of support from the EDM community with peers like Hardwell, Steve Aoki, Kaskade, Skrillex and Armin Van Buuren paying tribute on social media, at shows and in the press.\nMaarten was stunned and didn’t know what to do. But Paul’s last wishes were for his best friend and his musical partner to keep making music.\n“He always said to me, ‘If you can find solace and continue what we started, please do so. Please continue the music. Carry on the Bingo Players flag,’” said Maarten.\nHe wasn’t sure if he could carry on. But, he said, “I took a holiday, took some time off to clear my head and think about things. I thought, “Yeah Paul’s right. I should continue, and continue the name and continue his legacy.”\nAfter all, they have a library of finished tracks and ideas they worked on together that have yet to be released and Maarten can continue his and Paul’s dream of making the music that they heard on the radio as kids. The scene might have gotten bigger, and the production might be made on something more sophisticated than ProTracker, but, says Maarten, “The vibe is still the same. People want to have a good time, come together, and experience the music.” And Bingo Players live on.','13.752287565897','100.57495944445','2016-04-01','17:00:00',1873,'2016-04-02 14:13:46','2016-04-02 14:13:46','Thailand, Ayutthaya, RCA (Royal City Avenue), Soi Soonvijai, Rama 9 Rd, Bang Kapi, Huai Khwang, 10110',64,'https://scontent.xx.fbcdn.net/hphotos-xfl1/v/t1.0-9/535120_10153321568831898_8202171790451061276_n.jpg?oh=43cf8004b6658235ee5d3b339f7714bd&oe=577DADCD',1,'1271635066199187'),(73,'Maximum Beat @narz','Dance Party','13.737301638501','100.56488812252','2016-03-30','00:00:00',300,'2016-04-02 14:13:46','2016-04-02 14:13:46','Thailand, Bangkok, 112 Sukhumvit Soi 23 Road, 10110',65,'https://scontent.xx.fbcdn.net/hphotos-xpt1/t31.0-8/s720x720/12888618_10153495654878779_7019917819301994852_o.jpg',1,'1713286125627864'),(74,'The Urban Aloha Market','ALOHA! ต้อนรับหน้าร้อนกับงาน THE URBAN ALOHA MARKET   มหกรรมร้านค้าจากดาราเซเลบริตี้ อาทิ คุณเอ้ ศศิธร ,คุณกู๊ดดี้ อนัญญา ,คุณเต๊าะ วรัตตา ,คุณแก้มบุ๋ม ปรียาดา และสินค้าแบรนด์ออนไลน์ชื่อดัง กว่า 40 ร้านค้า เดินช้อปคลายร้อนชิลๆ ไปกับกลิ่นอายปาร์ตี้ริมชายหาดสไตล์ Tropical พร้อมดนตรีสดฟังสบายตลอดงาน ช้อปสนุกพร้อมกันได้ในวันที่ 1 - 3 เมษายน ศกนี้   ณ ลานหน้าทางเชื่อม BTS เอกมัย ชั้น M ศูนย์การค้าเกตเวย์ เอกมัย','13.7185898','100.586113','2016-04-01','14:00:00',3360,'2016-04-02 14:13:46','2016-04-02 14:13:46','Thailand, Bangkok, 982/22 Sukhumvit Road Prakanong Klongtoey, 10110',66,'https://scontent.xx.fbcdn.net/hphotos-xtl1/t31.0-8/s720x720/12671736_990867671005893_6498689951609366942_o.jpg',1,'1726557087560673'),(75,'Glow in the dark with DJ Swindali & MC Joelong','Glow in the dark with DJ Swindali & MC Joelong\n\nWe will be glowing in the dark with full body art on the seductive Insanity dancers, glowing effects and a chance to get your own body art with complementary neon body paint.\n\nStimulating eye candy & high quality music withDJ Swindali and with MC Joelong ready to party rock you late into the night! This powerful duo are sure to keep you dancing until late!\n\nWhat you can expect From DJ Swidali & MC Joelong is a booty shaking, hand pumping switch up, mash up of open Format, Big Room EDM, Trap, Twerk party rocking night!\n\nDoors Open: 10pm - Late\nEntrance: 300THB (includes 1 drink)\nMembers: Ladies Card 100THB (includes 1 free drink) \n\nInfo & VIP table reservations 02 653 2923\nRecent Posts\n\nInsanity Nightclub — DJ Swindali and MC Joelong did it again, full house on our GLOW IN THE DARK party at your favorite nightclub. With over 800 party people we sure knew ...\nJanuary 18\nRead More','13.737935849645','100.55839690887','2016-04-01','22:00:00',1573,'2016-04-02 14:13:47','2016-04-02 14:13:47','Thailand, Bangkok, Sukhumvit Soi 12, 10110',61,'https://scontent.xx.fbcdn.net/hphotos-xfa1/t31.0-8/s720x720/12771536_1052528128124317_2107903707243986983_o.jpg',1,'1151593904859080'),(76,'\"SWAG\" Ladies Night Bangkok Invaders DJ ONO & MC CALO Every Thursday','<สำหรับไทยเลื่อนลง>\n\n \"SWAG\" The Hottest HIP-HOP LADIES NIGHT with BANGKOK INVADERS, featuring DJ ONO, MC CALO, Double B & Coga. All rocking the house until very late!\n\nWatch out for our give-away\'s and many other perks! \n\nEvery group of 4 ladies receives a FREE BOTTLE of (sparkling-) wine. \n\nDoors open : 10pm till VERY LATE\nEntrance Fee : ฿300 incl. 2 drinks for ladies - 1 drink men.\nLadies Members: ฿100 incl 2 drinks \n\nFree Ladies Members & eXpat Card sign ups all month long.\n\nLine ID: InsanityNightclub - Info & VIP table reservations 02 653 2923\n\nSee you Thursday!\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\nSWAG ; สุดยอด Hip Hop ที่กำลังร้อนแรง ณ ตอนนี้กับ Bangkok Invaders พบกับ DJ ONO, MC CALO, Double B & Coga\n\nกิจกรรมของเรายังไม่หมดเพียงเท่านี้ เพียงแค่คุณลงทะเบียนหน้าทางเข้า คุณก็จะไก้สิทธิลุ้นรับเงินรางวัล 6000 บาท\n\nอีกทั้งสุภาพสตรี ที่มาพร้อมกันตั้งแต่ 4 ท่านขึ้นไป จะได้รับฟรี ไวน์ 1 ขวดทันที\n\nเปิดบริการตั้งแต่ 4 ทุ่มเป็นต้นไป\nค่าเข้า:สุภาพสตรี 300 บาทได้รับ 2 ดื่ม สุภาพบุรุษได้ฟรี 1 ดื่ม\nสำหรับท่านสุภาพสตรีที่มีบัตรสมาชิกค่าเข้า 100 บาท ได้ฟรี 2 ดื่ม\n\nฟรี Ladies members card และ Expat card เพียงลงทะเบียนระยะยาวกับเรา\nสอบถามเพิ่มเติมหรือจองโต๊ะได้ที่ \nLine ID: InsanityNightclub\nหรือโทร:02-653-2923 \nแล้วเจอกันวันพฤหัสบดี!!','13.737935849645','100.55839690887','2016-03-31','22:00:00',18900,'2016-04-02 14:13:47','2016-04-02 14:13:47','Thailand, Bangkok, Sukhumvit Soi 12, 10110',61,'https://scontent.xx.fbcdn.net/hphotos-xtf1/v/t1.0-9/s720x720/735128_1006093652767765_5520675919427115796_n.jpg?oh=983b384cc85e1baad127af102e48337e&oe=57924702',1,'443166822549780'),(77,'THE EM DISTRICT Beats','THE EM DISTRICT BEATS!!\nThe MUSIC will UNLEASH YOUR ENERGY with The First time ever TOP THAI ARTISTs on the same stage 4 days long, over Quartier Parc, The EmQuartier\nIt\'s FREE!!! Clear all your schedule if your energy needs to burst out! \n\n-Thursday 31 March 2016-\nTOR++ SAKSIT 18.15 - 19.00\nJETSET’ER  19.00 - 20.00\nJOEYBOY/FUCKING HERO & BEEAZY/ \nGANCORE CLUB 20.15 - 21.15\n\n-Friday 01 April 2016-\nFELLOW FELLOW 17.00 - 17.30\nBETTER WEATHER 17.45 - 18.15\nBEATS PERFORMANCE (ZANI | KACHA AF | PATCHA AF | NON I AM) 18.30 - 19.00\nPOD THANACHAI UJJIN 19.15 - 20.15\nSLOT MACHINE 20.15 - 21.15\n\n-Saturday 02 April 2016- \nSHOPPING BAG 15.50 - 16.30\n60 MILES 16. 45 - 17.20 \nROOM 39 17.40 - 18.40  \nSTAMP 18.50 - 19.30        \nTHAITANIUM 19.40 - 20.40\n*TRASHER PARTY “NOW THAT’S WHAT I CALL BAD MUSIC” 20.00 - 02.00\n\n-Sunday 03 April 2016-\nCHILLING SUNDAY 15.50 - 16.20\nBEDROOM AUDIO 16. 30 - 17.00\nSIN 17.10 - 17.50\nCHRISTINA AGUILAR  18.00 - 18.40\nELETRIC.NEON.AMP 18.50 - 19.30\nSCRUBB 19.40 - 20.30\n\nKeep following us if you need to unleash your energy!\n#TheEMDistrict \n#TheEMDistrictBeats \n#TheEmQuartier \n#UnleashTheEnergy','13.731253567709','100.56919817172','2016-03-31','17:45:00',4435,'2016-04-02 14:13:47','2016-04-02 14:13:47','Thailand, Bangkok, Sukhumvit 35, 10110',67,'https://scontent.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/s720x720/1917883_1763719537194265_4274928577139676946_n.jpg?oh=7faebea3e8abd03680e444a529f55429&oe=57934929',1,'1162807993765550'),(78,'★ MARI FERRARI ★ at LEVELS l Sunday 17th April 2016','Right after Songkran LEVELS will bring you DJ MARI Ferrari, one of the sexiest and most successful female DJs in the world to Bangkok!\n\nShe’s been touring through over 50 countries around the world and was a special guest DJ at the best parties organized by:\n\n* FASHION TV Spain\n* Sensation White\n* HED KANDI Festival Miami\n* Official Dj EUROVISION in Russia\n* VIP BELGIUM YACHT Cannes - \n* Fashion show Smet / Ed Hardy by Christian Audigier\n* Bruxelles Race and FORMULA ONE Dubai\n* PLAY BOY France / USA\n* FHM France / FHM Asia\n\nPlayed all over the world in the top notch Clubs she will be performing at LEVELS this 17th April 2016\n\nEntrance Fee: 300THB incl. 1 Drink\nTickets will be available at the Door!\n\nTable Reservations are recommended:\n* +66 (0)82 308 3246\n* info@levelsclub.com','13.744300604913','100.55661360921','2016-04-17','09:00:00',20686,'2016-04-02 14:13:48','2016-04-02 14:13:48','Thailand, Bangkok, 6th Floor, No.35, Sukhumvit Soi 11, Sukhumvit Rd. (เลขที่ 35, ชั้น 6, ซ.สุขุมวิท 11), 10110',68,'https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-xat1/t31.0-8/s720x720/12068742_1074569485918807_4494238504837539221_o.jpg',1,'525929674245699'),(79,'MICHAEL CALFAN @ SING SING THEATRE','**Very limited tickets are now live @ 600b each - we expect them to sell out quickly**\n\nwww.ticketmelon.com/event/michaelcalfan\n\nFrench house music superstar, Michael Calfan makes his long awaited Bangkok debut at the most talked about intimate Bangkok venue of the moment - the prestigious SING SING THEATRE.\n\nW/ support from LazyKay.\n\nHis latest release on Spinnin\', \'Nobody Does It Better\', has received a wide range of support, featuring prominently in the sets of Tiesto, Avicii, Don Diablo, Klingande, DJ Snake, Pete Tong & more','13.728160493659','100.57361662388','2016-04-21','16:00:00',360,'2016-04-02 14:13:48','2016-04-02 14:13:48','Thailand, Bangkok, Sukhumvit 45, 10110',69,'https://scontent.xx.fbcdn.net/hphotos-xal1/t31.0-8/s720x720/10633306_565047083659169_4819617461284062094_o.jpg',1,'212727962419502'),(80,'Westin Pool Party - SAVE THE DATE','SAVE THE DATE FOR OUR NEXT WESTIN POOL PARTY - THIS TIME UNTIL 9 PM\n\nFLYER COMING SOON..\n\nSpend Saturday in the city lounging around and dipping into our stunningly situated swimming pool. “Westin Pool Party”, coming up on Saturday, April 23, 2016, features Pool Bar liquid refreshments and classic poolside bites plus an awesome line-up of DJs: \n\nDJ\'s:\nNatsha (Russia)\nGroove(Thailand)\nDee Iris (Lebanon)\n\nFeaturing Stasha\n\nOPEN BAR FROM 2 - 3 PM. SPECIAL DRINK OFFERS THROUGHOUT THE DAY. \n\nEntrance is THB 300 including one drink.\n\nLet’s get wet and unwind in style!\n\nFor more information, please call (66)(2) 207 8000 or email fb.bangkok@westin.com.','13.7379257','100.5594245','2016-04-23','14:00:00',420,'2016-04-02 14:13:48','2016-04-02 14:13:48','Thailand, Bangkok, 259 Sukhumvit 19, Sukhumvit Road, Klongtoey-Nua, Wattana, 10110',62,'https://scontent.xx.fbcdn.net/hphotos-xfp1/v/t1.0-9/s720x720/600167_10153292726991266_7580846450961759080_n.jpg?oh=31accca4ed29d48862fb500dced0e22c&oe=578CF703',1,'1023004847770373'),(81,'PETE HERBERT at Sing Sing Theater - Sat 09th April 2016','A decade ago, Pete Herbert was co-proprietor of the infamous Atlas Records shop in Soho, London.\nThese days, he spends his time either behind the decks at clubs and parties around the globe (think Trapeze, Fabric, Ministry of Sound, Horse and Groom, plus a regular slot at the Sancho Panza Notting Hill Carnival Stage each year), plus a residency every two months at La Dame Noir club in Marseillle. \nAdditionally he also hosts his Music for Swimming Pools radio show weekly on Ibiza Sonica every Friday.\n\nDon’t miss out and catch Pete Herbert on Saturday, April 9th @ Sing Sing Theater - FREE ENTRY','13.728160493659','100.57361662388','2016-04-09','21:00:00',360,'2016-04-02 14:13:49','2016-04-02 14:13:49','Thailand, Bangkok, Sukhumvit 45, 10110',69,'https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpt1/v/t1.0-9/s720x720/12075004_570398523124025_5363676425896435801_n.jpg?oh=cf5ace784e4c0790d3e77033167f3577&oe=577A72D1&__gda__=1469387511_f770e3ea3bb78c27ec1235f849f08363',1,'1594693464186524'),(82,'★ SINNERS & SAINTS ★ at LEVELS l Wed 6th April 2016','★ SINNERS & SAINTS ★ at LEVELS\n\nAre you a saint or a sinner? \nAre you naughty or nice?\n\nWill you soar with the angels or dance with the devils at our Wednesday night party? \n\nThis is what you can expect on Wednesday 6th April at Levels:\n\n* Dress up competition, best ANGEL & DEVIL outfit get a chance to win 2x 2 voucher prices worth over 35,000THB total\n* Heaven \'n\' Hell decoration\n* Devil\'s Chair\n* A lot of Bautiful Angels & Naughy Devils \n* Hell Guards & Lucifer\n* Special heavenly cocktails and evil shots!\n* Free entrance!\n\n\nWhichever side you choose your soul will give you eternal salvation!','13.744300604913','100.55661360921','2016-04-06','21:00:00',5566,'2016-04-02 14:13:49','2016-04-02 14:13:49','Thailand, Bangkok, 6th Floor, No.35, Sukhumvit Soi 11, Sukhumvit Rd. (เลขที่ 35, ชั้น 6, ซ.สุขุมวิท 11), 10110',68,'https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-xpt1/t31.0-8/s720x720/12671773_1075933759115713_5196630495381173884_o.jpg',1,'865974780215626'),(83,'★ RAVE REPUBLIC ★ at LEVELS l THU 21 APRIL 2016','★ RAVE REPBULIC ★\n\nRave Republic is one of the most in-demand DJ duos based out of Asia having played headliner slots at multiple music festivals and produced chart topping tracks.\n\nExpect a high energy show featuring props and many memorable surprises along the way.\n\n* Thursday 21st April 2015\n* Entrance fee: 300THB incl. 1 standard drink\n\nBiography Rave Republic:\nTogether, Rave Republic have an impressive CV on and off the music stage. Rave Republic, established 2014, is already holding club residencies across Singapore, Hong Kong, Kuala Lumpur and Manila with more in the pipeline, waiting to be announced. They recently closed off an Australia tour with a gig at the renowned night club, Marquee.\n\nTheir explosive sets have made them a regular at the main stage of multiple music festivals. Their combined festival CV also includes: Asia Headliners at It’s the Ship music festival (2014) and Illumi Nation.\n\nThey were also announced as one of the Asia headliners for Future Music Festival Asia and were poised to play on the main stage alongside Avicii, Afrojack, Giorgio Moroder, Public Enemy and Knife Party.\n\nIn the second half of 2015 they will play alongside Skrillex, Alesso & Nicky Romero on the main stage at Road to Ultra, Singapore (Only act based in Asia on the lineup). They will also return to play It’s the Ship alongside Kaskade & Showtek (2015).\n\nIn May 2015, Rave Republic embarked on tour of Australia, performing in Melbourne, Sydney (Marquee) and Brisbane.\nThey have shared the stage with many A-list DJs including Dada Life, Sander van Doorn, Bingo Players, Martin Solveig, Dash Berlin, Basement Jaxx, Lil Jon, Sick Individuals, Isaac, Bloody Beetroots, Ally & Filla, LMFAO, Jazzy Jeff, Quintino, Third Party, John Dahlback, Lazy Rich and many more.\n\nThey have played across China, Singapore, Japan, Italy, Vietnam, Hong Kong, Myanmar, Taiwan, Malaysia, Philippines, Indonesia, Australia and Korea.\nTheir originals and remixes have received support from artists such as Tiesto, Above & Beyond, Lazy Rich, Beatbreaker and Zack Edward.\n\nTheir recent remix of Magic!’s hit song Rude was recently released in December and has received coverage across leading music blogs such as EDM Tunes. Their first original release, ‘Ready for the Drop’ hit #1 on Juno Download’s Progressive House Charts immediately after release. Their mix tape commissioned by DJ City hit #1 on Mixcloud’s House Chart.','13.744300604913','100.55661360921','2016-04-21','21:00:00',27166,'2016-04-02 14:13:49','2016-04-02 14:13:49','Thailand, Bangkok, 6th Floor, No.35, Sukhumvit Soi 11, Sukhumvit Rd. (เลขที่ 35, ชั้น 6, ซ.สุขุมวิท 11), 10110',68,'https://scontent.xx.fbcdn.net/hphotos-xpa1/t31.0-8/s720x720/12030459_1070453652997057_4363257606847331068_o.jpg',1,'767894140011325'),(84,'Wine and Chocolate Night','On the 29th of April, Diplomat Bar is hosting a free flow wine and chocolate event - Taste of Thai Wine and Chocolate. We bring to you 8 flavors of the new latitude wine “Granmonte” from Kao Yai, with more than 100 awards over the past 4 years. Together with 8 unique Thai spice and fruit flavored chocolates, to create the most incredible wine tasting experience. The Wine and Chocolate Night will happen at 19:00-22:00, with “Julia Constantine” taking over the stage with her hypnotizing voice from 20:30. Priced at THB 2,200 ++ per person.','13.738700791022','100.54862715665','2016-04-29','19:00:00',180,'2016-04-05 00:16:40','2016-04-05 00:16:40','Thailand, Bangkok, 87 Wireless Road, 10330',70,'https://scontent.xx.fbcdn.net/hphotos-xap1/t31.0-8/s720x720/12961327_10154153858369885_8536559716625969902_o.jpg',1,'1719422748306230'),(85,'★ DIMITRI VANGELIS & WYMAN ★ at LEVELS l Thursday 26th May 2016','DIMITRI VANGELIS & WYMAN, the Swedish Top DJ duo will have their Thailand debut at LEVELS on Thursday 26th May!\n\nWith massive releases on major labels such as Virgin EMI Records, Sony Music, SIZE & Spinnin\' Records this duo is not to be missed!\n\n* Tickets will be sold at the door\n* 300THB incl. 1 drink\n\nBiography:\n\nIn a world of hype, empty numbers and disposable music, Dimitri Vangelis & Wyman are cutting the clichés of electronic music one melodic anthem at a time. \n\nKings of the ID, connoisseurs of a new wave of melodic big room and globetrotting ambassadors to the Size Records family, the past three years has seen them established as a dominating force from the fertile Swedish talent pool. \n\nWith a career that has spotlighted the virtues of sticking to your guns in a crowded musical space, their journey to date is told through a sparing yet quality, one that has accumulated fans and industry repertoire from across the continents. \n\nScoped by Dancing Astronaut as Everything that is right about the next generation of Swedish trailblazers, precision over blind impact simply never sounded so good.  \n\nEven before widely sought unofficial remixes for Miike Snow and Tom Odell, the duos early studio work showed immediate bravado. DzSilver Sundz alongside Anna Yvette would showcase a rare balance of upfront energy and vocal stamina in the major label environment, playing soundtrack to Swedens Lights festival and scoring one of the labels most streamed cuts of the year. \n\nBehind the scenes, however,a union was brewing that would shape the course of their melodic voyage indefinitely. The attention of homeland peerand Size Records head honcho Steve Angello would see them land comfortably under the roof of this big room empire, the roof under which the duo would successfully make the transition from rising hopefuls to next generation leaders. \n\nEntering the labels discography on remix duties for Max Vangeli, AN21 and Tiëstos People of the Night, Size debut DzRebeldz alongside AN21 served as a serious first single, but would prove to be a mere warm up for the definitive career landmark ahead. After more than a year ofrotation in Steve Angellos sets, DzPaybackdz proved itself a definitive spike for the duo and their activity alongside Size, racking more than 10 million streams in the process. It would become Sonys second biggest Beatport seller of the year, upping the ante on an already consistent upwards spiral for the emerging Swedish powerhouse. \n\nWith solo follow-up DzID2dz gaining equally promising traction on festival crowds and the digital market, 2014 capped off as the year Dimitri Vangelis & Wyman took the heartstrings of a global industry on their own terms.  That reign was not limited to the digital market and a plethora of streams. Turning that sparing output into a global passport onto dance floors and festival stages across the continents, fierce action at the likes of Creamfields, Summerburst, Size in the Park, Electric Love Asutria and Lights Festival has been matched with appearances at venues as diverse as Brixton Academy, Avalon Hollywood, Octagon Korea and Paradise Club Mykonos. \n\nWith rapid global expansion, intriguing collaborations and a vow to let those sought after IDs see the light of day already on the cards for 2015, you can rest assured that the preservation of the big room sound remains safe in the hands of Dimitri Vangelis & Wyman.','13.744340189418','100.55655202427','2016-05-26','21:00:00',74083,'2016-04-05 00:16:40','2016-04-05 00:16:40','Thailand, Bangkok, 6th Floor, No.35, Sukhumvit Soi 11, Sukhumvit Rd. (เลขที่ 35, ชั้น 6, ซ.สุขุมวิท 11), 10110',68,'https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-xfp1/t31.0-8/q81/s720x720/12888637_1080933428615746_1796035492045686533_o.jpg',1,'856233934506101'),(86,'Sunday\'s \"Get Lucky\" with DJ VIDA & MC Dakotaa Jade','Sunday just got sexier, with DJ VIDA & Dakotaa Jade running the decks with her unique sound & Charm. She has ranked top 10 Asian Sexiest Female Djs in 2014 and 2015  catch her beauty & Talent.\n\nLady\'s get free drinks and have a chance to go home with some extra cash in their purse!Sunday ~ Thursday & every group of 4 ladies receives a FREE BOTTLE of (sparkling-) wine. \n\nDoors open : 10pm till VERY LATE\nEntrance Fee : ฿300 incl. 2 drinks for ladies - 1 drink men.\nLadies Members: ฿100 incl 2 drinks \n\nFree Ladies Members & eXpat Card sign ups all month long.\n\nLine ID: InsanityNightclub - Info & VIP table reservations 02 653 2923\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\nคืนวันอาทิตย์นี้จะเซ็กซี่ขึ้นไปอีกกับดีเจ VIDA ที่จะมาเขย่าเวทีแห่งนี้ด้วยซาวด์ดนตรีสุดพิเศษของเธอที่จะทำให้คุณหลงไหลไปกับมัน\nเธอเป็น1ในดีเจที่ติดอันดับท็อปเท็น ของสุดยอดดีเจเซ็กซี่แห่งเอเชียทั้งในปี 2014 และปีนี้ 2015 \n\n\nสาวๆทุกคนเตรียมตัวรับสิทธิพิเศษที่จะได้รางวัลมูลค่า 6,000 บาท ในการจับฉลากทุกๆคืนวันอาทิตย์-วันพฤหัส และพิเศษสุดๆกับ สาวที่มากันเป็นกลุ่ม 4 คนรับฟรี!! สปาร์กลิ้งไวน์\n\n\nประตูเปิดเวลา : 4ทุ่ม – ดึก\nค่าเข้า : 300บาท (สาวๆรับเครื่องดื่ม 2 แก้ว) (หนุ่มๆรับเครื่องดื่ม 1 แก้ว) สมาชิก เลดี้การ์ด : 100 บาท (รับเครื่องดื่ม 2 แก้ว)\n\nสมัครสมาชิก เลดี้การ์ด & eXpat ฟรีตลอดทั้งเดือน\n\nLINE ID : InsanityNightclub  ข้อมูลเพิ่มเติม หรือ สำรองที่นั่ง ติดต่อ 02 653 2923','13.737935849645','100.55839690887','2016-04-10','22:00:00',18900,'2016-04-05 00:16:40','2016-04-05 00:16:40','Thailand, Bangkok, Sukhumvit Soi 12, 10110',61,'https://scontent.xx.fbcdn.net/hphotos-xaf1/t31.0-8/s720x720/12697358_1031611900215940_1243620959369137217_o.jpg',1,'895347417220330'),(87,'Sonkran 2016 with DJ Big Daddy from Oslo','SONKRAN 2016 we welcome back DJ Big Daddy Oslo, a legend in the Norwegian fashionable music scene for decades and is known for establishing the house music scene in the capital city hence his name Big Daddy. Sure to be an outstanding set of electro house to Big Room celebrating Thailand\'s New Years.\n\nEnjoy another killer late night party at Bangkok\'s best late night club!\n\nDoors Open: 10pm - Late\nEntrance: 300THB (includes 1 drink)\nMembers: Ladies Card 100THB (includes 1 free drink) \n\nInfo & VIP table reservations 02 653 2923','13.737935849645','100.55839690887','2016-04-13','22:00:00',12223,'2016-04-05 00:16:40','2016-04-05 00:16:40','Thailand, Bangkok, Sukhumvit Soi 12, 10110',61,'https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-xpa1/t31.0-8/s720x720/10515092_1062360967141033_3937437419324298082_o.jpg',1,'859734920803338'),(88,'Fred Canal & Friends','F Session is on with Fred Canal the local ledgend bringing his electric touch and sexy friends to Bangkok\'s Big Room late night playground for the first time. Exspect the unexspected with a mix a floor fillers from disco to Techno house.\n\nDoors Open: 10pm - Late\nEntrance: 300THB (includes 1 drink)\nMembers: Ladies Card 100THB (includes 1 free drink) \n\nInfo & VIP table reservations 02 653 2923\n\nFRED CANAL\nThe course of Fred Canal is equally eclectic and exalting ... \nAs long as he can remember, Fred has always been immersed in music. \n\nFirst as opera singer and as a DJ in the most famous and very chic Parisian parties , but it is at Mozinor that his life will change. \nIt is at these early 90s legendary raves that he decided that his career will be in electronic music. \n\nHe founded and organized House Keeper and organized House of Japan, the music festival at Palais de Tokyo in Paris, mythical event which made the beautiful nights in the capital.                                      \nThen he headed to England where he will drop his suitcases for a few years and work as a Tour Manager for the famous club: Gatecrasher. \n\nHe will on this occasion do a world tour, accompanying the biggest djs in the world (Carl Cox, Paul van Dyke, Eric Morillo, Tiesto, Armin van Buren, Laurent Garnier, etc.) and will mix in the best clubs in the world through residency such as Space, Pacha in Ibiza or KM5 ... \n\nIn 2013, he moved to Thailand. First in Koh Samui where I wins the Best southern Thailand Dj award which gives him the opportunity to get residency in all the clubs of the area: Dreamers, Chacha Moon, Kc Beach etc but he is looking for bigger challenge and moves to Bangkok in 2014.\nHe is now music director of the famous Maggie Choo’s and regular player in some of Bkk best clubs: Sing Sing Theater, Oskar Bistro, Gramercy Park and also active in the after party scene at Mustache or Wax.\n\nHis great musical culture allows him to mix both in private parties, in trendy clubs, and parties for luxury brands. (Danone, Microsoft, LVMH ...).','13.737935849645','100.55839690887','2016-04-09','22:00:00',6463,'2016-04-05 00:16:40','2016-04-05 00:16:40','Thailand, Bangkok, Sukhumvit Soi 12, 10110',61,'https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-xpf1/t31.0-8/s720x720/12888782_1060715607305569_6763968327118781300_o.jpg',1,'449960058546754'),(89,'Zuccardi Wine Dinner','Offering a wonderful feast before the magical Songkran Festival, KiSara teams up with renound Zuccardi wines for an evening of incredible food and fine wines. Chef Kenji serves up an inspiring six course menu which has been perfectly paired with the best from the Zuccardi vineyards. Familia Zuccardi are known for producing some of the most exciting and drinkable wines from Argentina, and the KiSara team has found a way to sensationally match its authentic Japanese flavours. A night not to be missed! For further information and reservations please call 02-690-9999 or bkkci.info@conradhotels.com','13.738700791022','100.54862715665','2016-04-07','19:00:00',3403,'2016-04-05 00:16:41','2016-04-05 00:16:41','Thailand, Bangkok, 87 Wireless Road, 10330',70,'https://fbcdn-photos-d-a.akamaihd.net/hphotos-ak-xap1/t31.0-0/p180x540/12719421_10154134821604885_649051587961062634_o.jpg',1,'1023910121024578'),(90,'★ PETE SHAKER & FILEX ★ at LEVELS l Thursday 7th April 2016','On Thursday 7th April we have two very talented Electro House guest DJs performing at LEVELS!\n\n★ PETE SHAKER - Finalist of Poland\'s DJs Championship 2015\n\n★ FILEX - a young French DJ & Producer that won a huge DJ contest in Switzerland in 2013 and has played all over the world since then\n\nGet yourself prepared for an outstanding performance by both with the newest tunes fresh from overseas!\n\n* Free entrance\n\nAdd us on LINE to get updates on the newest promotions and events by adding @levelsbangkok (inclusive the @)\n\nFor table bookings: \n✉ info@levelsclub.com\n✆ +66 82 308 3246','13.744340189418','100.55655202427','2016-04-07','21:00:00',3523,'2016-04-05 00:16:41','2016-04-05 00:16:41','Thailand, Bangkok, 6th Floor, No.35, Sukhumvit Soi 11, Sukhumvit Rd. (เลขที่ 35, ชั้น 6, ซ.สุขุมวิท 11), 10110',68,'https://scontent.xx.fbcdn.net/hphotos-xta1/t31.0-8/s720x720/12900967_1080094955366260_1604390153027894822_o.jpg',1,'1593783717578258'),(91,'สงกรานต์ สเปเชียล ปาร์ตี้ และมหกรรมการสาดน้ำที่สนุก Narz Songkran','สงกรานต์ สเปเชียล ปาร์ตี้ และมหกรรมการสาดน้ำที่สนุกและมันส์ที่สุด ยิง น้ำพุขึ้น รอบงาน ที่นี่ ที่เดียว Narz Songkran Form Party สุดยิ่งใหญ่ อลังการ ที่คุณ จะพลาดไม่ได้เลย Thailand Festival\nSongkran Foam Party!!!!!\n12-15 April 2016\nNARZ Bangkok','13.737301638501','100.56488812252','2016-04-12','23:00:00',3060,'2016-04-05 00:16:41','2016-04-05 00:16:41','Thailand, Bangkok, 112 Sukhumvit Soi 23 Road, 10110',65,'https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-xpf1/t31.0-8/s720x720/12916168_10153514990218779_2306883378948115016_o.jpg',1,'526037260934321'),(92,'Songkran Foam Party','DJ Line Up\nSongKran Foam Pary !!!!!!!!!\n12-15 April 2016\nLet\'s the happy Festival and Enjoy The Party\n\nDJ Line Up\n-------------------------------------------\n12 APRIL 2016\n18.00-19.30    	DJ ANAN\n19.30-21.00 	DJ PAKA\n21.00-22.00	DJ DSTEAM\n22.00-CLOSE	DJ PAN\n------------------------------------------\n13 APRIL 2016\n18.00-19.30    	DJ ANAN\n19.30-21.00 	DJ ATHENA\n21.00-22.00	DJ APRIL\n22.00-CLOSE	DJ OLE\n------------------------------------------\n14 APRIL 2016\n18.00-19.30    	DJ BIG\n19.30-21.00 	DJ CREAM\n21.00-22.00	DJ KOR‘SE\n22.00-CLOSE	DJ PAN\n------------------------------------------\n15 APRIL 2016\n18.00-19.30    	DJ JOYYLY & BPLAA\n19.30-21.00 	DJ BENZE\n21.00-22.00	DJ DSTEAM\n22.00-CLOSE	DJ TANN\n\n★★★★★★★★★★★★★★★★★★★★★★★★\n#songkran #festival #songkranfestival\n#thailand #songkranthailand\n#narz #narzbangkok #narzbkk \n#nightlife #nightparty #bangkoknightlife \n#trance #edm #house #dance #party #dj #thaiedm\n#funnyparty\n★★★★★★★★★★★★★★★★★★★★★★★★\nEmail : narzclubbangkok@gmail.com \nWebsite : narzclubbangkok.net \nFacebook : narzbangkok \nInstagram : narzclubbangkok23 \nLine Id : narzbangkok23 \nReservation : 02-258-4805\n★★★★★★★★★★★★★★★★★★★★★★★★\n\n#songkran #festival #songkranfestival\n#thailand #songkranthailand\n#narz #narzbangkok #narzbkk \n#nightlife #nightparty #bangkoknightlife \n#trance #edm #house #dance #party #dj #thaiedm\n#funnyparty\n★★★★★★★★★★★★★★★★★★★★★★★★\nEmail : narzclubbangkok@gmail.com \nWebsite : narzclubbangkok.net \nFacebook : narzbangkok \nInstagram : narzclubbangkok23 \nLine Id : narzbangkok23 \nReservation : 02-258-4805\n★★★★★★★★★★★★★★★★★★★★★★★★','13.737301638501','100.56488812252','2016-04-12','18:00:00',3360,'2016-04-05 00:16:41','2016-04-05 00:16:41','Thailand, Bangkok, 112 Sukhumvit Soi 23 Road, 10110',65,'https://scontent.xx.fbcdn.net/hphotos-xap1/t31.0-8/s720x720/12440746_10153510496728779_413418102414202590_o.jpg',1,'1714064982212712');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_categories`
--

DROP TABLE IF EXISTS `events_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_categories` (
  `event_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `events_categories_event_id_foreign` (`event_id`),
  KEY `events_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `events_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `events_categories_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_categories`
--

LOCK TABLES `events_categories` WRITE;
/*!40000 ALTER TABLE `events_categories` DISABLE KEYS */;
INSERT INTO `events_categories` VALUES (67,60),(68,56),(68,57),(69,55),(70,55),(71,56),(71,57),(72,64),(73,57),(73,63),(74,59),(75,56),(75,57),(76,56),(76,57),(77,58),(77,59),(78,61),(78,56),(78,57),(79,62),(79,57),(80,55),(81,62),(81,57),(82,61),(82,56),(82,57),(83,61),(83,56),(83,57),(84,55),(85,61),(85,56),(85,57),(86,56),(86,57),(87,56),(87,57),(88,56),(88,57),(89,55),(90,61),(90,56),(90,57),(91,57),(91,63),(92,57),(92,63);
/*!40000 ALTER TABLE `events_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_locations`
--

DROP TABLE IF EXISTS `events_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_locations` (
  `event_id` int(10) unsigned NOT NULL,
  `location_id` int(10) unsigned NOT NULL,
  KEY `events_locations_event_id_foreign` (`event_id`),
  KEY `events_locations_location_id_foreign` (`location_id`),
  CONSTRAINT `events_locations_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
  CONSTRAINT `events_locations_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_locations`
--

LOCK TABLES `events_locations` WRITE;
/*!40000 ALTER TABLE `events_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `events_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'13.72444','100.500','Bangkok'),(2,'41.31','-72.92','New Haven');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_03_19_132810_extend_user_table',2),('2016_03_10_114715_create_cities_table',3),('2016_03_23_145331_create_events_table',4),('2016_03_23_171741_extend_events_table',5),('2016_03_23_175929_event_user_pivots',6),('2016_03_28_213332_categories_model',7),('2016_03_28_224404_extend_event_table',8),('2016_03_28_230938_extend_events_with_sources',9),('2016_03_28_231614_extend_categories',10),('2016_03_29_001636_add_original_id_to_events',11),('2016_03_30_215354_create_comments_table',12),('2014_10_12_104748_create_administrators_table',13),('2016_04_02_153815_extend_category_w_image',14),('2016_04_02_161017_extend_sources_w_image_url',15),('2016_04_02_164940_extend_venues_w_id_on_source',16),('2016_04_02_182525_extend_users_w_last_geo',17),('2016_04_02_183609_add_locations_model',18);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources`
--

DROP TABLE IF EXISTS `sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_url` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources`
--

LOCK TABLES `sources` WRITE;
/*!40000 ALTER TABLE `sources` DISABLE KEYS */;
INSERT INTO `sources` VALUES (1,'facebook','dYNrzlMHyU.jpeg','https://facebook.com','graph.facebook.com');
/*!40000 ALTER TABLE `sources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `hide_phone` tinyint(1) NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_lng` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (8,'andrey.degtyaruk','hlogeon1@gmail.com','$2y$10$P87lQClydajU6dahcsjuX.toZJk2NdGnldGfKd8NibwA.D9hBzAgi','D98m3P4GR6Om83EGT1yDB4Nk8LVAslPSgwX8mkWf9WquO2zBU7qc5Hgz2cs4','2016-03-19 10:40:02','2016-04-03 12:32:29','Andrew',1,0,'Degtyaruk','+79506039921','option1','41.31','-72.92'),(9,'vasya.pupkin','pupkin@gmail.com','$2y$10$9AxYYMURzQ79rmSYs055LOuV9bowsQ6yGs0cbOIpbxjU5znxqFcRK',NULL,'2016-03-19 11:09:43','2016-03-19 11:09:43','Vasya',1,0,'Pupkin','+420777127745','option1','',''),(10,'alexey.navalny','navalny@gmail.com','$2y$10$qsEoucVJ0Wl8LpqPXMEVG.fhWxeU0gLtAizk08PxrKYmcReQ9uGje',NULL,'2016-03-23 16:01:40','2016-03-23 16:01:40','Alexey',1,0,'Navalny','+420777127745','option2','','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venues`
--

DROP TABLE IF EXISTS `venues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `cover_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `general_info` text COLLATE utf8_unicode_ci,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_transit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_on_source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `venues_source_id_foreign` (`source_id`),
  CONSTRAINT `venues_source_id_foreign` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venues`
--

LOCK TABLES `venues` WRITE;
/*!40000 ALTER TABLE `venues` DISABLE KEYS */;
INSERT INTO `venues` VALUES (60,'VAKOM  ศึกษาต่อต่างประเทศ',NULL,'https://scontent.xx.fbcdn.net/hphotos-xtf1/v/t1.0-9/s720x720/579268_10151577824477114_1984056146_n.jpg?oh=2a10304b8f2782ac108c020f40ed6ee5&oe=577D7D74',1,'เรียนเมืองนอก สอบถามข้อมูล เรียนต่อต่างประเทศ \nสามารถโทรติดต่อได้ที่ 02 391 1200  , 081 5500 111\nหรือโทรหา พี่ๆ ได้ที่ \nNidnoi 086-345-4700\nAof	 086-366-1753\nMon	 086-345-4703 \nNice  086-3661751\nToey : 086-318 3010\n\nหรือ email มาที่ vakom@vakom.com \nหรือ ฝากข้อความไว้ที่หน้า Facebook ได้ค่ะ','www.vakom.com','13.7155199','100.5948105','257/7 , Sukhumvit 71 Road. ,Khongtan,Wattana ,Bangkok','10110','02 391-1200 , 081-5500-111,   หรือโทรหา พี่ๆ ได้ที่  Nidnoi:086-345-4700  /Aof:	086-366-1753 / Mon :086-345-4703','www.vakom.com','โดยรถไฟฟ้า BTS ลงสถานีพระโขนง ลงทางออก 3 และนั่งรถเมล์สาย 71 เข้ามาในซอยสุขุมวิท 71 - 3ป้ายรถเมล์ หรือจะนั่งรถแท็กซี่ เข้ามาก็ได้คะ \n\nตึก Vakom (สีส้มๆ) จะอยู่ทางขวามือติดถนนใหญ่ ระหว่างซอยปรีดีพนมยงค์ 13 และ 15   \n\nน้องคนใดที่นำรถส่วนตัวมาสามารถนำรถเข้าไ','263487217113'),(61,'Insanity Nightclub',NULL,'https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-xat1/t31.0-8/s720x720/12900967_1058203757556754_1897272666102019492_o.jpg',1,'Insanity Nightclub Bangkok is one of the cities best known nightspots that rocks to bass-heavy house, hip-hop, R&B and EDM seven nights a week.\n\nThe venue is one of the largest clubs in the city and has a large main room filled with incredible lasers, an amazing sound system, sexy podium dancers and a central stage for those who want to strut their stuff on the dance floor.\n\nBusy year-round, Insanity nightclub is one of Bangkok’s most in-demand nightlife destination\n\n\n\n\n',NULL,'13.737935849645','100.55839690887','Sukhumvit Soi 12','10110','026532923','www.clubinsanitybangkok.com/','BTS NANA station','471209556256180'),(62,'The Westin Grande Sukhumvit, Bangkok','2003-01-01','https://fbcdn-photos-g-a.akamaihd.net/hphotos-ak-xtf1/t31.0-0/p480x480/10855026_10153027626796266_7690443459533082501_o.jpg',1,'Situated in the heart of Bangkok, right next door to Terminal 21 Shopping Mall and with the added convenience of the skytrain and the subway mere steps away, you will be instantly transported to all that Bangkok has to offer.\n\nโรงแรม เดอะ  เวสทิน แกรนด์ สุขุมวิท  ตั้งอยู่ใจกลางกรุงเทพฯ บนถนนสุขุมวิท ศูนย์กลางของย่านธุรกิจ และศูนย์การค้าชื่อดังหลายแห่ง อีกทั้งสะดวกกับการเดินทางไปสถานที่ต่างๆ เนื่องด้วยโรงแรมอยู่ใกล้กับ รถไฟฟ้าบีทีเอส สถานีอโศก และรถไฟฟ้าใต้ดิน สถานีสุขุมวิท ','email: bangkok@westin.com\nphone: +66 (0) 2207.8000','13.7379257','100.5594245','259 Sukhumvit 19, Sukhumvit Road, Klongtoey-Nua, Wattana','10110','+66 (0) 2207.8000','www.westingrandesukhumvit.com','Enjoy direct access to Asok skytrain & Sukhumvit subway stations where you are instantly transported to the city\'s best shopping, famous temples, and Chatuchak Weekend Markets. ','175672431265'),(63,'The Ambassador Hotel Bangkok',NULL,'https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-xlf1/v/t1.0-9/s720x720/12799309_10153364548022478_5516046785883766524_n.jpg?oh=2c6754d157d5ef71cc68841f70bef6df&oe=57811619&__gda__=1468795781_12579340d34a7d19d26b3c7b640c7d54',1,'Website : www.amtel.co.th\nFacebook : www.facebook.com/AmbassadorBangkok\nTwitter : twitter.com/pr_ambassador\nInstagram : instagram.com/AMBASSADOR.HOTEL.BANGKOK\nGoogle+ : plus.google.com/+AmtelCoTh\nTripAdvisor : http://www.tripadvisor.com/Hotel_Review-g293916-d308724-Reviews-Ambassador_Hotel_Bangkok-Bangkok.html\nPinterest : pinterest.com/AmbassadorBKK/','Tel :+66(0)2 2540444 , \nReservation Ext.1220-4 \nSales & Catering : Ext 1604, 1612, 1631 , 1507 , 1508\nOffice time : 08.00 - 18.00\n\nBST: Nana, MRT: Sukhumvit','13.742172587061','100.55700317296','Ambassador Hotel Bangkok 171 Sukhumvit 11 (Chaiyot), Khlong Toei Nuea, Vadhana,','10110','+66(0)2 254-0444','http://www.amtel.co.th      http://www.facebook.com/AmbassadorBangkok','BTS Nana, MRT  Sukhumvit, ','128660352477'),(64,'ONYX',NULL,'https://fbcdn-photos-e-a.akamaihd.net/hphotos-ak-xaf1/v/t1.0-0/p480x480/12647554_757773964353391_297118357086621414_n.jpg?oh=e3e96f1a902933a31a7350af3970d240&oe=577BDCE4&__gda__=1468674177_a6b15a40b5bc58b3c5254cca954e5b8e',1,'Located on RCA, one of the largest Entertainment Clubbing area.\nExpect Tech, Electro, EDM, & Dance Music.\n',NULL,'13.752287565897','100.57495944445','RCA (Royal City Avenue), Soi Soonvijai, Rama 9 Rd, Bang Kapi, Huai Khwang','10110','0816451188','http://www.onyxbangkok.com',NULL,'402258646571593'),(65,'NARZ Bangkok',NULL,'https://scontent.xx.fbcdn.net/hphotos-xtp1/t31.0-8/s720x720/12916845_10153495172123779_7508271416459684776_o.jpg',1,'Formerly knew as NARCISSUS Club, which established since 1992 and located on Sukhumvit 23 road.\nThe huge place aimed at the higher-end Bangkok club scene.\nIt has terrific light and sound systems and occasional International guest DJs. \nIt is very popular among foreign expatriates.\nThe setting needs no introduction - with the lounge and serious dancefloor and places to get wild and intimate.\nThe stylish and signature decor style make it the ideal place celebrate with your friends.\nThis dynamic space makes it a playground for sophisticated twenty-something\'s who demand luxury and style. There are four floors, eight bars, three rooms with ultra-chic interiors for everyone and music goes from Hip Hop in the middle floor, House upstairs room to Trance in the main room, and is one of the biggest, most talked about and hippest late night venues in Bangkok.',NULL,'13.737301638501','100.56488812252','112 Sukhumvit Soi 23 Road','10110','+66-2-258-4805','http://www.narzclubbangkok.net','MRT Sukhumvit station or BTS Asoke Station','234674293778'),(66,'Gateway Ekamai','2012-07-18','https://scontent.xx.fbcdn.net/hphotos-xlf1/v/t1.0-9/s720x720/12744095_971503716275622_1470986798592087098_n.jpg?oh=2974c1173954acc1ece9a044762e88e3&oe=57856C2E',1,'เปิดให้บริการทุกวัน ตั้งแต่เวลา 10.00 - 22.00 น. ',NULL,'13.7185898','100.586113','982/22 Sukhumvit Road Prakanong Klongtoey','10110','02 108 2888','www.GatewayEkamai.com / www.youtube.com/GatewayEkamai / http://instagram.com/GatewayEkamai  ',NULL,'150057221753613'),(67,'The EM District',NULL,'https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t1.0-9/s720x720/12932936_1770289603203925_2162430028276459815_n.jpg?oh=28e2092c9d1cc56963e02ca07ad36722&oe=57745A30&__gda__=1469572763_6ef8ad429dd05ab14823d9c4797cd4c8',1,NULL,NULL,'13.731253567709','100.56919817172','Sukhumvit 35','10110','+6622691000','http://theemdistrict.com',NULL,'1483454928554062'),(68,'Levels','2012-07-11','https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-xap1/t31.0-8/s720x720/12440750_1075851009123988_5677064494228378377_o.jpg',1,'Situated in the heart of Bangkok, Sukhumvit area, LEVELS Club & Lounge hosts to a world-class NIGHTLIFE experience in an amazing audio-visual environment.\n\nPrevious Shows/Events: Markus Schulz, Laidback Luke, Sander van Doorn, Gareth Emery, Todd Terry, Fedde Le Grand, Riva Starr, Boy George, Joris Voorn, Ramon Tapia, Congorock, DJ Aqeel, Sensation White - After Party, Diageo World Class Finals 2012. \n\nFor table reservations please contact:\ninfo@levelsclub.com or +66 82 308 3246\n+66 95 248 3897 (after 22:00)',NULL,'13.744300604913','100.55661360921','6th Floor, No.35, Sukhumvit Soi 11, Sukhumvit Rd. (เลขที่ 35, ชั้น 6, ซ.สุขุมวิท 11)','10110','+66 82 308 3246','www.levelsclub.com','BTS Nana Station (Soi.Sukhumvit 11, 300m from Main Road) The club located on the left hand side in front of Aloft Hotel','275395785836185'),(69,'Sing Sing Theater',NULL,'https://scontent.xx.fbcdn.net/hphotos-xpt1/t31.0-8/s720x720/12828959_562252840605260_1359818779797174239_o.jpg',1,'THE PLACE\n\nSING SING is a theatrical performing entertainment venue that is wrapped in a veil of Chinese luxury with a modern twist. Entering SING SING visitors will be captured by a mysterious and sensual atmosphere. The architecture and interior design reminds on an ancient Chinese courtyard theater or a high end brothel that used to serve the nobility or Bourgeoisie. \n\nThe iconic appearance of SING SING is characterised by numerous lanterns and red lighting. The opulent usage of wood and metalwork in the shape of Chinese dragons that embrace the whole club sets the signature of SING SING. Symmetric arrangements of staircases and decors like metal birdcages gives the place a royal touch. The Exclusivity is also expressed through silk and imported leather from Italy. The stage with sophisticatedly processed metal screen as well the dance floor with the SING SING emblem mark the center of the venue. Huge rotating fan blades, red neon umbrellas and tattoo like mural painting indicate that SING SING Theater is not a relict of ancient time but a venue that exist far in the future.\nBangkok-based Australian designer Ashley Sutton built the place in the envisage to create deep feelings of the Far East in the past, present and futuristic impression by using traditional building design with modern machinery and materials. With over a dozen split levels he wanted to create a street scape of activity. Indeed the dense city feeling are encouraged by hidden rooms and secrets passages leading to pavilions where visitors can enjoy the happening in more privacy from afar. \n\nTHE CONCEPT\n\nSING SING aims to become Bangkok’s hottest party venue. It’s the destination for an audience who craves for quality entertainment. It’s a creative, fun and yet cultural venue. \n\nEvery evening at 8 PM SING SING Theater starts with screening of movie masterpieces from the 20th centuries. Among the selection films like “Blade Runner”, “The Wizard of Oz” or Chinese martial art movie “Hero” will be shown. At 10 PM on selected days various live performances will warm up the crowd to get ready for the dance floor. Live performances range from sexy dances, funny comedy acts or energizing martial art to leave an unique imprint. From midnight on music director and DJ KEVIN LAZYKAY will serve the crowd with quality dance music with the right touch of familiarity. Every Tuesday JUL&CO are special guests at SING SING.\n\nOn Sundays the stage is all reserved for the guests. “Face the Gong” is an open-stage acts evening where everybody is welcome to present his or hers talent on stage. A rotating jury of guest judges will designate and hand out a cash reward to the best act of the night.\n\nThe two resident SING SING Muses are the magical spirits to inhale SING SING Theater with a soul. They will perform different small theatrical or abstract contemporary acts to welcome the guests or between the breaks. \n\nTHE CREATORS\n\nSING SING Theater is a project that involved many key figures of the Bangkok F&B scene. Leading in creation Ashley Sutton got connected through his longtime partner Sanya Souvanna Phouma with the partners from QUINCE and OSKAR Bistro. The involvement of the partners are visibly reflected on the drink and food menu as well as in music. \n\nON THE MENU\n\nSignature cocktails have been designed by mixed drinks expert Chanond Purananda whilst the wine list is curated by boutique wine purveyor WINE GARAGE. Also the boutique Gin IRON BALLS from Ashley Sutton’s own Gin production will be served. SING SING Theater is also a proud partner of DIAGEO MOËT- HENNESSY THAILAND.\n\nAs SING SING just opened for the first two weeks the Chef of QUINCE is still working on the food menu and finding out what kind of late night treats will satisfy the hunger and taste buds of the valued guests.',NULL,'13.728160493659','100.57361662388','Sukhumvit 45','10110','0632251331','http://www.facebook.com/SingSingTheater','BTS Station: Phrom Phong\n','467740696723142'),(70,'Conrad Bangkok','2003-03-18','https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-xal1/t31.0-8/q82/s720x720/11046388_10153165999924885_1361701426525695130_o.jpg',1,NULL,NULL,'13.738700791022','100.54862715665','87 Wireless Road','10330','+66 2-690-9999','http://www.conradhotels.com/bangkok','Take the BTS Skytrain to Phloen Chit Station to hop on a free shuttle bus from the station to Conrad Bangkok.','136767784884');
/*!40000 ALTER TABLE `venues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venues_categories`
--

DROP TABLE IF EXISTS `venues_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venues_categories` (
  `venue_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `venues_categories_venue_id_foreign` (`venue_id`),
  KEY `venues_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `venues_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `venues_categories_venue_id_foreign` FOREIGN KEY (`venue_id`) REFERENCES `venues` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venues_categories`
--

LOCK TABLES `venues_categories` WRITE;
/*!40000 ALTER TABLE `venues_categories` DISABLE KEYS */;
INSERT INTO `venues_categories` VALUES (60,60),(61,56),(61,57),(62,55),(63,55),(64,64),(65,57),(65,63),(66,59),(67,58),(67,59),(68,61),(68,56),(68,57),(69,62),(69,57),(70,55);
/*!40000 ALTER TABLE `venues_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venues_locations`
--

DROP TABLE IF EXISTS `venues_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venues_locations` (
  `venue_id` int(10) unsigned NOT NULL,
  `location_id` int(10) unsigned NOT NULL,
  KEY `venues_locations_venue_id_foreign` (`venue_id`),
  KEY `venues_locations_location_id_foreign` (`location_id`),
  CONSTRAINT `venues_locations_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE,
  CONSTRAINT `venues_locations_venue_id_foreign` FOREIGN KEY (`venue_id`) REFERENCES `venues` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venues_locations`
--

LOCK TABLES `venues_locations` WRITE;
/*!40000 ALTER TABLE `venues_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `venues_locations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-05  6:20:26
