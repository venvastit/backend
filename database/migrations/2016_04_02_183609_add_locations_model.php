<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationsModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('name');
        });
        Schema::create('venues_locations', function (Blueprint $table) {
            $table->integer('venue_id')->unsigned();
            $table->integer('location_id')->unsigned();

            $table->foreign('venue_id')->references('id')->on('venues')
                ->onDelete('cascade');
            $table->foreign('location_id')->references('id')->on('locations')
                ->onDelete('cascade');
        });
        Schema::create('events_locations', function (Blueprint $table) {
            $table->integer('event_id')->unsigned();
            $table->integer('location_id')->unsigned();

            $table->foreign('event_id')->references('id')->on('events')
                ->onDelete('cascade');
            $table->foreign('location_id')->references('id')->on('locations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venues_locations', function (Blueprint $table) {
            $table->dropForeign('venues_locations_venue_id_foreign');
            $table->dropForeign('venues_locations_location_id_foreign');
        });
        Schema::table('events_locations', function (Blueprint $table) {
            $table->dropForeign('events_locations_event_id_foreign');
            $table->dropForeign('events_locations_location_id_foreign');
        });
        Schema::drop('events_locations');
        Schema::drop('venue_locations');
        Schema::dropIfExists('locations');
    }
}
