<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoriesModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('sources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        \Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foreign_id');
            $table->integer('source_id')->unsigned();
            $table->foreign('source_id')->references('id')->on('sources')
                ->onDelete('cascade');
        });
        \Schema::create('events_categories', function (Blueprint $table) {
            $table->integer('event_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events')
                ->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
