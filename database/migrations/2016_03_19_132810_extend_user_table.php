<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('users', function (Blueprint $table) {
            $table->string('first_name');
            $table->boolean('gender');
            $table->boolean('hide_phone');
            $table->string('last_name');
            $table->string('phone');
            $table->string('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('gender');
            $table->dropColumn('hide_phone');
            $table->dropColumn('last_name');
            $table->dropColumn('phone');
            $table->dropColumn('type');
        });
    }
}
