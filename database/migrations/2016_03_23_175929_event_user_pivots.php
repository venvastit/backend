<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventUserPivots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_user_attendee', function (Blueprint $table) {
            $table->integer('event_id');
            $table->integer('user_id');
        });
        Schema::create('event_user_organizer', function (Blueprint $table) {
            $table->integer('event_id');
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_user_attendee');
        Schema::drop('event_user_organizer');
    }
}
