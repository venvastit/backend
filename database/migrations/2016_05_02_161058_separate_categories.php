<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeparateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('event_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('image');
        });
        \Schema::create('events_event_categories', function (Blueprint $table) {
            $table->integer('event_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('event_categories')
                ->onDelete('cascade');
        });
        \Schema::drop('venues_categories');
        \Schema::drop('events_categories');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
