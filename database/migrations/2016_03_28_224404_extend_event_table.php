<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //        Schema::create('venues', function(Blueprint $table){
//            $table->increments('id');
//            $table->string('name');
//            $table->date('birthday')->nullable();
//            $table->string('cover_image')->nullable();
//            $table->integer('source_id')->unsigned();
//            $table->text('description')->nullable();
//            $table->text('general_info')->nullable();
//            $table->string('latitude')->nullable();
//            $table->string('longitude')->nullable();
//            $table->string('street')->nullable();
//            $table->string('zip')->nullable();
//            $table->string('phone')->nullable();
//            $table->string('website')->nullable();
//            $table->string('public_transit')->nullable();
//
//            $table->foreign('source_id')->references('id')->on('sources')
//                ->onDelete('cascade');
//        });
        Schema::create('venues_categories', function (Blueprint $table) {
            $table->integer('venue_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->foreign('venue_id')->references('id')->on('venues')
                ->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')
                ->onDelete('cascade');
        });
        Schema::table('events', function (Blueprint $table) {
            $table->integer('venue_id')->unsigned();
            $table->string('picture');

            $table->foreign('venue_id')->references('id')->on('venues')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
