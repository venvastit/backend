<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventUserOrganizerRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venue_user_organizer', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('venue_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('venue_id')->references('id')->on('venues')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venue_user_organizer', function (Blueprint $table) {

            $table->dropForeign('venue_user_organizer_user_id_foreign');
            $table->dropForeign('venue_user_organizer_venue_id_foreign');

            $table->dropColumn('user_id');
            $table->dropColumn('venue_id');
        });
        Schema::drop('venue_user_organizer');
    }
}
