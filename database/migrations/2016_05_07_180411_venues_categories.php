<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VenuesCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('venues_categories', function (BLueprint $table) {
            $table->integer('venue_id')->unsigned();
            $table->integer('category_id')->unsigned();

//            $table->foreign('venue_id')
//                ->references('id')->on('venues');
//
//            $table->foreign('category_id')
//                ->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('venues_categories');
    }
}
