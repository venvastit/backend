<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'login' => 'test',
            'password' => \Hash::make('test'),
            'email' => 'test@test.com',
        ]);
        \App\User::create([
            'login' => 'demo',
            'password' => \Hash::make('demo'),
            'email' => 'demo@demo.com',
        ]);
    }
}
