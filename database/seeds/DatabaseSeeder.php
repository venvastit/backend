<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(RolesAndPermissions::class);
//            $this->call(SourcesSeeder::class);
//         $this->call(UserSeeder::class);
//         $this->call(CitiesTableSeeder::class);
//         $this->command->info('Seeded the cities table ...');

        Model::reguard();
    }
}
