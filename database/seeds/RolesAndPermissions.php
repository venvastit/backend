<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesAndPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attendee = Role::where('name', 'attendee')->get()->first();
        $admin = Role::where('name', 'admin')->get()->first();
        $organizer = Role::where('name', 'organizer')->get()->first();

        if (!$attendee) {
            $attendee = new Role();
            $attendee->name = 'attendee';
            $attendee->display_name = 'Attendee';
            $attendee->description = 'Attendee can only view events, venues and take part in events';
            $attendee->save();
        }

        if (!$organizer) {
            $organizer = new Role();
            $organizer->name = 'organizer';
            $organizer->display_name = 'Event organizer';
            $organizer->description = 'Organizer can create new events';
            $organizer->save();
        }

        if (!$admin) {
            $admin = new Role();
            $admin->name = 'admin';
            $admin->display_name = 'Administrator';
            $admin->description = 'Admin can create\edit\delete venues\users\events and anything else';
            $admin->save();
        }

        $hlogeon = \App\User::where('email', 'hlogeon1@gmail.com')->get()->first();
        if ($hlogeon) {
            /* @var \App\User $hlogeon */
            $hlogeon->roles()->attach($admin->id);
        }
        $users = \App\User::all();
        /** @var \App\User $user */
        foreach ($users as $user) {
            $user->roles()->attach($attendee->id);
        }
    }
}
