<?php

use Illuminate\Database\Seeder;

class SourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Source::create([
            'id' => 1,
            'name' => 'facebook',
        ]);
    }
}
