<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    use DatabaseTransactions;

    public function testCategories()
    {
        $categories = \App\Models\Category::create([
            'name' => 'root',
            'foreign_id' => 12345,
            'source_id' => 1,
            'children' => [
                [
                    'name' => 'sub',
                    'foreign_id' => 123453,
                    'source_id' => 1,
                    'children' => [
                        [
                            'name' => 'sub-sub',
                            'foreign_id' => 123452,
                            'source_id' => 1,
                        ],
                    ],
                ],
            ],
        ]);
        dd($categories);
    }
}
