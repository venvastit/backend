<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section
    | like so: @section('title', 'Dashboard | My Great Admin Panel')
    |
    */

    'title' => 'VenVast admin',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<img src="/img/logo.png">VenVast Admin',

    'logo_mini' => '<img src="/img/logo.png">',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | first two must respond to a GET request, the last two to a POST.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'admin/dashboard',

    'logout_url' => 'admin/logout',

    'login_url' => 'admin/login',

    'register_url' => null,

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header.
    |
    */

    'menu' => [
        'MAIN NAVIGATION',
        [
            'text' => 'Events',
            'url' => 'admin/events',
            'icon' => 'ticket',
            'elements' => [
                [
                    'text' => 'All',
                    'url' => 'admin/events',
                    'icon' => 'globe',
                ],
                [
                    'text' => 'Published',
                    'url' => 'admin/events/published',
                    'icon' => 'circle',
                ],
                [
                    'text' => 'Unpublished',
                    'url' => 'admin/events/unpublished',
                    'icon' => 'circle-thin',
                ],
                [
                    'text' => 'Upcoming',
                    'url' => 'admin/events/upcoming',
                    'icon' => 'calendar',
                ],
                [
                    'text' => 'Passed',
                    'url' => 'admin/events/passed',
                    'icon' => 'calendar-o',
                ],
                [
                    'text' => 'Scraping',
                    'url' => 'admin/events/scraping',
                    'icon' => 'download',
                ],
                [
                    'text' => 'Create',
                    'url' => 'admin/events/create',
                    'icon' => 'plus',
                ],
//                [
//                    'text' => 'Remove passed',
//                    'url' => 'admin/events/passed/remove',
//                    'icon' => 'trash'
//                ],
                [
                    'text' => 'Statistics',
                    'url' => 'admin/events/stats',
                    'icon' => 'line-chart',
                ],
            ],
        ],
        [
            'text' => 'Venues',
            'url' => 'admin/venues',
            'icon' => 'building',
            'elements' => [
                [
                    'text' => 'All',
                    'url' => 'admin/venues',
                    'icon' => 'globe',
                ],
                [
                    'text' => 'Published',
                    'url' => 'admin/venues/published',
                    'icon' => 'circle',
                ],
                [
                    'text' => 'Unpublished',
                    'url' => 'admin/venues/unpublished',
                    'icon' => 'circle-thin',
                ],
                [
                    'text' => 'Has upcoming events',
                    'url' => 'admin/venues/upcoming_events',
                    'icon' => 'arrow-circle-right',
                ],
                [
                    'text' => 'Has no upcoming events',
                    'url' => 'admin/venues/no_upcoming_events',
                    'icon' => 'arrow-circle-o-right',
                ],
                [
                    'text' => 'Registered',
                    'url' => 'admin/venues/registered',
                    'icon' => 'registered',
                ],
                [
                    'text' => 'Scraping',
                    'url' => 'admin/venues/scraping',
                    'icon' => 'download',
                ],
                [
                    'text' => 'Create',
                    'url' => 'admin/venues/create',
                    'icon' => 'plus',
                ],
                [
                    'text' => 'Statistics',
                    'url' => 'admin/venues/stats',
                    'icon' => 'line-chart',
                ],
            ],
        ],
        [
            'text' => 'Hosts',
            'url' => 'admin/hosts',
            'icon' => 'building-o',
        ],
        [
            'text' => 'Categories',
            'url' => 'admin/categories',
            'icon' => 'bookmark',
            'elements' => [
                [
                    'text' => 'All',
                    'url' => 'admin/categories',
                    'icon' => 'globe',
                ],
                [
                    'text' => 'Event',
                    'url' => 'admin/categories/event',
                    'icon' => 'ticket',
                ],
                [
                    'text' => 'Venue',
                    'url' => 'admin/categories/venue',
                    'icon' => 'building',
                ],
                [
                    'text' => 'Deal',
                    'url' => 'admin/categories/deal',
                    'icon' => 'gift',
                ],
                [
                    'text' => 'Facebook',
                    'url' => 'admin/categories/facebook',
                    'icon' => 'facebook',
                ],
                [
                    'text' => 'Google',
                    'url' => 'admin/categories/google',
                    'icon' => 'google',
                ],
                [
                    'text' => 'Create',
                    'url' => 'admin/categories/create',
                    'icon' => 'plus',
                ],
            ],
        ],

//        [
//            'text' => 'Deals',
//            'url' => 'admin/deals',
//            'icon' => 'gift'
//        ],
        [
            'text' => 'Users',
            'url' => 'admin/users',
            'icon' => 'users',
            'elements' => [
                [
                    'text' => 'All',
                    'url' => 'admin/users',
                    'icon' => 'globe',
                ],
                [
                    'text' => 'Organizers',
                    'url' => 'admin/users/org',
                    'icon' => 'user-md',
                ],
                [
                    'text' => 'Attendees',
                    'url' => 'admin/users/attendees',
                    'icon' => 'user',
                ],
                [
                    'text' => 'Admins',
                    'url' => 'admin/users/admins',
                    'icon' => 'lock',
                ],
                [
                    'text' => 'Statistics',
                    'url' => 'admin/users/stats',
                    'icon' => 'line-chart',
                ],
                [
                    'text' => 'Create',
                    'url' => 'admin/users/create',
                    'icon' => 'plus',
                ],
            ],
        ],

    ],

];
