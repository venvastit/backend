<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'google' => [
        'key' => 'AIzaSyBfnCaACfxQD5YOCC7w1O2ItMIonYxLkXE',
        'server-key' => 'AIzaSyCkj3HFzcV0z2qNcHweF3-_PK4_6T7-TKY',
        'client_id' => '980211155235-oaaa6odst8ae2n9ngcfl4ebhmps91hfu.apps.googleusercontent.com',
        'client_secret' => 'rBn61E3ASZhBZZFxE-bRmHIx',
        'redirect' => 'https://venvast.com/auth/google/callback',
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'appId' => env('FACEBOOK_APP_ID', '1631594013727920'),
        'appSecret' => env('FACEBOOK_APP_SECRET', '9b8bea35e222b96c6f5fe3be127f7014'),

    ],

];
